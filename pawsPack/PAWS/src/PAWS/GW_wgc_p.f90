!
    subroutine GW_wgc_p()    
       ! XY: move GW_source.F90's river exchange here in parallel code
       ! Here is the first part, to gain Wgc and allocate Wgc to procs.
       ! the final part is in GW_source.f90, to change source terms.
     
       use proc_mod
       use vdata
       use sparse_Mod
       use memoryChunk, Only:tempStorage
       implicit none
       include 'mpif.h'
       integer :: ierr!, myrank, nproc
       integer, dimension(:), pointer :: Cidx=>NULL(), idx=>NULL()
       real*8, dimension(:,:), pointer :: Wgc=>null()!, TN=>NULL()
       type(csr_type), pointer :: TN=>NULL()
       real*8, dimension(:), allocatable :: WgcTemp
       real*8, dimension(:), pointer :: Wgc1Dp=>null(), Wgc1Dp2=>null()
       real*8, dimension(pci%YDim(1)*pci%XDim(1)) :: WgcAll1D
       real*8, dimension(pci%YDim(1),pci%XDim(1)) :: WgcAll
       integer :: i, rid, jj, fullsize, masterproc=0
       real*8 :: dt, Ar
       integer :: rivInter = 1
       integer :: rivSt = 0
       


       !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, ierr)
       !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)
       
       dt = g%GW(1)%dt
       Ar = g%DM%d(1) * g%DM%d(2)
       fullsize = pci%YDim(1) * pci%XDim(1)
       call tempStorage('WgcAll1D',Wgc1Dp,1) ! first initialized in river_ovnGW.f90
       WgcAll1D = 0D0
       if (riverPal) then
           rivSt = myrank; rivInter = nproc
       else
           if (myrank .ne. masterproc) rivSt = size(g%Riv)+1
       endif
       
       do i = 1+rivSt, size(g%Riv), rivInter
           rid = g%Riv(i)%ID
           Cidx => g%Riv(i)%Cidx
           idx => g%Riv(i)%Lidx
           TN => g%Riv(i)%TN
           if(allocated(WgcTemp)) deallocate(WgcTemp)
           !allocate(WgcTemp(size(TN,1)))
           allocate(WgcTemp(TN%nr))
           WgcTemp = matmul( TN, r(rid)%Riv%fG(Cidx) ) / (Ar*dt)         
           DO jj=1,size(WgcTemp)
               WgcAll1D(idx(jj)) = WgcAll1D(idx(jj)) + WgcTemp(jj)
           enddo
       enddo
       if(allocated(WgcTemp)) deallocate(WgcTemp)

       ! aggregate Wgc
       call MPI_Barrier(MPI_COMM_WORLD,ierr)  
       call Aggregate1Ddat(fullsize,WgcAll1D,masterproc)
       ! allocate
       if (myrank == masterproc) then ! XY: get it back (20181120) !CP: commented out. suspect this is a bug. for any other proc, Wgc1Dp is only one element.
           Wgc1Dp2 => oneD_ptr(fullsize,WgcAll)
       !if (myrank == masterproc) then  !CP: suspect this is a bug. for any other proc, Wgc1Dp is only one element. plus this does not save time anyway?
           Wgc1Dp = WgcAll1D ! use to broadcast 'WgcAll1D'
           Wgc1Dp2 = Wgc1Dp  ! use to scatter
       endif
       call tempStorage('Wgc',Wgc)
       call MPI_Barrier(MPI_COMM_WORLD,ierr)
       call MPI_Bcast(Wgc1Dp, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)  !CP: valgrind reports Invalid write of size 1?? why even BCAST it?
       call scatter2Ddat(1,pci%YDim(1),pci%XDim(1),WgcAll,size(g%OVN%h,1), size(g%OVN%h,2), Wgc,masterproc)
       nullify(Wgc1Dp)
       nullify(Wgc1Dp2)
       !call MPI_Barrier(MPI_COMM_WORLD,ierr) ! XY: not sure whether it is needed
       
       
    end subroutine
    