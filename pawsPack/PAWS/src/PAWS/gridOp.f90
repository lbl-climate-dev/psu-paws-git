#include <preproc.h>        
        module gridOp
        
    
        interface transferIdxDM
        ! depending on you send in a sub(n,2) or ind(n), it chooses the correct one
        ! it can be used to simply find x, if you do not send in grid two data
          module procedure transferIdxDM_Sub !(n,sub1,DM1,x,sub2,DM2)
          module procedure transferIdxDM_Ind !(n,sub1,DM1,x,sub2,DM2)
          module procedure transferIdxE      !E(n,sub1,o1,d1,x,sub2,o2,d2)
        end interface
        
        interface shrinkIdx
          module procedure shrinkIdxE    !(ndim,siz,n,r,c,nGho,Cidx,Sidx,Nidx,Widx,Eidx,offGrid)
          module procedure shrinkIdx_Ind !(ndim,siz,n,ind,nGho,Cidx,Sidx,Nidx,Widx,Eidx,offGrid) 
        end interface  
        PRIVATE
        ! replicate Matlab's functions, although needing size arguments
        PUBLIC :: sub2ind !sub2ind(int(ndim),DBLE(siz),int(nd),DBLE(sub(:,2))). output DBLE(:)        
        PUBLIC :: ind2sub !ind2sub(int(ndim),DBLE(siz),int(nd),DBLE(ind) ) so directly use DM
        PUBLIC :: gCopyData
        PUBLIC :: shrinkIdx  ! calculate indices and neighbor indices for some cells in a shrinked grid (Cidx). -99 for out of domain cells
        PUBLIC :: transferIdx ! transfer indices from one grid to another. Choose from several versions
        INTEGER, PUBLIC, parameter :: nGMax =5
        
    contains
            function gChannel(DM1,DM2,coarsen,option)
            ! translated from CP's matlab code
            ! (optional) option=0 figure out intersections
            ! option==1. figure how to copy from DM2 to DM1 without copying the ghost cells on DM2
            use Vdata, Only : DM_type
            type(DM_type) DM1,DM2
            integer,optional :: option
            integer :: opt, coarsen
            integer,target:: gChannel(4,3) ! gChannel(:,1): c1 -_> [yL yH xL xH]; gChannel(:,2): c2 -_> [yL yH xL xH];  gChannel(:,3): c3 -_> [ylow1 xlow1 ylow2 xlow2];  
            
            real*8,dimension(:),pointer :: x1,y1,x2,y2
            real*8 :: T1, B1, L1, R1, T2, B2, L2, R2 ! T here actually means lower end of x
            real*8 :: T,B,L,R, MH
            integer,dimension(:),pointer::c1,c2,c3
            real*8 :: PREC=1D-8

            
            opt = 0; if (present(option)) opt=option
            
            c1=> gChannel(:,1)
            c2=> gChannel(:,2)
            c3=> gChannel(:,3)
            x1 => DM1%x; y1 => DM1%y
            x2 => DM2%x; y2 => DM2%y
            N1x = 1; NMx = DM2%msize(2)
            N1y = 1; NMy = DM2%msize(1)
            T1=y1(1)+DM1%d(1)/2D0; B1=y1(size(y1))-DM1%d(1)/2D0; L1=x1(1)+DM1%d(1)/2D0; R1=x1(size(x1))-DM1%d(1)/2D0; 
            T2=y2(N1x)+DM2%d(1)/2D0; B2=y2(NMy)-DM2%d(1)/2D0; L2=x2(N1x)+DM2%d(1)/2D0; R2=x2(NMx)-DM2%d(1)/2D0;  
            T=max(T1,T2); B=min(B1,B2); L=max(L1,L2); R=min(R1,R2);
            
            c3(1)=findFirst(y1-T>PREC); c3(2)=findFirst(x1-L>PREC);
            c3(3)=findFirst(y2-T>PREC); c3(4)=findFirst(x2-L>PREC);

            
            if (T1>B2 .OR. T2>B1 .OR. L1>R2 .OR. L2>R1) then
                gChannel = -99
            elseif (coarsen == 2) then
                if (opt > 0) then !When copying data we don't copy from ghost cells
                    c1(1)=findFirst(y1-T>PREC); c1(2)=findFirst(y1-B>PREC)-1;
                    c1(3)=findFirst(x1-L>PREC); c1(4)=findFirst(x1-R>PREC)-1;
                    c2(1)=findFirst(y2-T>PREC); c2(2)=findFirst(y2-B>PREC)-1;
                    c2(3)=findFirst(x2-L>PREC); c2(4)=findFirst(x2-R>PREC)-1;
                elseif (opt .eq. 0) then
                    c1(1)=findFirst(y1-T>PREC)-1; c1(2)=findFirst(y1-B>PREC);
                    c1(3)=findFirst(x1-L>PREC)-1; c1(4)=findFirst(x1-R>PREC);
                    c2(1)=findFirst(y2-T>PREC)-1; c2(2)=findFirst(y2-B>PREC);
                    c2(3)=findFirst(x2-L>PREC)-1; c2(4)=findFirst(x2-R>PREC);
                else
                    call ch_assert('gChannel: not programmed yet')
                endif
            else
                if (opt > 0) then !When copying data we don't copy from ghost cells
                    c1(1)=findFirst(y1-T>PREC); c1(2)=findFirst(y1-B>PREC);
                    c1(3)=findFirst(x1-L>PREC); c1(4)=findFirst(x1-R>PREC);
                    c2(1)=findFirst(y2-T>PREC); c2(2)=findFirst(y2-B>PREC);
                    c2(3)=findFirst(x2-L>PREC); c2(4)=findFirst(x2-R>PREC);
                elseif (opt <= 0) then
                    c1(1)=findFirst(y1-T>PREC)-1; c1(2)=findFirst(y1-B>PREC);
                    c1(3)=findFirst(x1-L>PREC)-1; c1(4)=findFirst(x1-R>PREC);
                    c2(1)=findFirst(y2-T>PREC)-1; c2(2)=findFirst(y2-B>PREC);
                    c2(3)=findFirst(x2-L>PREC)-1; c2(4)=findFirst(x2-R>PREC);
                else
                    call ch_assert('gChannel: not programmed yet')
                endif
            endif
            end function
            
            subroutine gCopyData(g1,g2,datFr,datTo,C,option,datRef1,datRef2)
            ! this is a subroutine at a higher level
            ! copying from 1 to 2
            ! can we make it a gd version later?
            ! for the moment it 
            ! option 0: including ghost cell
            ! option 1: not include ghost cell
            ! option 10: not include ghost cell, coarse->fine don't interpolate
            ! option 2: fine->coarse is a flux-adding.
            ! option -1: coarse->fine, just assign.
            use vdata
#if (defined NC)
            use proc_mod, only : pci
            use memoryChunk, only : tempStorage
            integer ::g1,g2
            real*8 datFr(pci%YDim(g1), pci%XDim(g1))
            real*8 datTo(pci%YDim(g2), pci%XDim(g2))
            real*8, dimension(:,:),pointer::ovnH2D=>null()
            real*8, optional:: datRef1(pci%YDim(g2), pci%XDim(g2))
            real*8, optional:: datRef2(pci%YDim(g2), pci%XDim(g2))
#else
            integer ::g1,g2
            real*8 datFr(size(gA(g1)%OVN%h,1),size(gA(g1)%OVN%h,2))
            real*8 datTo(size(gA(g2)%OVN%h,1),size(gA(g2)%OVN%h,2))            
            real*8, optional:: datRef1(size(gA(g2)%OVN%h,1),size(gA(g2)%OVN%h,2))
            real*8, optional:: datRef2(size(gA(g2)%OVN%h,1),size(gA(g2)%OVN%h,2))
#endif  
            integer,target :: C(4,3)
            integer,dimension(:),pointer::c1(:),c2(:),c3(:)
            integer,optional :: option
            integer :: opt,coarsen
            integer ::ylow1,xlow1,ylow2,xlow2,stY,stX,rx,ry,ii,jj,NNzero
            integer ::i,j,idy1,idy2,idx1,idx2,nrr,ncc,nr,nc,fy1,fy2,fx1,fx2
            real*8 :: rel, invR, nf, tempM, deltaM, TS, SA, deficit, nsp, siw, minref
            real*8 :: PREC=1D-9, adp = 0.5D0
            real*8, dimension(:,:),pointer::datMid=>null(), ipx=>null(), ipy=>null(), datp=>null()
            logical, dimension(:,:),pointer::datMidMask => null()
            integer :: py,px,nG
     
            opt = 0; if (present(option)) opt=option
            
            rel = gA(g2)%DM%d(1)/gA(g1)%DM%d(1)
            nG = 1
            if (rel < 1D0 - PREC) then ! from a coarse grid to a fine grid
                coarsen = 0
                invR = gA(g1)%DM%d(1)/gA(g2)%DM%d(1)
                rx = int(invR); ry = int(invR);      
                allocate(datMid(ry,rx))
                allocate(datMidMask(ry,rx))
            elseif (rel - PREC > 1D0) then ! from a fine grid to a coarse grid
                coarsen = 1
                rx = int(rel); ry = int(rel);
            else ! same resolution
                coarsen = 2
            endif
            
            if (all(C .eq. 0)) then ! haven't been initialized yet
                C = gChannel(gA(g1)%DM,gA(g2)%DM,coarsen,opt)
            endif
            c1=> C(:,1); c2=>C(:,2); c3=>C(:,3)
            ylow1=c3(1);xlow1=c3(2);ylow2=c3(3);xlow2=c3(4);  
            nrr = gA(g2)%DM%msize(1); ncc = gA(g2)%DM%msize(2)
            nr = gA(g1)%DM%msize(1); nc = gA(g1)%DM%msize(2)
            
            if (coarsen == 2) then
                datTo(c2(1):c2(2),c2(3):c2(4)) = datFr(c1(1):c1(2),c1(3):c1(4));
            elseif (coarsen == 0) then ! coarse -> fine
                if (opt .eq. 1) then
                    stY=2; stX=2;
                else
                    stY=1; stX=1;
                endif
                do i=0,(c1(2)-c1(1))
                    do j=0,(c1(4)-c1(3))
                        idy1=ylow2 + (i-1)*ry; idy2=ylow2 + i*ry-1;
                        idx1=xlow2 + (j-1)*rx; idx2=xlow2 + rx*j-1;
                        fy1 = max(stY,idy1); fy2 = min(idy2,nrr)
                        if (fy1>fy2) cycle
                        fx1 = max(stX,idx1); fx2 = min(idx2,ncc)
                        if (fx1>fx2) cycle
                        if (opt .eq. -1) then ! replace
                            datTo(fy1:fy2,fx1:fx2) = datFr(ylow1+i-1, xlow1+j-1)
                            cycle
                        endif
                        if ((fx1 .eq. fx2) .or. (fy1 .eq. fy2)) then ! ghost cells
                            if (opt .eq. 10) then ! don't interpolate
                                datTo(fy1:fy2,fx1:fx2) = datFr(ylow1+i-1, xlow1+j-1)
                                cycle
                            endif
                            py = fy2-fy1+1; px = fx2-fx1+1;
                            allocate(ipy(py,px))
                            if (py .eq. 1) then
                                if (c1(1) .eq. (ylow1+i-1)) then
                                    ipy = c1(1) + 0.5D0 - 0.5D0*rel
                                elseif (c1(2) .eq. (ylow1+i-1)) then
                                    ipy = c1(2) - 0.5D0 + 0.5D0*rel
                                endif
                            else
                                do ii = 1,py
                                    ipy(ii,1) = real(ylow1+i-1) - 0.5D0 + 0.5D0*rel + rel*real(ii-1)
                                enddo
                            endif
                            allocate(ipx(py,px))
                            if (px .eq. 1) then
                                if (c1(3) .eq. (xlow1+j-1)) then
                                    ipx = c1(3) + 0.5D0 - 0.5D0*rel
                                elseif (c1(4) .eq. (xlow1+j-1)) then
                                    ipx = c1(4) - 0.5D0 + 0.5D0*rel
                                endif
                            else
                                do ii = 1,px
                                    ipx(1,ii) = real(xlow1+j-1) - 0.5D0 + 0.5D0*rel + rel*real(ii-1)
                                enddo
                            endif
                            allocate(datp(py,px))
                            
                            !call interp2D(py,px,ipy,ipx,datp,nr,nc,(gA(g1)%OVN%h + gA(g1)%Topo%E),nG)
#ifdef NC
                            call tempStorage('ovnH2D',ovnH2D)
                            call interp2D(py,px,ipy,ipx,datp,nr,nc,ovnH2D,nG)
#else
                            call interp2D(py,px,ipy,ipx,datp,nr,nc,gA(g1)%OVN%h,nG)
#endif 
                            !datp = datp - gA(g1)%Topo%E(ylow1+i-1, xlow1+j-1)
                            where (datp<1D-10)
                                datp = 0D0
                            endwhere
                            datTo(fy1:fy2,fx1:fx2) = datp
                            deallocate(ipy); deallocate(ipx); deallocate(datp)                
                        else
                            nf = real((fy2-fy1+1)*(fx2-fx1+1))
                            tempM = sum(datTo(fy1:fy2,fx1:fx2))/nf;
                            deltaM = datFr(ylow1+i-1, xlow1+j-1) - tempM;
                            ! Calado good?
                           ! datTo(fy1:fy2,fx1:fx2) = datTo(fy1:fy2,fx1:fx2) + deltaM;
                            
                            if ((present(datRef2)) .and. deltaM < 1D-10) then ! only for source term!
                                nsp = 0D0
                                ! XY: negative one don't substract. problem: finer mesh has more time-step
                                !do ii=fy1,fy2
                                !    do jj=fx1,fx2
                                !        if (datRef2(ii,jj) > 1D-10) then
                                !            nsp = nsp + 1D0
                                !        endif                                     
                                !    enddo
                                !enddo
                                !do ii=fy1,fy2
                                !    do jj=fx1,fx2
                                !        if (datRef2(ii,jj) > 1D-10) then
                                !            datTo(ii,jj) = datFr(ylow1+i-1, xlow1+j-1)*nf/nsp
                                !        endif
                                !    enddo
                                !enddo
                                ! XY: this version tried to applied negative flux here.
                                do ii=fy1,fy2
                                    do jj=fx1,fx2
                                        if ((datRef2(ii,jj) + deltaM * gA(1)%OVN%dt * 86400D0 ) > 1D-10) then ! subtract here
                                            nsp = nsp + 1D0
                                        else 
                                            if (datRef2(ii,jj) > 1D-10) then ! adjust
                                                deltaM = deltaM * nf + datRef2(ii,jj) /(gA(1)%OVN%dt * 86400D0)
                                                deltaM = deltaM / nf
                                                datRef2(ii,jj) = 0D0
                                            endif
                                        endif
                                    enddo
                                enddo
                                do ii=fy1,fy2
                                    do jj=fx1,fx2
                                        if ((datRef2(ii,jj) + deltaM * gA(1)%OVN%dt * 86400D0 ) > 1D-10) then
                                            !datTo(ii,jj) = datFr(ylow1+i-1, xlow1+j-1)*nf/nsp
                                            datTo(ii,jj) = deltaM*nf/nsp
                                        endif
                                    enddo
                                enddo
                                cycle
                            endif
                            
                            if (deltaM > 0D0) then
                                
                                if (.not. (present(datRef1))) then
                                    datTo(fy1:fy2,fx1:fx2) = datTo(fy1:fy2,fx1:fx2) + deltaM; ! XY 20180629
                                else
                                    siw = 0D0
                                    minref = minval(datRef1(fy1:fy2,fx1:fx2))
                                    do ii=fy1,fy2
                                        do jj=fx1,fx2
                                            siw = siw + 1D0 / (datRef1(ii,jj) - minref + adp)
                                        enddo
                                    enddo
                                    do ii=fy1,fy2
                                        do jj=fx1,fx2
                                            datTo(ii,jj) = datTo(ii,jj) + deltaM*nf*(1D0 / (datRef1(ii,jj) - minref + adp))/siw
                                        enddo
                                    enddo
                                endif
                                
                                !datMid = datTo(fy1:fy2,fx1:fx2)
                                !deficit = 0D0
                                !datMidMask = .false. ; NNzero = 0! record negative
                                !do ii=1,ry
                                !    do jj=1,rx
                                !        if (datMid(ii,jj)<-1D-12) then
                                !            datMidMask(ii,jj) = .true.
                                !            NNzero = NNzero + 1
                                !            deficit = deficit + datMid(ii,jj) 
                                !        endif
                                !    enddo
                                !enddo
                                !TS = deltaM * nf ! all can be allocated
                                !if (TS + deficit < 0D0) then ! still deficit
                                !    deltaM = TS/real(NNzero)
                                !    do ii=1,ry
                                !        do jj=1,rx
                                !            if (datMidMask(ii,jj)) then
                                !                datMid(ii,jj) = datMid(ii,jj) + deltaM
                                !            endif
                                !        enddo
                                !    enddo
                                !else 
                                !    deltaM = (TS + deficit)/(nf-real(NNzero))
                                !    do ii=1,ry
                                !        do jj=1,rx
                                !            if (datMidMask(ii,jj)) then
                                !                datMid(ii,jj) = 0D0
                                !            else ! positve
                                !                datMid(ii,jj) = datMid(ii,jj) + deltaM
                                !            endif
                                !        enddo
                                !    enddo
                                !endif
                                !datTo(fy1:fy2,fx1:fx2)  = datMid
                            else
                                datMid = datTo(fy1:fy2,fx1:fx2)
                                TS = deltaM * nf
                                do while (TS < -1D-12)
                                    NNZero = count(datMid > 1D-12)
                                    deltaM = -TS/real(NNZero)
                                    SA = 0D0
                                    if (NNZero .eq. 0) then
                                        deltaM = -TS/real(nf)
                                        do ii=1,ry
                                            do jj=1,rx
                                                SA = SA + deltaM
                                                datMid(ii,jj) = datMid(ii,jj) - deltaM
                                            enddo
                                        enddo
                                    else
                                        do ii=1,ry
                                            do jj=1,rx
                                                SA = SA + min(deltaM,datMid(ii,jj))
                                                datMid(ii,jj) = datMid(ii,jj) - min(deltaM,datMid(ii,jj))
                                            enddo
                                        enddo
                                    endif
                                    TS = TS + SA
                                enddo
                                datTo(fy1:fy2,fx1:fx2)  = datMid
                            endif
                        !    !datTo(max(stY,idy1):min(idy2,nrr),max(stX,idx1):min(idx2,ncc)) = datFr(ylow1+i-1, xlow1+j-1);
                        endif
                    enddo
                enddo
                !MH = minval(datTo)
                !IF (MH<-0.3d0) THEN
                !    DO I=c2(1),c2(2)
                !        DO J=c2(3),c2(4)
                !        IF (datTo(i,j)<-0.3d0) THEN
                !           call display('GRIDOP WRONG')
                !        ENDIF
                !        ENDDO
                !    ENDDO
                !ENDIF
                deallocate(datMid)
                deallocate(datMidMask)
            elseif (coarsen == 1) then ! fine -> coarse
                if (opt > 0) then
                    stY = max(2,c1(1)); stX = max(2,c1(3))
                else
                    stY=1; stX=1;
                endif
                do i=0,(c2(2)-c2(1))
                    do j=0,(c2(4)-c2(3))
                        idy1=ylow1 + (i-1)*ry; idy2=ylow1 + i*ry-1;
                        idx1=xlow1 + (j-1)*rx; idx2=xlow1 + rx*j-1;
                        fy1 = max(stY,idy1); fy2 = min(idy2,nr)
                        if (fy1>fy2) cycle
                        fx1 = max(stX,idx1); fx2 = min(idx2,nc)
                        if (fx1>fx2) cycle
                        nf = real((fy2-fy1+1)*(fx2-fx1+1))
                        if (opt .eq. 1) then
                            datTo(ylow2+i-1, xlow2+j-1) = sum(datFr(fy1:fy2,fx1:fx2))/nf;
                        elseif (opt .eq. 2) then ! flux-adding
                            datTo(ylow2+i-1, xlow2+j-1) = datTo(ylow2+i-1, xlow2+j-1) + sum(datFr(fy1:fy2,fx1:fx2))/nf;
                        endif
                    enddo
                enddo
            endif
            
            end subroutine
            
            function findFirst(L,n)
            logical :: L(*)
            integer findFirst,nn
            integer, optional:: n
            nn=1D8; if (present(n)) nn=n
            findFirst = 0
            do i=1,nn
                if (L(i)) then
                    findFirst=i; 
                    exit
                endif
            enddo
            end function
      
            subroutine shrinkIdxE(ndim,siz,n,r,c,nGho,Cidx,Sidx,Nidx,Widx,Eidx,offGrid)
            ! calculate the indices for some cells in a shrinked grid (Cidx)
            ! output include shrinked indices for all N,S,W,E neighbors
            ! they all have the same length. Use OutN for cells 
            ! if nGho>0, you get shrinked index
            ! Input:
            ! siz: [nr,nc] of original grid
            ! ind: indices for cells in original grid
            ! sk: size in the third dimension
            ! n: length of r & c.
            ! Output
            ! (n) Cidx,Sidx,Nidx,Widx,Eidx
            ! offGrid: if no neighbor touches the shrinked grid, offGrid=.true.
            
            integer ndim
            integer,dimension(:),pointer:: Cidx,Sidx,Nidx,Widx,Eidx !,Uidx,Didx what did I want them for?
            integer siz(ndim),n,nGho(ndim),ssiz(ndim)
            integer,dimension(n) :: c,r
            logical,dimension(n),optional :: offGrid
            
            integer,dimension(n) :: sr,sc
            integer :: domain(2,2)
            real*8 :: PREC=1D-12
            integer :: OutN = -99
            
            ssiz = siz - nGho*2
            sr = r-nGho(1)
            sc = c-nGho(2)
            
            call rcShrinkIdx(n,sr,sc,ssiz,OutN,Cidx)
            call rcShrinkIdx(n,sr-1,sc,ssiz,OutN,Sidx)
            call rcShrinkIdx(n,sr+1,sc,ssiz,OutN,Nidx)
            call rcShrinkIdx(n,sr,sc-1,ssiz,OutN,Widx)
            call rcShrinkIdx(n,sr,sc+1,ssiz,OutN,Eidx)
            
            if (present(offGrid)) then
            offGrid = .false.
            where (Cidx<0 .AND. Sidx<0 .AND. Widx<0 .AND. Nidx<0 .AND. Eidx<0) offGrid = .false.
            endif
            end subroutine shrinkIdxE
            
            
            subroutine shrinkIdx_ind(ndim,siz,n,ind,nGho,Cidx,Sidx,Nidx,Widx,Eidx,offGrid)
            ! calculate the indices for some cells in a shrinked grid (Cidx)
            ! output include shrinked indices for all N,S,W,E neighbors
            ! they all have the same length. Use OutN for cells 
            ! if nGho>0, you get shrinked index
            ! Input:
            ! siz: [nr,nc] of original grid
            ! ind: indices for cells in original grid
            ! sk: size in the third dimension
            ! n: length of r & c.
            ! Output
            ! (n) Cidx,Sidx,Nidx,Widx,Eidx
            ! offGrid: if no neighbor touches the shrinked grid, offGrid=.true.
            
            integer ndim
            integer,dimension(:),pointer:: Cidx,Sidx,Nidx,Widx,Eidx !,Uidx,Didx what did I want them for?
            integer siz(ndim),n,nGho(ndim),ssiz(ndim)
            integer,dimension(n) :: ind
            integer,dimension(n) :: c,r
            logical,dimension(n),optional :: offGrid
            
            integer,dimension(n) :: sr,sc
            integer,dimension(n,2) :: sub
            integer :: domain(2,2)
            real*8 :: PREC=1D-12
            integer :: OutN = -99
            
            sub = ind2sub(2,dble(siz),n,dble(ind))
            r = sub(:,1)
            c = sub(:,2)
            
            call shrinkIdxE(ndim,siz,n,r,c,nGho,Cidx,Sidx,Nidx,Widx,Eidx,offGrid)
            end subroutine shrinkIdx_ind
            
            subroutine transferIdxE(n,sub1,o1,d1,x,sub2,o2,d2)
            ! transfer indices from one grid to another
            ! convert it to spatial coordinates and convert it again to second grid
            ! use double precision so fractions can also be converted
            ! two grids can be different in d, but in practice it is probably seldom used.
            implicit none
            integer n
            real*8,dimension(n,2) :: sub1
            real*8,dimension(2) :: o1, d1
            ! output
            real*8,dimension(n,2) :: x
            real*8,dimension(n,2),optional :: sub2
            real*8,dimension(2),optional:: o2, d2  ! sub2 is output, o2,d2 are inputs
            integer i
            do i=1,n
                x(i,:) = o1 + (sub1(i,:)-1D0)*d1
            enddo
            if (present(sub2)) then
            do i=1,n
                sub2(i,:) = (x(i,:) - o2)/d2 + 1D0
            enddo   
            endif
            end subroutine
            
            
            subroutine transferIdxDM_Sub(n,sub1,DM1,x,sub2,DM2)
            ! transfer indices from one grid to another
            ! convert it to spatial coordinates and convert it again to second grid
            ! use double precision so fractions can also be converted
            ! two grids can be different in d, but in practice it is probably seldom used.
            use Vdata, Only: DM_Type
            implicit none
            integer n
            real*8,dimension(n,2) :: sub1
            real*8,dimension(:),pointer :: o1, d1
            type(DM_type) DM1,DM2
            ! output
            real*8,dimension(n,2) :: x
            real*8,dimension(n,2) :: sub2
            real*8,dimension(:),pointer:: o2, d2  ! sub2 is output, o2,d2 are inputs
            integer i
            
            o1 => DM1%origin
            d1 => DM1%d
            o2 => DM2%origin
            d2 => DM2%d
            
            call transferIdxE(n,sub1,o1,d1,x,sub2,o2,d2)
            end subroutine
            
            
            subroutine transferIdxDM_Ind(n,idx1,DM1,x,sub2,DM2)
            ! transfer indices from one grid to another
            ! convert it to spatial coordinates and convert it again to second grid
            ! use double precision so fractions can also be converted
            ! two grids can be different in d, but in practice it is probably seldom used.
            use Vdata, Only: DM_Type
            implicit none
            integer n
            real*8,dimension(n) :: idx1
            real*8,dimension(n,2) :: sub1
            real*8,dimension(:),pointer :: o1, d1
            type(DM_type) DM1,DM2
            ! output
            real*8,dimension(n,2) :: x
            real*8,dimension(n) :: sub2
            real*8,dimension(:),pointer:: o2, d2  ! sub2 is output, o2,d2 are inputs
            integer i
            
            o1 => DM1%origin
            d1 => DM1%d
            o2 => DM2%origin
            d2 => DM2%d

            sub1 = ind2sub(2,DM1%msize,n,idx1)
            call transferIdxE(n,sub1,o1,d1,x,sub2,o2,d2)
            end subroutine
            
            subroutine rcShrinkIdx(n,sr,sc,siz,OutN,idx)
            integer n,siz(2),OutN,nr,nc
            integer,dimension(n) :: sr, sc
            integer,dimension(:),pointer::idx
            logical,dimension(n)::inD
            
            nr = siz(1); nc=siz(2);
            if (.not. associated(idx)) allocate(idx(n))
            !inD = inDomain(n,sr,sc,siz) ! use the following to reduce a function call
            inD = .true.
            where (sc<1 .or. sc>nc .or. sr<1 .or. sr>nr) inD = .false.
            
            idx = OutN
            where (inD) idx(inD) = (sc(inD)-1)*siz(1)+sr(inD)
            end subroutine
            
            function inDomain(n,r,c,siz)
            implicit none
            integer n, siz(2), nr,nc
            integer,dimension(n) :: r,c
            logical inDomain(n)
            nr = siz(1); nc = siz(2)
            
            inDomain = .true.
            where (c<1 .or. c>nc .or. r<1 .or. r>nr) inDomain = .false.
                
            end function
            
            function sub2ind(ndim,siz,nd,sub)
            implicit none
            integer ndim,nd
            real*8 :: siz(ndim), sub(nd,ndim)
            real*8 :: sub2ind(nd)
            
            integer :: i,j,mult(ndim),n
            
            mult(1) = 1
            do i=2,ndim
                n=1
                do j=2,i
                   n=n*siz(j-1)
                enddo
                mult(i) = n
            enddo
            
            sub2ind = 1
            do i=1,nd
            do j=1,ndim
                sub2ind(i) = sub2ind(i) + (sub(i,j)-1)*mult(j)
            enddo
            enddo
            
            end function
            
            
            function ind2sub(ndim,siz,nd,ind)
            implicit none
            integer ndim,nd
            real*8 :: siz(ndim)
            real*8 :: ind2sub(nd,ndim)
            real*8 :: ind(nd)
            integer :: i,j,mult(ndim),n,idx
            
            mult(1) = 1
            do i=2,ndim
                n=1
                do j=2,i
                   n=n*siz(j-1)
                enddo
                mult(i) = n
            enddo
            
            ind = 1
            do i=1,nd
                idx = ind(i)
                do j=ndim,1,-1
                    n = mod(idx-1,mult(i))+1
                    ind2sub(i,j) = (idx-n)/mult(i)+1
                    idx = n
                enddo
            enddo    
            end function
            
            subroutine interp2D(py,px,ipy,ipx,datp,ny,nx,datD,nG)
               implicit none
               integer :: py,px ! interporlated zone size
               real*8, dimension(py,px) :: ipx, ipy, datp
               integer :: ny,nx,nG ! domain size and ghost
               real*8, dimension(ny,nx) :: datD
               real*8, dimension(:,:), pointer :: W0,W1,W2,W3,X0,Y0,XX,YY,U_0,U_1,U_2,U_3
               real*8, dimension(:,:,:), pointer :: AA
               real*8 PREC
               integer i,j,k
               
               PREC=1D-10
               
               allocate(W0(py,px))
               allocate(W1(py,px))
               allocate(W2(py,px))
               allocate(W3(py,px))
               allocate(X0(py,px))
               allocate(Y0(py,px))
               allocate(XX(py,px))
               allocate(YY(py,px))
               allocate(U_0(py,px))
               allocate(U_1(py,px))
               allocate(U_2(py,px))
               allocate(U_3(py,px))
               allocate(AA(4,py,px))
               
               ! if not considering out of bound
               X0=floor(ipx+PREC)-1D0
               Y0=floor(ipy+PREC)-1D0
               XX=ipx-X0!+0.5D0 ! why 0.5?
               YY=ipy-Y0!+0.5D0
               
               WHERE(X0 > nx-3-nG) 
                   X0 = real(nx-3-nG)
               ENDWHERE
               WHERE(Y0 > ny-3-nG) 
                   Y0 = real(ny-3-nG)
               ENDWHERE 
               WHERE(X0 <= PREC) 
                   X0 = 1D0
               ENDWHERE
               WHERE(Y0 <= PREC) 
                   Y0 = 1D0
               ENDWHERE
               W0=-(XX-1D0)*(XX-2D0)*(XX-3D0)/6D0;
               W1=XX*(XX-2D0)*(XX-3D0)/2D0;
               W2=-XX*(XX-1D0)*(XX-3D0)/2D0;
               W3=XX*(XX-1D0)*(XX-2D0)/6D0;
               
               DO K=1,4
                   DO I = 1,py
                       DO J= 1,px
                           U_0(I,J)=datD(Y0(I,J)+K-1,X0(I,J))
                           U_1(I,J)=datD(Y0(I,J)+K-1,X0(I,J)+1)
                           U_2(I,J)=datD(Y0(I,J)+K-1,X0(I,J)+2)
                           U_3(I,J)=datD(Y0(I,J)+K-1,X0(I,J)+3)
                       ENDDO
                   ENDDO     
                   AA(K,:,:)=W0*U_0+ W1*U_1+ W2*U_2 + W3*U_3;
               ENDDO
               
               W0=-(YY-1D0)*(YY-2D0)*(YY-3D0)/6D0;
               W1=YY*(YY-2D0)*(YY-3D0)/2D0;
               W2=-YY*(YY-1D0)*(YY-3D0)/2D0;
               W3=YY*(YY-1D0)*(YY-2D0)/6D0;
               datp=W0*AA(1,:,:)+W1*AA(2,:,:)+W2*AA(3,:,:)+W3*AA(4,:,:);  
               
               deallocate(W0)
               deallocate(W1)
               deallocate(W2)
               deallocate(W3)
               deallocate(X0)
               deallocate(Y0)
               deallocate(XX)
               deallocate(YY)
               deallocate(AA)
               deallocate(U_0)
               deallocate(U_1)
               deallocate(U_2)
               deallocate(U_3)
            
            end subroutine
    
        end module gridOp
    