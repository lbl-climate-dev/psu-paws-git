#include <preproc.h>
    
module budgetOperate
    use gd_mod
#ifdef NC
    use proc_mod
    include 'mpif.h'
#endif
    private

    integer, parameter :: masterproc = 0
    
    public :: averageBudget

    interface averageBudget
    module procedure SumSizeName2D,SumSizeNameMask,SumSizeName3DLayer!,SumSizeName3D
    module procedure SumDivideNum,SumDivideNum2D,SumDivideNum2Dmask,SumDivideNum3D!,SumDivideNum1D,SumDivideNum3DLayer
    end interface
    
    contains

!=======Group of search variable in GD and Sum Divide by DomainSize=======
!=======format:
    
    subroutine SumSizeName2D(bd, gd_rt, fieldname)
      implicit none
      real*8 bd, bd0
      type(gd), target :: gd_rt
      character(*), intent(in) :: fieldname
      
      real*8, dimension(:,:), pointer :: datptr=>null(), ptr2=>null()
      integer nL, ny, nx, ierr
      
      
      call gdRetrieve(datptr,gd_rt,fieldname)
      ny = size(datptr,1)
      nx = size(datptr,2)
#ifdef NC
      nL=1 ! assume only base domain(g(1))
      bd=0D0
      ptr2 =>datptr((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)))
      bd0 = sum(ptr2)/(pci%YDim(nL)*pci%XDim(nL))
      ! aggregate to master
      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#else
      bd = sum(datptr)/(ny*nx)
#endif

    end subroutine
    
    subroutine SumSizeName3DLayer(bd, gd_rt, fieldname, layer)
! if layer == 0, add the whole 3D
      implicit none
      real*8 bd, bd0
      type(gd), target :: gd_rt
      character(*), intent(in) :: fieldname
    
      real*8, dimension(:,:), pointer :: datptr=>null(), ptr2=>null()
      real*8, dimension(:,:,:), pointer :: datptr3=>null(), ptr3=>null()
      integer layer, nL, ny, nx, ierr
      
      
      call gdRetrieve(datptr3,gd_rt,fieldname)
      ny = size(datptr3,1)
      nx = size(datptr3,2)
      if (layer > 0) then
          datptr => datptr3(:,:,layer)
#ifdef NC
          nL=1 ! assume only base domain(g(1))
          bd=0D0
          ptr2 =>datptr((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)))
          bd0 = sum(ptr2)/(pci%YDim(nL)*pci%XDim(nL))
          ! aggregate to master
          call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#else
          bd = sum(datptr)/(ny*nx)
#endif
      else
#ifdef NC
          nL=1 ! assume only base domain(g(1))
          bd=0D0
          ptr3 =>datptr3((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)),:)
          bd0 = sum(ptr3)/(pci%YDim(nL)*pci%XDim(nL))
          ! aggregate to master
          call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#else
          bd = sum(datptr3)/(ny*nx)
#endif
      endif
    end subroutine
    
    subroutine SumSizeNameMask(bd, gd_rt, fieldname, my, mx, mask)
      implicit none
      real*8 bd, bd0
      type(gd), target :: gd_rt
      character(*), intent(in) :: fieldname
      integer :: my, mx
      real*8, dimension(my,mx):: mask
      
      real*8, dimension(:,:), pointer :: datptr=>null(), ptr2=>null(), datptr2=>null()
      integer nL, ny, nx, ierr
      
      
      call gdRetrieve(datptr,gd_rt,fieldname)
      ny = size(datptr,1)
      nx = size(datptr,2)
      allocate(datptr2(ny,nx))
      datptr2 = datptr * mask
      datptr => datptr2
#ifdef NC
      nL=1 ! assume only base domain(g(1))
      bd=0D0
      ptr2 =>datptr((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)))
      bd0 = sum(ptr2)/(pci%YDim(nL)*pci%XDim(nL))
      ! aggregate to master
      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#else
      bd = sum(datptr)/(ny*nx)
#endif
      nullify(ptr2)
      nullify(datptr)
      if (associated(datptr2)) deallocate(datptr2)
    end subroutine
    
    
!=======Group of Sum Divide by Number or DomainSize=======
!=======except 3D=======
!=======format:
    
    subroutine SumDivideNum(bd, val, opSize)
    ! defaultly divide by size of global domain
#ifndef NC
      use memorychunk, only : mm
#endif
      implicit none
      real*8 bd, bd0
      real*8 :: val, num
      real*8, optional :: opSize
      integer nL, ierr
      
      nL = 1
      if (present(opSize)) then
          num=opSize
      else
#ifdef NC
          num = pci%YDim(nL)*pci%XDim(nL)
#else 
          num = mm(1) * mm(2)
#endif
      endif

      bd = val/num
#ifdef NC
      bd0 = bd
      ! aggregate to master
      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#endif
    end subroutine
    
    subroutine SumDivideNum2D(bd, val, ny, nx, opSize)
    ! defaultly divide by size of global domain
#ifndef NC
      use memorychunk, only : mm
#endif
      implicit none
      integer ny, nx
      real*8, dimension(ny,nx), target :: val
      real*8, dimension(:,:), pointer :: ptr=>null()
      real*8, optional :: opSize
      real*8 :: bd, bd0, num
      integer nL, ierr
      
      nL = 1
      
      if (present(opSize)) then
          num=opSize
      else
#ifdef NC
          num = pci%YDim(nL)*pci%XDim(nL)
#else 
          num = ny * nx
#endif
      endif

#ifdef NC
      bd=0D0
      ptr => val((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)))
      bd0 = sum(ptr)/num
      ! aggregate to master
      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#else
      bd = sum(val)/num
#endif

    end subroutine
    
    subroutine SumDivideNum3D(bd, val, ny, nx, nz, opSize)
    ! defaultly divide by size of global domain
#ifndef NC
      use memorychunk, only : mm
#endif
      implicit none
      integer nz, ny, nx
      real*8, dimension(ny,nx,nz), target :: val
      real*8, dimension(:,:,:), pointer :: ptr=>null()
      real*8, optional :: opSize
      real*8 :: bd, bd0, num
      integer nL, ierr

      nL = 1
      if (present(opSize)) then
          num=opSize
      else
#ifdef NC
          num = pci%YDim(nL)*pci%XDim(nL)*nz
#else 
          num = ny * nx * nz
#endif
      endif
      
#ifdef NC
      bd=0D0
      ptr => val((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)), :)
      bd0 = sum(ptr)/num
      ! aggregate to master
      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#else
      bd = sum(val)/num
#endif

    end subroutine 

    subroutine SumDivideNum2Dmask(bd, val, ny, nx, mask, opSize)
    ! defaultly divide by size of global domain
#ifndef NC
      use memorychunk, only : mm
#endif
      implicit none
      integer ny, nx
      real*8, dimension(ny,nx), target :: val, mask, vm
      real*8, dimension(:,:), pointer :: ptr=>null()
      real*8, optional :: opSize
      real*8 :: bd, bd0, num
      integer nL, ierr
      
      nL = 1
      
      if (present(opSize)) then
          num=opSize
      else
#ifdef NC
          num = pci%YDim(nL)*pci%XDim(nL)
#else 
          num = mm(1) * mm(2)
#endif
      endif
      vm = val * mask
#ifdef NC
      bd=0D0
      ptr => vm((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)))
      bd0 = sum(ptr)/num
      ! aggregate to master
      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
#else
      bd = sum(vm)/num
#endif

    end subroutine
    
end module budgetOperate

!    subroutine SumDivideNum1D(bd, ptr, opSize)
!    ! defaultly divide by size of global domain
!#ifndef NC
!      use memorychunk, only : mm
!#endif
!      implicit none
!      real*8 bd, bd0
!      real*8, dimension(:), pointer :: ptr
!      real*8 :: num
!      real*8, optional :: opSize
!      integer nL, ierr
!      
!      nL = 1
!      if (present(opSize)) then
!          num=opSize
!      else
!#ifdef NC
!          num = pci%YDim(nL)*pci%XDim(nL)
!#else 
!          num = mm(1) * mm(2)
!#endif
!      endif
!
!      bd = sum(ptr)/num
!#ifdef NC
!      bd0 = bd
!      ! aggregate to master
!      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
!#endif
!    end subroutine


!
   
!      
!    subroutine SumDivideNum3DLayer(bd, ptr, layer)
!    ! defaultly divide by size of global domain
!#ifndef NC
!      use memorychunk, only : mm
!#endif
!      implicit none
!      real*8 bd, bd0
!      real*8, dimension(:,:,:), pointer :: ptr
!      integer :: layer
!      real*8, dimension(:,:), pointer :: ptr2D => null()
!      real*8 :: num
!      integer nL, ny, nx, ierr
!      
!#ifdef NC
!      nL = 1
!      ny = size(ptr,1)
!      nx = size(ptr,2)
!      ptr2D => ptr((1+pci%nG(nL)):(ny-pci%nG(nL)),(1+pci%nG(nL)):(nx-pci%nG(nL)), layer)
!      bd=0D0
!      bd0 = sum(ptr2D)/(pci%YDim(nL)*pci%XDim(nL))
!      call MPI_reduce(bd0, bd, 1, MPI_real8, MPI_SUM, masterproc, MPI_COMM_WORLD, ierr)
!#else
!      ptr2D => ptr(:,:,layer)
!      bd = sum(ptr2D)/(mm(1) * mm(2))
!#endif
!
!    end subroutine    
    
    

    