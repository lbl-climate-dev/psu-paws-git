!
    subroutine river_ovnGw(dir, nL, ovnH2D)
       ! parallel version: ovn.h and ovn.Qoc are influenced by river
       ! and GW(1).h, Topo.e, manning coefficient are needed.
       ! this function arranges the gather and scatter
       ! dir 1: gather for computation
       ! dir 2: scatter back
       use proc_mod
       use vdata
       use mgTemp_mod
       use memoryChunk, Only:tempStorage
       implicit none
       include 'mpif.h'
       integer :: dir, nL
       real*8, dimension(pci%YDim(nL), pci%XDim(nL)) :: TopoE2D,ovnMan2D,ovnH2D,ovnQoc2D,gwH2D,DM2D
       integer :: ierr, masterproc = 0
       real*8, dimension(:), pointer :: TopoE1D=>null(), ovnMan1D=>null(), DMMap1D=>null()
       real*8, dimension(:), pointer :: ovnH1D=>null(), ovnQoc1D=>null(), gwH1D=>null()
       real*8, dimension(:), pointer :: ovnH1Dp=>null(), ovnQoc1Dp=>null(), gwH1Dp=>null(), Wgc1Dp => null()
       real*8, dimension(:), pointer :: ovnu1D=>null(), ovnv1D=>null(), ovnQx1D=>null(), ovnQy1D=>null(),ovns1D=>null()
       integer, save :: Init1D = -1
       integer :: fullsize 
       
       call MPI_Barrier(MPI_COMM_WORLD, ierr)
       fullsize = pci%YDim(nL) * pci%XDim(nL)
       
       ! g => gA(nL)
       
       if (dir .eq. 1) then ! gather after ovn_driver
           call gather2Ddat(1,size(g%OVN%h,1), size(g%OVN%h,2), g%OVN%h, pci%YDim(nL),pci%XDim(nL),ovnH2D,masterproc)
           call gather2Ddat(1,size(g%GW(1)%h,1), size(g%GW(1)%h,2), g%GW(1)%h, pci%YDim(nL),pci%XDim(nL),gwH2D,masterproc)
           ! for rLateral
           call gather2Ddat(1,size(g%OVN%S,1), size(g%OVN%S,2), g%OVN%S, pci%YDim(nL),pci%XDim(nL),mg_Base(nL)%ovns,masterproc)
           call gather2Ddat(1,size(g%OVN%u,1), size(g%OVN%u,2), g%OVN%u, pci%YDim(nL),pci%XDim(nL),mg_Base(nL)%ovnU,masterproc)
           call gather2Ddat(1,size(g%OVN%V,1), size(g%OVN%V,2), g%OVN%V, pci%YDim(nL),pci%XDim(nL),mg_Base(nL)%ovnV,masterproc)
           call gather2Ddat(1,size(g%OVN%Qx,1), size(g%OVN%Qx,2), g%OVN%Qx, pci%YDim(nL),pci%XDim(nL),mg_Base(nL)%ovnQx,masterproc)
           call gather2Ddat(1,size(g%OVN%Qy,1), size(g%OVN%Qy,2), g%OVN%Qy, pci%YDim(nL),pci%XDim(nL),mg_Base(nL)%ovnQy,masterproc)
           if (init1D < 0) then ! XY: these variables won't change
              call gather2Ddat(1,size(g%Topo%E,1), size(g%Topo%E,2), g%Topo%E, pci%YDim(nL),pci%XDim(nL),TopoE2D,masterproc)
              call gather2Ddat(1,size(g%OVN%Mann,1), size(g%OVN%Mann,2), g%OVN%Mann, pci%YDim(nL),pci%XDim(nL),ovnMan2D,masterproc)
              if (associated(g%DM%Map)) then
                  call gather2Ddat(1,size(g%DM%Map,1), size(g%DM%Map,2), g%DM%Map, pci%YDim(nL),pci%XDim(nL),DM2D,masterproc)
                  call tempStorage('DMMapFull',DMMap1D,fullsize)
              endif
              call tempStorage('TopoEFull',TopoE1D,fullsize)
              call tempStorage('ovnManFull',ovnMan1D,fullsize)
           endif
           call tempStorage('ovnHFull',ovnH1D,fullsize)
           call tempStorage('gwHFull',gwH1D,fullsize)
           call tempStorage('WgcAll1D',Wgc1Dp,fullsize)
           
           call tempStorage('ovnSFull',ovns1D,fullsize)
           call tempStorage('ovnUFull',ovnu1D,fullsize)
           call tempStorage('ovnVFull',ovnv1D,fullsize)
           call tempStorage('ovnQxFull',ovnQx1D,fullsize)
           call tempStorage('ovnQyFull',ovnQy1D,fullsize)
           ! according tempstrorage would be in the F_oc_dw. 
           ! so only for gA(1) mesh.
           if (myrank == masterproc) then
              if (init1D < 0) then
                  TopoE1D = oneD_ptr(fullsize,TopoE2D)
                  ovnMan1D = oneD_ptr(fullsize,ovnMan2D)
                  if (associated(g%DM%Map)) DMMap1D = oneD_ptr(fullsize,DM2D)
              endif
              ovnH1D = oneD_ptr(fullsize,ovnH2D)
              gwH1D = oneD_ptr(fullsize,gwH2D) 
              ovnu1D = oneD_ptr(fullsize,mg_Base(nL)%ovnU)
              ovnv1D = oneD_ptr(fullsize,mg_Base(nL)%ovnV)
              ovnQx1D = oneD_ptr(fullsize,mg_Base(nL)%ovnQx)
              ovnQy1D = oneD_ptr(fullsize,mg_Base(nL)%ovnQy)
              ovns1D = oneD_ptr(fullsize,mg_Base(nL)%ovns)
           endif
           call MPI_Bcast(ovnH1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
           call MPI_Bcast(gwH1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
           call MPI_Bcast(ovnu1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
           call MPI_Bcast(ovnv1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
           call MPI_Bcast(ovnQx1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
           call MPI_Bcast(ovnQy1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
           call MPI_Bcast(ovns1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
           if (init1D < 0) then
              call MPI_Bcast(TopoE1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
              call MPI_Bcast(ovnMan1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
              if (associated(g%DM%Map)) call MPI_Bcast(DMMap1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
              init1D = 1
           endif
           call tempStorage('ovnQocTemp',ovnQoc1D,fullsize)
           ovnQoc1D = 0D0
           
       elseif (dir .eq. 2) then ! scatter back
           call tempStorage('ovnQocTemp',ovnQoc1D,fullsize)
           call tempStorage('ovnHFull',ovnH1D,fullsize)
          
           call Aggregate1Ddat(fullsize,ovnQoc1D,masterproc)
           call gather2Ddat(1,size(g%OVN%Qoc,1), size(g%OVN%Qoc,2), g%OVN%Qoc, pci%YDim(nL),pci%XDim(nL),ovnQoc2D,masterproc) 
           
          if (riverPal) then ! need to gather and scatter  
           ! ovnQoc2D: real g.ovn.Qoc
           ! ovnQoc1D: newly generated Qoc in this time step.
           if (myrank == masterproc) then
               ovnQoc1Dp => oneD_ptr(fullsize,ovnQoc2D)
               ovnQoc1Dp = ovnQoc1Dp + ovnQoc1D ! adjust g.ovn.Qoc
               ovnH1Dp => oneD_ptr(fullsize,ovnH2D)
               ovnH1Dp = ovnH1Dp - ovnQoc1D ! adjust g.ovn.h
               ovnH1D = ovnH1Dp
           endif
          else
           if (myrank == masterproc) then
               ovnQoc1Dp => oneD_ptr(fullsize,ovnQoc2D)
               ovnQoc1Dp = ovnQoc1Dp + ovnQoc1D ! adjust g.ovn.Qoc
               ovnH1Dp => oneD_ptr(fullsize,ovnH2D)
               ovnH1Dp = ovnH1D
           endif
          endif
           ovnQoc1D = 0D0
           call MPI_Barrier(MPI_COMM_WORLD,ierr)
           call scatter2Ddat(1,pci%YDim(nL),pci%XDim(nL),ovnH2D,size(g%OVN%h,1), size(g%OVN%h,2), g%OVN%h,masterproc)
           call scatter2Ddat(1,pci%YDim(nL),pci%XDim(nL),ovnQoc2D,size(g%OVN%Qoc,1), size(g%OVN%Qoc,2), g%OVN%Qoc,masterproc)
           call MPI_Barrier(MPI_COMM_WORLD,ierr)
           !if (riverPal) 
           call MPI_Bcast(ovnH1D, fullsize, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
    
       else
           call ch_assert('river_ovnGw :: wrong dir')
       endif
       
       ! g => gA(1)

    end subroutine