
    module sparse_Mod
      public:: csr_type,amux,initializeA,get
      TYPE csr_type
          integer :: nr=0,nc=0
          real*8,dimension(:),pointer::val,valT
          integer,dimension(:),pointer:: ia=>null(),ja=>null() ! ia is row pointer (read CSR), ja is colum indices
          integer,dimension(:),pointer:: iaT=>null(),jaT=>null() ! ia is row pointer (read CSR), ja is colum indices
          ! ia is of length nr+1; ja is of length sumALL(a>0)==length(val)==nzmax
          ! A[nr,nc]*x[nc]=b[nr]
      end Type
      interface matmul ! overland intrinsic function so that it will decide based on what inputs was put in
         module procedure amux
         module procedure xmua
      end interface
      interface get
         module procedure get_elem
      end interface
    contains
!====================================================================================================
        subroutine initializeA_csr( A, nzmax, val, nia, ia, nja, ja, nc)
        ! compute iaT, jaT & 
        implicit none
        integer nzmax, nia, nja, nc
        type(csr_type) :: A
        real*8,dimension(nzmax) :: val
        integer :: ia(nia), ja(nja)
        
        integer :: job=1,ipos=1
        
        A%nr    = nia - 1
        A%nc    = nc
        
        if (.not. associated(a%ja)) allocate(a%jaT(size(a%val)))
        if (.not. associated(a%ia)) allocate(a%iaT(a%nc+1))
        if (.not. associated(a%val)) allocate(a%valT(nzmax))
        
        call csrcsc ( A%nr, job, ipos, a%val, a%ja, a%ia, a%valT, a%jaT, a%iaT )
        
        end subroutine
        
        function get_elem(A,i,j)
        implicit none
        type(csr_type) :: A
        integer ::i,j
        real*8 :: get_elem, res(1)
        integer :: job=1,ipos=1,nr,nc
        integer,dimension(1) :: jao, iao
        
        jao=0; iao=0;
        call submat(A%nr,job,i,i,j,j,A%val,A%ja,A%ia,nr,nc,res,jao,iao)
        if (jao(1) <=0) then
            get_elem = 0D0
        else
            get_elem = res(1)
        endif
                   
        end function
        
 !       
 !!====================================================================================================       
 !       subroutine SparseToFull(values, Ir, Nzmax, Jc, columns, dims, ptr)
 !       implicit none
 !       integer Nzmax, columns
 !       real*8 values(Nzmax)
 !       mwPointer :: Ir(Nzmax), Jc(columns)
 !       mwPointer :: dims(3), n, i, j, row_start, row_end
 !       real*8, dimension(dims(1),dims(2)) :: ptr
 !
 !       n = 0
 !       do j = 1, columns - 1
 !         row_start = Ir(Jc(j)+1)+1
 !         row_end = Ir(Jc(j+1))+1
 !         do i = row_start, row_end
 !           n = n + 1
 !           ptr(i,j) = values(n)
 !         end do
 !       end do
 !
 !       end subroutine
        
       
!====================================================================================================
        subroutine initializeA( A )
        ! A has already been linked with: nc, ia, ja, val (from Matlab)
        ! populate other fields
        implicit none
        type(csr_type) :: A
        
        !A%nzmax = size(A%val)
        A%nr    = size(A%ia)-1
        
        call initializeAT(A)
        
        end subroutine
!====================================================================================================
        subroutine initializeAT( A )
        ! compute iaT, jaT & 
        implicit none
        type(csr_type) :: A
        real*8,dimension(A%nc) :: x
        real*8,dimension(A%nr) :: amux_1
        integer :: job=1,ipos=1
        
        if (A%nr .eq. 0) A%nr = size(A%val)-1
        ! A%nc must be notified from outside.
        if (.not. associated(a%jaT)) allocate(a%jaT(size(a%val)))
        if (.not. associated(a%iaT)) allocate(a%iaT(a%nc+1)) ! the number of rows after transposition is the number of columns before it
        if (.not. associated(a%valT)) allocate(a%valT(size(a%val)))
        
        call csrcsc ( A%nc, job, ipos, a%val, a%ja, a%ia, a%valT, a%jaT, a%iaT )
        
        end subroutine
!====================================================================================================
        function amux ( A, x )
        implicit none
        type(csr_type) :: A
        real*8,dimension(A%nc) :: x
        real*8,dimension(A%nr) :: amux
        call amux_sub(A%nr, x, amux, A%val, A%ja, A%ia)
        
        end function
!====================================================================================================
        function xmua ( xp, A )
        implicit none
        ! (x*A) = (A'*x')', but a vector transport means nothing for Fortran
        type(csr_type) :: A
        real*8,dimension(A%nr) :: xp
        real*8,dimension(A%nc) :: xmua
        
        if (.not. associated(A%valT)) call initializeAT(A)
        
        call amux_sub(A%nc, xp, xmua, A%valT, A%jaT, A%iaT)
        
        end function
        
!====================================================================================================       
        subroutine submat ( n, job, i1, i2, j1, j2, a, ja, ia, nr, nc, ao, jao, iao )

        !*****************************************************************************80
        !
        !! SUBMAT extracts the submatrix A(i1:i2,j1:j2).
        !
        !  Discussion:
        !
        !    This routine extracts a submatrix and puts the result in
        !    matrix ao,iao,jao.  It is an "in place" routine, so ao,jao,iao may be 
        !    the same as a,ja,ia.
        !
        !  Modified:
        !
        !    07 January 2004
        !
        !  Author:
        !
        !    Youcef Saad
        !
        !  Parameters:
        !
        !    Input, integer ( kind = 4 ) N, the row dimension of the matrix.
        !
        ! i1,i2 = two integer ( kind = 4 )s with i2 >= i1 indicating the range of rows to be
        !          extracted.
        !
        ! j1,j2 = two integer ( kind = 4 )s with j2 >= j1 indicating the range of columns
        !         to be extracted.
        !         * There is no checking whether the input values for i1, i2, j1,
        !           j2 are between 1 and n.
        !
        !    Input, real A(*), integer ( kind = 4 ) JA(*), IA(NROW+1), the matrix in CSR
        !    Compressed Sparse Row format.
        !
        ! job      = job indicator: if job /= 1 then the real values in a are NOT
        !         extracted, only the column indices (i.e. data structure) are.
        !         otherwise values as well as column indices are extracted...
        !
        ! on output
        !
        ! nr      = number of rows of submatrix
        ! nc      = number of columns of submatrix
        !        * if either of nr or nc is nonpositive the code will quit.
        !
        ! ao,
        ! jao,iao = extracted matrix in general sparse format with jao containing
        !      the column indices,and iao being the pointer to the beginning
        !      of the row,in arrays a,ja.
        !
          implicit none

          real ( kind = 8 ) a(*)
          real ( kind = 8 ) ao(*)
          integer ( kind = 4 ) i
          integer ( kind = 4 ) i1
          integer ( kind = 4 ) i2
          integer ( kind = 4 ) ia(*)
          integer ( kind = 4 ) iao(*)
          integer ( kind = 4 ) ii
          integer ( kind = 4 ) j
          integer ( kind = 4 ) j1
          integer ( kind = 4 ) j2
          integer ( kind = 4 ) ja(*)
          integer ( kind = 4 ) jao(*)
          integer ( kind = 4 ) job
          integer ( kind = 4 ) k
          integer ( kind = 4 ) k1
          integer ( kind = 4 ) k2
          integer ( kind = 4 ) klen
          integer ( kind = 4 ) n
          integer ( kind = 4 ) nc
          integer ( kind = 4 ) nr

          nr = i2 - i1 + 1
          nc = j2 - j1 + 1

          if ( nr <= 0 .or. nc <= 0 ) then
            return
          end if

          klen = 0
        !
        !  Simple procedure that proceeds row-wise.
        !
          do i = 1, nr
              
            if (i > i2) return !CP added

            ii = i1 + i - 1
            k1 = ia(ii)
            k2 = ia(ii+1) - 1
            iao(i) = klen + 1

            do k = k1, k2
              j = ja(k)
              if ( j1 <= j .and. j <= j2 ) then
                klen = klen + 1
                if ( job == 1 ) then
                  ao(klen) = a(k)
                end if
                jao(klen) =j
              end if
            end do
            

          end do

          iao(nr+1) = klen + 1

          return
        end
 
!====================================================================================================
        subroutine amux_sub ( nr, x, y, a, ja, ia )
        ! modified from http://people.sc.fsu.edu/~jburkardt/f_src/sparsekit/sparsekit.f90
        ! CSR format
        !CP: nr == length(y), also nr of A
        !ia: row pointers (nr+1)
        !ja: column indices (sumALL(a>0))
        ! x: of size nc, but is assumed shape here
        ! y: output of size nr
        
          implicit none
          integer ( kind = 4 ) nr
          real ( kind = 8 ) a(*)
          integer ( kind = 4 ) i
          integer ( kind = 4 ) ia(nr+1)
          integer ( kind = 4 ) ja(*)
          integer ( kind = 4 ) k
          real ( kind = 8 ) t
          real ( kind = 8 ) x(*)
          real ( kind = 8 ) y(nr)

          do i = 1, nr
        !  Compute the inner product of row I with vector X.
            t = 0.0D+00
            do k = ia(i), ia(i+1)-1
              t = t + a(k) * x(ja(k))
            end do
            y(i) = t
          end do
          return
        end
        
        subroutine csrcsc ( n, job, ipos, a, ja, ia, ao, jao, iao )
        ! modified from http://people.sc.fsu.edu/~jburkardt/f_src/sparsekit/sparsekit.f90
        !! CSRCSC converts Compressed Sparse Row to Compressed Sparse Column.
        !  Discussion:
        !    This is essentially a transposition operation.  
        !    It is NOT an in-place algorithm.
        !    This routine transposes a matrix stored in a, ja, ia format.
        !  Author:
        !    Youcef Saad
        !  Parameters:
        !    Input, integer ( kind = 4 ) N, the order of the matrix.
        !    Input, integer ( kind = 4 ) JOB, indicates whether or not to fill the values of the
        !    matrix AO or only the pattern (IA, and JA).  Enter 1 for yes.
        !
        ! ipos  = starting position in ao, jao of the transposed matrix.
        !         the iao array takes this into account (thus iao(1) is set to ipos.)
        !         Note: this may be useful if one needs to append the data structure
        !         of the transpose to that of A. In this case use
        !                call csrcsc (n,1,n+2,a,ja,ia,a,ja,ia(n+2))
        !        for any other normal usage, enter ipos=1.
        !
        !    Input, real A(*), integer ( kind = 4 ) JA(*), IA(N+1), the matrix in CSR
        !    Compressed Sparse Row format.
        !
        !    Output, real AO(*), JAO(*), IAO(N+1), the matrix in CSC
        !    Compressed Sparse Column format.
        !
          implicit none

          integer ( kind = 4 ) n

          real ( kind = 8 ) a(*)
          real ( kind = 8 ) ao(*)
          integer ( kind = 4 ) i
          integer ( kind = 4 ) ia(n+1)
          integer ( kind = 4 ) iao(n+1)
          integer ( kind = 4 ) ipos
          integer ( kind = 4 ) j
          integer ( kind = 4 ) ja(*)
          integer ( kind = 4 ) jao(*)
          integer ( kind = 4 ) job
          integer ( kind = 4 ) k
          integer ( kind = 4 ) next
        !
        !  Compute lengths of rows of A'.
        !
          iao(1:n+1) = 0

          do i = 1, n
            do k = ia(i), ia(i+1)-1
              j = ja(k) + 1
              iao(j) = iao(j) + 1
            end do
          end do
        !
        !  Compute pointers from lengths.
        !
          iao(1) = ipos
          do i = 1, n
            iao(i+1) = iao(i) + iao(i+1)
          end do
        !
        !  Do the actual copying.
        !
          do i = 1, n
            do k = ia(i), ia(i+1)-1
              j = ja(k)
              next = iao(j)
              if ( job == 1 ) then
                ao(next) = a(k)
              end if
              jao(next) = i
              iao(j) = next + 1
            end do
          end do
        !
        !  Reshift IAO and leave.
        !
          do i = n, 1, -1
            iao(i+1) = iao(i)
          end do
          iao(1) = ipos

          return
        end
    
    end module