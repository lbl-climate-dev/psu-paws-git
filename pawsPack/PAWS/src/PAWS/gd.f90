
      MODULE specialMod
         integer,public :: nanVal = -999999999
      END MODULE
      MODULE gd_Mod
      ! *****************************************************
      ! Chaopeng Shen
      ! Pennsyvalnia State University
      ! *****************************************************
      ! the general syntax is
      ! ++ call getPtr(gd_base,fieldname,ptr,num)  ! num is optional, default to 1
      ! this will link the data to the pointer, ptr, which may be a real*8,integer, logical array supported up to 6 dimension
      ! or a type(gd) array pointer.
      ! depending on what pointer is passed in, the calling interface will pick the corresponding type/dimension subroutine
      ! If the queried field is not there or if the field type/dimension does not match that of the ptr, an error is thrown.
      ! num is the index, if omitted, default to 1. type(gd) allows to access sub-structures.
      ! Overloaded operators as a fast access:
      ! Besides the calling interface getPtr, we have overloaded operator .G.,.F., .FF. and .FFF. serving as shorthand methods
      ! for extracting sub-structure (.G.), [real*8,dimension(:,:)] and [real*8,dimension(:,:,:)] data, respectively. 
      ! With these operators, num is always 1.
      
      ! Look at matTable to see how this is used to create an interface to Matlab file
      ! How to directly put stuff into the structure?
      ! Since in Fortran, we cannot pass pointers around (they will just get re-directed)
      
      PRIVATE
      
      integer, parameter :: maxDim = 6
      ! integer, parameter :: maxStrLen = 100  ! default value
      integer, parameter :: maxStrLen = 500 
      type p_t
        ! you can comment out never-to-be-used pointers to save storage
        real*4, dimension(:,:,:), pointer :: s3 => null()
        real*8, dimension(:), pointer :: p1  => null()
        real*8, dimension(:,:), pointer :: p2 => null()
        real*8, dimension(:,:,:), pointer :: p3 => null()
        real*8, dimension(:,:,:,:), pointer :: p4 => null()
        real*8, dimension(:,:,:,:,:), pointer :: p5 => null()
        real*8, dimension(:,:,:,:,:,:), pointer :: p6 => null()
        integer,dimension(:), pointer :: j1 => null()
        integer,dimension(:,:), pointer :: j2 => null()
        integer,dimension(:,:,:), pointer :: j3 => null()
        integer,dimension(:,:,:,:), pointer :: j4 => null()
        integer,dimension(:,:,:,:,:), pointer :: j5 => null()
        integer,dimension(:,:,:,:,:,:), pointer :: j6 => null()
        logical*1,dimension(:), pointer :: l1 => null()
        logical*1,dimension(:,:), pointer :: l2 => null()
        logical*1,dimension(:,:,:), pointer :: l3 => null()
        logical*1,dimension(:,:,:,:), pointer :: l4 => null()
        logical*1,dimension(:,:,:,:,:), pointer :: l5 => null()
        logical*1,dimension(:,:,:,:,:,:), pointer :: l6 => null()
        character,pointer :: c => null()
        !procedure(), nopass, pointer :: proc => null() ! commented out to save storage
      end type p_t
      
      type gd ! general data type
        ! it comes with a strs array, an integer array, a pointers array, and an integer nf, the number of valid fields
        integer np                                    ! number of associated fields
        type(p_t),dimension(:),pointer::p  => null() ! for all pointers
        type(gd),dimension(:),pointer::g  => null() ! for sub-structures
        character(len=maxStrLen),dimension(:),pointer :: name => null()                 ! a name that traces from root to current object
        character(len=maxStrLen),dimension(:),pointer::f  => null() ! fieldnames
        type(gd),pointer:: parent=>null()           ! uplink, this link must be implemented when the hierarchy is created
        integer parent_fnum                         ! field number of this object in parent
        integer*8 :: fileLink=0                         ! an optional link to file handle, etc
      end type gd
      
      
      interface operator (.G.) ! pointer to field
      module procedure getPtrStrFunc
      end interface
      interface operator (.F.) ! pointer to field
      module procedure getPtrR1Func
      end interface
      interface operator (.FF.) ! pointer to field
      module procedure getPtrR2Func
      end interface
      interface operator (.FFF.) ! pointer to field
      module procedure getPtrR3Func
      end interface
      interface operator (.SSS.) ! pointer to field
      module procedure getPtrS3Func
      end interface
      
!      interface connectField
!      module procedure connectFieldReal8,connectFieldInteger,connectFieldLogical
!      module procedure connectFieldStruct
!      end interface
      type(gd),save,target :: gd_base,gd_local
      type(gd),pointer::gd_ptr, gd_modelFileRoot,gd_tempA(:)=>null(),gd_temp=>null() !gd_tempA is an array gd_temp points to.
      integer totalCounter
      
      ! get access to a field
      interface getPtr         ! call getPtr(main,field,ptr,[num])
      module procedure getPtrR0,getPtrR1,getPtrR2,getPtrR3,getPtrR4,getPtrR5,getPtrS3
      module procedure getPtrI0,getPtrI1,getPtrI2,getPtrI3,getPtrI4,getPtrI5
      module procedure getPtrL0,getPtrL1,getPtrL2,getPtrL3,getPtrL4,getPtrL5
      module procedure getPtrR6,getPtrI6,getPtrL6
      module procedure getPtrStr!,getPtrProc
      end interface
      
      ! assign a piece of data to a structure (n-th)
      interface assignPtr      ! call assignPtr(nn,datND,main,n)
      module procedure mAssignR1,mAssignR2,mAssignR3,mAssignR4,mAssignR5,mAssignS3
      module procedure mAssignI1,mAssignI2,mAssignI3,mAssignI4,mAssignI5
      module procedure mAssignL1,mAssignL2,mAssignL3,mAssignL4,mAssignL5
      module procedure mAssignR6,mAssignI6,mAssignL6
      module procedure mAssignPtrStr!, mAssignProc
      end interface
      
      ! add a piece of data into structure, with its name also added
      interface addPtr         ! call addPtr(nn,field,datND,main)
      module procedure addR1,addR2,addR3,addR4,addR5,addR6,addS3
      module procedure addI1,addI2,addI3,addI4,addI5,addI6
      module procedure addL1,addL2,addL3,addL4,addL5,addL6
      module procedure addPtrStr!, addPtrProc
      end interface

      ! add a piece of data into structure, with its name also added
      interface addPtrCp         ! call addPtr(nn,field,datND,main)
      module procedure addR3cp,addS3cp
      end interface

      interface gdRetrieve         ! call gdRetrieve(ptr,gd_rt,fieldname)
      module procedure gdRetrieveR1,gdRetrieveR2,gdRetrieveR3,gdRetrieveR4,gdRetrieveR5,gdRetrieveR6
      module procedure gdRetrieveI1,gdRetrieveI2,gdRetrieveI3,gdRetrieveI4,gdRetrieveI5,gdRetrieveI6
      module procedure gdRetrieveL1,gdRetrieveL2,gdRetrieveL3,gdRetrieveL4,gdRetrieveL5,gdRetrieveL6
      end interface
      
      interface setFieldVal
      module procedure setFieldValR, setFieldValI,setFieldValL
      end interface
      
      interface I2L
      module procedure I1tL1,I2tL2,I3tL3,I4tL4,I5tL5,I6tL6
      end interface
      
      PUBLIC:: p_t, gd, totalCounter, countRepNum, setFieldVal, gdPartialCopy, checkNaN, maxDim
      PUBLIC:: gd_base,gd_local,gd_temp, gd_tempA, gd_ptr, gd_modelFileRoot, isempty, ise, isAllocated
      PUBLIC:: getPtr, addPtr, assignPtr, addPtrCp
      PUBLIC:: returnType, returnTypeN, addFieldName, setFieldName
      PUBLIC:: searchStr, searchStrF, findRepNum, returnSizes, returnFieldSize
      PUBLIC:: deallocate_p,  deallocate_gd, allocate_gd ! allocate the subfields of gd
      PUBLIC:: operator(.G.),operator(.F.),operator(.FF.),                &
     &  operator(.FFF.),operator(.SSS.)
      PUBLIC:: gdR1Retrieval, gdnDim, gdValueBoundsCheck, valueBoundsCheck, str2num
      PUBLIC:: gdRetrieve, retrieveEndNode
      
      ! ###########################################################################
      ! we cannot do similar pretty things for assignment because you cannot 
      !get a pointer (where in fact a local pointer is assigned to the memory) 
      !and then assign it to sth else, it will only redirect your local pointer without
      !touching the actual pointer in the structure
      
    CONTAINS
    
       FUNCTION oneD_Ptr(nn,datND)
       ! a replica of the one in vdata. However this one is private
       INTEGER nn,nz,beg
       REAL*8,Target:: datND(nn)
       REAL*8,DIMENSION(:),POINTER::oneD_Ptr
       !print *, dat2D
       oneD_Ptr => datND
       END FUNCTION oneD_Ptr
       
        function isAllocated(main)
        implicit none
        type(gd) main
        logical isAllocated
        isAllocated = .false.
        if (associated(main%p)) isAllocated = .true.
        
        end function
       
        recursive subroutine checkNaN(gdMain,recur)
        use displayMod
        implicit none
        type(gd) gdMain
        logical*1, optional::recur
        integer:: rec = 0, i
        character*10 fieldType
        character typeS
        do i=1,gdMain%np
            fieldType = returnTypeN(gdMain,i)
            if (fieldType .eq. 'R1') then
                if (any(isnan(gdMain%p(i)%p1))) then
                    call display( 'NaN checked: '// gdMain%name(1)//gdMain%f(i))
                endif
            elseif (fieldType .eq. 'R2') then
                if (any(isnan(gdMain%p(i)%p2))) then
                    call display( 'NaN checked: '// gdMain%name(1)//gdMain%f(i))
                endif
            elseif (fieldType .eq. 'R3') then
                if (any(isnan(gdMain%p(i)%p3))) then
                    call display( 'NaN checked: '// gdMain%name(1)//gdMain%f(i))
                endif
            !elseif (fieldType .eq. 'J1') then
            !    if (any(isnan(gdMain%p(i)%j1))) then
            !        call display( 'NaN checked: '// gdMain%f(i))
            !    endif
            !elseif (fieldType .eq. 'J2') then
            !    if (any(isnan(gdMain%p(i)%j2))) then
            !        call display( 'NaN checked: '// gdMain%f(i))
            !    endif
            !elseif (fieldType .eq. 'J3') then
            !    if (any(isnan(gdMain%p(i)%j3))) then
            !        call display( 'NaN checked: '// gdMain%f(i))
            !    endif
            !elseif (fieldType .eq. 'L1') then
            !    if (any(isnan(gdMain%p(i)%l1))) then
            !        call display( 'NaN checked: '// gdMain%f(i))
            !    endif
            !elseif (fieldType .eq. 'L2') then
            !    if (any(isnan(gdMain%p(i)%l2))) then
            !        call display( 'NaN checked: '// gdMain%f(i))
            !    endif
            !elseif (fieldType .eq. 'L3') then
            !    if (any(isnan(gdMain%p(i)%l3))) then
            !        call display( 'NaN checked: '// gdMain%f(i))
            !    endif  
            elseif (fieldType .eq. 'gd') then
                if (recur) then
                    call checkNaN(gdMain%g(i),recur)
                endif    
            endif
        enddo    
        end subroutine
        
        
        recursive subroutine setFieldValR(gdMain,typeS,val,recur)
        implicit none
        type(gd) gdMain
        logical*1, optional::recur
        real*8 val
        real*8,pointer::R1(:),R2(:,:),R3(:,:,:)
        integer:: rec = 0, i
        character*10 fieldType
        character typeS
        do i=1,gdMain%np
            fieldType = returnTypeN(gdMain,i)
            if (typeS .eq. 'R') then
               if (fieldType .eq. 'R1') then
                   gdMain%p(i)%p1 = val
               elseif (fieldType .eq. 'R2') then
                   gdMain%p(i)%p2 = val
               elseif (fieldType .eq. 'R3') then
                   gdMain%p(i)%p3 = val
               elseif (fieldType .eq. 'gd') then
                   if (recur) then
                       call setFieldValR(gdMain%g(i),typeS,val,recur)
                   endif    
               endif
            endif
        enddo    
        end subroutine
      
      recursive subroutine setFieldValI(gdMain,typeS,val,recur)
        implicit none
        type(gd) gdMain
        logical*1, optional::recur
        integer val
        real*8,pointer::R1(:),R2(:,:),R3(:,:,:)
        integer:: rec = 0, i
        character*10 fieldType
        character typeS
        do i=1,gdMain%np
            fieldType = returnTypeN(gdMain,i)
            if (typeS .eq. 'I') then
               if (fieldType .eq. 'J1') then
                   gdMain%p(i)%j1 = val
               elseif (fieldType .eq. 'J2') then
                   gdMain%p(i)%j2 = val
               elseif (fieldType .eq. 'J3') then
                   gdMain%p(i)%j3 = val
               elseif (fieldType .eq. 'gd') then
                   if (recur) then
                       call setFieldValI(gdMain%g(i),typeS,val,recur)
                   endif    
               endif
            endif
        enddo    
      end subroutine
      
      recursive subroutine setFieldValL(gdMain,typeS,val,recur)
        implicit none
        type(gd) gdMain
        logical*1, optional::recur
        logical*1 val
        real*8,pointer::R1(:),R2(:,:),R3(:,:,:)
        integer:: rec = 0, i
        character*10 fieldType
        character typeS
        do i=1,gdMain%np
            fieldType = returnTypeN(gdMain,i)
            if (typeS .eq. 'L') then
               if (fieldType .eq. 'L1') then
                   gdMain%p(i)%l1 = val
               elseif (fieldType .eq. 'L2') then
                   gdMain%p(i)%l2 = val
               elseif (fieldType .eq. 'L3') then
                   gdMain%p(i)%l3 = val
               elseif (fieldType .eq. 'gd') then
                   if (recur) then
                       call setFieldValL(gdMain%g(i),typeS,val,recur)
                   endif    
               endif
            endif
        enddo    
        end subroutine
        
      recursive subroutine gdPartialCopy(gd1,gd2,recur)
        implicit none
        type(gd) gd1, gd2 ! copy: from gd1 -> gd2
        logical*1, optional::recur
        integer:: rec = 0, i, j, k, num
        character*10 fieldType
        do i=1,gd1%np
            do j = 1, gd2%np
                num = countRepNum(gd1,gd1%F(i))
                if (num>1) then
                    rec = rec + 1 ! here assumes only one repeative case.
                    k = searchStrF(gd2,gd1%F(i),rec)
                else
                    k = searchStrF(gd2,gd1%F(i))
                endif
                if (k>0) exit
            enddo
            if (k>0) then
                fieldType = returnTypeN(gd1,i)
                if (fieldType .eq. 'R1') then
                   gd2%p(k)%p1 = gd1%p(i)%p1
                elseif (fieldType .eq. 'R2') then
                   gd2%p(k)%p2 = gd1%p(i)%p2
                elseif (fieldType .eq. 'R3') then
                   gd2%p(k)%p3 = gd1%p(i)%p3
                elseif (fieldType .eq. 'J1') then
                   gd2%p(k)%j1 = gd1%p(i)%j1
                elseif (fieldType .eq. 'J2') then
                   gd2%p(k)%j2 = gd1%p(i)%j2
                elseif (fieldType .eq. 'J3') then
                   gd2%p(k)%j3 = gd1%p(i)%j3  
                elseif (fieldType .eq. 'L1') then
                   gd2%p(k)%l1 = gd1%p(i)%l1
                elseif (fieldType .eq. 'L2') then
                   gd2%p(k)%l2 = gd1%p(i)%l2
                elseif (fieldType .eq. 'L3') then
                   gd2%p(k)%l3 = gd1%p(i)%l3   
                elseif (fieldType .eq. 'gd') then
                   if (recur) then
                       call gdPartialCopy(gd1%g(i),gd2%g(k),recur)
                   endif    
               endif
            endif
        enddo    
        end subroutine  
      
      function searchStr(Fs,str,nf,num)
      integer nf, searchStr
      integer, optional :: num
      character(*),intent(in)::str
      character(*),intent(in),dimension(nf)::Fs
      integer I,J,K
      
      K = 0
      searchStr = -99
      DO i = 1,nf
        IF (TRIM(str) == TRIM(Fs(i))) THEN
          K = K + 1
        !write(*,*) i,TRIM(str),K,TRIM(Fs(i)),num
          IF (K .eq. num) then
            searchStr = i
            RETURN
          ENDIF
        ENDIF
      ENDDO
      
      end function
      
      FUNCTION str2num(str)
       ! Converting a string to a double variable
       REAL*8 str2num
       CHARACTER*(*) str
       
       read(str,*) str2num
       
      END FUNCTION
      
      FUNCTION getdim(main,field)
      type(gd) main
      character*10 typeN
      character(*),intent(in)::field
      integer getdim
       ! Converting a string to a double variable
       typeN = returnType(main,field)
       getDim = floor(str2num(typeN(2:2))+1D-8)
      END FUNCTION
             
      function searchStrF(main,str,num)  ! wrapper
      ! searchStrF(main,str)
      type(gd) main
      character(*),intent(in)::str
      integer, optional :: num
      integer nf, searchStrF
      integer I,J,K,n
      
      n = 1
      if (present(num)) n=num
      searchStrF = searchStr(main%f,str,main%np,n)
      end function
      
      subroutine allocate_gd(main,nAllo)
      type(gd) main
      integer, optional :: nAllo
      integer n,i
      n = 500
      if (present(nAllo)) n=nAllo
      
      allocate(main%p(n))
      allocate(main%f(n))
      if (present(nAllo)) allocate(main%g(n))
      allocate(main%name(1))
      do i=1,n
        main%f(i) = ' '
      enddo
      main%np = 0
      main%name(1) = ' '
      totalCounter = totalCounter + 1
      end subroutine
   
      subroutine deallocate_gd(main)
      type(gd) main
      integer m,n,i
      character*10, rt, cur
      do i=1,main%np 
        call deallocate_p(main,i)
      enddo
      main%np = 0
      deallocate(main%f)
      deallocate(main%p)
      end subroutine

      
      function addFieldName(main,str)
      ! Caution, this must be immediately followed by field populating
      type(gd) main
      character(*),intent(in)::str
      integer addFieldName,n
      n = main%np + 1
      main%f(n) = str
      !main%np = n  ! don't do this in here
      addFieldName = n
      end function
      
      function setFieldName(main,str,n)
      ! return the same-name index
      type(gd) main
      character(*),intent(in)::str
      integer setFieldName,n
      main%f(n) = str
      setFieldName = findRepNum(main, str, n)
      !write(*,*) 'setFieldName = ', setFieldName
      end function
      
      function countRepNum(main,str)
      integer countRepNum, num, i
      type(gd) main
      character(*),intent(in)::str
      countRepNum = 0
      DO I=1,main%np
         IF (TRIM(str) == TRIM(main%f(i))) THEN
            countRepNum = countRepNum + 1  
         ENDIF
      ENDDO
      end function
      
      function findRepNum(main,str,n)
      ! some field names are replicated
      ! this function finds the same-name index
      type(gd) main
      character(*),intent(in)::str
      integer findRepNum, n, k, num
      num = 0; k = 0;
      do while (k >= 0)
        num = num + 1
        k = searchStr(main%f,str,main%np,num)
            !write(*,*) 'wtf ', num, k, n, TRIM(main%f(num))
        if (k .eq. n) then
          findRepNum = num
          return
        endif
      enddo
      findRepNum = -99
      end function
      
      
      
      function returnType(main,field, num)
      ! Give you a type of the argument
      ! order of else if arranged to improve performance on common types
      character*10 returnType
      character(*),intent(in)::field
      integer, optional::num
      integer n, i, rt, m
      ! get the type of the return
      type(gd) main
      n = 0; m = 1; 
      if (present(num)) m =  num
      
      n = searchStr(main%f,field,main%np,m)
      returnType = returnTypeN(main,n)
      
      end function
      
      function returnTypeN(main,n)
      integer n
      type(gd) main
      character*10 rt, returntypeN
      
      if (n < 0 .or. n>main%np) then
        rt = 'NULL'
      else
        if (associated(main%g) .AND. main%g(n)%np>0) then
          rt = 'gd'
        elseif (associated(main%p(n)%p1)) then
          rt = 'R1'
        elseif (associated(main%p(n)%p2)) then
          rt = 'R2'
        elseif (associated(main%p(n)%p3)) then
          rt = 'R3'
        elseif (associated(main%p(n)%s3)) then
          rt = 'S3'
        elseif (associated(main%p(n)%p4)) then
          rt = 'R4'
        elseif (associated(main%p(n)%j1)) then
          rt = 'J1'
        elseif (associated(main%p(n)%j2)) then
          rt = 'J2'
        elseif (associated(main%p(n)%j3)) then
          rt = 'J3'
        elseif (associated(main%p(n)%j4)) then
          rt = 'J4'
        elseif (associated(main%p(n)%l1)) then
          rt = 'L1'
        elseif (associated(main%p(n)%l2)) then
          rt = 'L2'
        elseif (associated(main%p(n)%l3)) then
          rt = 'L3'
        elseif (associated(main%p(n)%l4)) then
          rt = 'L4'
        elseif (associated(main%p(n)%j5)) then
          rt = 'J5'
        elseif (associated(main%p(n)%j6)) then
          rt = 'J6'
        elseif (associated(main%p(n)%p5)) then
          rt = 'R5'
        elseif (associated(main%p(n)%p6)) then
          rt = 'R6'
        elseif (associated(main%p(n)%l5)) then
          rt = 'L5'
        elseif (associated(main%p(n)%l6)) then
          rt = 'L6'
        !elseif (associated(main%p(n)%proc)) then
        !  rt = 'proc'
        else
          rt = 'NULL'
        endif  
      endif
      returntypeN = rt
      end function
      
      function returnFieldSize(main,field)
      ! Give you a type of the argument
      ! order of else if arranged to improve performance on common types
      integer :: returnSizesI(maxDim)
      integer :: returnFieldSize
      character(*),intent(in)::field
      integer n, i, rt, m
      ! get the type of the return
      type(gd) main
      n = 0; m = 1; 
      n = searchStr(main%f,field,main%np,m)
      if (n<0) then
         returnFieldSize = 0
         return
      endif 
      returnSizesI = returnSizesN(main,n)
      returnFieldSize = returnSizesI(1)
      
      end function
      
      
        function gdnDim(main,field)
        implicit none
        type(gd) :: main
        character*(*) field
        character*10 datType
        integer gdnDim
        datType = returnType(main,field)
        if (datType(2:2) .eq. '1') then
            gdnDim = 1
        elseif (datType(2:2) .eq. '2') then
            gdnDim = 2
        elseif (datType(2:2) .eq. '3') then
            gdnDim = 3
        elseif (datType(2:2) .eq. '4') then
            gdnDim = 4
        elseif (datType(2:2) .eq. '5') then
            gdnDim = 5
        elseif (datType(2:2) .eq. '6') then
            gdnDim = 6
        else
            gdnDim = 0
        endif
        end function gdnDim
    
    
    
    !subroutine gdR1Retrieval(main, nlev, dict, ptr, dims)
    subroutine gdR1Retrieval(main,nlev,dict,ptr,dims)
    implicit none
    type(gd),target:: main
    type(gd),pointer:: gdt=>NULL(), gdt1=>NULL()
    real*8,dimension(:),pointer:: ptr
    integer dim, dims(6),i,j,nlev
    character*(100) :: dict(nlev)
    character*(100)  :: field
    real*8, pointer :: p1(:), p2(:,:), p3(:,:,:), p4(:,:,:,:), p5(:,:,:,:,:), p6(:,:,:,:,:,:)
    
    gdt => main
    field = dict(1) ! get ready for dictionary
    
    ! get to the bottom
    DO I=1,nlev-1
        !gdt => gdt .G. dict(i)
        call getptr(gdt, dict(i), gdt1)
        gdt=>gdt1
    ENDDO
    field = dict(nlev)
    dims = 0
    if (.true.) then
        dim = gdnDim(gdt,field )
        if (dim .eq. 1) then
            call getptr(gdt,field,p1)
            dims = returnSizes(gdt,field)
            ptr => p1
        elseif (dim .eq. 2) then
            call getptr(gdt,field,p2)
            dims = returnSizes(gdt,field)
            ptr  => oneD_ptr(size(p2),p2)
        elseif (dim .eq. 3) then
            call getptr(gdt,field,p3)
            dims = returnSizes(gdt,field)
            ptr  => oneD_ptr(size(p3),p3)
        elseif (dim .eq. 4) then
            call getptr(gdt,field,p4)
            dims= returnSizes(gdt,field)
            ptr  => oneD_ptr(size(p4),p4)
        elseif (dim .eq. 5) then
            call getptr(gdt,field,p5)
            dims = returnSizes(gdt,field)
            ptr  => oneD_ptr(size(p5),p5)
        elseif (dim .eq. 6) then
            call getptr(gdt,field,p6)
            dims = returnSizes(gdt,field)
            ptr  => oneD_ptr(size(p6),p6)
        endif
    endif
    end subroutine gdR1Retrieval
    !subroutine return
    
    
    subroutine valueBoundsCheck(n, dat, bounds, violate, nmask, mask)
    ! decides if bounds are exceeded. 
    ! NaN is counted as a violation
    ! mask is optional. We allow mask to be smaller in size than dat
    ! 
    implicit none
    integer :: n,m
    real*8 :: dat(n)
    integer :: nmask, j, i
    logical*1, dimension(nmask), optional :: mask
    logical*1 violate
    logical*1, dimension(n),target :: res ! true if violation
    logical*1, pointer :: L1(:)
    real*8 bounds(2)
    
    violate = .false.
    res = .NOT. ((dat>=bounds(1)) .AND. (dat<=bounds(2))) ! this way nan will be a violation
    
    if (present(mask)) then
        m = n/size(mask)
        if (mod(n,nmask) .ne. 0) call ch_assert('valueBoundsCheck :: length of data is not multiple of length of mask. Wrong data?')
        do j=1,m
            L1 => res( (j-1)*nmask+1: j* nmask )
            do i=1,nmask
                if (mask(i) .AND. L1(i)) then
                   violate = .true.;
                endif   
            enddo
        enddo    
    else    
        do i=1,n
            if (res(i)) then
               violate = .true.;
            endif   
        enddo
    endif
    end subroutine valueBoundsCheck
    
    
    subroutine gdValueBoundsCheck(main, nlev, dict, bounds, nmask, mask, dict2)
    ! check, using a dictionary, whether the subfields exceeds the bounds
    ! call CH_ASSERT with field name if it does.
    ! this short-hand format only supports checking of num(1)
    implicit none
    type(gd),target :: main
    integer nlev
    character*(100) :: dict(nlev)
    character*(100), optional :: dict2(nlev)
    logical :: is_comp 
    real*8 bounds(2)
    integer :: nmask, i
    logical*1, dimension(nmask),optional :: mask
    
    real*8, pointer, dimension(:) :: ptr=>null(),ptr2=>null()
    integer:: dims(6), k
    logical*1 violate
    character*500 :: msg
    type(gd), pointer:: gdt
    
    violate = .false.; is_comp = .false.
    
    if (present(dict2)) is_comp = .true. ! we are talking the comparison between two arrays
    
    !call gdR1Retrieval(main, nlev, dict, ptr, dims)
    !if(associated(ptr)) deallocate(ptr)
    call gdR1Retrieval(main,nlev,dict,ptr,dims)
    if (is_comp) call gdR1Retrieval(main,nlev,dict2,ptr2,dims)
    
    if (is_comp) then
        if (present(mask)) then
            call valueBoundsCheck(size(ptr), ptr-ptr2, bounds, violate, nmask, mask)
        else
            call valueBoundsCheck(size(ptr), ptr-ptr2, bounds, violate, 1)
        endif
    else
        if (present(mask)) then
            call valueBoundsCheck(size(ptr), ptr, bounds, violate, nmask, mask)
        else
            call valueBoundsCheck(size(ptr), ptr, bounds, violate, 1)
        endif
    endif    
    
    if (violate) then
        gdt => main
        DO I=1,nlev-1
            gdt => gdt .G. dict(i)
        ENDDO
        if (is_comp) then
            msg = 'gdValueBoundsCheck:: DIFFERENCE violation! : '//trim(gdt%name(1))//trim('.')//trim(dict(nlev))
        else
            msg = 'gdValueBoundsCheck:: bounds violation! : '//trim(gdt%name(1))//trim('.')//trim(dict(nlev))
        endif
        call CH_ASSERT(msg)
    endif
    end subroutine
    
      function returnSizes(main,field, num)
      ! Give you a type of the argument
      ! order of else if arranged to improve performance on common types
      integer :: returnSizes(maxDim)
      character(*),intent(in)::field
      integer, optional::num
      integer n, i, rt, m
      ! get the type of the return
      type(gd) main
      n = 0; m = 1; 
      if (present(num)) m =  num
      
      n = searchStr(main%f,field,main%np,m)
      returnSizes = returnSizesN(main,n)
      
      end function
      
      function returnSizesN(main,n)
      use specialMod
      integer :: n
      integer :: returnSizesN(maxDim)
      type(gd) main
      integer rt(maxDim)
      character(len=maxStrLen) field
      
      rt = 0
      if (n < 0 .or. n>main%np) then
        rt(1) = nanVal
      else
        if (associated(main%g) .AND. main%g(n)%np>0) then
          field = main%f(n)
          rt(1) = countRepNum(main,field)
        elseif (associated(main%p(n)%p1)) then
          rt(1) = size(main%p(n)%p1,1)
        elseif (associated(main%p(n)%p2)) then
          rt(1) = size(main%p(n)%p2,1)
          rt(2) = size(main%p(n)%p2,2)
        elseif (associated(main%p(n)%p3)) then
          rt(1) = size(main%p(n)%p3,1)
          rt(2) = size(main%p(n)%p3,2)
          rt(3) = size(main%p(n)%p3,3)
        elseif (associated(main%p(n)%s3)) then
          rt(1) = size(main%p(n)%s3,1)
          rt(2) = size(main%p(n)%s3,2)
          rt(3) = size(main%p(n)%s3,3)
        elseif (associated(main%p(n)%p4)) then
          rt(1) = size(main%p(n)%p4,1)
          rt(2) = size(main%p(n)%p4,2)
          rt(3) = size(main%p(n)%p4,3)
          rt(4) = size(main%p(n)%p4,4)
        elseif (associated(main%p(n)%j1)) then
          rt = size(main%p(n)%j1,1)
        elseif (associated(main%p(n)%j2)) then
          rt = size(main%p(n)%j2,1)
          rt = size(main%p(n)%j2,2)
        elseif (associated(main%p(n)%j3)) then
          rt(1) = size(main%p(n)%j3,1)
          rt(2) = size(main%p(n)%j3,2)
          rt(3) = size(main%p(n)%j3,3)
        elseif (associated(main%p(n)%j4)) then
          rt(1) = size(main%p(n)%j4,1)
          rt(2) = size(main%p(n)%j4,2)
          rt(3) = size(main%p(n)%j4,3)
          rt(4) = size(main%p(n)%j4,4)
        elseif (associated(main%p(n)%l1)) then
          rt = size(main%p(n)%l1,1)
        elseif (associated(main%p(n)%l2)) then
          rt(1) = size(main%p(n)%l2,1)
          rt(2) = size(main%p(n)%l2,2)
        elseif (associated(main%p(n)%l3)) then
          rt(1) = size(main%p(n)%l3,1)
          rt(2) = size(main%p(n)%l3,2)
          rt(3) = size(main%p(n)%l3,3)
        elseif (associated(main%p(n)%l4)) then
          rt(1) = size(main%p(n)%l4,1)
          rt(2) = size(main%p(n)%l4,2)
          rt(3) = size(main%p(n)%l4,3)
          rt(4) = size(main%p(n)%l4,4)
        elseif (associated(main%p(n)%j5)) then
          rt(1) = size(main%p(n)%j5,1)
          rt(2) = size(main%p(n)%j5,2)
          rt(3) = size(main%p(n)%j5,3)
          rt(4) = size(main%p(n)%j5,4)
          rt(5) = size(main%p(n)%j5,5)
        elseif (associated(main%p(n)%j6)) then
          rt(1) = size(main%p(n)%j6,1)
          rt(2) = size(main%p(n)%j6,2)
          rt(3) = size(main%p(n)%j6,3)
          rt(4) = size(main%p(n)%j6,4)
          rt(5) = size(main%p(n)%j6,5)
          rt(6) = size(main%p(n)%j6,6)
        elseif (associated(main%p(n)%p5)) then
          rt(1) = size(main%p(n)%p5,1)
          rt(2) = size(main%p(n)%p5,2)
          rt(3) = size(main%p(n)%p5,3)
          rt(4) = size(main%p(n)%p5,4)
          rt(5) = size(main%p(n)%p5,5)
        elseif (associated(main%p(n)%p6)) then
          rt(1) = size(main%p(n)%p6,1)
          rt(2) = size(main%p(n)%p6,2)
          rt(3) = size(main%p(n)%p6,3)
          rt(4) = size(main%p(n)%p6,4)
          rt(5) = size(main%p(n)%p6,5)
          rt(6) = size(main%p(n)%p6,6)
        elseif (associated(main%p(n)%l5)) then
          rt(1) = size(main%p(n)%l5,1)
          rt(2) = size(main%p(n)%l5,2)
          rt(3) = size(main%p(n)%l5,3)
          rt(4) = size(main%p(n)%l5,4)
          rt(5) = size(main%p(n)%l5,5)
        elseif (associated(main%p(n)%l6)) then
          rt(1) = size(main%p(n)%l6,1)
          rt(2) = size(main%p(n)%l6,2)
          rt(3) = size(main%p(n)%l6,3)
          rt(4) = size(main%p(n)%l6,4)
          rt(5) = size(main%p(n)%l6,5)
          rt(6) = size(main%p(n)%l6,6)
        else
          rt = nanVal
        endif  
      endif
      returnSizesN = rt
      end function
      
      recursive subroutine deallocate_p(main,n)
      ! (attempt to) deallocate dynamic memory
      ! we must use alloc_err because some memory cannot be deallocated
      integer n,i
      type(gd) main
      integer alloc_err 
      character*200, ev
      
      if (n < 0 .or. n>main%np) then
        ! nothing to be done
      else
        if (associated(main%p(n)%p1)) then
          deallocate(main%p(n)%p1, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%p2)) then
          deallocate(main%p(n)%p2, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%p3)) then
          deallocate(main%p(n)%p3, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%s3)) then
          deallocate(main%p(n)%s3, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%p4)) then
          deallocate(main%p(n)%p4, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%j1)) then
          deallocate(main%p(n)%j1, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%j2)) then
          deallocate(main%p(n)%j2, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%j3)) then
          deallocate(main%p(n)%j3, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%j4)) then
          deallocate(main%p(n)%j4, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%l1)) then
          deallocate(main%p(n)%l1, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%l2)) then
          deallocate(main%p(n)%l2, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%l3)) then
          deallocate(main%p(n)%l3, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%l4)) then
          deallocate(main%p(n)%l4, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%j5)) then
          deallocate(main%p(n)%l5, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%j6)) then
          deallocate(main%p(n)%l6, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%p5)) then
          deallocate(main%p(n)%p5, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%p6)) then
          deallocate(main%p(n)%p6, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%l5)) then
          deallocate(main%p(n)%l5, STAT = ALLOC_ERR)
        elseif (associated(main%p(n)%l6)) then
          deallocate(main%p(n)%l6, STAT = ALLOC_ERR)
        elseif (associated(main%g)) then
          if (main%g(n)%np>0) then
          do i = 1,main%g(n)%np
            call deallocate_p(main%g(n),i)
          enddo
          endif
        endif  
      endif
      end subroutine
      
      function isempty(main,field)
      logical isempty
      type(gd) main
      character*(*) field
      integer i
      i = searchStr(main%f,field,main%np,1)
      if (i<=0) then
        isempty = .true.
      else
        isempty = .false.
      endif
      end function
      
      function ise(main,field)
      logical ise, isempty
      type(gd) main
      character*(*) field
      integer i
      i = searchStr(main%f,field,main%np,1)
      if (i<=0) then
        isempty = .true.
      else
        isempty = .false.
      endif
      ise =  isempty
      end function
      
      
      !! ###########  ALL THAT IS BELOW IS VERY NASTY ########################
      !########### But I've given you the interfaces above ###################
      
      !#############  accessor routines: dimension and type ##################
      !#######################################################################
      subroutine getPtrR0(main,field,val,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      real*8, pointer:: val
      real*8,dimension(:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrR0:: no field called "//field)
      elseif (.not. associated(main%p(i)%p1)) then ! dimension and type may change
        call CH_ASSERT("getPtrR0:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%p1                          ! dimension and type may change
        ! allocate(val) ! XY: I doubt whether it is true.
        val => ptr(1)
      endif
      end subroutine
      
      subroutine getPtrR1(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      real*8,dimension(:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrR1:: no field called "//field)
      elseif (.not. associated(main%p(i)%p1)) then ! dimension and type may change
        call CH_ASSERT("getPtrR1:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%p1                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrR2(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      real*8,dimension(:,:),pointer:: ptr          ! dimension and type may change
      real*8,dimension(:),pointer:: ptr1D
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrR2:: no field called "//field)
      elseif (.not. associated(main%p(i)%p2)) then ! dimension and type may change
          if (associated(main%p(i)%p1)) then
             ptr1D => main%p(i)%p1 
             allocate(main%p(i)%p2(size(ptr1D),1))
             ptr => main%p(i)%p2
             ptr(:,1) = ptr1D
          else
             call CH_ASSERT("getPtrR2:: type/dimension between pointer and table    &
     & class do not match "//field) 
          endif
      else
        ptr => main%p(i)%p2                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrR3(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      real*8,dimension(:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrR3:: no field called "//field)
      elseif (.not. associated(main%p(i)%p3)) then ! dimension and type may change
        call CH_ASSERT("getPtrR3:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%p3                          ! dimension and type may change
      endif
      end subroutine
      
      
      subroutine getPtrS3(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      real*4,dimension(:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrS3:: no field called "//field)
      elseif (.not. associated(main%p(i)%s3)) then ! dimension and type may change
        call CH_ASSERT("getPtrS3:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%s3                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrR4(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      real*8,dimension(:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrR4:: no field called "//field)
      elseif (.not. associated(main%p(i)%p4)) then ! dimension and type may change
        call CH_ASSERT("getPtrR4:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%p4                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrR5(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      real*8,dimension(:,:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrR5:: no field called "//field)
      elseif (.not. associated(main%p(i)%p5)) then ! dimension and type may change
        call CH_ASSERT("getPtrR5:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%p5                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrR6(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      real*8,dimension(:,:,:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrR6:: no field called "//field)
      elseif (.not. associated(main%p(i)%p6)) then ! dimension and type may change
        call CH_ASSERT("getPtrR6:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%p6                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrI0(main,field,val,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      integer, pointer:: val
      integer,dimension(:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrI0:: no field called "//field)
      elseif (.not. associated(main%p(i)%j1)) then ! dimension and type may change
        call CH_ASSERT("getPtrI0:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%j1                          ! dimension and type may change
        ! allocate(val)! XY: I doubt whether it is true.
        val => ptr(1)
      endif
      end subroutine
      
      subroutine getPtrI1(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      integer,dimension(:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrI1:: no field called "//field)
      elseif (.not. associated(main%p(i)%j1)) then ! dimension and type may change
        call CH_ASSERT("getPtrI1:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%j1                          ! dimension and type may change
      endif
      end subroutine
      
      
      subroutine getPtrI2(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      integer,dimension(:,:),pointer:: ptr          ! dimension and type may change
      integer,dimension(:),pointer:: ptr1D
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrI2:: no field called "//field)
      elseif (.not. associated(main%p(i)%j2)) then ! dimension and type may change
          if (associated(main%p(i)%j1)) then
             ptr1D => main%p(i)%j1 
             allocate(main%p(i)%j2(size(ptr1D),1))
             ptr => main%p(i)%j2
             ptr(:,1) = ptr1D
          else
             call CH_ASSERT("getPtrI2:: type/dimension between pointer and table    &
     & class do not match "//field) 
          endif
      else
        ptr => main%p(i)%j2                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrI3(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      integer,dimension(:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrI3:: no field called "//field)
      elseif (.not. associated(main%p(i)%j3)) then ! dimension and type may change
        call CH_ASSERT("getPtrI3:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%j3                          ! dimension and type may change
      endif
      end subroutine
      
      
      subroutine getPtrI4(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      integer,dimension(:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrI4:: no field called "//field)
      elseif (.not. associated(main%p(i)%j4)) then ! dimension and type may change
        call CH_ASSERT("getPtrI4:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%j4                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrI5(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      integer,dimension(:,:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrI5:: no field called "//field)
      elseif (.not. associated(main%p(i)%j5)) then ! dimension and type may change
        call CH_ASSERT("getPtrI5:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%j5                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrI6(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      integer,dimension(:,:,:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrI6:: no field called "//field)
      elseif (.not. associated(main%p(i)%j6)) then ! dimension and type may change
        call CH_ASSERT("getPtrI6:: type/dimension between pointer and table    &
     & class do not match "//field)
      else
        ptr => main%p(i)%j6                          ! dimension and type may change
      endif
      end subroutine

      subroutine getPtrL0(main,field,val,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      logical*1, pointer:: val
      logical*1,dimension(:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrL0:: no field called "//field)
      elseif (.not. associated(main%p(i)%l1)) then ! dimension and type may change
       if (associated(main%p(i)%j1)) then    ! saved as integer
         call I2L(main%p(i)%j1,main%p(i)%l1)  
         ptr => main%p(i)%l1                          ! dimension and type may change
         ! allocate(val) ! XY: I doubt whether it is true.
         val => ptr(1)
       else
         call CH_ASSERT("getPtrL0:: type/dimension between pointer and table    &
     & class do not match "//field)
       endif
      else
        ptr => main%p(i)%l1                          ! dimension and type may change
        ! allocate(val) ! XY: I doubt whether it is true.
        val => ptr(1)
      endif
      end subroutine      
      
      subroutine getPtrL1(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      logical*1,dimension(:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrL1:: no field called "//field)
      elseif (.not. associated(main%p(i)%l1)) then ! dimension and type may change
       if (associated(main%p(i)%j1)) then    ! saved as integer
         call I2L(main%p(i)%j1,main%p(i)%l1)  
         ptr => main%p(i)%l1           
       else
        call CH_ASSERT("getPtrL1:: type/dimension between pointer and table    &
     & class do not match "//field)
       endif
      else
        ptr => main%p(i)%l1                          ! dimension and type may change
      endif
      end subroutine
      
      
      subroutine getPtrL2(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      logical*1,dimension(:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrL2:: no field called "//field)
      elseif (.not. associated(main%p(i)%l2)) then ! dimension and type may change
       if (associated(main%p(i)%j2)) then   ! saved as integer    
         call I2L(main%p(i)%j2,main%p(i)%l2)  
         ptr => main%p(i)%l2
       else 
         call CH_ASSERT("getPtrL2:: type/dimension between pointer and table    &
     & class do not match "//field)
       endif
      else
        ptr => main%p(i)%l2                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrL3(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      logical*1,dimension(:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrL3:: no field called "//field)
      elseif (.not. associated(main%p(i)%l3)) then ! dimension and type may change
       if (associated(main%p(i)%j3)) then  ! saved as integer
         call I2L(main%p(i)%j3,main%p(i)%l3)  
         ptr => main%p(i)%l3
       else
         call CH_ASSERT("getPtrL3:: type/dimension between pointer and table    &
     & class do not match "//field)
       endif
      else
        ptr => main%p(i)%l3                          ! dimension and type may change
      endif
      end subroutine
      
      
      subroutine getPtrL4(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      logical*1,dimension(:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrL4:: no field called "//field)
      elseif (.not. associated(main%p(i)%l4)) then ! dimension and type may change
       if (associated(main%p(i)%j4)) then  ! saved as integer  
         call I2L(main%p(i)%j4,main%p(i)%l4)  
         ptr => main%p(i)%l4
       else
         call CH_ASSERT("getPtrL4:: type/dimension between pointer and table    &
     & class do not match "//field)
       endif
      else
        ptr => main%p(i)%l4                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrL5(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      logical*1,dimension(:,:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrL5:: no field called "//field)
      elseif (.not. associated(main%p(i)%l5)) then ! dimension and type may change
       if (associated(main%p(i)%j5)) then  ! saved as integer  
         call I2L(main%p(i)%j5,main%p(i)%l5)  
         ptr => main%p(i)%l5
       else
         call CH_ASSERT("getPtrL5:: type/dimension between pointer and table    &
     & class do not match "//field)
       endif
      else
        ptr => main%p(i)%l5                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrL6(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      logical*1,dimension(:,:,:,:,:,:),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrL6:: no field called "//field)
      elseif (.not. associated(main%p(i)%l6)) then ! dimension and type may change
       if (associated(main%p(i)%j6)) then  ! saved as integer  
         call I2L(main%p(i)%j6,main%p(i)%l6)  
         ptr => main%p(i)%l6
       else
         call CH_ASSERT("getPtrL6:: type/dimension between pointer and table    &
     & class do not match "//field)
       endif
      else
        ptr => main%p(i)%l6                          ! dimension and type may change
      endif
      end subroutine
      
      subroutine getPtrStr(main,field,ptr,num)    ! names change
      ! Get the "field" from "main" to "ptr"
      ! return <0 for trouble and =0 for success
      
      type(gd),pointer:: ptr          ! dimension and type may change
      type(gd) main
      character*(*) field
      integer, optional :: num
      integer i,n,s
      n = 1
      if (present(num)) n=num
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrStr:: no field called "//field)
      !elseif (.not. associated(main%g(i))) then ! dimension and type may change
      !  call CH_ASSERT("getPtrStr:: type/dimension between pointer and table class do not match "//field)
      else
        ptr => main%g(i)                            ! dimension and type may change
      endif
      end subroutine
      
      !subroutine getPtrProc(main,field,ptr,num)    ! names change
      !! Get the "field" from "main" to "ptr"
      !! return <0 for trouble and =0 for success
      !
      !procedure(),pointer:: ptr          ! dimension and type may change
      !type(gd) main
      !character*(*) field
      !integer, optional :: num
      !integer i,n,s
      !n = 1
      !if (present(num)) n=num
      !i = searchStr(main%f,field,main%np,n)
      !if (i<=0) then
      !  call CH_ASSERT("getPtrStr:: no field called "//field)
      !!elseif (.not. associated(main%g(i))) then ! dimension and type may change
      !!  call CH_ASSERT("getPtrStr:: type/dimension between pointer and table class do not match "//field)
      !else
      !  ptr => main%p(i)%proc                            ! dimension and type may change
      !endif
      !end subroutine
    
      function getPtrStrFunc(main,field)
      ! same as getPtrStr, but in the form of a function, (in order to overload operator .G.
      ! also can only search for the first item
      type(gd),pointer:: getPtrStrFunc          ! dimension and type may change
      type(gd),intent(in) :: main
      character*(*),intent(in) :: field
      integer i,n,s
      n = 1
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrStr:: no field called "//field)
      else
        getPtrStrFunc => main%g(i)                ! dimension and type may change
      endif
      end function
      
      function getPtrR1Func(main,field)
      ! same as getPtrStr, but in the form of a function, (in order to overload operator .G.
      ! also can only search for the first item
      real*8,dimension(:),pointer:: getPtrR1Func          ! dimension and type may change
      type(gd),intent(in) :: main
      character*(*),intent(in) :: field
      integer i,n,s
      n = 1
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrStr:: no field called "//field)
      else
        getPtrR1Func => main%p(i)%p1                ! dimension and type may change
      endif
      end function
      
      function getPtrR2Func(main,field)
      ! same as getPtrStr, but in the form of a function, (in order to overload operator .G.
      ! also can only search for the first item
      real*8,dimension(:,:),pointer:: getPtrR2Func          ! dimension and type may change
      type(gd),intent(in) :: main
      character*(*),intent(in) :: field
      integer i,n,s
      n = 1
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrStr:: no field called "//field)
      else
        getPtrR2Func => main%p(i)%p2                ! dimension and type may change
      endif
      end function
      
      function getPtrR3Func(main,field)
      ! same as getPtrStr, but in the form of a function, (in order to overload operator .G.
      ! also can only search for the first item
      real*8,dimension(:,:,:),pointer:: getPtrR3Func          ! dimension and type may change
      type(gd),intent(in) :: main
      character*(*),intent(in) :: field
      integer i,n,s
      n = 1
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrStr:: no field called "//field)
      else
        getPtrR3Func => main%p(i)%p3                ! dimension and type may change
      endif
      
      end function
      
      function getPtrS3Func(main,field)
      ! same as getPtrStr, but in the form of a function, (in order to overload operator .G.
      ! also can only search for the first item
      real*4,dimension(:,:,:),pointer:: getPtrS3Func          ! dimension and type may change
      type(gd),intent(in) :: main
      character*(*),intent(in) :: field
      integer i,n,s
      n = 1
      i = searchStr(main%f,field,main%np,n)
      if (i<=0) then
        call CH_ASSERT("getPtrStr:: no field called "//field)
      else
        getPtrS3Func => main%p(i)%s3                ! dimension and type may change
      endif
      
      end function
      !#############  assign pointer routines: dimension and type (in module) #############
      !####################################################################################
      ! the reason of existence for this module is the above dilemma of assign pointer:
      ! (1) you cannot put these assignPointers in a module due to %VAL (that will be used in matTable)
      ! (2) it already used gd_mod (therefore gd_mod cannot call them back)
      ! gd itself must be allocated (with allocate_gd) before it is used 
      ! Otherwise you get an error
      
      subroutine mAssignR1(nn,datND,main,n)
      INTEGER nn(1),n
      REAL*8,Target:: datND(nn(1))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p1 => datND
      END subroutine 
      
      subroutine mAssignR2(nn,datND,main,n)
      INTEGER nn(2),n
      REAL*8,Target:: datND(nn(1),nn(2))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p2 => datND
      END subroutine 
      
      subroutine mAssignR3(nn,datND,main,n)
      INTEGER nn(3),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p3 => datND
      END subroutine 
      
      subroutine mAssignS3(nn,datND,main,n)
      INTEGER nn(3),n
      REAL*4,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%s3 => datND
      END subroutine 
      
      subroutine mAssignR4(nn,datND,main,n)
      INTEGER nn(4),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p4 => datND
      END subroutine 
      
      subroutine mAssignR5(nn,datND,main,n)
      INTEGER nn(5),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p5 => datND
      END subroutine 
      
      subroutine mAssignR6(nn,datND,main,n)
      INTEGER nn(6),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p6 => datND
      END subroutine 
      
      subroutine mAssignI1(nn,datND,main,n)
      INTEGER nn(1),n
      integer,Target:: datND(nn(1))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j1 => datND
      END subroutine 
      
      subroutine mAssignI2(nn,datND,main,n)
      INTEGER nn(2),n
      integer,Target:: datND(nn(1),nn(2))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j2 => datND
      END subroutine 
      
      subroutine mAssignI3(nn,datND,main,n)
      INTEGER nn(3),n
      integer,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j3 => datND
      END subroutine 
      
      subroutine mAssignI4(nn,datND,main,n)
      INTEGER nn(4),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j4 => datND
      END subroutine 
      
      subroutine mAssignI5(nn,datND,main,n)
      INTEGER nn(5),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j5 => datND
      END subroutine 
      
      subroutine mAssignI6(nn,datND,main,n)
      INTEGER nn(6),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j6 => datND
      END subroutine 
      
      subroutine mAssignL1(nn,datND,main,n)
      INTEGER nn(1),n
      logical*1,Target:: datND(nn(1))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l1 => datND
      END subroutine 
      
      subroutine mAssignL2(nn,datND,main,n)
      INTEGER nn(2),n
      logical*1,Target:: datND(nn(1),nn(2))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l2 => datND
      END subroutine 
      
      subroutine mAssignL3(nn,datND,main,n)
      INTEGER nn(3),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l3 => datND
      END subroutine 
      
      subroutine mAssignL4(nn,datND,main,n)
      INTEGER nn(4),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l4 => datND
      END subroutine 
      
      subroutine mAssignL5(nn,datND,main,n)
      
      INTEGER nn(5),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l5 => datND
      END subroutine 
      
      subroutine mAssignL6(nn,datND,main,n)
      INTEGER nn(6),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l6 => datND
      END subroutine
      
      !subroutine mAssignProc(datND,main,n)
      !INTEGER n
      !procedure():: datND  
      !type(gd) main
      !call deallocate_p(main,n)
      !main%p(n)%proc => datND
      !END subroutine 
      
      subroutine mAssignPtrStr(ptr,main,n)    ! names change
      type(gd),pointer:: ptr                  ! dimension and type may change
      type(gd) :: main
      integer i,n,s
      !ptr => main%g(n)
      !main%g(n) => ptr                           ! dimension and type may change
      call CH_ASSERT('not coded yet')
      end subroutine
      
      !####################################################################################
      !####################################################################################
      ! Unfortunately there is nothing such as Code re-use in Fortran
      subroutine addR3cp(field,datND,main)
      ! if it finds a field with the same name and same dimensions: just do copy
      ! if not, deallocate previously data and reallocate for the corect size befrore copying
      INTEGER nn(3),n
      REAL*8,Target:: datND(:,:,:)
      type(gd) main
      character*(*),intent(in) :: field
      integer :: ns(3),ns2(3)
      logical :: doAllo
      doAllo = .true.
      n = searchStrF(main,field)
      if (n<=0) then
          n = addFieldName(main,field)
      else
          if (size(main%p(n)%p3) .eq. size(datND)) then
              doAllo= .false.
          endif
      endif
      if (doAllo) then
          if (associated(main%p(n)%p3)) deallocate(main%p(n)%p3)
          allocate(main%p(n)%p3(size(datND,1),size(datND,2),size(datND,3)) )
      endif
      main%p(n)%p3 = datND; main%np = max(n,main%np)
      END subroutine 
      
      subroutine addS3cp(field,datND,main)
      ! if it finds a field with the same name and same dimensions: just do copy
      ! if not, deallocate previously data and reallocate for the corect size befrore copying
      INTEGER nn(3),n
      REAL*4,Target:: datND(:,:,:)
      type(gd) main
      character*(*),intent(in) :: field
      integer :: ns(3),ns2(3)
      logical :: doAllo
      doAllo = .true.
      n = searchStrF(main,field)
      if (n<=0) then
          n = addFieldName(main,field)
      else
          if (size(main%p(n)%s3) .eq. size(datND)) then
              doAllo= .false.
          endif
      endif
      if (doAllo) then
          if (associated(main%p(n)%s3)) deallocate(main%p(n)%s3)
          allocate(main%p(n)%s3(size(datND,1),size(datND,2),size(datND,3)) )
      endif
      main%p(n)%s3 = datND; main%np = max(n,main%np)
      ! because it is copying and may copy to old structure, do not set to n
      END subroutine 
      
      subroutine addR1(nn,field,datND,main)
      INTEGER nn(1),n
      REAL*8,Target:: datND(nn(1))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addR2(nn,field,datND,main)
      INTEGER nn(2),n
      REAL*8,Target:: datND(nn(1),nn(2))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addR3(nn,field,datND,main)
      INTEGER nn(3),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addS3(nn,field,datND,main)
      INTEGER nn(3),n
      REAL*4,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addR4(nn,field,datND,main)
      INTEGER nn(4),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addR5(nn,field,datND,main)
      INTEGER nn(5),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addR6(nn,field,datND,main)
      INTEGER nn(6),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addI1(nn,field,datND,main)
      INTEGER nn(1),n
      integer,Target:: datND(nn(1))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addI2(nn,field,datND,main)
      INTEGER nn(2),n
      integer,Target:: datND(nn(1),nn(2))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addI3(nn,field,datND,main)
      INTEGER nn(3),n
      integer,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addI4(nn,field,datND,main)
      INTEGER nn(4),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addI5(nn,field,datND,main)
      INTEGER nn(5),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addI6(nn,field,datND,main)
      INTEGER nn(6),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addL1(nn,field,datND,main)
      INTEGER nn(1),n
      logical*1,Target:: datND(nn(1))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addL2(nn,field,datND,main)
      INTEGER nn(2),n
      logical*1,Target:: datND(nn(1),nn(2))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addL3(nn,field,datND,main)
      INTEGER nn(3),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addL4(nn,field,datND,main)
      INTEGER nn(4),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addL5(nn,field,datND,main)
      
      INTEGER nn(5),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addL6(nn,field,datND,main)
      INTEGER nn(6),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      character*(*),intent(in) :: field
      n = addFieldName(main,field)
      call assignPtr(nn,datND,main,n); main%np = n
      END subroutine 
      
      subroutine addPtrStr(ptr,main,n)    ! names change
      type(gd),pointer:: ptr                  ! dimension and type may change
      type(gd) :: main
      integer i,n,s
      !ptr => main%g(n)
      !main%g(n) => ptr                           ! dimension and type may change
      call CH_ASSERT('not coded yet')
      end subroutine
      !
      !subroutine addPtrProc(field,datND,main)
      !INTEGER n
      !procedure() :: datND
      !type(gd) main
      !character*(*),intent(in) :: field
      !n = addFieldName(main,field)
      !call assignPtr(datND,main,n); main%np = n
      !END subroutine 
      subroutine I1tL1(datI,datL)
        integer, dimension(:) :: datI
        logical*1, pointer :: datL(:)
        integer :: i,s
        
        s = size(datI)
        allocate(datL(s))
        do i=1,s
            if (datI(i)>0) then
                datL(i) = 1
            else
                datL(i) = 0
            endif
        enddo
      end subroutine    
  
      subroutine I2tL2(datI,datL)
        integer, dimension(:,:) :: datI
        logical*1, pointer :: datL(:,:)
        integer :: s(2)
        integer :: i1,i2
        
        s = shape(datI)
        allocate(datL(s(1),s(2)))
        do i1=1,s(1)
            do i2=1,s(2)
                if (datI(i1,i2) >0) then
                    datL(i1,i2) = 1
                else
                    datL(i1,i2) = 0
                endif
            enddo
        enddo
      end subroutine    
    
      subroutine I3tL3(datI,datL)
        integer, dimension(:,:,:) :: datI
        logical*1, pointer :: datL(:,:,:)
        integer :: s(3)
        integer :: i1,i2,i3
        
        s = shape(datI)
        allocate(datL(s(1),s(2),s(3)))
        do i1=1,s(1)
            do i2=1,s(2)
                do i3=1,s(3)
                    if (datI(i1,i2,i3) >0) then
                        datL(i1,i2,i3) = 1
                    else
                        datL(i1,i2,i3) = 0
                    endif
                enddo
            enddo
        enddo
      end subroutine      
     
      subroutine I4tL4(datI,datL)
        integer, dimension(:,:,:,:) :: datI
        logical*1, pointer :: datL(:,:,:,:)
        integer :: s(4)
        integer :: i1,i2,i3,i4
        
        s = shape(datI)
        allocate(datL(s(1),s(2),s(3),s(4)))
        do i1=1,s(1)
            do i2=1,s(2)
                do i3=1,s(3)
                    do i4=1,s(4)
                        if (datI(i1,i2,i3,i4) >0) then
                            datL(i1,i2,i3,i4) = 1
                        else
                            datL(i1,i2,i3,i4) = 0
                        endif
                    enddo
                enddo
            enddo
        enddo
      end subroutine      
     
      subroutine I5tL5(datI,datL)
        integer, dimension(:,:,:,:,:) :: datI
        logical*1, pointer :: datL(:,:,:,:,:)
        integer :: s(5)
        integer :: i1,i2,i3,i4,i5
        
        s = shape(datI)
        allocate(datL(s(1),s(2),s(3),s(4),s(5)))
        do i1=1,s(1)
            do i2=1,s(2)
                do i3=1,s(3)
                    do i4=1,s(4)
                        do i5=1,s(5)
                            if (datI(i1,i2,i3,i4,i5) >0) then
                                datL(i1,i2,i3,i4,i5) = 1
                            else
                                datL(i1,i2,i3,i4,i5) = 0
                            endif
                        enddo
                    enddo
                enddo
            enddo
        enddo
      end subroutine      
     
      subroutine I6tL6(datI,datL)
        integer, dimension(:,:,:,:,:,:) :: datI
        logical*1, pointer :: datL(:,:,:,:,:,:)
        integer :: s(6)
        integer :: i1,i2,i3,i4,i5,i6
        
        s = shape(datI)
        allocate(datL(s(1),s(2),s(3),s(4),s(5),s(6)))
        do i1=1,s(1)
            do i2=1,s(2)
                do i3=1,s(3)
                    do i4=1,s(4)
                        do i5=1,s(5)
                            do i6=1,s(6)
                              if (datI(i1,i2,i3,i4,i5,i6) >0) then
                                datL(i1,i2,i3,i4,i5,i6) = 1
                              else
                                datL(i1,i2,i3,i4,i5,i6) = 0
                              endif
                            enddo
                        enddo
                    enddo
                enddo
            enddo
        enddo
      end subroutine      

      subroutine RetrieveEndNode(gdt, gd_rt, wholename,field)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        type(gd), pointer :: gdt=>null(), gdt2=>null()
        character(*), optional :: field
        character(maxStrLen) :: subname, mdname, mdSeq, recurName, mdname2
        integer indD, indB1, indB2, Seq

        gdt => gd_rt
        ! hierarchy digging loop
        recurName = wholename
        indD = index(recurName,'%')
        do while (indD > 0)
           mdname = recurName(1:indD-1)
           subname = recurName(indD+1:)
           ! structure array judgement
           indB1 = index(mdname,'(')
           Seq = 1
           if (indB1> 0) then
              indB2 = index(mdname,')')
              mdname2 = mdname(1:indB1-1)
              mdSeq = mdname(indB1+1:indB2-1)
              Seq = str2num(trim(mdSeq))
              mdname = mdname2
           endif
           call getptr(gdt,mdname,gdt2,Seq)
           gdt => gdt2
           recurName = subname
           indD = index(recurName,'%')
        end do
        ! return gdt as the ending gd-node
        ! recurName now as the field
        if (present(field)) field = trim(recurName)
        !write(*,*) gdt%name 

      end subroutine
      
      subroutine gdRetrieveR1(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        real*8, dimension(:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveR2(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        real*8, dimension(:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveR3(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        real*8, dimension(:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveR4(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        real*8, dimension(:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveR5(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        real*8, dimension(:,:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveR6(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        real*8, dimension(:,:,:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveI1(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        integer, dimension(:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveI2(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        integer, dimension(:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveI3(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        integer, dimension(:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveI4(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        integer, dimension(:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveI5(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        integer, dimension(:,:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveI6(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        integer, dimension(:,:,:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveL1(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        logical*1, dimension(:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveL2(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        logical*1, dimension(:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveL3(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        logical*1, dimension(:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveL4(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        logical*1, dimension(:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveL5(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        logical*1, dimension(:,:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      subroutine gdRetrieveL6(ptr, gd_rt, wholename)
        character(*), intent(in) :: wholename
        type(gd), target :: gd_rt ! root node above the whole name
        logical*1, dimension(:,:,:,:,:,:), pointer :: ptr
        type(gd), pointer :: gdt=>null()
        character(maxStrLen) :: field
        
        call RetrieveEndNode(gdt, gd_rt, wholename, field)
        call getptr(gdt,field,ptr)
      end subroutine        

      END MODULE gd_mod
      
      
      function prepareEntryToGd_base(name)
      use gd_Mod
      ! adds a field (sub-structure into base). This is likely mat_file, or 'entrance'
      !integer, parameter :: maxStrLen = 100  !!Duplicated if declaration in gd_Mod is changed, this also needs to be changed ! <<<<<=====defaut
      integer, parameter :: maxStrLen = 500  !!Duplicated if declaration in gd_Mod is changed, this also needs to be changed
      ! character(len=maxStrLen) :: names(100), name  ! default
      character(len=maxStrLen) :: names(500), name
      integer prepareEntryToGd_base, np
      integer nbasePtr ! I found that if I declare integer gdbasePtr = 10, there will be retrieval problems, astonishing
      parameter(nbasePtr = 10) ! shouldn't be so many base level structures (mat_file, 'entrance')
      if (.not. associated(gd_base%p)) call allocate_gd(gd_base)
      gd_base%name(1) = 'base'
      
      np = gd_base%np + 1
      gd_base%f(np) = name
      gd_base%np    = np
      
      if (.not. associated(gd_base%g)) then
         allocate(gd_base%g(nbasePtr))
      endif
      call allocate_gd(gd_base%g(np))
      prepareEntryToGd_base = np
      gd_base%g(np)%name(1)= trim('base.') // trim(name)
      
      end function
      
      !#############  assign pointer routines: dimension and type ############
      !#######################################################################
      ! put this outside matTable because I may want to use it in mex environment
      ! if it is put inside a module, %VAL fails
!      subroutine assign1dR(nn,datND,ptr)
!      INTEGER nn(1)
!      REAL*8,Target:: datND(nn(1))
!      REAL*8,DIMENSION(:),POINTER::ptr
!      ptr => datND
!      END subroutine
      ! This subroutine above is a lost cause. pointers cannot be passed around when they are not associated
      ! Making this subroutine a function still doesn't work, because function cannot take %val construct
      ! therefore, below is our only choice: pass the damn structure itself in
      
      subroutine assignR1(nn,datND,main,n)
      use gd_mod
      INTEGER nn(1),n
      REAL*8,Target:: datND(nn(1))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p1 => datND
      END subroutine 
      
      subroutine assignR2(nn,datND,main,n)
      use gd_mod
      INTEGER nn(2),n
      REAL*8,Target:: datND(nn(1),nn(2))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p2 => datND
      END subroutine 
      
      subroutine assignR3(nn,datND,main,n)
      use gd_mod
      INTEGER nn(3),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p3 => datND
      END subroutine 
      
      subroutine assignS3(nn,datND,main,n)
      use gd_mod
      INTEGER nn(3),n
      REAL*4,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%s3 => datND
      END subroutine 
      
      subroutine assignR4(nn,datND,main,n)
      use gd_mod
      INTEGER nn(4),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p4 => datND
      END subroutine 
      
      subroutine assignR5(nn,datND,main,n)
      use gd_mod
      INTEGER nn(5),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p5 => datND
      END subroutine 
      
      subroutine assignR6(nn,datND,main,n)
      use gd_mod
      INTEGER nn(6),n
      REAL*8,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%p6 => datND
      END subroutine 
      
      subroutine assignI1(nn,datND,main,n)
      use gd_mod
      INTEGER nn(1),n
      integer,Target:: datND(nn(1))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j1 => datND
      END subroutine 
      
      subroutine assignI2(nn,datND,main,n)
      use gd_mod
      INTEGER nn(2),n
      integer,Target:: datND(nn(1),nn(2))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j2 => datND
      END subroutine 
      
      subroutine assignI3(nn,datND,main,n)
      use gd_mod
      INTEGER nn(3),n
      integer,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j3 => datND
      END subroutine 
      
      subroutine assignI4(nn,datND,main,n)
      use gd_mod
      INTEGER nn(4),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j4 => datND
      END subroutine 
      
      subroutine assignI5(nn,datND,main,n)
      use gd_mod
      INTEGER nn(5),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j5 => datND
      END subroutine 
      
      subroutine assignI6(nn,datND,main,n)
      use gd_mod
      INTEGER nn(6),n
      integer,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%j6 => datND
      END subroutine 
      
      subroutine assignL1(nn,datND,main,n)
      use gd_mod
      INTEGER nn(1),n
      logical*1,Target:: datND(nn(1))
      type(gd) main
      main%p(n)%l1 => datND
      END subroutine 
      
      subroutine assignL2(nn,datND,main,n)
      use gd_mod
      INTEGER nn(2),n
      logical*1,Target:: datND(nn(1),nn(2))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l2 => datND
      END subroutine 
      
      subroutine assignL3(nn,datND,main,n)
      use gd_mod
      INTEGER nn(3),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l3 => datND
      END subroutine 
      
      subroutine assignL4(nn,datND,main,n)
      use gd_mod
      INTEGER nn(4),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l4 => datND
      END subroutine 
      
      subroutine assignL5(nn,datND,main,n)
      use gd_mod
      INTEGER nn(5),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l5 => datND
      END subroutine 
      
      subroutine assignL6(nn,datND,main,n)
      use gd_mod
      INTEGER nn(6),n
      logical*1,Target:: datND(nn(1),nn(2),nn(3),nn(4),nn(5),nn(6))
      type(gd) main
      call deallocate_p(main,n)
      main%p(n)%l6 => datND
      END subroutine 

    

        
      !#########  end assign pointer routines: dimension and type ############
      !#######################################################################
      
!      
!      interface connectField
!      subroutine connectFieldReal8
!      
!      subroutine connectFieldInteger
!      
!      subroutine connectFieldLogical
!      
!      end subroutine
!      end interface
      
