MODULE pncTableHier
    ! netcdf file interface
    ! modified form matTable
    ! This code replaces matTable.f90 when we link to netcdf (in serial) instead of .mat file
    ! *****************************************************
    ! Xinye Ji
    ! Pennsylvania State University
    ! Civil and Environmental Engineering
    ! jixinye91@gmail.com
    ! *****************************************************
    ! build an intermediate table object which stores data read from a netcdf file. 
    ! It can be used later to conveniently link the data to computational objects
    ! syntax:
    ! ####################################################
    ! SOME WHERE IN THE PROGRAM YOU MUST FIRST DO:
    ! ++ use ncTableHier
    ! ++ call buildNCTable(nc_file)
    ! ####################################################
    ! after this, you can access all variable stored in the nc_file via gd_base, which is a type(gd) variable in module ncTable
    ! the general syntax is
    ! ++ call getPtr(gd_base,fieldname,ptr,num)  ! num is optional, default to 1
    ! this will link the data to the pointer, ptr, which may be a real*8,integer, logical array supported up to 6 dimension
    ! or a type(gd) array pointer.
    ! depending on what pointer is passed in, the calling interface will pick the corresponding type/dimension subroutine
    ! If the queried field is not there or if the field type/dimension does not match that of the ptr, an error is thrown.
    ! num is the index, if omitted, default to 1. type(gd) allows to access sub-structures.
    ! Overloaded operators as a fast access:
    ! Besides the calling interface getPtr, we have overloaded operator .G., .FF. and .FFF. serving as shorthand methods
    ! for extracting sub-structure, [real*8,dimension(:,:)] and [real*8,dimension(:,:,:)] data, respectively. 
    ! With these operators, num is always 1.
      ! *****************************************************
      !Copyright (c) 2015, Chaopeng Shen, Xinye Ji
      !All rights reserved.
      !
      !THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
      !ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
      !WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
      !DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
      !ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
      !(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
      !LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
      !ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
      !(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
      !SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
      !    
      ! Please cite the following two papers if this tool is used in your academic research work
      ! 1. Ji, XY and CP. Shen, The introspective may achieve more: enhancing existing Geoscientific 
      !      models with native-language structural reflection, Environmental Modeling & Software.
      ! 2. Shen, CP., J. Niu and K. Fang, Quantifying the Effects of Data Integration Algorithms
      !      on the Outcomes of a Subsurface - Land Surface Processes Model, 
      !      Environmental Modeling & Software., doi: 10.1016/j.envsoft.2014.05.006 (2014)
      !
      !Commercial use of the software need to obtain explicit consent from the copyright owner.
      !Redistribution and use in source and binary forms for non-commercial purposes, with or without
      !modification, are permitted provided that the following conditions are met:
      !
      !1. Redistributions of source code must retain the above copyright notice, this
      !   list of conditions and the following disclaimer.
      !2. Redistributions in binary form must reproduce the above copyright notice,
      !   this list of conditions and the following disclaimer in the documentation
      !   and/or other materials provided with the distribution.
      !
      !*******************************************************      
    use gd_mod
    use netcdf
    use proc_mod
    include 'mpif.h'
    
    integer nG, nsG
    parameter (nG =600)
    parameter (nsG = 1500)
    !integer, parameter::maxStrLen=100    ! <<<<<===== default
    integer, parameter::maxStrLen=500
    PUBLIC :: buildNCTable, connectNCField
    contains

    subroutine buildNCTable(nc_file,gdpIn,MasterRead)
      ! An entrance.
      use vdata, only : num2str
      implicit none
      !integer, parameter :: cLen = 100
      integer*4 num, nbase
      !character*(cLen) names
      integer   i, status, np
      integer ntemp,j
      character*(*) nc_file!,nc_Subfile
      type(gd),optional,target :: gdpIn
      logical,optional :: MasterRead
      type(gd),pointer  :: gdp
      !character(len=100)::nc_Subfile    ! <<<<<===== default
      character(len=500)::nc_Subfile
      logical :: doMasterRead
      !integer :: ncid, varid, dimid, numVar

      nbase = 10
      if (present(gdpIn)) then
          gdp => gdpIn
      else
          gdp => gd_base
      endif

      if (present(MasterRead)) then
          doMasterRead = MasterRead
      else
          doMasterRead = .false.
      endif
      
      if (.not. associated(gdp%p)) call allocate_gd(gdp,nbase)
      gdp%name(1) = 'base'
      np = gdp%np + 1   
      gdp%f(np) = nc_file
      gdp%np    = np
     
      if (.not. associated(gdp%g)) then
         allocate(gdp%g(nbase))
      endif
      call allocate_gd(gdp%g(np),nG)
      gdp%g(np)%name(1) = nc_file
      
      open(unit=666, file=nc_file,status='old',action='read',iostat=status)
      if (status/=0) then
          i = index(nc_file, '.')
          nc_Subfile =' '
          !nc_Subfile(1:i-1) = nc_file(1:i-1)
          j = 1
          nc_Subfile = nc_file(1:i-1)//'('//trim(num2str(j))//').nc' 
          open(unit=667, file=trim(nc_Subfile),status='old',action='read',iostat=status)
          if (status/=0) stop "no input file. Simulation terminates."  
          close(unit=667)
          write(*,*)'reading seperate input .nc file' 
          do while (j > 0)
              nc_Subfile = nc_file(1:i-1)//'('//trim(num2str(j))//').nc' 
              open(unit=667, file=trim(nc_Subfile),status='old',action='read',iostat=status)
              if (status/=0) EXIT
              close(unit=667)
              call buildNCTableSub(trim(nc_Subfile), nc_file, np, gdp, doMasterRead)
              j = j + 1
          enddo
      else
          close(unit=666)
          write(*,*)'reading single input .nc file'
!write(*,*) "debug ==== pncTableHierarchy before buildNCTableSub"          
          call buildNCTableSub(nc_file, nc_file, np, gdp, doMasterRead)
!write(*,*) "debug ==== pncTableHierarchy after buildNCTableSub", nc_file          
          
          if (.not. doMasterRead) then
              call MPI_Barrier(MPI_COMM_WORLD, status) 
          endif
      endif    

    end subroutine
    
    subroutine buildNCTableSub(nc_file, svName, np, gdp, doMasterRead)
      ! who actually reads .nc
      ! read a workspace saved in nc_file into (gd_base .G. nc_file)
      implicit none
      integer, parameter :: cLen = 100
      integer*4 num, nbase
      character*(cLen) names
      integer   i, status, np, myrank
      integer ntemp,j
      character*(*) nc_file, svName
      character*(cLen), dimension(10) :: skipVar
      integer nSkipVar, skip, nL
      integer :: ncid, varid, dimid, numVar
      type(gd) :: gdp
      logical :: doMasterRead
      integer :: masterproc = 0


      nSkipVar = 1
      skipVar(1) = 'Prob'
      
      call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
      
      if (doMasterRead) then
          if (myrank .ne. masterproc) then
              return
          endif
      endif

      status = nf90_open(nc_file, NF90_NOWRITE, ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_open"
      end if

      status = nf90_inquire(ncid, nvariables = numVar)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_inquire"
      end if

      ! Get variable name
      !do i=1,numVar
      !    status = nf90_inquire_variable(ncid,i,name = names(i))
      !    if (status /= nf90_noerr) then 
      !        print *, trim(nf90_strerror(status))
      !        stop "Stopped in nf90_inquire_variable"
      !    end if
      !enddo
      
      ! XY: sub doesn't involve in gd_base
      !nbase = 10
      !if (.not. associated(gd_base%p)) call allocate_gd(gd_base,nbase)
      !gd_base%name(1) = 'base'
      !np = gd_base%np + 1   
      !gd_base%f(np) = nc_file
      !gd_base%np    = np
      !
      !if (.not. associated(gd_base%g)) then
      !   allocate(gd_base%g(nbase))
      !endif
      !call allocate_gd(gd_base%g(np),nG)
      !gd_base%g(np)%name(1) = nc_file
      
      do i=1,numVar
         status = nf90_inquire_variable(ncid,i,name = names)
         if (status /= nf90_noerr) then 
             print *, trim(nf90_strerror(status))
             stop "Stopped in nf90_inquire_variable"
         end if
         skip = 0
         do j=1, nSkipVar
           if (names .eq. skipVar(j)) skip=1
         enddo
         if (skip .ne. 1) then
             call LayerIdentify(names, nL)
             call connectNCField(gdp%g(np),i,names,ncid,nL,doMasterRead,svName) 
         endif
      enddo
      
      status = nf90_close(ncid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
      
    end subroutine
    
    
    
    
    recursive subroutine connectNCField(main,dat,field,ncid,nL,doMasterRead,gdname)
      ! add "field" to the gd table "main" by connecting to "dat"
      ! so that main%f(np+1)=field and main%p(np+1)%ptr => dat (unwrapped from mx)
      ! XY: now dat = varid
      implicit none
      type(gd),target:: main
      integer m,n,j
      character(*),intent(in)::field
      character(*),intent(in),optional::gdname
      integer :: dat, ncid, nL
      logical :: doMasterRead
      
      integer*4 fieldnum,nfields,nallo
      integer*4 i
      integer :: ndims, status, varid, xtype
      integer :: dimids(NF90_MAX_VAR_DIMS)
      logical :: islogic, isreal, isint, ischar
      
      real*4, pointer :: data_S3(:,:,:) => null() 
      real*8, pointer :: data_R1(:) => null()
      real*8, pointer :: data_R2(:,:) => null()
      real*8, pointer :: data_R3(:,:,:) => null()
      real*8, pointer :: data_R4(:,:,:,:) => null()
      real*8, pointer :: data_R5(:,:,:,:,:) => null()
      real*8, pointer :: data_R6(:,:,:,:,:,:) => null()
      integer, pointer :: data_I1(:) => null()
      integer, pointer :: data_I2(:,:) => null()
      integer, pointer :: data_I3(:,:,:) => null()
      integer, pointer :: data_I4(:,:,:,:) => null()     
      integer, pointer :: data_I5(:,:,:,:,:) => null()
      integer, pointer :: data_I6(:,:,:,:,:,:) => null()    
      
      integer di(6), ns(6), flag_2D, ind_X, myrank, flagX, flagY
      integer numel
      integer :: dims(6) = (/0, 0, 0, 0, 0, 0/)
      integer indD, indB1, indB2, Seq, nfield 
      character(maxStrLen) :: subname, mdName, mdName2, mdSeq
      character*(maxStrLen), pointer :: namestr=>null()
     
      integer, pointer :: startInd(:)=>null(), countInd(:)=>null() 
      
      if (dat .eq. 0) return ! empty!!

      varid = dat
      isreal  = .false.
      islogic = .false.
      isint   = .false.
      ischar  = .false.
     
      
      ! Hierarchy judgement
      indD = index(field,'.')
      if (indD > 0) then! there is hierarchy
          mdname = field(1:indD-1)
          subname = field(indD+1:)
        
          allocate(namestr)
          if (present(gdname)) then
              namestr = trim(gdname) // trim('.')// trim(mdname)
          else
              namestr = trim(main%name(1)) // trim('.')// trim(mdname)
          endif 
          
          ! structure array judgement
          indB1 = index(mdname,'(')
          Seq = 1
          if (indB1 > 0) then
              indB2 = index(mdname,')')
              mdname2 = mdname(1:indB1-1)
              mdSeq = mdname(indB1+1:indB2-1)
              Seq = str2num(trim(mdSeq))
              mdname = mdname2
          end if
          
          ! search if it exists
          call sp_brackets(mdname, nfield) ! xxxx[y], extract y here (number of sub-structure)
          i = searchStr(main%f,mdname,main%np,Seq)
          if (i<=0) then ! create it
              if (.not. associated(main%g)) then
                  !allocate(main%g(nsG))
                  allocate(main%g(nfield))
              endif
              n = main%np + 1
              !call allocate_gd(main%g(n))
              call allocate_gd(main%g(n),nfield)  

              main%g(n)%name(1) = namestr
              main%f(n) = mdname
              main%np = n
          else ! point to it
              n = i
          endif
          deallocate(namestr)
          call connectNCField(main%g(n), dat, subname, ncid, nL, doMasterRead)    
      else
          status = nf90_inquire_variable(ncid, varid, xtype=xtype, ndims=ndims, dimids=dimids)
          do i = 1, ndims
             status = nf90_inquire_dimension(ncid, dimids(i), len=dims(i))
          end do

          call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
 
          numel = 1; ns = 0
          flag_2D = 0; ind_X = 0 ! flag for decomposition - pnetcdf
          flagY = 0; flagX = 0
          !do i=1,size(pci%YDim) ! XY: replaced by introducing nL as input
          
           DO j=1,ndims
            numel = numel*dims(j)
            !if (dims(j) > 1) ns(j)=1
             if (dims(j)==pci%YDim(nL) .and. flagY < 1) then
               flag_2D = flag_2D + 1
               flagY = 2
               cycle
             endif
             if (dims(j)==pci%XDim(nL) .and. flagX < 1) then
               flag_2D = flag_2D + 1 
               ind_X = j
               flagX = 2
               cycle
             endif
           ENDDO
           !if (flag_2D > 1) then
           !    nL = i
           !    exit
           !else
           !    flagY = 0; flagX = 0; flag_2D = 0;
           !endif
          !enddo
      
          di = dims
          if (numel .eq. 0) return ! empty!
          
          allocate(startInd(ndims))
          allocate(countInd(ndims))
          startInd = 1
          do j=1,ndims
             countInd(j) = dims(j)
          enddo
          if (flag_2D > 1) then ! 2D flag hit   
             !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
             startInd(ind_X) = pci%Range(nL)%X(1,myrank+1)
             countInd(ind_X) = pci%Range(nL)%X(2,myrank+1) - pci%Range(nL)%X(1,myrank+1) + 1
             !if(myrank==0)write(*,*) flag_2D, field
             !write(*,*) myrank, startInd, countInd, trim(field)
          end if  
 
          ! datatype and get data
          if ((xtype .eq. NF90_DOUBLE) .or. (xtype .eq. NF90_FLOAT)) then
              isreal = .true.
              n = main%np + 1              
              select case (ndims)
              case (1)
                  allocate(data_R1(countInd(1)))
                  status = nf90_get_var(ncid, varid, data_R1, start=startInd, count=countInd)              
                  if (status /= nf90_noerr) then                   
                      print *, trim(nf90_strerror(status))                  
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR1(numel,data_R1,main,n)
              case (2)
                  allocate(data_R2(countInd(1),countInd(2)))
                  status = nf90_get_var(ncid, varid, data_R2, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR2(countInd,data_R2,main,n)
              case (3)
                  if ((xtype .eq. NF90_FLOAT)) then
!write(*,*) 'S3', varid
                      allocate(data_S3(countInd(1),countInd(2),countInd(3)))
                      status = nf90_get_var(ncid, varid, data_S3, start=startInd, count=countInd)
                      if (status /= nf90_noerr) then 
                          print *, trim(nf90_strerror(status))
                          stop "Stopped in nf90_get_var"
                      end if
                      call assignS3(countInd,data_S3,main,n)
                  else
!write(*,*) 'R3', varid                      
                      allocate(data_R3(countInd(1),countInd(2),countInd(3)))
                      status = nf90_get_var(ncid, varid, data_R3, start=startInd, count=countInd)
                      if (status /= nf90_noerr) then 
                          print *, trim(nf90_strerror(status))
                          stop "Stopped in nf90_get_var"
                      end if
                      call assignR3(countInd,data_R3,main,n)
                  endif
              case (4)
                  allocate(data_R4(countInd(1),countInd(2),countInd(3),countInd(4)))
                  status = nf90_get_var(ncid, varid, data_R4, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR4(countInd,data_R4,main,n)
              case (5)
                  allocate(data_R5(countInd(1),countInd(2),countInd(3),countInd(4),countInd(5)))
                  status = nf90_get_var(ncid, varid, data_R5, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR5(countInd,data_R5,main,n)
              case (6)
                  allocate(data_R6(countInd(1),countInd(2),countInd(3),countInd(4),countInd(5),countInd(6)))
                  status = nf90_get_var(ncid, varid, data_R6, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR6(countInd,data_R6,main,n)  
              end select
              main%f(n) = field
              main%np = n
          elseif ((xtype .eq. NF90_SHORT) .or. (xtype .eq. NF90_INT) .or.  (xtype .eq. NF90_BYTE)) then
              isint = .true.
              n = main%np + 1
              select case (ndims)
              case (1)
                  allocate(data_I1(countInd(1)))
                  status = nf90_get_var(ncid, varid, data_I1, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI1(numel,data_I1,main,n)
              case (2)
                  allocate(data_I2(countInd(1),countInd(2)))
                  status = nf90_get_var(ncid, varid, data_I2, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI2(countInd,data_I2,main,n)
              case (3)
                  allocate(data_I3(countInd(1),countInd(2),countInd(3)))
                  status = nf90_get_var(ncid, varid, data_I3, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI3(countInd,data_I3,main,n)
              case (4)
                  allocate(data_I4(countInd(1),countInd(2),countInd(3),countInd(4)))
                  status = nf90_get_var(ncid, varid, data_I4, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI4(countInd,data_I4,main,n)
              case (5)
                  allocate(data_I5(countInd(1),countInd(2),countInd(3),countInd(4),countInd(5)))
                  status = nf90_get_var(ncid, varid, data_I5, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI5(countInd,data_I5,main,n)
              case (6)
                  allocate(data_I6(countInd(1),countInd(2),countInd(3),countInd(4),countInd(5),countInd(6)))
                  status = nf90_get_var(ncid, varid, data_I6, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI6(countInd,data_I6,main,n)  
              end select
              main%f(n) = field
              main%np = n
          elseif (xtype .eq. NF90_CHAR) then
              ischar = .true.
              n = main%np + 1
              allocate(main%p(n)%c)
              status = nf90_get_var(ncid, varid, main%p(n)%c, start=(/1/), count=(/1/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              main%f(n) = field
              main%np = n
              ! temporary char data only 1 digit.
          end if
   
          deallocate(startInd)
          deallocate(countInd)
             !if(myrank==0)write(*,*) flag_2D, trim(field)
             !write(*,*) flag_2D, trim(field)
          !if (.not. doMasterRead) then
          !    call MPI_Barrier(MPI_COMM_WORLD, status) 
          !endif
      endif ! hierarchy judgement
      
    end subroutine
    
    subroutine LayerIdentify(gdname, nL)
       ! return the layer where the structure belongs to
       ! now only for 'g'
      implicit none   
      character(*),intent(in)::gdname
      character(maxStrLen) :: mdSeq, mdName
      integer :: nL, nt
      integer indD, indB1, indB2, Seq
      
      indD = index(gdname,'.')
      nL = 1
      if (indD < 0) then ! non-structure, then defaultly 1
          return
      else          
          indB1 = index(gdname,'(')
          if (indB1 > 0) then
              indB2 = index(gdname,')')
              mdSeq = gdname(indB1+1:indB2-1)
              mdName = gdname(1:indB1-1)
              Seq = str2num(trim(mdSeq))
          else
              return
          end if
          if (indB1 > indD) then ! not 'g().', but 'xx.yy()'
              return
          else
              call sp_brackets(mdname, nt) 
              if (trim(mdName)=='g') then
                  nL = Seq
              endif
          endif
      endif
      
    end subroutine

    subroutine sp_brackets(gdname, nL)
    implicit none
    character(*),intent(inout)::gdname
    character(maxStrLen):: mdSeq, mdName
    integer :: nL
    integer indB1, indB2, Seq
    
         indB1 = index(gdname,'[')
          if (indB1 > 0) then
              indB2 = index(gdname,']')
              mdSeq = gdname(indB1+1:indB2-1)
              mdName = gdname(1:indB1-1)
              Seq = str2num(trim(mdSeq))
              gdname=mdName
              nL=Seq
          else
              nL = 0
          end if
          
    end subroutine
    
END MODULE pncTableHier
