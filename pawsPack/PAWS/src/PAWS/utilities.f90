module exportASCII
    ! a simple ASCII exporter
    use vdata, Only: oneD_ptr
    integer :: matUnit=5871, tsUnit=5872
    integer, parameter :: nM = 4
    public:: writeDimMat

      interface writeDimMat         ! call getPtr(main,field,ptr,[num])
       !writeDimMat(dat,name,time)
       !dat: 1D-4D matrices. 
       !name: a string denoting the name of the variable. could be sth like 'g.OVN.h'; 
       !time: a timestamp for the variable
      module procedure writeDimMat1,writeDimMat2,writeDimMat3,writeDimMat4
      end interface
      
    contains
    
    subroutine writeDimMat1m(dat,name,t,m)
    use vdata, Only: num2str
    real*8 dat(:)
    real*8 :: t
    integer m(nM)
    character*(*) :: name
    integer nr,nc
    logical ok
    character*32 :: fmt
    nr = size(dat)
    inquire(unit = matUnit, OPENED = ok)
    if (.not. ok) open(unit = matUnit, file='matUnit.dat', status='unknown')
    fmt = trim('('// trim(num2str(nr))//'e13.5)')
    write(matUnit,'(A1,A32,e13.5,5(i5))') '#',name,t,m
    !write(matUnit, '(<nr>e23.16)') dat ! (dat(i),i=1,nr)
    write(matUnit, fmt) dat 
    end subroutine
    
    subroutine writeDimMat1(dat,name,t)
    real*8,target :: dat(:)
    real*8,pointer:: dat_p(:)
    real*8, optional :: t
    character*(*) :: name
    integer :: m(nM)=1
    m(1) = size(dat)
    call writeDimMat1m(dat,name,t,m)
    end subroutine
    
    subroutine writeDimMat2(dat,name,t)
    real*8,target :: dat(:,:)
    real*8,pointer:: dat_p(:)
    real*8, optional :: t
    character*(*) :: name
    integer :: m(nM)=1
    integer :: nd=2,i
    do i=1,nd
        m(i)=size(dat,i)
    enddo    
    dat_p => oneD_ptr(size(dat),dat)
    call writeDimMat1m(dat_p,name,t,m)
    end subroutine
    
    subroutine writeDimMat3(dat,name,t)
    real*8,target :: dat(:,:,:)
    real*8,pointer:: dat_p(:)
    real*8, optional :: t
    character*(*) :: name
    integer :: m(nM)=1
    integer :: nd=3,i
    do i=1,nd
        m(i)=size(dat,i)
    enddo    
    dat_p => oneD_ptr(size(dat),dat)
    call writeDimMat1m(dat_p,name,t,m)
    end subroutine
    
    subroutine writeDimMat4(dat,name,t)
    real*8,target :: dat(:,:,:,:)
    real*8,pointer:: dat_p(:)
    real*8, optional :: t
    character*(*) :: name
    integer :: m(nM)=1
    integer :: nd=4,i
    do i=1,nd
        m(i)=size(dat,i)
    enddo    
    dat_p => oneD_ptr(size(dat),dat)
    call writeDimMat1m(dat_p,name,t,m)
    end subroutine
     
    end module
      
!====================================================================================
      subroutine find1DL(array,m,filter,n)
      ! this is an emulation of matlab find function
      ! this function finds the 1D indices of 'array' where value is > 0
      ! returns a pointer allocated with indices
      implicit none
      logical array(m)
      integer i,k,m,n
      integer filter(m)
      
      k = 0
      do i = 1,m
        if (array(i)) then
          k = k + 1
          filter(k) = i
        endif
      enddo
      n = k

      END subroutine
      
      
      function digit(n)
      integer n, digit,i
      if (abs(n) < 10) then
        digit = 0;
      else
        do i=1,100
          if (abs(n) < 10**(I+1) .and. abs(n) >= 10**(I) ) then
            digit = I
            EXIT
          endif
        enddo
      endif
      end function
!====================================================================================
      
      function nfileLines(file)
      character*(*) file
      integer :: uid = 1000081
      integer :: nfileLines, eof
      
      nFile = 0
      OPEN(unit = uid, file = trim(file), ACTION='READ',IOSTAT = eof )
      if (eof .eq. 0) then
          do while (eof .eq. 0)
             read(uid,*,iostat = eof) 
             if (eof .eq. 0) nFile = nFile + 1
          enddo
          close(uid)
      endif
      nfileLines = nFile
      end function
!====================================================================================
    module polyEval
    private
    public:: polyIntVal,polyIntWeights,gauss,polyIntIntpInvMat,polyInterp1,linearInterp1d
    public:: polyInterp3
    
      interface polyIntAvgW3
      module procedure polyIntAvgW3double,polyIntAvgW3single
      end interface
      
      interface polyInterp3
      module procedure polyInterp3double,polyInterp3single
      end interface
    contains
    
    subroutine linearInterp1d(x,y,x1,y1,loc,lastLoc)
    real*8, optional :: lastLoc ! good for a loop of calls with monotonically ascending search position
    real*8, intent(in) :: x(:), y(:), x1
    real*8, intent(out) :: y1, loc
    real*8 d, d1
    integer n, i,j, k, i1
    n = size(x)
    if (present(lastLoc)) then
        i1 = max(int(lastLoc),1)
    else
        i1 = 1
    endif
    if (x1<x(1)) then
        y1=y(1)
        return
    elseif (x1>x(n)) then
        y1=y(n)
        return
    else
      do i=i1, n-1
        if (x(i)<=x1 .and. x(i+1)>x1) then
           d = x(i+1)-x(i)
           d1= x1 - x(i)
           loc = d1/d + i
           y1= y(i)+ (loc-int(loc))*(y(i+1)-y(i))
           exit
        endif
      enddo
    endif  
    end subroutine
    
    subroutine polyIntWeights(n,stencil, mode, A)
    implicit none
      integer n,i,j,k,mode
      real*8 stencil(n+1)
      real*8 A(n,n),coeff(n)
      
      
      do i =1,n
        do j=1,n
            A(i,j) = (stencil(i+1)**(n+1D00-j) - stencil(i)**(n+1D0-j) )/((n+1D0-j)*(stencil(i+1)-stencil(i)))
        enddo    
      enddo
    end subroutine
    
    subroutine polyInterp1(m,t,v,t1,res,mode,n,MX,state,stencilIn)
    ! Look at PAWS documentation, Appendix A2.1 for maths
    ! this subroutine takes existing data (t & v), assuming they mean cell averages, and do interpolation to other intervals
    ! notice cell average is not the integral. It is the integral divided by cell interval
    ! mode suggests what t means. mode ==0, each time stamp means the interval forward, i.e., t(1)=datenum2(20020101), means v(1) is for 20020101
    ! mode==0 works for USGS interpolation and climate forcing interpolation, because those values mean the average value in the interval beginning from the timestamp
    ! mode==1 is centered time stamp, if t(1)=datenum2(20020101), it means v(1) is for 20011231 12pm to 20020101 11:59am
    ! MX (matched to stencil)  is the weights of the inverse matrix to be stored, it can be used without recomputing. 
    ! n is the order of the polynomial
    ! a stencil means how the polynomial will be constructed, treating a base cell center as 0
    ! the default n=2 stencil puts interval starting position to be in the 0-th stencil
    ! if not desired, you can put in stencilIn. Just need to match with MX
    ! stencil can be fixed regardless of bounds, as long as relative indexing is correct
    ! you can pass in a number of 
    ! assuming uniform time step of t
    implicit none
    integer m,mode,n,state
    integer k0,kn,k1,k2,i,j
    real*8, dimension(m):: t,v
    real*8 t1(2) ! interpolate to an interval. if only to a point, the second value can be whatever
    real*8,dimension(4),target:: stencilF3=(/-1D0, 0D0, 1D0, 2D0 /),stencilC3=(/-1.5D0, -0.5D0, 0.5D0, 1.5D0 /)
    real*8,dimension(3),target:: stencilF2=(/0D0, 1D0, 2D0 /),stencilCF2=(/-0.5D0, 0.5D0, 1.5D0 /),stencilC2=(/-1D0, 0D0, 1D0 /)
    real*8,dimension(:),pointer::stencil
    real*8,dimension(:),target,optional::stencilIn  
    real*8 :: MX(n,n), res
    real*8 :: interval(2),vp(10),P(n)
    real*8 :: PREC=1D-13,dt,frac,fracI
    integer :: internal_state = 0
    real*8,dimension(:,:),pointer :: MX0
    real*8 :: dStencil
    
    integer,parameter:: i1=3
    integer,parameter:: i2=4 ! i1--highest order; i2 -- max # of stencils. i2=i1+1
    real*8, target:: Stencils(i1,i2,i2), Ks(3)
    real*8, target :: MX_Save(i1,i1,i1,i2) ! 3rd i1 --highest order, i2-- max # of stencils (also max stencil size)
    integer,dimension(3) :: cPos = (/1, 2, 3/)
    integer cP
    
    select case(mode)
    case (0) !
         dStencil = 1D0
    case (1)
        dStencil = 0.5D0
    case (2)
        dStencil = 1D0    
    end select
    
    if (internal_state .eq. 0) then
        call polyIntpInit(MX_SAVE,Stencils)
        internal_state = 1
        ! initiate a bunch of weights. select MX_Save(1:n,1:n,n,j)
        ! for j, prefer the one where the base cell is nearest to the interpolation interval, i.e., central
        ! approximation with a forward bias
        ! however, when this stencil goes out of bound, need to shift
    endif
    
    !if (state .eq. 0) then
    !    call polyIntIntpInvMat(n,stencil,MX)
    !    state = 1
    !endif
    
    dt = t(2)-t(1)
    Ks = boundStencil(n,t,t1,Stencils,cPos,dStencil)
    k0=Ks(1); K1=Ks(2); K2=Ks(3)
    
    if (k0>0) then
        stencil => Stencils(n,:,k0)
        MX0 => MX_Save(1:n,1:n,n,k0)
    else
        call ch_assert('polyInterp1:: not expected interval')
    endif
    
    ! positions are all relative
    ! as long as k1 is correct, and interval is not far from the stencil. this works  
    ! only one stencil is ever needed. it only matters where you put the base relative to the new interval
    ! ensure stencil fits.
    k1 = max(1-floor(stencil(1)),k1) 
    k1 = min(size(t)-ceiling(stencil(size(stencil))-PREC),k1)
          
    interval(1) = max(t1(1) - t(K1),0D0)/dt
    interval(2) = max(t1(2) - t(K1),0D0)/dt
    do i=1,size(stencil)
        vp(i) = v(min(max(1,k1+int(stencil(i))),size(v)))
    enddo
    res = polyIntAvgW(n,MX0,vp(1:n),interval)
    
    end subroutine
    
    
    subroutine polyInterp3double(m,tIn,v,t1,datOut,mode,n)
    ! Look at PAWS documentation, Appendix A2.1 for maths
    ! same as polyInterp1, but works for 3D data, 3rd dim is time and is interpolated
    ! tIn is either the time series itself, with size m, or it is [ts, te, dt] but m is 0. 
    !    where te is the ending bounding, i.e., t(end)+dt
    implicit none
    integer m,mode,n,state
    integer k0,kn,k1,k2,i,j
    real*8, dimension(:),target:: tIn
    real*8, dimension(:), pointer:: t
    real*8 t1(2) ! interpolate to an interval. if only to a point, the second value can be whatever
    real*8,dimension(:),pointer::stencil
    real*8,target :: MX(n,n)
    real*8,target :: v(:,:,:), datOut(:,:)
    real*8 :: interval(2),P(n)
    real*8, dimension(:,:,:),pointer:: vp
    real*8 :: PREC=1D-13,dt
    integer,save :: internal_state = 0
    real*8,dimension(:,:),pointer :: MX0
    real*8 :: dStencil
    
    integer,parameter:: i1=3
    integer,parameter:: i2=4 ! i1--highest order; i2 -- max # of stencils. i2=i1+1
    real*8, target:: Stencils(i1,i2,i2), Ks(3)
    real*8, target :: MX_Save(i1,i1,i1,i2) ! 3rd i1 --highest order, i2-- max # of stencils (also max stencil size)
    integer,dimension(3) :: cPos = (/1, 2, 3/)
    integer cP, dP, nS
    logical tAllocated
    
    tAllocated = .false.
    if (size(tIn) .eq. 3 .AND. m .eq. 0) then
    ! what is passed is [ts,te,dt]
        tAllocated = .true.
        nS = (tIn(2)-tIn(1))/tIn(3) ! no +1 because tE is the ending bound
        allocate(t(nS))
        do i=1,ns
            t(i) = tIn(1)+tIn(3)*(i-1)
        enddo
    else
        t=> tIn
    endif
    
    select case(mode)
    case (0) !
         dStencil = 1D0
    case (1)
        dStencil = 0.5D0
    case (2)
        dStencil = 1D0    
    end select
    
    if (internal_state .eq. 0) then
        call polyIntpInit(MX_SAVE,Stencils)
        internal_state = 1
        ! initiate a bunch of weights. select MX_Save(1:n,1:n,n,j)
        ! for j, prefer the one where the base cell is nearest to the interpolation interval, i.e., central
        ! approximation with a forward bias
        ! however, when this stencil goes out of bound, need to shift
    endif
    
    dt = t(2)-t(1)
    Ks = boundStencil(n,t,t1,Stencils,cPos,dStencil)
    k0=Ks(1); K1=Ks(2); K2=Ks(3)
    
    if (k0>0) then
        stencil => Stencils(n,:,k0)
        MX0 => MX_Save(1:n,1:n,n,k0)
    else
        call ch_assert('polyInterp3double:: not expected interval')
    endif
    
    ! positions are all relative
    ! as long as k1 is correct, and interval is not far from the stencil. this works  
    ! only one stencil is ever needed. it only matters where you put the base relative to the new interval
    ! ensure stencil fits.
    k1 = max(1-floor(stencil(1)),k1) 
    k1 = min(size(t)-ceiling(stencil(size(stencil))-PREC),k1)
          
    interval(1) = max(t1(1) - t(K1),0D0)/dt
    interval(2) = max(t1(2) - t(K1),0D0)/dt
    dP = min(max(1,k1+int(stencil(1))),size(v,3))
    nS = stencil(size(stencil)) - stencil(1)
    vp => v(:,:,dP:(dP+nS))
    ! assuming stencil is consecutive, for efficiency
    
    !do i=1,size(stencil)
    !    vp(i) = v()
    !enddo
    !res = polyIntAvgW(n,MX0,vp(1:n),interval)
    call polyIntAvgW3(n,MX0,vp,interval,datOut)
    
    if (tAllocated) deallocate(t)
    
    end subroutine
    
    
    
    subroutine polyInterp3single(m,tIn,v,t1,datOut,mode,n)
    ! Look at PAWS documentation, Appendix A2.1 for maths
    ! same as polyInterp1, but works for 3D data, 3rd dim is time and is interpolated
    ! tIn is either the time series itself, with size m, or it is [ts, te, dt] but m is 0. 
    !    where te is the ending bounding, i.e., t(end)+dt
    implicit none
    integer m,mode,n,state
    integer k0,kn,k1,k2,i,j
    real*8, dimension(:),target:: tIn
    real*8, dimension(:), pointer:: t
    real*8 t1(2) ! interpolate to an interval. if only to a point, the second value can be whatever
    real*8,dimension(:),pointer::stencil
    real*8,target :: MX(n,n)
    real*4,target :: v(:,:,:)             ! difference from double version 
    real*4, dimension(:,:,:),pointer:: vp ! difference from double version
    real*8,target :: datOut(:,:)
    real*8 :: interval(2),P(n)
    real*8 :: PREC=1D-13,dt
    integer,save :: internal_state = 0
    real*8,dimension(:,:),pointer :: MX0
    real*8 :: dStencil
    
    integer,parameter:: i1=3
    integer,parameter:: i2=4 ! i1--highest order; i2 -- max # of stencils. i2=i1+1
    real*8, target:: Stencils(i1,i2,i2), Ks(3)
    real*8, target :: MX_Save(i1,i1,i1,i2) ! 3rd i1 --highest order, i2-- max # of stencils (also max stencil size)
    integer,dimension(3) :: cPos = (/1, 2, 3/)
    integer cP, dP, nS
    logical tAllocated
    
    tAllocated = .false.
    if (size(tIn) .eq. 3 .AND. m .eq. 0) then
    ! what is passed is [ts,te,dt]
        tAllocated = .true.
        nS = (tIn(2)-tIn(1))/tIn(3) ! no +1 because tE is the ending bound
        allocate(t(nS))
        do i=1,ns
            t(i) = tIn(1)+tIn(3)*(i-1)
        enddo
    else
        t=> tIn
    endif
    
    select case(mode)
    case (0) !
         dStencil = 1D0
    case (1)
        dStencil = 0.5D0
    case (2)
        dStencil = 1D0    
    end select
    
    if (internal_state .eq. 0) then
        call polyIntpInit(MX_SAVE,Stencils)
        internal_state = 1
        ! initiate a bunch of weights. select MX_Save(1:n,1:n,n,j)
        ! for j, prefer the one where the base cell is nearest to the interpolation interval, i.e., central
        ! approximation with a forward bias
        ! however, when this stencil goes out of bound, need to shift
    endif
    
    dt = t(2)-t(1)
    Ks = boundStencil(n,t,t1,Stencils,cPos,dStencil)
    k0=Ks(1); K1=Ks(2); K2=Ks(3)
    
    if (k0>0) then
        stencil => Stencils(n,:,k0)
        MX0 => MX_Save(1:n,1:n,n,k0)
    else
        call ch_assert('polyInterp3single:: not expected interval')
    endif
    
    ! positions are all relative
    ! as long as k1 is correct, and interval is not far from the stencil. this works  
    ! only one stencil is ever needed. it only matters where you put the base relative to the new interval
    ! ensure stencil fits.
    k1 = max(1-floor(stencil(1)),k1) 
    k1 = min(size(t)-ceiling(stencil(size(stencil))-PREC),k1)
          
    interval(1) = max(t1(1) - t(K1),0D0)/dt
    interval(2) = max(t1(2) - t(K1),0D0)/dt
    dP = min(max(1,k1+int(stencil(1))),size(v,3))
    nS = stencil(size(stencil)) - stencil(1)
    vp => v(:,:,dP:(dP+nS))
    ! assuming stencil is consecutive, for efficiency
    
    !do i=1,size(stencil)
    !    vp(i) = v()
    !enddo
    !res = polyIntAvgW(n,MX0,vp(1:n),interval)
    call polyIntAvgW3(n,MX0,vp,interval,datOut)
    
    if (tAllocated) deallocate(t)
    
    end subroutine
    
    function boundStencil(n,t,t1,Stencils,cPos,dStencil)
    implicit none
    real*8, dimension(:) :: t
    real*8, dimension(2) :: t1
    real*8, dimension(:,:,:) :: Stencils
    real*8 dStencil
    integer, dimension(:) :: cPos
    integer :: boundStencil(3) ! [k0,k1,k2]
    integer k0, k1, k2, n, cp, j
    real*8 dt, frac, fraci
    real*8 :: prec=1D-12
    
    
    dt = t(2)-t(1)
    !if (mode .eq. 0) then ! each value means integral of the forward interval. interpolate to an interval
    frac = (t1(1) + PREC - t(1))/dt
      
    k0 = 0
    cp = cPos(n) ! the index of the stencil which is a default central difference
    if (frac>=ceiling(n/2D0) .AND. frac <= size(t)-ceiling(n/2D0)) then
        k0 = cp
    elseif (frac < ceiling(n/2D0)) then
        ! left-end extend out
        ! walk right to find the stencil that just came inside
        ! the most negative stencil is -int(frac)
        do j=cp,n+1
            if (Stencils(n,1,j)>=-int(frac)) then
                k0= j;
                exit
            endif
        enddo
    elseif (frac >= size(t)-ceiling(n/2D0)) then
        ! right-end may extend out
        ! walk left to find the stencil that 
        fracI = (t(size(t))+dt*dStencil-t1(2)+ PREC)/dt
        ! the most positive stencil is int(fracI)
        do j=cp,1,-1
            if (Stencils(n,n+1,j)<=int(fracI)) then
                k0 = j;
                exit
            endif  
        enddo
    endif
        
          
    frac = (t1(1) + PREC - t(1))/dt
    k1 = int((t1(1) + PREC - t(1))/dt ) + 1 ! base. this setup means x is just pass stencil_0
    k2 = int((t1(2) + PREC - t(1))/dt ) + 1 !
    if (k2-k1> n+1) then
       write(*,*) 'polyInterp1:: this portion not coded yet'
       stop
    endif 
    
    boundStencil = (/k0,k1,k2/); 
    end function
    
    subroutine polyIntpInit(MX,Stencils)
    ! pre-compute MX for different orders and shifts
    implicit none
    integer n
    integer,parameter:: i1=3
    integer,parameter:: i2=4 ! i1--highest order; i2 -- max # of stencils. i2=i1+1
    real*8 interval(2)
    real*8 Stencils(i1,i2,i2)
    real*8 MX(i1,i1,i1,i2) ! 3rd i1 --highest order, i2-- max # of stencils (also max stencil size)
    
    integer i,j,k
    
    Stencils(2,1:3,1) = (/-2D0, -1D0, 0D0/)
    Stencils(2,1:3,2) = (/-1D0, 0D0, 1D0/)
    Stencils(2,1:3,3) = (/0D0, 1D0, 2D0/)
    
    Stencils(3,1:4,1) = (/-3D0,-2D0, -1D0, 0D0 /)
    Stencils(3,1:4,2) = (/-2D0,-1D0, 0D0, 1D0 /)
    Stencils(3,1:4,3) = (/-1D0, 0D0, 1D0, 2D0 /)
    Stencils(3,1:4,4) = (/ 0D0, 1D0, 2D0, 3D0 /)
    
    do n=2,i1
        do i=1,i2
            call polyIntIntpInvMat(n,Stencils(n,1:n+1,i),MX(1:n,1:n,n,i))
        enddo
    enddo
    
    end subroutine
    
    
    function polyIntVal(n,P,interval)
    ! integrate polynomial P of (n-1)-th order (so n coefficients) in interval
    implicit none
    integer n
    real*8 P(n)
    real*8 interval(2)
    real*8 polyIntVal
    
    integer i,j,k
    polyIntVal = 0D0
    do j=1,n
        polyIntVal = polyIntVal+P(j)*(interval(2)**(n+1-j) - interval(1)**(n+1-j) )/(n+1-j)
    enddo    
    end function
    
    
    function polyIntAvg(n,P,interval)
    ! integrate polynomial P of (n-1)-th order (so n coefficients) in interval
    implicit none
    integer n
    real*8 P(n)
    real*8 interval(2)
    real*8 polyIntVal, polyIntAvg
    
    integer i,j,k
    polyIntVal = 0D0
    do j=1,n
        polyIntVal = polyIntVal+P(j)*(interval(2)**(n+1-j) - interval(1)**(n+1-j) )/(n+1-j)
    enddo    
    polyIntAvg = polyIntVal/(interval(2)-interval(1))
    end function
    
    
    function polyIntAvgW(n,MX,v,interval)
    ! integrate polynomial P of (n-1)-th order (so n coefficients) in interval
    implicit none
    integer n,j
    real*8 MX(n,n), v(n)
    real*8 interval(2)
    real*8 BA(n),B(n)
    real*8 polyIntAvgW,polyIntVal
    integer :: code=1
    
    do j=1,n ! polynomial is (n-1) order, will not have a power of n, i.e., j>=1
        B(j)=(interval(2)**(n+1-j) - interval(1)**(n+1-j) )/((n+1-j)*(interval(2)-interval(1))) 
    enddo
    
    if (code .eq. 0) then
        ! 07/25/18, CP verified code == 0, 1 or 2 gives the same correct answer.
        BA = matmul(B,MX)
        polyIntAvgW = DOT_PRODUCT(BA,v)
    elseif (code .eq. 1) then
        BA = matmul(B,MX)
        polyIntVal = 0D0
        do j=1,n
            polyIntVal = polyIntVal + BA(j)*v(j)
        enddo
        polyIntAvgW = polyIntVal
    else
        BA = matmul(MX,v)
        polyIntVal = 0D0
        do j=1,n
            polyIntVal = polyIntVal+BA(j)*B(j)
        enddo    
        polyIntAvgW = polyIntVal
    endif
    end function
    
    
    subroutine polyIntAvgW3double(n,MX,v3,interval,datOut)
    ! integrate polynomial P of (n-1)-th order (so n coefficients) in interval
    ! apply in 3D --> 3rd dim is time, and every pixel interpolating to the same time
    implicit none
    integer n,j
    real*8 MX(n,n)
    real*8,dimension(:,:,:) :: v3
    real*8,dimension(size(v3,1),size(v3,2)) :: datOut
    real*8 interval(2)
    real*8 BA(n),B(n)
    
    do j=1,n ! polynomial is (n-1) order, will not have a power of n, i.e., j>=1
        B(j)=(interval(2)**(n+1-j) - interval(1)**(n+1-j) )/((n+1-j)*(interval(2)-interval(1))) 
    enddo
    
    BA = matmul(B,MX)
    datOut = 0D0
    do j=1,n
        datOut = datOut + BA(j)*v3(:,:,j)
    enddo
    end subroutine
    
    subroutine polyIntAvgW3single(n,MX,v3,interval,datOut)
    ! integrate polynomial P of (n-1)-th order (so n coefficients) in interval
    ! apply in 3D --> 3rd dim is time, and every pixel interpolating to the same time
    implicit none
    integer n,j
    real*8 MX(n,n)
    real*4,dimension(:,:,:) :: v3
    real*8,dimension(size(v3,1),size(v3,2)) :: datOut
    real*8 interval(2)
    real*8 BA(n),B(n)
    
    do j=1,n ! polynomial is (n-1) order, will not have a power of n, i.e., j>=1
        B(j)=(interval(2)**(n+1-j) - interval(1)**(n+1-j) )/((n+1-j)*(interval(2)-interval(1))) 
    enddo
    
    BA = matmul(B,MX)
    datOut = 0D0
    do j=1,n
        datOut = datOut + BA(j)*v3(:,:,j)
    enddo
    end subroutine
    
    
    subroutine polyIntIntpInvMat(n,stencil,MX)
    ! polynomial integral interpolation inverse matrix
    ! (n-1)-th order polynomal integral interpolation
    ! given n cell averages over stencil intervals
    ! construct n-th polynomial to be evaluated in interval
    ! here the inverse matrix is passed out
    ! to obtain the polynomial, do matmul(MX,integrals)
    ! then for integral evaluation,  res =polyIntVal
    integer n,mode
    real*8:: stencil(n+1),ints(n),interval(2),res
     real*8 BB(N,N),MX(N,N),t1
    integer::INDX(N)
    !real*8, intent(out)::P(3)
    
     call polyIntWeights(n,stencil,1,BB)
     if (n > 2) then
       call MIGS(BB,3,MX,INDX)
     else
       t1 = BB(1,1)*BB(2,2)-BB(1,2)*BB(2,1)
       MX(1,:)= (/BB(2,2)/t1, -BB(1,2)/t1 /) 
       MX(2,:)= (/-BB(2,1)/t1, BB(1,1)/t1 /) 
     endif
     ![  b22/(b11*b22 - b12*b21), -b12/(b11*b22 - b12*b21)]
     ![ -b21/(b11*b22 - b12*b21),  b11/(b11*b22 - b12*b21)]
     !MIGS(A,N,X,INDX)
     !call  gauss(3,BB,b,x)
     !P = matmul(MX,b)
     !res = polyIntVal(3,x,(/0D0,1D0/))
    endsubroutine
    

!########## BELOW ARE SOME FILES OBTAINED AS LIBRARIES ###########################
!########## ORIGINAL COPYRIGHT INFO MAINTAINED ###################################
    subroutine gauss(n,a,bi,x)
    implicit none
      integer n
      real*8 a(n,n),s(n),bi(n),b(n)
      real*8 x(n)
      integer p(n), pk,i,j,k
      real*8 smax,rmax,r,z,sum
      
      b= bi
      do i=1,n
         p(i) = i
         smax = 0.0D0 
         do j=1,n
            smax = max(smax,abs(a(i,j)))
         enddo
         s(i) = smax
     enddo
    
      do k=1,n-1
         rmax = 0.0D0
         do i=k,n
            r = abs(a(p(i),k))/s(p(i))
            if (r .gt. rmax) then
               j = i
               rmax = r
            endif
         enddo
         
         pk = p(j)
         p(j) = p(k)
         p(k) = pk
         
         do i=k+1,n      
            z = a(p(i),k)/a(p(k),k)       
            a(p(i),k) = z
            do j=k+1,n    
               a(p(i),j) = a(p(i),j) - z*a(p(k),j)      
            enddo
         enddo
      enddo   
      
      do k=1,n-1
         do i=k+1,n
            b(p(i)) = b(p(i)) - a(p(i),k)*b(p(k))
         enddo
      enddo
    
      do i=n,1,-1
         sum = b(p(i))
         do j=i+1,n
            sum = sum - a(p(i),j)*x(j)
         enddo
         x(i) = sum/a(p(i),i)
      enddo

    end subroutine
    
    
    ! Two subroutines below were modified from Tao Pang's code
    ! original statements retained below
      SUBROUTINE MIGS(A,N,X,INDX)
      IMPLICIT REAL*8(a-h,o-z)
      IMPLICIT INTEGER(I-N)
      INTEGER N
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c                                                                      c
!c Please Note:                                                         c
!c                                                                      c
!c (1) This computer program is part of the book, "An Introduction to   c
!c     Computational Physics," written by Tao Pang and published and    c
!c     copyrighted by Cambridge University Press in 1997.               c
!c                                                                      c
!c (2) No warranties, express or implied, are made for this program.    c
!c                                                                      c
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      REAL*8:: A(N,N),X(N,N),B(N,N)
      integer::INDX(N),i,j,k
! Subroutine to invert matrix A(N,N) with the inverse stored
! in X(N,N) in the output.
      DO    I = 1, N
        DO     J = 1, N
          B(I,J) = 0.0D0
        ENDDO
      ENDDO
      DO I = 1, N
          B(I,I) = 1.0D0
      ENDDO
      CALL ELGS(A,N,INDX)
      DO I = 1, N-1
        DO  J = I+1, N
          DO   K = 1, N
            B(INDX(J),K) = B(INDX(J),K)-A(INDX(J),I)*B(INDX(I),K)
          ENDDO
        ENDDO
      ENDDO
      DO   I = 1, N
        X(N,I) = B(INDX(N),I)/A(INDX(N),N)
        DO  J = N-1, 1, -1
          X(J,I) = B(INDX(J),I)
          DO K = J+1, N
            X(J,I) = X(J,I)-A(INDX(J),K)*X(K,I)
          ENDDO
          X(J,I) =  X(J,I)/A(INDX(J),J)
        ENDDO
      ENDDO
      RETURN
      ENDsubroutine
      
      SUBROUTINE ELGS(A,N,INDX)
      IMPLICIT REAL*8(a-h,o-z)
      IMPLICIT INTEGER(I-N)
      INTEGER N
 !Subroutine to perform the partial-pivoting Gaussian elimination.
 !A(N,N) is the original matrix in the input and transformed
 !matrix plus the pivoting element ratios below the diagonal in
 !the output.  INDX(N) records the pivoting order.
      REAL*8:: A(N,N),C(N)
      REAL*8 C1,PI,pi1,pj
      integer K,I,j,itmp
      INTEGER:: INDX(N)
      DO I = 1, N
        INDX(I) = I
      ENDDO

  ! Find the rescaling factors, one from each row
        DO  I = 1, N
          C1= 0.0D0
          DO J = 1, N
            C1 = max(C1,ABS(A(I,J)))
          ENDDO
          C(I) = C1
        ENDDO

 !C Search the pivoting (largest) element from each column
      DO  J = 1, N-1
        PI1 = 0.0
        DO   I = J, N
          PI = ABS(A(INDX(I),J))/C(INDX(I))
          IF (PI.GT.PI1) THEN
            PI1 = PI
            K   = I
          ELSE
          ENDIF
        ENDDO
 !Interchange the rows via INDX(N) to record pivoting order

        ITMP    = INDX(J)
        INDX(J) = INDX(K)
        INDX(K) = ITMP
        DO   I = J+1, N
          PJ  = A(INDX(I),J)/A(INDX(J),J)

 !Record pivoting ratios below the diagonal

          A(INDX(I),J) = PJ

 !Modify other elements accordingly

          DO  K = J+1, N
            A(INDX(I),K) = A(INDX(I),K)-PJ*A(INDX(J),K)
          ENDDO
        ENDDO
      ENDDO
      RETURN
      ENDSubroutine
    
    end module