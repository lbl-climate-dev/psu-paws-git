MODULE pncTable
    ! parallel netcdf file interface
    ! modified form matTable
    ! *****************************************************
    ! Xinye Ji
    ! Pennsylvania State University
    ! Civil and Environmental Engineering
    ! jixinye91@gmail.com
    ! *****************************************************
    ! build an intermediate table object which stores data read from a netcdf file. 
    ! It can be used later to conveniently link the data to computational objects
    ! syntax:
    ! ####################################################
    ! SOME WHERE IN THE PROGRAM YOU MUST FIRST DO:
    ! ++ use ncTable
    ! ++ call buildpNCTable(nc_file)
    ! ####################################################
    ! If you want to change the way of decomposition for parallel I/O,
    ! you need to see and modify the subroutine connectpNCField
    ! ####################################################    
    ! after this, you can access all variable stored in the mat_file via gd_base, which is a type(gd) variable in module ncTable
    ! the general syntax is
    ! ++ call getPtr(gd_base,fieldname,ptr,num)  ! num is optional, default to 1
    ! this will link the data to the pointer, ptr, which may be a real*8,integer, logical array supported up to 6 dimension
    ! or a type(gd) array pointer.
    ! depending on what pointer is passed in, the calling interface will pick the corresponding type/dimension subroutine
    ! If the queried field is not there or if the field type/dimension does not match that of the ptr, an error is thrown.
    ! num is the index, if omitted, default to 1. type(gd) allows to access sub-structures.
    ! Overloaded operators as a fast access:
    ! Besides the calling interface getPtr, we have overloaded operator .G., .FF. and .FFF. serving as shorthand methods
    ! for extracting sub-structure, [real*8,dimension(:,:)] and [real*8,dimension(:,:,:)] data, respectively. 
    ! With these operators, num is always 1.    
    
    use gd_mod
    use netcdf
    include 'mpif.h'
    
    integer nG
    parameter (nG =10)
    
    PUBLIC :: buildpNCTable, connectpNCField
    contains
    
    subroutine buildpNCTable(nc_file,my_rank,p)
      ! read a workspace saved in nc_file into (gd_base .G. nc_file)
      implicit none
      integer, parameter :: cLen = 100
      integer*4 num, nbase
      character*(cLen) names(400), name
      integer   i, status, np
      integer ntemp,j
      character*(*) nc_file
      character*(cLen), dimension(10) :: skipVar
      integer nSkipVar, skip
      integer :: ncid, varid, dimid, numVar
      
      integer :: p, my_rank, ierr ! p: # of proc 

      nSkipVar = 1
      skipVar(1) = 'Prob'
      numVar = 1
      
      status = nf90_open(nc_file, IOR(NF90_NOWRITE, NF90_MPIIO), ncid, comm = MPI_COMM_WORLD, info = MPI_INFO_NULL)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_open"
      end if

      status = nf90_var_par_access(ncid, varid, nf90_collective)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_var_par_access"
      end if
      
      status = nf90_inquire(ncid, nvariables = numVar)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_inquire"
      end if
      !write(*,*) my_rank, numVar 
      ! Get variable name
      do i=1,numVar
          status = nf90_inquire_variable(ncid,i,name = names(i))
          if (status /= nf90_noerr) then 
              print *, trim(nf90_strerror(status))
              stop "Stopped in nf90_inquire_variable"
          end if
      enddo
      
      nbase = 10
      if (.not. associated(gd_base%p)) call allocate_gd(gd_base,nbase)
      gd_base%name(1) = 'base'
      np = gd_base%np + 1   
      gd_base%f(np) = nc_file
      gd_base%np    = np

      if (.not. associated(gd_base%g)) then
         allocate(gd_base%g(nbase))
      endif
      call allocate_gd(gd_base%g(np),numVar)
      gd_base%g(np)%name = nc_file
      call MPI_BARRIER (MPI_COMM_WORLD,ierr)
      do i=1,numVar
         skip = 0
         do j=1, nSkipVar
           if (names(i) .eq. skipVar(j)) skip=1
         enddo
         if (skip .ne. 1) call connectpNCField(gd_base%g(np),i,names(i),ncid,nc_file,my_rank,p)
      enddo
      
      status = nf90_close(ncid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
      
    end subroutine
    
    
    
    
    subroutine connectpNCField(main,dat,field,ncid,gdname,my_rank,p)
      ! add "field" to the gd table "main" by connecting to "dat"
      ! so that main%f(np+1)=field and main%p(np+1)%ptr => dat (unwrapped from mx)
      ! XY: now dat = varid
      implicit none
      type(gd),target:: main
      integer m,n,j
      character(*),intent(in)::field
      character(*),intent(in),optional::gdname
      integer :: dat, ncid
      integer :: p, my_rank, ierr 
      
      integer*4 fieldnum,nfields,nallo
      integer*4 i
      integer :: ndims, status, varid, xtype
      integer :: dimids(NF90_MAX_VAR_DIMS)
      logical :: islogic, isreal, isint, ischar
      
      real*8, pointer :: data_R1(:)
      real*8, pointer :: data_R2(:,:)
      real*8, pointer :: data_R3(:,:,:)
      real*8, pointer :: data_R4(:,:,:,:)
      real*8, pointer :: data_R5(:,:,:,:,:)
      real*8, pointer :: data_R6(:,:,:,:,:,:)
      integer, pointer :: data_I1(:)
      integer, pointer :: data_I2(:,:)
      integer, pointer :: data_I3(:,:,:)
      integer, pointer :: data_I4(:,:,:,:)     
      integer, pointer :: data_I5(:,:,:,:,:)
      integer, pointer :: data_I6(:,:,:,:,:,:)    
      
      integer di(6),ns(6)
      integer numel
      integer :: dims(6) = (/0, 0, 0, 0, 0, 0/)
      
      integer :: count1, start1, dm, mm
      ! decomposition here only in first dimension
      
      
      if (dat .eq. 0) return ! empty!!
      
      varid = dat
      isreal  = .false.
      islogic = .false.
      isint   = .false.
      ischar  = .false.
      
      status = nf90_inquire_variable(ncid, varid, xtype=xtype, ndims=ndims, dimids=dimids)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_inquire_variable"
      end if
      call MPI_BARRIER (MPI_COMM_WORLD,ierr)
      do i = 1, ndims
          status = nf90_inquire_dimension(ncid, dimids(i), len=dims(i))
          if (status /= nf90_noerr) then 
              print *, trim(nf90_strerror(status))
              stop "Stopped in nf90_inquire_dimension"
          end if
      end do
      numel = 1; ns = 0
      
      ! decomposition
      ! This just a simple example of decomposition along columns. specialized decomposition module can replace this part
      ! If more sophisticated decomposition, /start1, 1/ needs to modified to starting positions in different dimensions, 
      !  count = (/count1,dims(2)/)) need to include lengths to be extracted along each dimension
      !!! ######################## START 11111111111111111111111111111111111111111111111
      mm = mod(dims(1),p)
      dm = dims(1)/p
      if (my_rank < mm) then
          start1 = my_rank * (dm+1) + 1
          count1 = dm + 1
      else
          start1 = mm * (dm+1) + dm*(my_rank-mm) + 1
          count1 = dm
      endif
      !write(*,*)my_rank, start1, count1
      
      numel = numel * count1
      !!! ######################## END 11111111111111111111111111111111111111111111111
      
      
      DO j=2,ndims
        numel = numel*dims(j)
        if (dims(j) > 1) ns(j)=1
      ENDDO
      
      di = dims
      di(1) = count1
      
      if (numel .eq. 0) return ! empty!
      
      ! datatype and get data
      if ((xtype .eq. NF90_DOUBLE) .or. (xtype .eq. NF90_FLOAT)) then
          isreal = .true.
          n = main%np + 1
          select case (ndims)
          case (1)
              allocate(data_R1(count1))
              status = nf90_get_var(ncid, varid, data_R1, start = (/start1/), count = (/count1/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignR1(numel,data_R1,main,n)
          case (2)
              allocate(data_R2(count1,dims(2)))
              status = nf90_get_var(ncid, varid, data_R2, start =(/start1, 1/), count = (/count1,dims(2)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignR2(di,data_R2,main,n)
          case (3)
              allocate(data_R3(count1,dims(2),dims(3)))
              status = nf90_get_var(ncid, varid, data_R3, start =(/start1, 1, 1/), count = (/count1,dims(2),dims(3)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignR3(di,data_R3,main,n)
          case (4)
              allocate(data_R4(count1,dims(2),dims(3),dims(4)))
              status = nf90_get_var(ncid, varid, data_R4, start =(/start1, 1, 1, 1/), &
                  count = (/count1,dims(2),dims(3),dims(4)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignR4(di,data_R4,main,n)
          case (5)
              allocate(data_R5(count1,dims(2),dims(3),dims(4),dims(5)))
              status = nf90_get_var(ncid, varid, data_R5, start =(/start1, 1, 1, 1, 1/), &
                  count = (/count1,dims(2),dims(3),dims(4),dims(5)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignR5(di,data_R5,main,n)
          case (6)
              allocate(data_R6(count1,dims(2),dims(3),dims(4),dims(5),dims(6)))
              status = nf90_get_var(ncid, varid, data_R6, start =(/start1, 1, 1, 1, 1, 1/), &
                  count = (/count1,dims(2),dims(3),dims(4),dims(5),dims(6)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignR6(di,data_R6,main,n)  
          end select
          main%f(n) = field
          main%np = n
      elseif ((xtype .eq. NF90_SHORT) .or. (xtype .eq. NF90_INT) .or. (xtype .eq. NF90_BYTE)) then
          isint = .true.
          n = main%np + 1
          select case (ndims)
          case (1)
              allocate(data_I1(count1))
              status = nf90_get_var(ncid, varid, data_I1, start = (/start1/), count = (/count1/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignI1(numel,data_I1,main,n)
          case (2)
              allocate(data_I2(count1,dims(2)))
              status = nf90_get_var(ncid, varid, data_I2, start =(/start1, 1/), count = (/count1,dims(2)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignI2(di,data_I2,main,n)
          case (3)
              allocate(data_I3(count1,dims(2),dims(3)))
              status = nf90_get_var(ncid, varid, data_I3, start =(/start1, 1, 1/), count = (/count1,dims(2),dims(3)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignI3(di,data_I3,main,n)
          case (4)
              allocate(data_I4(count1,dims(2),dims(3),dims(4)))
              status = nf90_get_var(ncid, varid, data_I4, start =(/start1, 1, 1, 1/), &
                  count = (/count1,dims(2),dims(3),dims(4)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignI4(di,data_I4,main,n)
          case (5)
              allocate(data_I5(count1,dims(2),dims(3),dims(4),dims(5)))
              status = nf90_get_var(ncid, varid, data_I5, start =(/start1, 1, 1, 1, 1/), &
                  count = (/count1,dims(2),dims(3),dims(4),dims(5)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignI5(di,data_I5,main,n)
          case (6)
              allocate(data_I6(count1,dims(2),dims(3),dims(4),dims(5),dims(6)))
              status = nf90_get_var(ncid, varid, data_I6, start =(/start1, 1, 1, 1, 1, 1/), &
                  count = (/count1,dims(2),dims(3),dims(4),dims(5),dims(6)/))
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_get_var"
              end if
              call assignI6(di,data_I6,main,n)  
          end select
          main%f(n) = field
          main%np = n
      elseif (xtype .eq. NF90_CHAR) then
          ischar = .true.
          ! temporary no char data.
      end if
      
      
    end subroutine
    
END MODULE pncTable