#include <preproc.h>
!

      subroutine wtrshd_day(T,JD,TT,sig)
      !% (E) The daily marching script that runs watershed model for a day.
      !% Basic time step is hourly (or the macro watershed time step w.dt).-
      !% Surface flow and river flow may run faster.
      !% Input:
      !% wid: watershed id, most likely 1, T: final time, most likely current-
      !% time + 1 (day), JD: julian day (of the year)
      !% Output:
      !% TT: CPU time collected for different components:
      !% [1vegation+vadoseZone, 2overlandFlow, 3riverNetwork, 4groundwater, 5output]
      !% sig: whether the model exited the day normally (0) or with error (1)-
      !% updated all relevant fields in g and r
      use Vdata
      use flow
      use surface_proc_mod, Only: surface_proc
      use budgeter_mod, Only: budgeter
      use memoryChunk, Only:tempStorage,MBTerms
      use pawsControl
      use schedulerMod
      use gridOp
      !use impSolverTemp_mod
#if (defined NC)
      use proc_mod
      use mgTemp_mod
#endif
#if (defined CLM45)      
      use PAWS_CLM_SVC, only : passV
#endif
#ifdef CONCEPTS      
      use CONCEPTS_Input_Types
#endif

      implicit none
#if (defined NC)
      include 'mpif.h'
#endif
      !global g r w
      real*8 :: T, TT(5)
      integer :: JD, sig
      real*8, parameter :: PREC = 1D-6
      real*8 :: gid, nFT, diff, mxv, RDT,lastLoc(100)
      real*8, save:: lastDt =0D0
      integer, dimension(size(w%CR)) :: rL
      real*8, dimension(size(w%CR)) :: DTS
      integer, dimension(size(w%ER)) :: eL
      real*8, dimension(size(g%OVN%h,1),size(g%OVN%h,2)):: hf
      integer, dimension(size(g%OVN%h,1)*size(g%OVN%h,2)):: filter
      real*8, dimension(size(g%OVN%hc,1),size(g%OVN%hc,2)):: hc
      real*8, dimension(size(g%Veg%hI,1),size(g%Veg%hI,2)):: hI, h      
      integer :: i, j, SCODE, Hour, kk, m2(3), c, nstep, ii, cc, ci ,idx
      character*32 :: rMB, rSig, wSig, gSig, pondSig,colSig
      real*8 :: time_start=0d0, time_end=0d0, dtNew, dtOld
      real*8, dimension(:,:), pointer :: mask=>NULL(), GWBCflux=>null(), hcAfterLand=>null(),highGWFix=>null(),RfF=>NULL()
      real*8, dimension(:,:), pointer :: ovnDrain=>null()
      logical*1, dimension(:,:), pointer :: nanMap2D=>NULL()
      integer :: scalar(2)
      real*8 :: MBTerms_step(3) !MBTerms: [BC_Add,IntoTransect,riverTransfer]
      logical*1 :: subp, ovnp, rivp,dbgsave = .false.
      integer :: dbflag,k,kMap,k1,k2,kRiv, rloop
      type(OV_type), pointer :: OV
      type(vege_type), pointer:: Veg
      TYPE(VDZ_type),POINTER:: VD
      type(Rec_type), pointer :: Rec
      integer, save :: rivInter = 1
      integer, save :: rivSt = 0
      integer :: nv2(10)=-1
      integer paws_grid_size(6) !added for coarse RCM - 2018-09-08
      !integer, save :: hhm = 1
      real*8, pointer:: temphead(:,:)=>null()
      integer :: dsig
      !integer, save, target:: C_ALL(4,3,nGMax,nGMax) = 0
      !integer,dimension(:,:),pointer:: C_g
      real*8,dimension(:),pointer::gOVN1D=>NULL(), LakeMap1D=>NULL()
      real*8, save :: ovnagg(4)
#if (defined NC)
      integer :: ierr, masterproc = 0, rroot!, myrank, nproc
      real*8, dimension(:), pointer :: TopoE1D=>null(), ovnMan1D=>null(), sRiv1D=>null()
      real*8, dimension(:), pointer :: ovnH1D=>null(), ovnQoc1D=>null(), gwH1D=>null()
      real*8, dimension(:), pointer :: ovnH1Dp=>null(), ovnQoc1Dp=>null(), gwH1Dp=>null()
      real*8, dimension(:,:), pointer :: sRiv=>null(),sRivChunk=>null()
      integer, save :: Init1D = -1
      real*8, dimension(pci%YDim(1), pci%XDim(1)) :: ovnH2D!,TopoE2D,ovnMan2D,ovnQoc2D,gwH2D
      integer :: fullsize ! = pci%YDim(1) * pci%XDim(1)
#endif
      
      Rec => w%Rec
      Veg => g%Veg
      OV => g%OVN
      VD => g%VDZ      
      dbflag = 0
      
      rMB = 'rMB'; rSig = 'r'; wSig = 'w'; gSig = 'g'; pondSig = 'pond'
      colSig = 'col'
      scalar = (/1, 1/)
            
      
      TT = 0.0D0
      gid = w%g      !% Should only contain root level grids
      !rL=w(wid).CR; % Control River List, should have been sorted from upstream to downstream
      rL = w%CR
      !eL=w(wid).ER; % Exchange River
      eL = w%ER
      !Sol = w(wid).Sol;
      !COMP= Sol.COMP;
      !Exch= w(wid).Exch;
      !for i=1:length(rL), DTS(i)=r(rL(i)).Riv.dt; end
      do i = 1, size(rL)
        DTS(i) = r(rL(i))%Riv%dt
      enddo
      SCODE = 1
      !% weather data for the day
      sig = 0
      mask => w%DM%mask2
      OV => g%OVN
      Hour = 0; NSTEP = 0
      nFT = nint(w%dt/g%OVN%dt)      !% flow time step is smaller than wtrshd time step
      
   
      do while (w%t < (T-PREC))
        !write(*,*) 'g.OVN.h = ', sum(g%OVN%h)
        !write(*,*) 'g.Veg.h = ', sum(g%Veg%h)
        !write(*,*) 'g.VDZ.h = ', sum(g%VDZ%h)
        !write(*,*) 'g.GW(1).h = ', sum(g%GW(1)%h)
        !write(*,*) 'r(25).Riv.h = ', sum(r(25)%Riv%h)
        !write(*,*) '*****************'        !%disp([JD H])
        Hour = floor((w%t-floor(w%t))*24D0+1D-7)+1       !% hour of day
        NSTEP = NSTEP + 1
        diff = MBTerms(4)
        MBTerms = 0D0
        MBTerms(3) = diff
        
        if (JD .eq. 245 .and. Hour .eq. 11) then
            !%pp=maxALL((g.GW(1).h-g.Topo.E).*w.DM.mask);%TV;
            ![JD H];
            dbgsave = .false.
#if (defined NC)
#else
            if (dbgsave) call saveMat2('debugDay.mat')
#endif
        else 
            dbgsave = .false.
        endif
        
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=startingdt', myrank, HOUR, kk
#if (defined NC)
          if (init1D < 0) then
              init1D = 1       
              if (size(rL) < (nproc*2)) then ! for parallel river
              !if (.true.) then !  force one core!!! may speed up 
                 riverPal = .false.
                 rivInter = 1
                 if (myrank == masterproc) then
                     rivSt = myrank
                 else
                     rivSt = size(rL)+1 ! don't run
                 endif
              else
                 riverPal = .true.
                 rivSt = myrank
                 rivInter = nproc
              endif 
          endif
#endif        
        !hf = g.OVN.h; hc  = g.OVN.hc; hI = g.Veg.hI; h = hf + hc + hI; w.Rec.hSave = h; % debug
        hf = g%OVN%h
        hc = g%OVN%hc
        hI = g%Veg%hI
        h = hf + hc + hI
        if(.not. associated(w%Rec%hSave)) allocate(w%Rec%hSave(size(h,1),size(h,2)))
        w%Rec%hSave = h
        !Sol.wea(wid,Hour,JD)

#ifdef NC
!======== which one??? need check again ; ny=85, nx=70, ns(1)=7, ns(2)=9, 2018-11-06 ======== 
      !paws_grid_size(1)=g%VDZ%M(1) !added for coarse RCM - 2018-09-24
      !paws_grid_size(2)=g%VDZ%M(2) !added for coarse RCM - 2018-09-24

      paws_grid_size(2)=g%VDZ%M(1) !added for coarse RCM - 2018-09-24  ! <<<<<===== validated !!! Right!!! WP 2018-11-07
      paws_grid_size(1)=g%VDZ%M(2) !added for coarse RCM - 2018-09-24  ! <<<<<===== validated !!! Right!!! WP 2018-11-07
#else
      paws_grid_size(2)=size(h,1) !added for coarse RCM - 2018-09-08
      paws_grid_size(1)=size(h,2) !added for coarse RCM - 2018-09-08
#endif 
        call schedulerMain(w%t,w%dt,nv2,paws_grid_size)        
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=afterSched', myrank, HOUR, kk
        call distWea(Hour,JD)

        
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=afterdistWea', myrank, HOUR, kk
        !%releaseSchedule(wid,H,JD)
        !do i = 1, size(w%child)
        !    c = w(wid).child(i);
        !    w(c).watershed(c);
        !enddo
!ovnagg(1) = sum(g%OVN%h)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(2) = sum(g%OVN%hc)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(3) = sum(g%Veg%hI)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(4) = sum(g%OVN%hd * g%OVN%dP(:,:,4))*(g%DM%d(1)*g%DM%d(1))
        
        !%GW_source(gid,1);
#if (defined CLM_Only)
#else
        if (SCODE == 0) then
          g%VDZ%h(:,:,1) = g%OVN%h        !% do not use F_contribute
        else
          g%VDZ%h(:,:,1) = g%OVN%hc
        endif
#endif
        !tic
        time_start = 0d0
        call timer(time_start)
        !Sol.veg(wid,H) % interception, ET, unconfined aquifer
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=JustOutsideSurface', myrank, HOUR, kk
        call surface_proc(Hour)
        
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=afterSurface', myrank, HOUR, kk
        !if (dbgsave) call saveMat2('afterLand.mat')
        
#ifndef CLM_Only
        !TT(1)=TT(1)+toc;
        time_end = 0d0
        call timer(time_end)
        TT(1) = TT(1) + (time_end - time_start)
        
        if (SCODE == 0) then
            g%OVN%h = g%VDZ%h(:,:,1)
        else
            g%OVN%hc = g%VDZ%h(:,:,1)       ! WHAT IF hc is negative (from VDZ)
        endif
     
        call tempStorage('hcAfterLand',hcAfterLand)
        hcAfterLand = g%VDZ%h(:,:,1) ! for mapOutput2 calculation
        call tempStorage('highGWFix',highGWFix)
        call tempStorage('ovnDrain',ovnDrain)
        call tempStorage('RfF',RfF)                
        
        !if (no_Surf) goto 200 !! THIS NEEDS TO BE CHANGED LATER!!!
!ovnagg(1) = sum(g%OVN%h)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(2) = sum(g%OVN%hc)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(3) = sum(g%Veg%hI)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(4) = sum(g%OVN%hd * g%OVN%dP(:,:,4))*(g%DM%d(1)*g%DM%d(1))        
        !budgeter('pond',wid,[],H)           ! debug        
        !call budgeter(pondSig,1,mask,Hour)
    
        !% Operator Splitting: First infiltrate then OVN flow
        !%disp([JD H g.GW(2).h(i,j) g.GW(1).h(i,j) g.VDZ.DF(i,j) g.VDZ.Dperc(i,j) g.VDZ.h(i,j,2) g.VDZ.SRC(i,j,2)])
        subp = .false.
        ovnp = .false.
        !if ~isempty(find(isnan(g(gid).GW(1).h), 1)) ||...
        !        any(isnan(g(gid).VDZ.h(:))) !%|| anyALL((g(gid).GW(1).h(:)>310))
        if ( any( isnan(g%GW(1)%h) ) .or. any( isnan(g%VDZ%h) ) ) then
            subp = .true.
            sig = 1
            call find1DL(isnan(g%GW(1)%h),w%m(1)*w%m(2),filter,k1)
            call display( 'JD: '//trim(num2str(JD))//' hour:'//trim(num2str(hour)))
            call display( 'Subsurface NaN! GW k1:'//trim(num2str(k1))// 'Location(1)'//trim(num2str(filter(1))))
            call find1DL(isnan(g%VDZ%h),w%m(1)*w%m(2)*w%m(3),filter,k2)
            call display( 'Subsurface NaN! VDZ k2:',k2,'Location(1)',filter(1))
            return
        endif
        !loc = (g(gid).GW(1).h>g(gid).GW(1).topBound) & w.DM.mask;
        !if anyALL(loc)
        !    diff = g(gid).GW(1).h(loc)-g(gid).GW(1).topBound(loc);
        !    g(gid).GW(1).h(loc) = g(gid).GW(1).topBound(loc);
        !    g(gid).OVN.hc(loc) = g(gid).OVN.hc(loc)+g(gid).GW(1).ST(loc).*diff;
        !end
        do j = 1, size(g%GW(1)%h,2)
          do i = 1, size(g%GW(1)%h,1)
            if(w%DM%mask(i,j) .AND. g%GW(1)%h(i,j) > g%GW(1)%topBound(i,j)) then              
              diff = g%GW(1)%h(i,j)-g%GW(1)%topBound(i,j)
              g%GW(1)%h(i,j) = g%GW(1)%topBound(i,j)
              highGWFix(i,j) = g%GW(1)%ST(i,j)*diff
              g%OVN%hc(i,j)  = g%OVN%hc(i,j)+highGWFix(i,j)
            else
              highGWFix(i,j) = 0D0
            endif
          enddo
        enddo
        
        !do I = 2, size(gA)-1
        !    C_g=>C_ALL(:,:,1,I)
        !    call gCopyData(1,I,g%GW(1)%h,gA(I)%GW(1)%h,C_g,-1)
        !enddo
    
        do kk = 1, int(nFT)
          RDT = g%OVN%dt
          !tic
          time_start = 0d0
          call timer(time_start)
          if (SCODE == 0) then  !% not using F_contribute
          else
            !%F_contribute(gid)
          endif
          !Sol.ovn(gid);
          if (g%VDZ%dtP(3) > 0) then
            g%OVN%S = g%OVN%Rf * (1.0D0 / g%VDZ%dtP(3) / 86400.0D0)     !% The unit of Rf is m (come in g.VDZ.dtP(3) time)
            if(kk .eq. 1) RfF = RfF * (1.0D0 / g%VDZ%dtP(3) / 86400.0D0)  
#ifdef NC
            sRiv => mg_Base(1)%ovns
            sRiv = 0D0
            call tempStorage('SRiv',sRivChunk)
            sRiv1D => OneD_ptr(pci%YDim(1)*pci%XDim(1),sRiv)       
            sRivChunk = 0D0
            do i = 1+rivSt, size(rL), rivInter
                if (r(rL(i))%Riv%DST .ge. 5) then
                    idx = g%Riv(i)%Lidx(size(g%Riv(i)%Lidx))
                    ! gOVN1D(idx) = gOVN1D(idx) + r(rL(i))%Riv%Qx(r(rL(i))%DM%msize(1)-1)/g%DM%d(1)**2
                    sRiv1D(idx) = sRiv1D(idx) +  r(rL(i))%Riv%Cond(1) / (g%OVN%dt * 86400.0D0 * g%DM%d(1)**2) ! m^3 -> m/s
                    r(rL(i))%Riv%Cond(1) = 0D0
                endif
            enddo
            sRiv1D=allReduce1D(sRiv1D,pci%YDim(1)*pci%XDim(1))
            call scatter2Ddat(1,pci%YDim(1),pci%XDim(1),sRiv,size(g%OVN%S,1), size(g%OVN%S,2), sRivChunk,masterproc)
            g%OVN%S = g%OVN%S + sRivChunk
#else
            gOVN1D => OneD_ptr(size(g%OVN%S),g%OVN%S)
            do i = 1,size(rL)
                if (r(rL(i))%Riv%DST .eq. 5) then !20180606
                ! if (r(rL(i))%Riv%DST .ge. 5) then
                !if (r(rL(i))%Riv%DST .ge. 5 .or. r(rL(i))%Riv%DST .eq. 3) then
                    idx = g%Riv(i)%Lidx(size(g%Riv(i)%Lidx))
                    ! gOVN1D(idx) = gOVN1D(idx) + r(rL(i))%Riv%Qx(r(rL(i))%DM%msize(1)-1)/g%DM%d(1)**2
                    gOVN1D(idx) = gOVN1D(idx) +  r(rL(i))%Riv%Cond(1) / (g%OVN%dt * 86400.0D0 * g%DM%d(1)**2) ! m^3 -> m/s
                    r(rL(i))%Riv%Cond(1) = 0D0
                endif
                !if (r(rL(i))%Riv%DST  .eq. 3) then ! averagely applied source term
                !    if (associated(g%DM%Map)) then
                !        LakeMap1D => OneD_ptr(size(g%DM%Map),g%DM%Map)
                !    endif
                !    where (LakeMap1D < 0.5D0)
                !        gOVN1D = gOVN1D +  r(rL(i))%Riv%Cond(1) / (g%OVN%dt * 86400.0D0 * g%DM%d(1)**2)/ (size(LakeMap1D) - sum(LakeMap1D)) ! m^3 -> m/s
                !    endwhere
                !    r(rL(i))%Riv%Cond(1) = 0D0
                !endif
            enddo
            !MBTerms(3)=0D0
#endif
          else
          endif

          !prism(6,g.OVN,g.Topo,g.DM.d)
          !call prism(6, g%OVN, g%Topo, g%DM%d)
          call ovnMesh_driver()
          
!ovnagg(1) = sum(g%OVN%h)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(2) = sum(g%OVN%hc)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(3) = sum(g%Veg%hI)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(4) = sum(g%OVN%hd * g%OVN%dP(:,:,4))*(g%DM%d(1)*g%DM%d(1))
#ifdef NC
          call river_ovnGw(1, 1, ovnH2D)
#endif
          g%OVN%t = g%OVN%t + g%OVN%dt
 !    write(*,*)   g%OVN%t
   ! write(*,*) g%OVN%ms,'ovh', g%OVN%h
          !TT(2)=TT(2)+toc;
          time_end = 0d0  
          call timer(time_end)
          TT(2) = TT(2) + (time_end - time_start)
          !%[r(27).Riv.h' r(27).Riv.Qx']
          !%test=0;
          !%if min(g.OVN.h(:))<-0.02%g.OVN.t>=7.328091805558624e+005-2*g.OVN.dt-1e-5;%
          !%if g.OVN.t>=7.320321249999766e+005-g.VDZ.dt-1e-4;%datenum('31-Aug-2007 08:30:00')-1e-5
          !%    4;%TV;
          !%end
        
          subp = .false.
          !if ~isempty(find(isnan(g(gid).OVN.h), 1))
          if (any(isnan(g%OVN%h))) then
            call find1DL(isnan(g%OVN%h),w%m(1)*w%m(2),filter,k)
            call display( 'OVN NaN!!! k:',k,' location(1): ',filter(1))
            ovnp = .true.
            sig = 1
          endif
          !tic;
          time_start = 0d0          
          call timer(time_start)
          kRiv = 0
          MBTerms_step(3) = 0D0
!#if (defined NC)
!      if (myrank == 0) then
!#endif
          

!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRiv', myrank, HOUR, kk

          do while (RDT > PREC)     !% River Network Module. This module needs to adaptively select time step.

            !write(777,'(F13.5,F20.16)') w%t, r(25)%Riv%umax
            !if(w%t>=731095.08D0) then
            !    write(*,*) 'err'
            !endif
            ![dtNew, dtOld] = rMinDtSet(rL,RDT);
            kRiv = kRiv+1
            
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT1', myrank, kRiv
        
            call rMinDtSet(rL, RDT, dtNew, dtOld)
            
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT2', myrank, kRiv
!write(*,*) kRiv,RDT,PREC,dtOld,dtNew  

            !rAvgStages(wid);
            call rAvgStages()
            !if(.not. associated(g%Riv(i)%Qoc)) allocate(g%Riv(i)%Qoc(size(rL)))

!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT3', myrank, kRiv
            do i = 1+rivSt, size(rL), rivInter
                !Exch.OC(gid,i,dtNew);
                !do j = 1, max(1,size(gA)-1)
                    !call F_oc_dw(i,j)
!write(*,*) 'debug=inRDT9', myrank, i
                call F_oc_dw(i,1)
                
!write(*,*) 'debug=inRDT10', myrank, i
                !enddo
                !do j = 2, size(gA)-1
                !    if (associated(gA(j)%Riv(i)%Len)) then
                !        C_g=>C_ALL(:,:,j,1)
                !        call gCopyData(j,1,gA(j)%OVN%Qoc,g%OVN%Qoc,C_g,2)
                !        gA(j)%OVN%Qoc = 0D0
                !    endif
                !enddo
            enddo
#ifdef NC

!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT4', myrank, kRiv
            if (riverPal) then
             do i = 1, size(rL)
                cc = rL(i)
                if (associated(r(cc)%Net%Trib)) then
                    do ii = 1, size(r(cc)%Net%Trib)
                        ci = r(cc)%Net%Trib(ii)
                        call riverDSh(ci, cc)
                    enddo
                endif
             enddo
             rloop = ((size(rL)/nproc)+1)*nproc
            else
             rloop = size(rL)
             !call MPI_Bcast(dtNew, 1, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
            endif
#else
            rloop = size(rL)
#endif

!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT5', myrank, kRiv
            do i = 1+rivSt, rloop, rivInter ! XY: change the loop, so that everyone gets into the exchange step.
              if (i<=size(rL)) then
                c = rL(i)
                
                !%Exch.GC(gid)
                
                call rLateral(c, kRiv)          
                !Sol.rte(c);
                call r_MacCormack(c)
                !%disp([H kk c])
                !%if w.signals(1)==1
                !  % transport
                !  %runQ(r(c).q); % March
                !%end
                !%if c==30, 
                !    %budgeter('r',c); 
                !%end
                !recAcc('rMB',c)

                call recAcc(rMB,c)
                !rClearTFlux(c)
                call rClearTFlux(c)
                
                if (r(c)%Riv%DST .eq. 5) then !20180606
                !if (r(c)%Riv%DST .ge. 5 .or. r(c)%Riv%DST .eq. 3) then    
                    MBTerms_step(3) = MBTerms_step(3) + r(c)%Riv%Qx(r(c)%DM%msize(1)-1) * r(c)%Riv%dt * 86400.0D0 / g%DM%d(1)**2 ! m
                    r(c)%Riv%Cond(1) = r(c)%Riv%Cond(1) + r(c)%Riv%Qx(r(c)%DM%msize(1)-1) * r(c)%Riv%dt * 86400.0D0 ! m^3
                    r(c)%Riv%Cond(2) = r(c)%Riv%Cond(2) + r(c)%Riv%Qx(r(c)%DM%msize(1)-1) * r(c)%Riv%dt * 86400.0D0 ! m^3
                endif
                !if (r(c)%Riv%DST .eq. 3) then 
                !    r(c)%Riv%Cond(2) = r(c)%Riv%Cond(2) + r(c)%Riv%Qx(r(c)%DM%msize(1)-1) * r(c)%Riv%dt * 86400.0D0 ! m^3
                !endif
            
                
              endif !! ==>if (i<=size(rL)) then
#ifdef NC
              if (riverPal) then
                do j = i-myrank, min(i-myrank-1+nproc,size(rL))
                   cc = rL(j)
                   if (associated(r(cc)%Net%DS)) then
                      ii = r(cc)%Net%DS
                      call riverTrib(cc,ii)
                   endif
                   if (associated(r(cc)%Net%Trib)) then
                      do ii = 1, size(r(cc)%Net%Trib)
                         ci = r(cc)%Net%Trib(ii)
                         call riverDS(ci, cc)
                      enddo
                   endif
                enddo
              endif
#endif
            enddo
            !recAcc('r')
            call recAcc(rSig,0)
            !%budgeter('OVN',1,[],H); 
            rivp = .false.
            do j = 1, size(w%CR)
                !if (~isempty(find(isnan(r(w.CR(j)).Riv.h), 1)) || ~isempty(find(r(w.CR(j)).Riv.h<0, 1)))
                if(any(isnan(r(w%CR(j))%Riv%h)) .or. any(r(w%CR(j))%Riv%h<-1D-10)) then
                    rivp = .true.
                    sig = 1
                    call find1DL(isnan(r(w%CR(j))%Riv%h),size(r(w%CR(j))%Riv%h),filter,k)
                    call display( 'River NaN!!!, j:', j, ' k: ',k, ' location(1):',filter(1))
                    return
                endif
#ifdef NC
               if (riverPal) then
                ! XY: broadcast umax
                rroot = mod(j-1, nproc) ! computation node
                c = w%CR(j)  
                !r(c)%Riv%umax = bcast0D(r(c)%Riv%umax,rroot)
                !r(c)%Riv%dt = bcast0D(r(c)%Riv%dt,rroot)
                call MPI_Bcast(r(c)%Riv%umax, 1, MPI_real8, rroot, MPI_COMM_WORLD, ierr)
                call MPI_Bcast(r(c)%Riv%dt, 1, MPI_real8, rroot, MPI_COMM_WORLD, ierr)
               endif
#endif
            enddo
            if (any(isnan(g%OVN%h))) then
                ovnp = .true.
                call find1DL(isnan(g%OVN%h),w%m(1)*w%m(2),filter,k)
                call display( 'OVN Exchange NaN!!! k:', k,' location(1): ', filter(1))
            endif
            
            !if rivp  || ~isempty(find(isnan(g(gid).OVN.h), 1)) || subp || ovnp
            if(rivp  .or. subp .or. ovnp) then
                !warning('4');
                call ch_assert('wtrshd_day :: NaN detected')
                return
            endif
            RDT = RDT - dtNew
#ifdef NC

!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT6', myrank, kRiv
          if (riverPal) call river_ovnGw(2, 1, ovnH2D)
          
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT7', myrank, kRiv
#endif
          enddo! XY: ===> do while (RDT > PREC)
#ifdef NC
          if (.not. riverPal) call river_ovnGw(2, 1, ovnH2D)
#endif
          do i = 1, size(rL)
            r(rL(i))%Riv%dt = DTS(i)
          enddo   !% TEMPORARY
          
          if (kk .eq. int(nFT)) then
              MBTerms(4)= MBTerms_step(3) 
          else
              MBTerms(3)= MBTerms(3) + MBTerms_step(3)  ! m
          endif
          
          !TT(3)=TT(3)+toc;       
#ifdef CONCEPTS   
         ! currently, link happens  links(ilink)%ptrreach => Input_Reach(ilink) in ProcessCrossSections
          linkFromPAWS =  .true. 
          do i = 1,size(w%conceptsR)
            call concepts(w%conceptsR(i), g.OVN.dt * 86400D0)
          enddo     
#endif             

          time_end = 0d0          
          call timer(time_end)
          TT(3) = TT(3) + (time_end - time_start)
!ovnagg(1) = sum(g%OVN%h)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(2) = sum(g%OVN%hc)*(g%DM%d(1)*g%DM%d(1))
!call savemat2('ini_merge.mat')
!ovnagg(3) = sum(g%Veg%hI)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(4) = sum(g%OVN%hd * g%OVN%dP(:,:,4))*(g%DM%d(1)*g%DM%d(1))
        enddo ! XY: ===>do kk = 1, int(nFT)
        do i = 1, size(rL) !20180606
            c = rL(i)
            if (r(c)%Riv%DST .eq. 5) then !20180606
            !if (r(c)%Riv%DST .ge. 5 .or. r(c)%Riv%DST .eq. 3) then    
                write(272+c-100,FMT='(4E20.10)')  w%t,r(c)%Riv%Cond(2)/3600D0 !m3/s
                r(c)%Riv%Cond(2) = 0D0
            endif
        enddo
        
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=inRDT8', myrank, kRiv
        
#if (defined CLM45)              
        call passV(3)  
#endif        

!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=beforeGW', myrank, HOUR
        if (dbgsave) then
          !call saveMat2('afterSurfaceFlow.mat')
        endif
        !%disp(['beyond surface ',num2str(H)])
        !% Saturated Zone. 
        !% ############################### GW(2)???
        !%for i=1:length(g(gid).GW)-1
        !tic
 200   time_start = 0d0        
        call timer(time_start)
#ifdef NC
        call GW_wgc_p()  
#endif
        do i = 1, size(g%GW)
            if (abs(g%GW(i)%tt+g%GW(i)%dt-(w%t + w%dt)) < PREC) then
                !% Sum up recharge (or release)
                !GW_source(gid,i);
                call GW_source(i)   
                !Sol.gw(gid,i);
                call GW_Sol(i)
            endif
        enddo
        
!ovnagg(1) = sum(g%OVN%h)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(2) = sum(g%OVN%hc)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(3) = sum(g%Veg%hI)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(4) = sum(g%OVN%hd * g%OVN%dP(:,:,4))*(g%DM%d(1)*g%DM%d(1)) 

        call tempStorage('GWBCFlux',GWBCflux)
        !call fixedHeadGWupdate(g%GW(1)%ST,int(g%DM%msize),g%GW(1)%h,w%tData%h1save,GWBCflux)
        call fixedHeadGWupdate(g%GW(1)%ST,(/w%m(1), w%m(2)/),g%GW(1)%h,w%tData%h1save,GWBCflux)
        if (dbgsave)then
          !call saveMat2('afterGW.mat')
        endif
        
        !%ii=13;jj=14; [g.GW(1).W(ii,jj,:) g.GW(1).h(ii,jj,:) g.GW(2).h(ii,jj) g.GW(2).ST(ii,jj)]
        !TT(4)=TT(4)+toc;
        time_end = 0d0        
        call timer(time_end)
!write(272,*) W%T
!write(272,*) 's', sum(gA(2)%OVN%S)
!write(272,*) 'h', sum(gA(2)%OVN%h)
!WRITE(272,*) '======'
        TT(4) = TT(4) + (time_end - time_start)
#ifdef NC
!if(myrank==0)    write(272+myrank, *) myrank, w%t, g%ovn%s(30,24)
!if(myrank==2)    write(272+myrank, *) myrank, w%t, r(14)%Riv%h
!if(myrank==0)    write(272+myrank, *) myrank, w%t, g%VDZ%Dperc(45,33)
!write(272+myrank, *) myrank, g%GW(2)%h
!write(272+myrank, *) myrank, w%t, '4' 
!if(w%t > 731096)  then
!write(272+myrank, *) myrank, w%t, g%ovn%h
!call MPI_Barrier(MPI_COMM_WORLD, ierr)
!stop
!endif
#else
!write(272, *) w%t,r(14)%Riv%h
#endif
        call columnOutput(0)
        call recAcc(gSig,0)

        !tic
        time_start = 0d0        
        call timer(time_start)
        !budgeter('w',wid,[],H); % BASIN Average mass balance output
        call writeRec()      
        call budgeter(wSig,scalar(1),mask,Hour,1)         
        
        !dsig = 0; call saveVar('H',size(g%OVN%h),g%OVN%h,dsig,hhm)  
        !dsig = 1; call saveVar('Qoc',size(g%OVN%Qoc),g%OVN%Qoc,dsig,hhm) 
        !dsig = 1; call saveVar('V',size(g%OVN%h),g%OVN%V,dsig,hhm) 
        !dsig = 1; call saveVar('U',size(g%OVN%h),g%OVN%u,dsig,hhm) 
        !call tempStorage('EvapOvn',temphead)
        !dsig = 1; call saveVar('ovnET',size(temphead),temphead,dsig,hhm) 
        !dsig = 1; call saveVar('Gprcp',size(g%Veg%Gprcp),g%Veg%Gprcp,dsig,hhm) 
        !dsig = 2; call saveVar('C',size(g%OVN%c),g%OVN%C,dsig,hhm)
        !hhm = hhm + 1

!ovnagg(1) = sum(g%OVN%h)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(2) = sum(g%OVN%hc)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(3) = sum(g%Veg%hI)*(g%DM%d(1)*g%DM%d(1))
!ovnagg(4) = sum(g%OVN%hd * g%OVN%dP(:,:,4))*(g%DM%d(1)*g%DM%d(1)) 
#endif        
  
        !k=1; filter(1)=483
        if (dbgsave) THEN
        call find1D(w%DM%MAP,w%m(1)*w%m(2),filter,k)
        call budgeter(colSig,filter,mask,Hour,k)
        endif
        !mapOutput(0); % Temporal Average spatial maps
        !call mapOutput(0, w%tData%maps)
        ! depending on many objects there are, evenly put them in 
        kMap = min(size(w%tData%maps), ceiling(JD*size(w%tData%maps)/365D0))
        call mapOutput2(0, w%tData%maps(kMap))
        
        !writeRec % recording
        !TT(5)=TT(5)+toc;
        time_end = 0d0
        call timer(time_end)
        TT(5) = TT(5) + (time_end - time_start)
        w%t = w%t + w%dt

        !%movOutput(0);  %wtrshd_day, calendarMarch to do movie
        !% save for display::::
        if(.not. associated(w%tData%m3%R)) then
          allocate(w%tData%m3%R(size(g%VDZ%Dperc,1),size(g%VDZ%Dperc,2)))
        endif
        w%tData%m3%R = g%VDZ%Dperc
        if(.not. associated(w%tData%m3%E)) then
          allocate(w%tData%m3%E(size(g%Veg%ET,1),size(g%Veg%ET,2)))
        endif        
        w%tData%m3%E = g%Veg%ET
        !gClearTFlux(gid)        
        call gClearTFlux()
        !% Transport
        !%checkEndDt(wid);
        
        !%figure(20); i=25; plot(r(i).DM.x,r(i).Riv.E+r(i).Riv.h); hold on; plot(r(i).DM.x,r(i).Riv.hg,'r'); plot(r(i).DM.x,r(i).Riv.E,'k'); hold off
        !%figure(30); ii=[19 15 14 17]; jj=[27 19 18 21]; cols='brgkmybrgkmy';
        !%plotColumn(ii,jj,cols);
        !%TV   
        
!#ifdef NC              
!call MPI_Barrier(MPI_COMM_WORLD,ierr)
!write(*,*) 'debug=wtrshd_day', myrank, HOUR
!#endif
      enddo
      end subroutine wtrshd_day
          
          
          subroutine ovnMesh_driver()
          ! drives multiple meshes. At the moment, assuming g==>gA(1) is covered by sub-meshes. gCopyData now only supports 
          ! copying between meshes with the same resolution. Later, we can modify this subroutine to support different resolutions
          ! gA(2:end) are sub-meshes
          ! (0) gA(1)==g has got g%OVN%h & g%OVN%S
          ! (1) for each sub-mesh, copy data into gA(i)%OVN%h & gA(i)%OVN%S, using gd!
          ! (2)                    set up boundary conditions. obtain necessary data from neighbor meshes
          ! (3)                    run ovn solver depending on their OVN%scheme
          ! (4) copy gA(i)%OVN%h back to g
          use Vdata
          use flow
          use surface_proc_mod, Only: surface_proc
          use pawsControl
          use impSolverTemp_mod
          use gridOp
          use memoryChunk, Only:tempStorage,MBTerms
          
          implicit none
          integer i,j,kt
          !integer :: nGMax=5
          integer, save, target:: C_ALL(4,3,nGMax,nGMax) = 0
          integer,dimension(:,:),pointer:: C
          type(GlobalG),pointer :: g1,g2
          integer, save, target :: lastLocA(100,nGMax)
          integer, pointer:: lastLoc(:)
          logical :: skip1 = .false.
          real*8 :: MBTerms_step(10)
          real*8, save :: lastDt(nGMax)=0D0
          TYPE(solvTemp2D_type), pointer :: sT
          
          integer :: swics(4)
          integer,save:: state = 0
          real*8, parameter :: PREC = 1D-6, Grav = 9.81D0
          real*8, dimension(:,:), pointer :: Fr=>null(), UU=>null()
          real*8 FrMax
          real*8, save :: ovnagg(3)
          real*8 z03,z02,z13,z12,z11,z11f
          !integer,save:: mmM = 1

          swics(:)=(/1,1,1,0/)
          ! (1) for each sub-mesh, copy data into gA(i)%OVN%h & gA(i)%OVN%S, using gd!
          
#ifdef NC
          call ovnGw_multigrid(C_ALL,1,0,state) !for calado
#else
          DO I=2,size(gA)
              C=>C_ALL(:,:,1,I) ! C will be populated if they are zero going into gCopyData, and it will be done only once
               ! 3D data?
              call gCopyData(1,I,g%OVN%h,gA(I)%OVN%h,C,0) ! interpolate ghost 
              !call gCopyData(1,I,g%OVN%h,gA(I)%OVN%h,C,10) !for calado
              gA(I)%OVN%S = 0D0
              call gCopyData(1,I,g%OVN%S,gA(I)%OVN%S,C,10,gA(I)%OVN%E,gA(I)%OVN%h)
              if (state .eq. 0) call gCopyData(1,I,g%OVN%Mann,gA(I)%OVN%Mann,C,10)
          ENDDO
#endif          
          if (state .eq. 0) then
              ! if multiple meshes are enabled, a different subroutine needs to manage sT_Base
              allocate(sT_Base(size(gA)))
              ! XY: examine the flood bound or something else
              !open(unit=1997, file='floodres.log', action='write')
              !open(unit=1998, file='GW_5_10.log', action='write')
              state = 1
          endif
      
          if (size(gA)>1) skip1 = .true.
          DO I=1,size(gA)
              if (I > 1 .OR. .NOT. skip1) then
                  g=>gA(I) ! code re-use, but we need to change it back to base
                  sT => sT_base(I)
                  MBTerms_step =0D0
                  kt = int(gA(1)%OVN%dt/g%OVN%dt)
                  !allocate(UU(g%OVN%ms(1),g%OVN%ms(2)))
                  !allocate(Fr(g%OVN%ms(1),g%OVN%ms(2)))
                  
                  ! (2) set up boundary conditions. obtain necessary data from neighbor meshes
                  lastLoc=>lastLocA(:,I)
                  call setOvnBC(I,lastLoc)
              
                  ! (3) run ovn solver depending on their OVN%scheme
                  do j = 1,kt
                     sT%Counter(1) = j
                     ! scheme switch
                     !Fr = 0D0
                     !UU = g%OVN%u**2 + g%OVN%v**2
                     !where (g%OVN%h > 0)
                     !    Fr = UU/(Grav*g%OVN%h)!sqrt(Grav*g%OVN%h)
                     !endwhere
                     !FrMax = SQRT(maxval(Fr))   
                     !if (FrMax > 0.3D0) then
                     !    g%OVN%scheme = 1
                     !else
                     !    g%OVN%scheme = 0
                     !endif
                     SELECT CASE (int(g%OVN%scheme+PREC)) ! by default g%OVN%scheme is 0
                     CASE (0) ! explicit
                      CALL ovn_cv(g%OVN%ms,g%OVN%h,g%OVN%U,g%OVN%V,g%OVN%S,g%DM%d,g%OVN%E,g%Topo%Ex,g%Topo%Ey    &  ! g%Topo%E-->g%OVN%E 05/22/18
                     &    , g%OVN%Mann, g%DM%Mask,g%OVN%dt,lastDt(I), size(g%OVN%Cond),g%OVN%Cond, g%OVN%ovnDrain,sT,i)
                      CASE (1) ! implicit
                       if (associated(g%OVN%C)) then
                        CALL sisl(g%OVN%ms,g%OVN%h,g%OVN%U,g%OVN%V,g%OVN%S,g%DM%d,g%OVN%E,g%Topo%Ex,g%Topo%Ey    &  ! g%Topo%E-->g%OVN%E 05/22/18
                          &    , g%OVN%Mann, g%DM%Mask,g%OVN%dt*86400D0,lastDt(I), size(g%OVN%Cond),g%OVN%Cond ,g%OVN%ovnDrain ,sT,Swics, g%OVN%Qx, g%OVN%Qy,MBTerms_step,i,g%OVN%C)
                       else
                        CALL sisl(g%OVN%ms,g%OVN%h,g%OVN%U,g%OVN%V,g%OVN%S,g%DM%d,g%OVN%E,g%Topo%Ex,g%Topo%Ey    &  ! g%Topo%E-->g%OVN%E 05/22/18
                          &    , g%OVN%Mann, g%DM%Mask,g%OVN%dt*86400D0,lastDt(I), size(g%OVN%Cond),g%OVN%Cond ,g%OVN%ovnDrain ,sT,Swics, g%OVN%Qx, g%OVN%Qy,MBTerms_step,i)
                       endif
                      MBTerms(1:2) = MBTerms(1:2) + MBTerms_step(1:2)*(g%OVN%ms(1)*g%OVN%ms(2))*(g%DM%d(1)*g%DM%d(1))/(gA(1)%OVN%ms(1)*gA(1)%OVN%ms(2)*gA(1)%DM%d(1)*gA(1)%DM%d(1)) !! XY: correct the size to the gA(1) 
                     END SELECT
                  enddo
                  !deallocate(UU)
                  !deallocate(Fr)
              endif       
          ENDDO
          g=>gA(1) ! change it back!!
          
          ! (4) copy gA(i)%OVN%h back to g
#ifdef NC          
          call ovnGw_multigrid(C_ALL,2,1,state)
#else 
          DO I=size(gA),2,-1 ! XY: in overlapping multi-mesh, overwrite would happen. Maybe other way to get around it.
              C=>C_ALL(:,:,I,1) ! C will be populated if they are zero going into gCopyData, and it will be done only once
              ! XY: this one we shouldn't copy ghost cells.
               ! 3D data?
              call gCopyData(I,1,gA(I)%OVN%h,g%OVN%h,C,1)
              call gCopyData(I,1,gA(I)%OVN%u,g%OVN%u,C,1)
              call gCopyData(I,1,gA(I)%OVN%v,g%OVN%v,C,1)
          ENDDO
#endif          
          
          end subroutine

          