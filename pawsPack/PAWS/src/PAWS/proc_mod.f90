
module proc_mod

   use gd_mod
   include 'mpif.h'
   private

   public :: proc_info, pci
   public :: createPCInfo_2D, linkPciFromGd, readPCInfo_2D
   public :: GWCondOfProcs, ovnCondOfProcs, ExchFluxReduce, ovnBCTypeBV, ovnCondOfProcs_60
   public :: proc_exchange_ghost, gather2Ddat, scatter2Ddat, proc_use_ghost
   public :: riverTrib, riverDS, riverRunPatch, riverDSh
   public :: Aggregate1Ddat, Aggregate2Ddat, allReduce1D, allReduce0D, bcast0D
   public :: AllReduceOr, AllReduceAnd, AllReduceMin, AllReduceMax, AllReduceMinI
   
   integer, public, save :: myrank, nproc
   logical, public, save :: riverPal

   type Range_array
     integer, pointer :: X(:,:)=>null(), Y(:,:)=>null()
   end type Range_array
   
   type proc_info
     !integer, pointer :: nx(:)=>null(), nrow(:)=>null(), YDim(:)=>null(), XDim(:)=>null(), nG(:)=>null()
     !integer, pointer :: Xrange(:,:)=>null(), Yrange(:,:)=>null()
     integer, pointer :: nx(:,:)=>null(), nrow(:,:)=>null(), YDim(:)=>null(), XDim(:)=>null(), nG(:)=>null()
     ! 2D array: [#proc, nL]
     type(Range_array), dimension(:), pointer :: Range=>null()
   end type proc_info

   type(proc_info), save, target :: pci
   
   interface gather2Ddat
      module procedure gather2DdatR,gather2DdatL
   end interface
   
   interface scatter2Ddat
      module procedure scatter2DdatR,scatter2DdatL
   end interface
   
   interface proc_use_ghost
      module procedure proc_use_ghostR,proc_use_ghostL
   end interface

  contains

   subroutine linkPciFromGd(gd_rt,nc_file) 
      implicit none
      type(gd), target :: gd_rt
      type(gd), pointer :: gdT, gdR
      character*(*), optional ::  nc_file
      integer :: i, nP
          
      if (present(nc_file)) then
         gdT => gd_rt .G. nc_file
      else
         gdT => gd_rt .G. 'proc'
      endif

      call getPtr(gdT,'nx',pci%nx)
      call getPtr(gdT,'nrow',pci%nrow)
      !call getPtr(gdT,'Yrange',pci%Yrange)
      !call getPtr(gdT,'Xrange',pci%Xrange)
      call getPtr(gdT,'XDim',pci%XDim)
      call getPtr(gdT,'YDim',pci%YDim)
      call getPtr(gdT,'nG',pci%nG)
      nP = countRepNum(gdT,'Range')
      allocate(pci%Range(nP)) 
      do i=1,nP
         call getptr(gdT,'Range',gdR,i)
         call getptr(gdR,'X',pci%Range(i)%X)
         call getptr(gdR,'Y',pci%Range(i)%Y)
      enddo
      
   end subroutine


   subroutine readPCInfo_2D(nc_file)
      implicit none 
      character*(*) ::  nc_file
      integer :: ierr

      call buildNC(nc_file)
      call linkPciFromGd(gd_base, nc_file)
      call MPI_Init(ierr) ! just have to initial in somewhere of this mod
      call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)

   end subroutine


   subroutine createPCInfo_2D(domainSize)

      implicit none
      integer :: domainSize(3)
      integer :: md, dm!,nproc
      integer :: i, ierr
      integer, pointer :: nx(:,:)=>null(), nrow(:,:)=>null(), Yrange(:,:)=>null()
      integer, pointer :: Xrange(:,:)=>null(), Ydim(:)=>null(),Xdim(:)=>null(),nG(:)=>null()
      integer :: ngd = 10, nbase = 10

      ! 2D domain decomposition
      ! fortran stores 2nd dim consecutively.
      ! we regard 1st dim as y, 2nd dim as x.
      call MPI_Init(ierr)
      call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
      allocate(nx(nproc,1))
      allocate(nrow(nproc,1))
      allocate(Yrange(2,nproc))
      allocate(Xrange(2,nproc))
      allocate(XDim(1))
      allocate(YDim(1))
      allocate(nG(1))

      Ydim(1) = domainSize(1)
      Xdim(1) = domainSize(2)
      nG(1) = domainSize(3)

      md = mod(Xdim(1)-2*nG(1),nproc)
      dm = (Xdim(1)-2*nG(1))/nproc
   !   write(*,*) md, dm
      do i=0,nproc-1
         ! nx: the number of columns the proc gets (not including ghost cells)
         if (i < md) then
            nx(i+1,1) = dm + 1
         else
            nx(i+1,1) = dm
         endif
         ! Yrange, Xrange: including ghost cell, the range of Y(1st), X(2nd) in ths proc's domain
         Yrange(1,i+1)=1; Yrange(2,i+1)=domainSize(1)  ! including ghost cells: 1 - msize(1). same for all procs.
         if (i==0) then
            Xrange(1,i+1)=1; Xrange(2,i+1)=nx(i+1,1)+2*nG(1) 
            ! including ghost cells: 1 - nx(0)+2, where:
            ! 1 is the domain's ghost cell in this direction,
            ! nx(0)+1 is 'calculating domain''s ending, so nx(0)+2 is the ghost cell for proc #0
         else
            !Xrange(1,i)=Xrange(2,i-1)-1; Xrange(2,i)=Xrange(1,i)+nx(i)+1
            ! The left ghost cell is the last proc's last 'calculating end'
            ! and the decompoced domain length in this direction is nx(i)+2
            ! or:
            Xrange(1,i+1)=Xrange(1,i)+nx(i,1); Xrange(2,i+1)=Xrange(2,i)+nx(i+1,1)
            ! this relation should be universal for any layers of ghost
         endif
         nrow(i+1,1)=(domainSize(1)-2*nG(1))*nx(i+1,1)
      enddo
      pci%nx => nx
      pci%nrow => nrow
! if (myrank ==0 )write(*,*) XRange
      pci%XDim => Xdim
      pci%YDim => YDim
      pci%nG => nG
      allocate(pci%range(1))
      pci%range(1)%Y => Yrange
      pci%range(1)%X => Xrange
      ! put into gd
      if (.not. associated (gd_base%p)) call allocate_gd(gd_base,nbase)
      i = gd_base%np + 1
      gd_base%f(i) = 'proc'
      call allocate_gd(gd_base%g(i),ngd)
      gd_base%np = i
      gd_base%g(i)%name(1) = 'proc'
      !!write(*,*) i
      !call addptr((/nproc/),'nx',nx,gd_base%g(i))   
      !call addptr((/nproc/),'nrow',nrow,gd_base%g(i))   
      !call addptr((/2,nproc/),'Yrange',Yrange,gd_base%g(i))   
      !call addptr((/2,nproc/),'Xrange',Xrange,gd_base%g(i))
      !call addptr((/1/),'XDim',Xdim,gd_base%g(i))   
      !call addptr((/1/),'YDim',Ydim,gd_base%g(i))   
      !call addptr((/1/),'nG',nG,gd_base%g(i))    

   end subroutine

   subroutine ovnCondOfProcs(nL,NC,Cond)
      use vdata, only: BC_type 
      implicit none
      integer :: NC, nL
      type(BC_type), dimension(NC) :: Cond
      integer :: Ns, newLength, i, j, L, offsetbuf, tSign, NNPos, NNPosL, upBound
      integer :: procRowBoundWG(2) ! it means for Cond%indices, the origin index is under global domain with Ghost layer.
      integer :: procRowBoundNG(2) ! it means for Cond%'some index', the origin index is under globally domain without ghost -> shrinked matrix
      integer :: ierr, alloc_err!, myrank
      integer, pointer :: idBuffer(:)=>null()
      !real*8, pointer :: datBuffer(:)=>null()
      !integer, pointer :: idNew(:)=>null()
      !real*8, pointer :: datNew(:)=>null()

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)

      !NC = size(Cond)
      do j = 1,NC
         
         if (Cond(j)%JType > 60) cycle ! another subroutine will take care of it 
          
         !L = size(Cond(j)%indices)
         procRowBoundWG(1) = (pci%Range(nL)%X(1,myrank+1) - 1) * pci%YDim(nL) + 1 
         procRowBoundWG(2) = pci%Range(nL)%X(2,myrank+1) * pci%YDim(nL)
         !write(*,*) myrank, procRowBoundWG     

         procRowBoundNG(1) = (pci%Range(nL)%X(1,myrank+1) - 1) * (pci%YDim(nL) - pci%nG(nL)*2) + 1 ! Don't include procs'Ghost here
         procRowBoundNG(2) = (pci%Range(nL)%X(1,myrank+1) + pci%nx(myrank+1,nL) - 1) * (pci%YDim(nL) - pci%nG(nL)*2)
         !write(*,*) myrank, procRowBoundNG     

         call CondIndBound(size(Cond(j)%indices),Cond(j)%indices, procRowBoundWG, Ns, newLength)

         ! special method for type 51: use BIndexIn2 to save indices
         if ((.not. associated(Cond(j)%BIndexIn2)) .and. Cond(j)%type .eq. 51) then
             allocate(Cond(j)%BIndexIn2(size(Cond(j)%indices)))
             Cond(j)%BIndexIn2 = Cond(j)%indices
         endif
         
         ! replace the data in proc
         allocate(idBuffer(newLength))
         offsetbuf = procRowBoundWG(1) - 1
         idBuffer = Cond(j)%indices(Ns:(Ns+newLength-1))
         call CondIndLocalize(Cond(j)%indices, newLength, idBuffer, offsetbuf)
         !write(*,*) myrank, 'i', size(Cond(j)%idx), Cond(j)%idx 
         deallocate(idBuffer)
         
         ! idx, idv
         ! I plan to do all-reduce, so it shouldn't have overlapping -> procRowBoundNG
         if (associated(Cond(j)%idxU)) then
             do i=1,size(Cond(j)%idxU,1)
                 tSign = 1
                 if (Cond(j)%idxU(i,1)<0) tSign = -1
                 if ((abs(Cond(j)%idxU(i,1)) > (procRowBoundWG(2)-pci%YDim(nL))) .or. (abs(Cond(j)%idxU(i,1)) < (procRowBoundWG(1)+pci%YDim(nL)-1))) then
                     Cond(j)%idxU(i,1) = 0
                 else
                     if (tSign > 0) then
                         Cond(j)%idxU(i,1) = Cond(j)%idxU(i,1) - offsetbuf
                     else
                         Cond(j)%idxU(i,1) = Cond(j)%idxU(i,1) + offsetbuf
                     endif
                 endif
             enddo
         endif
         
         if (associated(Cond(j)%idxV)) then
             do i=1,size(Cond(j)%idxV,1)
                 tSign = 1
                 if (Cond(j)%idxV(i,1)<0) tSign = -1
                 if ((abs(Cond(j)%idxV(i,1)) > (procRowBoundWG(2)-pci%YDim(nL))) .or. (abs(Cond(j)%idxV(i,1)) < (procRowBoundWG(1)+pci%YDim(nL)-1))) then
                     Cond(j)%idxV(i,1) = 0
                 else
                     if (tSign > 0) then
                         Cond(j)%idxV(i,1) = Cond(j)%idxV(i,1) - offsetbuf
                     else
                         Cond(j)%idxV(i,1) = Cond(j)%idxV(i,1) + offsetbuf
                     endif
                 endif
             enddo
         endif
         
         
         if (associated(Cond(j)%Cidx)) then
            if ((Cond(j)%JType .eq. 0)) then 
                call CondIndBound(size(Cond(j)%Cidx),Cond(j)%Cidx, procRowBoundWG, Ns, newLength)
                offsetbuf = procRowBoundWG(1) - 1 ! it should be the same for the remaining index.
                upBound = procRowBoundWG(2) - offsetbuf
            elseif ((Cond(j)%JType >= 5)) then 
                ! keep it
                ! upbound: the length is from indices, so the Xidx could exceed the range.
                offsetbuf = procRowBoundNG(1) - 1 ! it should be the same for the remaining index.
                upBound = procRowBoundNG(2) - offsetbuf
            else ! shrinked index
                call CondIndBound(size(Cond(j)%Cidx),Cond(j)%Cidx, procRowBoundNG, Ns, newLength)
                offsetbuf = procRowBoundNG(1) - 1 ! it should be the same for the remaining index.
                upBound = procRowBoundNG(2) - offsetbuf 
            endif
            allocate(idBuffer(newLength))
            idBuffer = Cond(j)%Cidx(Ns:(Ns+newLength-1))
            call CondIndLocalize(Cond(j)%Cidx, newLength, idBuffer, offsetbuf, upBound)
            deallocate(idBuffer)
         endif
         
         ! XY: it implies they just follow the Cidx's decomposition
         if (associated(Cond(j)%Widx)) then
             allocate(idBuffer(newLength))
             idBuffer = Cond(j)%Widx(Ns:(Ns+newLength-1))
             call CondIndLocalize(Cond(j)%Widx, newLength, idBuffer, offsetbuf, upBound)
             deallocate(idBuffer)    
         endif
         
         if (associated(Cond(j)%Nidx)) then
             allocate(idBuffer(newLength))
             idBuffer = Cond(j)%Nidx(Ns:(Ns+newLength-1))
             call CondIndLocalize(Cond(j)%Nidx, newLength, idBuffer, offsetbuf, upBound)
             deallocate(idBuffer)    
            !endif
         endif     
    
         if (associated(Cond(j)%Eidx)) then
             allocate(idBuffer(newLength))
             idBuffer = Cond(j)%Eidx(Ns:(Ns+newLength-1))
             call CondIndLocalize(Cond(j)%Eidx, newLength, idBuffer, offsetbuf, upBound)
             deallocate(idBuffer)  
         endif
         
         if (associated(Cond(j)%Sidx)) then
             allocate(idBuffer(newLength))
             idBuffer = Cond(j)%Sidx(Ns:(Ns+newLength-1))
             call CondIndLocalize(Cond(j)%Sidx, newLength, idBuffer, offsetbuf, upBound)
             deallocate(idBuffer)    
         endif         
                  
         if (associated(Cond(j)%BIndexIn1) .and. Cond(j)%type < 51) then ! ovnBCTypeBV needs it to be global
             call CondIndBound(size(Cond(j)%BIndexIn1),Cond(j)%BIndexIn1, procRowBoundWG, Ns, newLength)
             allocate(idBuffer(newLength))
             offsetbuf = procRowBoundWG(1) - 1
             idBuffer = Cond(j)%BindexIn1(Ns:(Ns+newLength-1))
             call CondIndLocalize(Cond(j)%BindexIn1, newLength, idBuffer, offsetbuf)
             !write(*,*) myrank, 'bi1',size(Cond(j)%BIndexIn1),Cond(j)%BIndexIn1 
             deallocate(idBuffer)
         endif
         
         if (associated(Cond(j)%BIndexIn2) .and. Cond(j)%type .ne. 51) then ! 51's BIndexIn2 should be empty 
             call CondIndBound(size(Cond(j)%BIndexIn2),Cond(j)%BIndexIn2, procRowBoundWG, Ns, newLength)
             allocate(idBuffer(newLength))
             offsetbuf = procRowBoundWG(1) - 1
             idBuffer = Cond(j)%BindexIn2(Ns:(Ns+newLength-1))
             call CondIndLocalize(Cond(j)%BIndexIn2, newLength, idBuffer, offsetbuf)
             !write(*,*) myrank,'bi2', size(Cond(j)%BIndexIn2),Cond(j)%BIndexIn2 
             deallocate(idBuffer)
         endif
      enddo
   end subroutine
   
   subroutine ovnCondOfProcs_60(nL,NC,Cond)
      use vdata, only: BC_type 
      implicit none
      integer :: NC, nL
      type(BC_type), dimension(NC) :: Cond
      integer :: Ns, newLength, i, j, L, offsetbuf, tSign, NNPos, NNPosL, upBound
      integer :: procRowBoundWG(2) ! it means for Cond%indices, the origin index is under global domain with Ghost layer.
      integer :: procRowBoundNG(2) ! it means for Cond%'some index', the origin index is under globally domain without ghost -> shrinked matrix
      integer :: ierr, alloc_err!, myrank
      integer, pointer :: idBuffer(:)=>null()
      !real*8, pointer :: datBuffer(:)=>null()
      !integer, pointer :: idNew(:)=>null()
      !real*8, pointer :: datNew(:)=>null()

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)

      !NC = size(Cond)
      do j = 1,NC
          
         !L = size(Cond(j)%indices)
         procRowBoundWG(1) = (pci%Range(nL)%X(1,myrank+1) - 1) * pci%YDim(nL) + 1 
         procRowBoundWG(2) = pci%Range(nL)%X(2,myrank+1) * pci%YDim(nL)
         write(*,*) myrank, procRowBoundWG     

         procRowBoundNG(1) = (pci%Range(nL)%X(1,myrank+1)) * pci%YDim(nL) + 1 ! Don't include one-side Ghost here
         procRowBoundNG(2) = (pci%Range(nL)%X(2,myrank+1) - 1) * pci%YDim(nL)
         write(*,*) myrank, procRowBoundNG  
         
         if (Cond(j)%JType > 60) then
             
             ! indices is the key, at the same time modified the BindexIn2
             ! When BindexIn2 is set -99, skip!
             ! BIndexIn1 & Exchflux untouched
             
             if (myrank .eq. 0) then
                 if (.not. associated(Cond(j)%Cidx)) allocate(Cond(j)%Cidx(size(Cond(j)%indices)))
                 Cond(j)%Cidx = Cond(j)%indices
                 do i=1,size(Cond(j)%indices)
                     if (Cond(j)%indices(i) > procRowBoundNG(2)) then
                         Cond(j)%BIndexIn2(i) = -99
                         if (Cond(j)%indices(i) > procRowBoundWG(2)) then
                             Cond(j)%indices(i) = -99
                         endif
                     !else
                     !    Cond(j)%indices(i) = Cond(j)%indices(i)
                     endif
                 end do                 
             elseif (myrank .eq. (nproc-1)) then
                 do i=1,size(Cond(j)%indices)
                     if (Cond(j)%indices(i) < procRowBoundNG(1)) then
                         Cond(j)%BIndexIn2(i) = -99
                         if (Cond(j)%indices(i) < procRowBoundWG(1)) then
                             Cond(j)%indices(i) = -99
                         else
                             Cond(j)%indices(i) = Cond(j)%indices(i) - procRowBoundWG(1) + 1
                         endif
                     else
                         Cond(j)%indices(i) = Cond(j)%indices(i) - procRowBoundWG(1) + 1
                     endif
                 end do                 
             else
                 do i=1,size(Cond(j)%indices)
                     if (Cond(j)%indices(i) < procRowBoundNG(1)) then
                         Cond(j)%BIndexIn2(i) = -99
                         if (Cond(j)%indices(i) < procRowBoundWG(1)) then
                             Cond(j)%indices(i) = -99
                         else
                             Cond(j)%indices(i) = Cond(j)%indices(i) - procRowBoundWG(1) + 1
                         endif
                     elseif(Cond(j)%indices(i) > procRowBoundNG(2)) then
                         Cond(j)%BIndexIn2(i) = -99
                         if (Cond(j)%indices(i) > procRowBoundWG(2)) then
                             Cond(j)%indices(i) = -99
                         else
                             Cond(j)%indices(i) = Cond(j)%indices(i) - procRowBoundWG(1) + 1
                         endif
                     else
                         Cond(j)%indices(i) = Cond(j)%indices(i) - procRowBoundWG(1) + 1
                     endif
                 end do
             endif
             
             write(272+myrank,*) myrank, '61-ind',Cond(j)%indices
             write(272+myrank,*) myrank, '61-iIn2',Cond(j)%BIndexIn2
             
         endif
         
      enddo
      
   end subroutine
   

   subroutine GWCondOfProcs(nL,NC,Cond)
      use vdata, only: BC_type 
      implicit none
      integer :: NC, nL
      type(BC_type), dimension(NC) :: Cond
      integer :: Ns, newLength, i, j, L, offsetbuf
      integer :: procRowBoundWG(2) ! it means for Cond%indices, the origin index is under global domain with Ghost layer.
      integer :: procRowBoundNG(2) ! it means for Cond%'some index', the origin index is under globally domain without ghost -> shrinked matrix
      integer :: ierr, alloc_err!, myrank
      integer, pointer :: idBuffer(:)=>null()
      real*8, pointer :: datBuffer(:)=>null()
      !integer, pointer :: idNew(:)=>null()
      !real*8, pointer :: datNew(:)=>null()

      call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)

      !NC = size(Cond)
      do j = 1,NC
         !L = size(Cond(j)%idx)
         !procRowBoundWG(1) = (pci%Xrange(1,myrank+1) - 1) * pci%YDim(1) + 1 
         !procRowBoundWG(2) = pci%Xrange(2,myrank+1) * pci%YDim(1)
         ! XY @ 2017/5/24: the index should count 'with ghost', but not include them.
         ! because it would be used to modify H/K/D
         procRowBoundWG(1) = pci%Range(nL)%X(1,myrank+1) * pci%YDim(nL) + 1 
         procRowBoundWG(2) = (pci%Range(nL)%X(2,myrank+1) - 1) * pci%YDim(nL)
         !write(*,*) myrank, procRowBoundWG     

         procRowBoundNG(1) = (pci%Range(nL)%X(1,myrank+1) - 1) * (pci%YDim(nL) - pci%nG(nL)*2) + 1 ! Don't include procs'Ghost here
         procRowBoundNG(2) = (pci%Range(nL)%X(1,myrank+1) + pci%nx(myrank+1,nL) - 1) * (pci%YDim(nL) - pci%nG(nL)*2)
         !write(*,*) myrank, procRowBoundNG     

         call CondIndBound(size(Cond(j)%indices),Cond(j)%indices, procRowBoundWG, Ns, newLength)
         !write(272+myrank,*) myrank, Ns, newLength
 
         ! replace the data in proc
         allocate(idBuffer(newLength))
         !offsetbuf = procRowBoundWG(1) - 1
         offsetbuf = procRowBoundWG(1) - 1 -pci%YDim(nL)
         idBuffer = Cond(j)%indices(Ns:(Ns+newLength-1))
         !deallocate(Cond(j)%idx, STAT = alloc_err) ! XY: this sentence doesn't solve memory issue! just leave here as reminder.
         !allocate(idNew(newLength))
         !nullify(Cond(j)%idx)
         !Cond(j)%idx => idNew
         !nullify(idNew)
         !where(idBuffer>0) Cond(j)%idx = idBuffer - offsetbuf
         call CondIndLocalize(Cond(j)%indices, newLength, idBuffer, offsetbuf)
         !write(272+myrank,*) myrank, size(Cond(j)%idx) 
         !write(272+myrank,*) '=='
         !write(272+myrank,*) Cond(j)%idx
         
         deallocate(idBuffer)

         if (associated(Cond(j)%Cidx)) then
            call CondIndBound(size(Cond(j)%Cidx),Cond(j)%Cidx, procRowBoundNG, Ns, newLength)
            !write(*,*) myrank, Ns, newLength
            allocate(idBuffer(newLength))
            offsetbuf = procRowBoundNG(1) - 1 ! it should be the same for the remaining index.
            idBuffer = Cond(j)%Cidx(Ns:(Ns+newLength-1))
            call CondIndLocalize(Cond(j)%Cidx, newLength, idBuffer, offsetbuf)
            deallocate(idBuffer)
            !write(*,*) myrank, Cond(j)%Cidx 
         endif

         ! K, D, H shares the same logic of index with Cond%Cidx
         ! temporarily just for jtype == 2, others would have significant problems.
         allocate(datBuffer(newLength))
         if (associated(Cond(j)%K)) then
            datBuffer = Cond(j)%K(Ns:(Ns+newLength-1))
            !allocate(datNew(newLength))
            !nullify(Cond(j)%K)
            !Cond(j)%K => datNew
            !nullify(datNew)
            !Cond(j)%K = datBuffer
            call CondNumLocalize(Cond(j)%K, newLength, datBuffer)
            !write(*,*) myrank, Cond(j)%K 
         endif

         if (associated(Cond(j)%H)) then
            datBuffer = Cond(j)%H(Ns:(Ns+newLength-1))
            call CondNumLocalize(Cond(j)%H, newLength, datBuffer)
            !write(*,*) myrank, Cond(j)%H 
         endif

         if (associated(Cond(j)%D)) then
            datBuffer = Cond(j)%D(Ns:(Ns+newLength-1))
            call CondNumLocalize(Cond(j)%D, newLength, datBuffer)
            !write(*,*) myrank, Cond(j)%D 
         endif

         deallocate(datBuffer)

         if (Cond(j)%jtype == 1) then
            if (associated(Cond(j)%Sidx)) then
               call CondIndBound(size(Cond(j)%Sidx),Cond(j)%Sidx, procRowBoundNG, Ns, newLength)
               allocate(idBuffer(newLength))
               idBuffer = Cond(j)%Sidx(Ns:(Ns+newLength-1))
               call CondIndLocalize(Cond(j)%Sidx, newLength, idBuffer, offsetbuf)
               deallocate(idBuffer)
               !write(*,*) myrank, Cond(j)%Sidx 
            endif

            if (associated(Cond(j)%Nidx)) then
               call CondIndBound(size(Cond(j)%Nidx),Cond(j)%Nidx, procRowBoundNG, Ns, newLength)
               allocate(idBuffer(newLength))
               idBuffer = Cond(j)%Nidx(Ns:(Ns+newLength-1))
               call CondIndLocalize(Cond(j)%Nidx, newLength, idBuffer, offsetbuf)
               deallocate(idBuffer)
               !write(*,*) myrank, Cond(j)%Nidx 
            endif

            if (associated(Cond(j)%Widx)) then
               call CondIndBound(size(Cond(j)%Widx),Cond(j)%Widx, procRowBoundNG, Ns, newLength)
               allocate(idBuffer(newLength))
               idBuffer = Cond(j)%Widx(Ns:(Ns+newLength-1))
               call CondIndLocalize(Cond(j)%Widx, newLength, idBuffer, offsetbuf)
               deallocate(idBuffer)
               !write(*,*) myrank, Cond(j)%Widx 
            endif

            if (associated(Cond(j)%Eidx)) then
               call CondIndBound(size(Cond(j)%Eidx),Cond(j)%Eidx, procRowBoundNG, Ns, newLength)
               write(*,*) myrank, Ns, newLength
               allocate(idBuffer(newLength))
               idBuffer = Cond(j)%Eidx(Ns:(Ns+newLength-1))
               call CondIndLocalize(Cond(j)%Eidx, newLength, idBuffer, offsetbuf)
               deallocate(idBuffer)
               !write(*,*) myrank, Cond(j)%Eidx 
            endif
         endif
      end do
   end subroutine


   subroutine proc_exchange_ghost(ny,nx,dat)
      ! now only for 2D, 1 ghost layer
      implicit none
      integer :: ny, nx ! the domain size (with ghost) in this proc
      real*8, dimension(ny,nx) :: dat
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: i, nG
      !real*8, dimension(ny) :: buffer
      !integer :: length, indLG, indRG

      ! tag is identified by receiver

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)
   
      !indLG = pci%Xrange(1,myrank+1)
      !indRG = pci%Xrange(2,myrank+1)
      if (nproc .eq. 1) return
      nG = pci%nG(1)
      ! exchange: send left internal, receive right ghost
      ! #0 has nothing to send, but need to receive
      ! #(nproc-1) has nothing to receive, but need to send
      
      !CP : better not assume myrank+1 or myrank-1, as it may change in the future. Use an object to store neighbor proc indices
      ! this way, you can encapsulate the procedure so that it can be upgraded later
      ! I would do: 
      ! (1) a service subroutine that returns (index_array,neighbor_proc) pairs, without assumptions about their spatial structures
      ! index_array can be used to point to any regularly-spaced or irregularly-spaced data, e.g., the top boundary which have nr:nr:(nc*nr)
      ! (2) then, automatically loop through the pairs and send/receive data. It does not involve assumptions about spatial organization.
      ! (3) the results of this service subroutine, after initiation, can be stored in pci. At the moment, the service subroutine can be as simple as left goes to myrank-1, right goes to myrank+1.
      ! (4) this data can be used in various different subroutines.
 
      do i=1,nG
         if (myrank == 0) then
            call MPI_Irecv(dat(:,nx-nG+i), ny, MPI_real8, myrank+1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Wait(recv_request, status)
         elseif (myrank == (nproc-1)) then
            !write(*,*) '**', dat(2:4,2),'***'
            call MPI_Send(dat(:,nG+i), ny, MPI_real8, myrank-1, myrank-1, MPI_COMM_WORLD, ierr)
         else
            !if (myrank == 1) write(*,*) dat(:,nx)
            call MPI_Irecv(dat(:,nx-nG+i), ny, MPI_real8, myrank+1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Send(dat(:,nG+i), ny, MPI_real8, myrank-1, myrank-1, MPI_COMM_WORLD, ierr)
            call MPI_Wait(recv_request, status)
         endif
      enddo
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      !if (myrank == 1) then
      !   write(*,*) '+++++++'
      !   write(*,*) dat(:,nx)  
      !endif

      ! exchange: send right internal, receive left ghost
      ! #0 has nothing to receive, but need to send
      ! #(nproc-1) has nothing to send, but need to receive
      do i=1,nG
         if (myrank == (nproc-1)) then
            !write(*,*) dat(:,1)
            call MPI_Irecv(dat(:,i), ny, MPI_real8, myrank-1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Wait(recv_request, status)
         elseif (myrank == 0) then
            !write(*,*) '**', dat(2:7,nx-1),'***'
            call MPI_Send(dat(:,nx-2*nG+i), ny, MPI_real8, myrank+1, myrank+1, MPI_COMM_WORLD, ierr)
         else
            !if (myrank == 1) write(*,*) '**', dat(:,1),'**'
            call MPI_Irecv(dat(:,i), ny, MPI_real8, myrank-1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Send(dat(:,nx-2*nG+i), ny, MPI_real8, myrank+1, myrank+1, MPI_COMM_WORLD, ierr)
            call MPI_Wait(recv_request, status)
         endif
      enddo
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      !if (myrank == 1) then
      !   write(*,*) '+++++++'
      !   write(*,*) dat(:,1)  
      !endif
   end subroutine

   subroutine proc_use_ghostL(ny,nx,dat)
      ! now only for 2D, 1 ghost layer
      ! this one use ghost to replace original value, only for logical now
      implicit none
      integer :: ny, nx ! the domain size (with ghost) in this proc
      logical, dimension(ny,nx) :: dat
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: i, nG
      !real*8, dimension(ny) :: buffer
      !integer :: length, indLG, indRG

      ! tag is identified by receiver

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)
   
      !indLG = pci%Xrange(1,myrank+1)
      !indRG = pci%Xrange(2,myrank+1)
      if (nproc .eq. 1) return
      nG = pci%nG(1)
      ! exchange_use: send left ghost, receive right internal
      ! #0 has nothing to send, but need to receive
      ! #(nproc-1) has nothing to receive, but need to send
      
      !CP : better not assume myrank+1 or myrank-1, as it may change in the future. Use an object to store neighbor proc indices
      ! this way, you can encapsulate the procedure so that it can be upgraded later
      ! I would do: 
      ! (1) a service subroutine that returns (index_array,neighbor_proc) pairs, without assumptions about their spatial structures
      ! index_array can be used to point to any regularly-spaced or irregularly-spaced data, e.g., the top boundary which have nr:nr:(nc*nr)
      ! (2) then, automatically loop through the pairs and send/receive data. It does not involve assumptions about spatial organization.
      ! (3) the results of this service subroutine, after initiation, can be stored in pci. At the moment, the service subroutine can be as simple as left goes to myrank-1, right goes to myrank+1.
      ! (4) this data can be used in various different subroutines.
 
      !do i=1,nG
         if (myrank == 0) then
            call MPI_Irecv(dat(:,nx-nG), ny, MPI_logical, myrank+1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Wait(recv_request, status)
         elseif (myrank == (nproc-1)) then
            !write(*,*) '**', dat(2:4,2),'***'
            call MPI_Send(dat(:,nG), ny, MPI_logical, myrank-1, myrank-1, MPI_COMM_WORLD, ierr)
         else
            !if (myrank == 1) write(*,*) dat(:,nx)
            call MPI_Irecv(dat(:,nx-nG), ny, MPI_logical, myrank+1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Send(dat(:,nG), ny, MPI_logical, myrank-1, myrank-1, MPI_COMM_WORLD, ierr)
            call MPI_Wait(recv_request, status)
         endif
      !enddo
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      !if (myrank == 1) then
      !   write(*,*) '+++++++'
      !   write(*,*) dat(:,nx)  
      !endif

      ! exchange_use: send right ghost, receive left internal
      ! #0 has nothing to receive, but need to send
      ! #(nproc-1) has nothing to send, but need to receive
      !do i=1,nG
         if (myrank == (nproc-1)) then
            !write(*,*) dat(:,1)
            call MPI_Irecv(dat(:,nG+1), ny, MPI_logical, myrank-1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Wait(recv_request, status)
         elseif (myrank == 0) then
            !write(*,*) '**', dat(2:7,nx-1),'***'
            call MPI_Send(dat(:,nx), ny, MPI_logical, myrank+1, myrank+1, MPI_COMM_WORLD, ierr)
         else
            !if (myrank == 1) write(*,*) '**', dat(:,1),'**'
            call MPI_Irecv(dat(:,nG+1), ny, MPI_logical, myrank-1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Send(dat(:,nx), ny, MPI_logical, myrank+1, myrank+1, MPI_COMM_WORLD, ierr)
            call MPI_Wait(recv_request, status)
         endif
      !enddo
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      !if (myrank == 1) then
      !   write(*,*) '+++++++'
      !   write(*,*) dat(:,1)  
      !endif
   end subroutine

   subroutine proc_use_ghostR(ny,nx,dat)
      ! now only for 2D, 1 ghost layer
      ! this one use ghost to replace original value, only for logical now
      implicit none
      integer :: ny, nx ! the domain size (with ghost) in this proc
      real*8, dimension(ny,nx) :: dat
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: i, nG
      !real*8, dimension(ny) :: buffer
      !integer :: length, indLG, indRG

      ! tag is identified by receiver

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)
   
      !indLG = pci%Xrange(1,myrank+1)
      !indRG = pci%Xrange(2,myrank+1)
      if (nproc .eq. 1) return
      nG = pci%nG(1)
      ! exchange_use: send left ghost, receive right internal
      ! #0 has nothing to send, but need to receive
      ! #(nproc-1) has nothing to receive, but need to send
      
      !CP : better not assume myrank+1 or myrank-1, as it may change in the future. Use an object to store neighbor proc indices
      ! this way, you can encapsulate the procedure so that it can be upgraded later
      ! I would do: 
      ! (1) a service subroutine that returns (index_array,neighbor_proc) pairs, without assumptions about their spatial structures
      ! index_array can be used to point to any regularly-spaced or irregularly-spaced data, e.g., the top boundary which have nr:nr:(nc*nr)
      ! (2) then, automatically loop through the pairs and send/receive data. It does not involve assumptions about spatial organization.
      ! (3) the results of this service subroutine, after initiation, can be stored in pci. At the moment, the service subroutine can be as simple as left goes to myrank-1, right goes to myrank+1.
      ! (4) this data can be used in various different subroutines.
 
      !do i=1,nG
         if (myrank == 0) then
            call MPI_Irecv(dat(:,nx-nG), ny, MPI_real8, myrank+1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Wait(recv_request, status)
         elseif (myrank == (nproc-1)) then
            !write(*,*) '**', dat(2:4,2),'***'
            call MPI_Send(dat(:,nG), ny, MPI_real8, myrank-1, myrank-1, MPI_COMM_WORLD, ierr)
         else
            !if (myrank == 1) write(*,*) dat(:,nx)
            call MPI_Irecv(dat(:,nx-nG), ny, MPI_real8, myrank+1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Send(dat(:,nG), ny, MPI_real8, myrank-1, myrank-1, MPI_COMM_WORLD, ierr)
            call MPI_Wait(recv_request, status)
         endif
      !enddo
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      !if (myrank == 1) then
      !   write(*,*) '+++++++'
      !   write(*,*) dat(:,nx)  
      !endif

      ! exchange_use: send right ghost, receive left internal
      ! #0 has nothing to receive, but need to send
      ! #(nproc-1) has nothing to send, but need to receive
      !do i=1,nG
         if (myrank == (nproc-1)) then
            !write(*,*) dat(:,1)
            call MPI_Irecv(dat(:,nG+1), ny, MPI_real8, myrank-1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Wait(recv_request, status)
         elseif (myrank == 0) then
            !write(*,*) '**', dat(2:7,nx-1),'***'
            call MPI_Send(dat(:,nx), ny, MPI_real8, myrank+1, myrank+1, MPI_COMM_WORLD, ierr)
         else
            !if (myrank == 1) write(*,*) '**', dat(:,1),'**'
            call MPI_Irecv(dat(:,nG+1), ny, MPI_real8, myrank-1, myrank, MPI_COMM_WORLD, recv_request, ierr)     
            call MPI_Send(dat(:,nx), ny, MPI_real8, myrank+1, myrank+1, MPI_COMM_WORLD, ierr)
            call MPI_Wait(recv_request, status)
         endif
      !enddo
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      !if (myrank == 1) then
      !   write(*,*) '+++++++'
      !   write(*,*) dat(:,1)  
      !endif
   end subroutine
   

   subroutine CondIndBound(L, idx, bound, Ns, newLength)
      ! service function
      ! given bound of index in this proc, it returns the corresponding index in proc#. 
      implicit none
      integer :: L
      integer :: Ns, bound(2), newLength
      integer, dimension(L) :: idx
      integer :: i
        
      Ns = 0
      !L = size(idx)
      do i=1,L
         if (idx(i) >= bound(1)) then
            Ns = i; exit 
         endif
      end do
      newLength = 0
      if (Ns .eq. 0) then
          return
      endif
      do i=Ns,L 
         if (idx(i) > bound(2)) then
            exit
         endif
         newLength = newLength + 1
      enddo
   end subroutine

   
   subroutine CondIndLocalize(idptr, newLength, buffer, offsetbuf, upBound)
      ! service function
      ! decouple cond's DMS. gd handles the old one, and computation code handles local one.
      implicit none
      integer :: newLength, offsetbuf
      integer, optional :: upBound
      integer, dimension(:), pointer, intent(INOUT) :: idptr
      integer, dimension(newLength) :: buffer
      integer, pointer :: idNew(:)=>null()
      
      allocate(idNew(newLength))
      nullify(idptr) ! decouple cond's DMS
      idptr => idNew
      nullify(idNew) 
      where(buffer>0) 
          idptr = buffer - offsetbuf
      elsewhere
          idptr = buffer
      endwhere
      
      if (present(upBound)) then
          where(idptr > upBound)
              idptr = -99
          endwhere
      endif
   
   end subroutine

   
   subroutine CondNumLocalize(valptr, newLength, buffer)
      ! service function
      ! decouple cond's DMS. gd handles the old one, and computation code handles local one.
      implicit none
      integer :: newLength, offsetbuf
      real*8, dimension(:), pointer, intent(INOUT) :: valptr
      real*8, dimension(newLength) :: buffer
      real*8, pointer :: valNew(:)=>null()
      
      allocate(valNew(newLength))
      nullify(valptr) ! decouple cond's DMS
      valptr => valNew
      nullify(valNew) 
      valptr = buffer
   
   end subroutine   
   
   
   subroutine gather2DdatNG(nL,datPiece,DAT,masterproc)
      ! gather other proc's data pieces to masterproc's DAT
      ! this subroutine didn't pass ghost cells
      implicit none
      integer :: ierr, recv_request!,myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: masterproc
      real*8, dimension(:,:), pointer :: datPiece, DAT
      integer :: nG, ny, nx, nny, nnx, i, nL
      integer, pointer :: counts(:)=>NULL(), displs(:)=>NULL() 

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      nG = pci%nG(1)
      ny = size(DAT, 1)
      nx = size(DAT, 2)
      nny = size(datPiece, 1)
      nnx = size(datPiece, 2)

      allocate(counts(nproc))
      allocate(displs(nproc))

      ! MPI doing things in C point of view
      ! it is said that sending array, start from 1;
      ! but for recv subarray, start from 0.
      do i=1, nproc
         counts(i) = pci%nrow(i,nL)
         if (i==1) then        
            displs(i) = 0
         else
            displs(i) = displs(i-1) + counts(i-1)
         endif
      enddo

      call MPI_Gatherv(datPiece((1+nG):(nny-nG),(1+nG):(nnx-nG)), counts(myrank+1), MPI_real8, &
         DAT((1+nG):(ny-nG),(1+nG):(nx-nG)), counts, displs, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)

   end subroutine

   subroutine gather2DdatR(nL,nny,nnx,datPiece,ny,nx,DAT,masterproc)
      ! gather other proc's data pieces to masterproc's DAT
      use mgTemp_mod
      implicit none
      integer :: nG, ny, nx, nny, nnx, i, nL
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: masterproc
      !real*8, dimension(:,:), pointer :: datPiece, DAT, datPass
      real*8, dimension(nny,nnx) :: datPiece
      real*8, dimension(ny,nx) :: DAT
      real*8, dimension(:,:), pointer :: datPass=>NULL() 
      integer, pointer :: counts(:)=>NULL(), displs(:)=>NULL() 

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      nG = pci%nG(nL)
      !ny = size(DAT, 1)
      !nx = size(DAT, 2)
      !nny = size(datPiece, 1)
      !nnx = size(datPiece, 2)

      allocate(counts(nproc))
      allocate(displs(nproc))
      
      datPass => mg_Base(nL)%datPass

      if (myrank == 0) then
         !allocate(datPass(nny,nnx-nG))
         datPass = datPiece(:,1:(nnx-nG))
      elseif (myrank == nproc-1) then
         !allocate(datPass(nny,nnx-nG))
         datPass = datPiece(:,(1+nG):nnx)
      else
         !allocate(datPass(nny,nnx-2*nG))
         datPass = datPiece(:,(1+nG):(nnx-nG))
      endif

      !write(myrank+272,*) myrank, size(datPass)
      !write(myrank+272,*) myrank, datPass

      ! MPI doing things in C point of view
      ! it is said that sending array, start from 1;
      ! but for recv subarray, start from 0.
      do i=1, nproc
         if (i==1) then        
            counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1 - nG)
            displs(i) = 0
         elseif (i==nproc) then
            counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1 - nG)
            displs(i) = displs(i-1) + counts(i-1)
         else
            counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1 - 2*nG)
            displs(i) = displs(i-1) + counts(i-1)
         endif
      enddo
      !write(272+myrank,*) counts
      !write(272+myrank,*) displs

      call MPI_Gatherv(datPass, counts(myrank+1), MPI_real8, DAT, counts, displs, MPI_real8, masterproc, MPI_COMM_WORLD, ierr)

      !deallocate(datPass)
      nullify(datPass)
      deallocate(counts)
      deallocate(displs)

   end subroutine
   
   subroutine gather2DdatL(nL,nny,nnx,datPiece,ny,nx,DAT,masterproc)
      ! gather other proc's data pieces to masterproc's DAT
      use mgTemp_mod
      implicit none
      integer :: nG, ny, nx, nny, nnx, i, nL
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: masterproc
      !real*8, dimension(:,:), pointer :: datPiece, DAT, datPass
      logical, dimension(nny,nnx) :: datPiece
      logical, dimension(ny,nx) :: DAT
      logical, dimension(:,:), pointer :: datPass=>NULL() 
      integer, pointer :: counts(:)=>NULL(), displs(:)=>NULL() 

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      nG = pci%nG(nL)
      !ny = size(DAT, 1)
      !nx = size(DAT, 2)
      !nny = size(datPiece, 1)
      !nnx = size(datPiece, 2)

      allocate(counts(nproc))
      allocate(displs(nproc))
      
      datPass => mg_Base(nL)%datPassL

      if (myrank == 0) then
         !allocate(datPass(nny,nnx-nG))
         datPass = datPiece(:,1:(nnx-nG))
      elseif (myrank == nproc-1) then
         !allocate(datPass(nny,nnx-nG))
         datPass = datPiece(:,(1+nG):nnx)
      else
         !allocate(datPass(nny,nnx-2*nG))
         datPass = datPiece(:,(1+nG):(nnx-nG))
      endif

      !write(myrank+272,*) myrank, size(datPass)
      !write(myrank+272,*) myrank, datPass

      ! MPI doing things in C point of view
      ! it is said that sending array, start from 1;
      ! but for recv subarray, start from 0.
      do i=1, nproc
         if (i==1) then        
            counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1 - nG)
            displs(i) = 0
         elseif (i==nproc) then
            counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1 - nG)
            displs(i) = displs(i-1) + counts(i-1)
         else
            counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1 - 2*nG)
            displs(i) = displs(i-1) + counts(i-1)
         endif
      enddo
      !write(272+myrank,*) counts
      !write(272+myrank,*) displs

      call MPI_Gatherv(datPass, counts(myrank+1), MPI_logical, DAT, counts, displs, MPI_logical, masterproc, MPI_COMM_WORLD, ierr)

      !deallocate(datPass)
      nullify(datPass)
      deallocate(counts)
      deallocate(displs)

   end subroutine

   subroutine scatter2DdatR(nL,ny,nx,DAT,nny,nnx,datPiece,masterproc)
      ! scatter masterproc's DAT to other proc's data pieces 
      implicit none
      integer :: nG, ny, nx, nny, nnx, i, nL
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: masterproc
      !real*8, dimension(:,:), pointer :: datPiece, DAT, datPass
      real*8, dimension(nny,nnx) :: datPiece
      real*8, dimension(ny,nx) :: DAT
      !real*8, dimension(:,:), pointer :: datPass
      integer, pointer :: counts(:)=>NULL(), displs(:)=>NULL() 

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      nG = pci%nG(nL)
      !ny = size(DAT, 1)
      !nx = size(DAT, 2)
      !nny = size(datPiece, 1)
      !nnx = size(datPiece, 2)

      allocate(counts(nproc))
      allocate(displs(nproc))

      !if (myrank == 0) then
      !   allocate(datPass(nny,nnx-nG))
      !   datPass = datPiece(:,1:(nnx-nG))
      !elseif (myrank == nproc-1) then
      !   allocate(datPass(nny,nnx-nG))
      !   datPass = datPiece(:,(1+nG):nnx)
      !else
      !   allocate(datPass(nny,nnx-2*nG))
      !   datPass = datPiece(:,(1+nG):(nnx-nG))
      !endif

      !write(*,*) myrank, size(datPass)

      ! MPI doing things in C point of view
      ! it is said that sending array, start from 1;
      ! but for recv subarray, start from 0.
      do i=1, nproc
         counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1)
         displs(i) = pci%YDim(nL) * (pci%Range(nL)%X(1,i) - 1) 
      enddo
      !write(*,*) counts
      !write(*,*) displs

      call MPI_Scatterv(DAT, counts, displs, MPI_real8, datPiece, counts(myrank+1), MPI_real8, masterproc, MPI_COMM_WORLD, ierr)

      !deallocate(datPass)
      deallocate(counts)
      deallocate(displs)

   end subroutine
   
   subroutine scatter2DdatL(nL,ny,nx,DAT,nny,nnx,datPiece,masterproc)
      ! scatter masterproc's DAT to other proc's data pieces 
      implicit none
      integer :: nG, ny, nx, nny, nnx, i, nL
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: masterproc
      !real*8, dimension(:,:), pointer :: datPiece, DAT, datPass
      logical, dimension(nny,nnx) :: datPiece
      logical, dimension(ny,nx) :: DAT
      !real*8, dimension(:,:), pointer :: datPass
      integer, pointer :: counts(:)=>NULL(), displs(:)=>NULL() 

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      nG = pci%nG(nL)
      !ny = size(DAT, 1)
      !nx = size(DAT, 2)
      !nny = size(datPiece, 1)
      !nnx = size(datPiece, 2)

      allocate(counts(nproc))
      allocate(displs(nproc))

      !if (myrank == 0) then
      !   allocate(datPass(nny,nnx-nG))
      !   datPass = datPiece(:,1:(nnx-nG))
      !elseif (myrank == nproc-1) then
      !   allocate(datPass(nny,nnx-nG))
      !   datPass = datPiece(:,(1+nG):nnx)
      !else
      !   allocate(datPass(nny,nnx-2*nG))
      !   datPass = datPiece(:,(1+nG):(nnx-nG))
      !endif

      !write(*,*) myrank, size(datPass)

      ! MPI doing things in C point of view
      ! it is said that sending array, start from 1;
      ! but for recv subarray, start from 0.
      do i=1, nproc
         counts(i) = pci%YDim(nL) * (pci%Range(nL)%X(2,i)-pci%Range(nL)%X(1,i) + 1)
         displs(i) = pci%YDim(nL) * (pci%Range(nL)%X(1,i) - 1) 
      enddo
      !write(*,*) counts
      !write(*,*) displs

      call MPI_Scatterv(DAT, counts, displs, MPI_logical, datPiece, counts(myrank+1), MPI_logical, masterproc, MPI_COMM_WORLD, ierr)

      !deallocate(datPass)
      deallocate(counts)
      deallocate(displs)

   end subroutine

    subroutine buildNC(nc_file)
      ! read a workspace saved in nc_file into (gd_base .G. nc_file)
      use netcdf
      implicit none
      integer, parameter :: cLen = 100
      integer*4 num, nbase
      character*(cLen) names
      integer   i, status, np
      integer ntemp,j
      character*(*) nc_file
      integer :: ncid, varid, dimid, numVar
      integer nG, nsG

      nG =10
      nsG = 100

      status = nf90_open(nc_file, NF90_NOWRITE, ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_open"
      end if

      status = nf90_inquire(ncid, nvariables = numVar)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_inquire"
      end if

      ! Get variable name
      !do i=1,numVar
      !    status = nf90_inquire_variable(ncid,i,name = names(i))
      !    if (status /= nf90_noerr) then 
      !        print *, trim(nf90_strerror(status))
      !        stop "Stopped in nf90_inquire_variable"
      !    end if
      !enddo
      
      nbase = 10
      if (.not. associated(gd_base%p)) call allocate_gd(gd_base,nbase)
      gd_base%name(1) = 'base'
      np = gd_base%np + 1   
      gd_base%f(np) = nc_file
      gd_base%np    = np
     
      if (.not. associated(gd_base%g)) then
         allocate(gd_base%g(nbase))
      endif
      call allocate_gd(gd_base%g(np),nsG)
      gd_base%g(np)%name(1) = nc_file
      
      do i=1,numVar
         status = nf90_inquire_variable(ncid,i,name = names)
         if (status /= nf90_noerr) then 
             print *, trim(nf90_strerror(status))
             stop "Stopped in nf90_inquire_variable"
         end if
         call connectField(gd_base%g(np),i,names,ncid)
      enddo
      
      status = nf90_close(ncid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
      
    end subroutine

    recursive subroutine connectField(main,dat,field,ncid,gdname)
      ! add "field" to the gd table "main" by connecting to "dat"
      ! so that main%f(np+1)=field and main%p(np+1)%ptr => dat (unwrapped from mx)
      ! XY: now dat = varid
      use netcdf
      implicit none
      type(gd),target:: main
      integer m,n,j
      character(*),intent(in)::field
      character(*),intent(in),optional::gdname
      integer :: dat, ncid
      
      integer*4 fieldnum,nfields,nallo
      integer*4 i
      integer :: ndims, status, varid, xtype
      integer :: dimids(NF90_MAX_VAR_DIMS)
      logical :: islogic, isreal, isint, ischar
      
      real*8, pointer :: data_R1(:) => null()
      real*8, pointer :: data_R2(:,:) => null()
      real*8, pointer :: data_R3(:,:,:) => null()
      integer, pointer :: data_I1(:) => null()
      integer, pointer :: data_I2(:,:) => null()
      integer, pointer :: data_I3(:,:,:) => null()

      
      integer di(6), ns(6), flag_2D, ind_X, flagX, flagY!, myrank
      integer numel
      integer :: dims(6) = (/0, 0, 0, 0, 0, 0/)
      integer indD, indB1, indB2, Seq
      integer, parameter::maxStrLen=100
      character(maxStrLen) :: subname, mdName, mdName2, mdSeq
      character*(maxStrLen), pointer :: namestr=>null()
      integer :: nsG=10

     
      integer, pointer :: startInd(:)=>null(), countInd(:)=>null() 
      
      if (dat .eq. 0) return ! empty!!

      varid = dat
      isreal  = .false.
      islogic = .false.
      isint   = .false.
      ischar  = .false.

      ! Hierarchy judgement
      indD = index(field,'.')      
      if (indD > 0) then! there is hierarchy
          mdname = field(1:indD-1)
          subname = field(indD+1:)
        
          allocate(namestr)
          if (present(gdname)) then
              namestr = trim(gdname) // trim('.')// trim(mdname)
          else
              namestr = trim(main%name(1)) // trim('.')// trim(mdname)
          endif 
          
          ! structure array judgement
          indB1 = index(mdname,'(')
          Seq = 1
          if (indB1 > 0) then
              indB2 = index(mdname,')')
              mdname2 = mdname(1:indB1-1)
              mdSeq = mdname(indB1+1:indB2-1)
              Seq = str2num(trim(mdSeq))
              mdname = mdname2
          end if
          
          ! search if it exists
          i = searchStr(main%f,mdname,main%np,Seq)
          if (i<=0) then ! create it
              if (.not. associated(main%g)) then
                  allocate(main%g(nsG))
              endif
              n = main%np + 1
              call allocate_gd(main%g(n))
              main%g(n)%name(1) = namestr
              main%f(n) = mdname
              main%np = n
          else ! point to it
              n = i
          endif
          deallocate(namestr)
          call connectField(main%g(n), dat, subname, ncid)    
      else      
          status = nf90_inquire_variable(ncid, varid, xtype=xtype, ndims=ndims, dimids=dimids)
          do i = 1, ndims
             status = nf90_inquire_dimension(ncid, dimids(i), len=dims(i))
          end do

          di = dims
          
          allocate(startInd(ndims))
          allocate(countInd(ndims))
          startInd = 1
          do j=1,ndims
             countInd(j) = dims(j)
          enddo
          numel = dims(1)

          ! datatype and get data
          if ((xtype .eq. NF90_DOUBLE) .or. (xtype .eq. NF90_FLOAT)) then
              isreal = .true.
              n = main%np + 1
              select case (ndims)
              case (1)
                  allocate(data_R1(countInd(1)))
                  status = nf90_get_var(ncid, varid, data_R1, start=startInd, count=countInd)              
                  if (status /= nf90_noerr) then                   
                      print *, trim(nf90_strerror(status))                  
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR1(numel,data_R1,main,n)
              case (2)
                  allocate(data_R2(countInd(1),countInd(2)))
                  status = nf90_get_var(ncid, varid, data_R2, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR2(countInd,data_R2,main,n)
              case (3)
                  allocate(data_R3(countInd(1),countInd(2),countInd(3)))
                  status = nf90_get_var(ncid, varid, data_R3, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignR3(countInd,data_R3,main,n)
              end select
              main%f(n) = field
              main%np = n
          elseif ((xtype .eq. NF90_SHORT) .or. (xtype .eq. NF90_INT) .or.  (xtype .eq. NF90_BYTE)) then
              isint = .true.
              n = main%np + 1
              select case (ndims)
              case (1)
                  allocate(data_I1(countInd(1)))
                  status = nf90_get_var(ncid, varid, data_I1, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI1(numel,data_I1,main,n)
              case (2)
                  allocate(data_I2(countInd(1),countInd(2)))
                  status = nf90_get_var(ncid, varid, data_I2, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI2(countInd,data_I2,main,n)
              case (3)
                  allocate(data_I3(countInd(1),countInd(2),countInd(3)))
                  status = nf90_get_var(ncid, varid, data_I3, start=startInd, count=countInd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_get_var"
                  end if
                  call assignI3(countInd,data_I3,main,n)
              end select
              main%f(n) = field
              main%np = n
          end if
   
          deallocate(startInd)
          deallocate(countInd)
      endif ! hierarchy judgement
      
    end subroutine

   subroutine riverTrib(ridUp, ridDn, dir)
      ! request for upstream data of current step
      ! in rLateral.f90(recv), r_MacCormack.f90(send)
      ! Tag: upstream -> downstream ID
      use vdata, only: r, w
      implicit none
      integer :: ridUp, ridDn, direction
      integer, optional :: dir 
      integer :: ierr, recv_request(3)!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: OtherNode, tag, i, k, nn, k1, k2
      integer :: Node1, Node2

      tag = 10 * ridUp + ridDn

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)
   
      if (present(dir)) then
          if (dir == 0) then
              direction = 0
              do i= 1, size(w%CR)
                  if (w%CR(i) .eq. ridUp) then
                      k = i
                      exit
                  endif
              enddo
          else
              direction = 1
              do i= 1, size(w%CR)
                  if (w%CR(i) .eq. ridDn) then
                      k = i
                      exit
                  endif
              enddo
          endif
          OtherNode = mod(k-1, nproc)
          if (OtherNode .eq. myrank) return
      else
          nn = 0
          do i= 1, size(w%CR)
              if (w%CR(i) .eq. ridUp) then
                  k1 = i
                  nn = nn + 1
              endif
              if (w%CR(i) .eq. ridDn) then
                  k2 = i
                  nn = nn + 1
              endif
              if (nn > 1) exit
          enddo
          Node1 = mod(k1-1, nproc)
          Node2 = mod(k2-1, nproc)
          if (Node1 .eq. Node2) return
          if (myrank .eq. Node1)  then
              direction = 1! I am upstream
              OtherNode = Node2
          elseif (myrank .eq. Node2) then
              direction = 0! I am downstream
              OtherNode = Node1
          else
              return ! none of my business
          endif
      endif

      if (direction == 0) then ! recving, so I am downstream
         call MPI_Irecv(r(ridUp)%Riv%t, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(1), ierr) 
         call MPI_Irecv(r(ridUp)%Riv%dt, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(2), ierr) 
         call MPI_Irecv(r(ridUp)%Net%DSQ, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(3), ierr) 
         call MPI_Wait(recv_request(1), status)
         call MPI_Wait(recv_request(2), status)
         call MPI_Wait(recv_request(3), status)
      else ! sending, so I am Upstream
         call MPI_Send(r(ridUp)%Riv%t, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
         call MPI_Send(r(ridUp)%Riv%dt, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
         call MPI_Send(r(ridUp)%Net%DSQ, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr)
         r(ridUp)%Net%DSQ = 0D0
      endif

   end subroutine

   subroutine riverDS(ridUp, ridDn, dir)
      ! request for downstream data from last step
      ! in rLateral.f90(recv), r_MacCormack.f90(send)
      ! Tag: downstream -> upstream ID
      use vdata, only: r, w
      implicit none
      integer :: ridUp, ridDn, direction
      integer, optional :: dir 
      integer :: ierr, recv_request(5)!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: OtherNode, tag, i, k, nn, k1, k2
      integer :: Node1, Node2

      tag = 10 * ridDn + ridUp

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      if (present(dir)) then
          if (dir == 0) then
              direction = 0
              do i= 1, size(w%CR)
                  if (w%CR(i) .eq. ridUp) then
                      k = i
                      exit
                  endif
              enddo
          else
              direction = 1
              do i= 1, size(w%CR)
                  if (w%CR(i) .eq. ridDn) then
                      k = i
                      exit
                  endif
              enddo
          endif
          OtherNode = mod(k-1, nproc)
          if (OtherNode .eq. myrank) return
      else
          nn = 0
          do i= 1, size(w%CR)
              if (w%CR(i) .eq. ridUp) then
                  k1 = i
                  nn = nn + 1
              endif
              if (w%CR(i) .eq. ridDn) then
                  k2 = i
                  nn = nn + 1
              endif
              if (nn > 1) exit
          enddo
          Node1 = mod(k1-1, nproc)
          Node2 = mod(k2-1, nproc)
          if (Node1 .eq. Node2) return
          if (myrank .eq. Node1)  then
              direction = 0! I am upstream
              OtherNode = Node2
          elseif (myrank .eq. Node2) then
              direction = 1! I am downstream
              OtherNode = Node1
          else
              return ! none of my business
          endif
      endif

      if (direction == 0) then ! recving, so I am Upstream
         call MPI_Irecv(r(ridDn)%Riv%t, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(1), ierr) 
         call MPI_Irecv(r(ridDn)%Riv%dt, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(2), ierr) 
         call MPI_Irecv(r(ridDn)%Riv%h, size(r(ridDn)%Riv%h), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(3), ierr) 
         call MPI_Irecv(r(ridDn)%Riv%w, size(r(ridDn)%Riv%w), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(4), ierr) 
         call MPI_Irecv(r(ridDn)%Riv%E, size(r(ridDn)%Riv%E), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(5), ierr) 
         call MPI_Wait(recv_request(1), status)
         call MPI_Wait(recv_request(2), status)
         call MPI_Wait(recv_request(3), status)
         call MPI_Wait(recv_request(4), status)
         call MPI_Wait(recv_request(5), status)
      else ! sending, so I am downstream
         call MPI_Send(r(ridDn)%Riv%t, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
         call MPI_Send(r(ridDn)%Riv%dt, 1, MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
         call MPI_Send(r(ridDn)%Riv%h, size(r(ridDn)%Riv%h), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
         call MPI_Send(r(ridDn)%Riv%w, size(r(ridDn)%Riv%w), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
         call MPI_Send(r(ridDn)%Riv%E, size(r(ridDn)%Riv%E), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
      endif

   end subroutine

   subroutine Aggregate1Ddat(nn,DAT,masterproc)
      ! gather other proc's data pieces to masterproc's DAT
      implicit none
      integer :: nn, i
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: masterproc
      !real*8, dimension(:,:), pointer :: datPiece, DAT, datPass
      !real*8, dimension(nn) :: datPiece
      real*8, dimension(nn) :: DAT
      real*8, dimension(:), pointer :: datTemp => null()

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      if (myrank == masterproc) then
         allocate(datTemp(nn))
         datTemp = 0D0
      endif
      !write(*,*) myrank, size(datPass)

      call MPI_reduce(DAT,datTemp,nn,MPI_real8,MPI_SUM,masterproc,MPI_COMM_WORLD, ierr)
      
      ! MPI doing things in C point of view
      ! it is said that sending array, start from 1;
      ! but for recv subarray, start from 0.
      !do i=0,nproc-1
      !   if (i .ne. masterproc) then
      !      if (myrank == masterproc) then ! receive
      !         call MPI_Irecv(datTemp, nn, MPI_real8, i, i, MPI_COMM_WORLD, recv_request, ierr)     
      !         call MPI_Wait(recv_request, status)
      !         DAT = DAT + datTemp
      !         datTemp = 0D0
      !      elseif (myrank == i) then
      !         call MPI_Send(DAT, nn, MPI_real8, masterproc, i, MPI_COMM_WORLD, ierr)
      !      endif
      !   endif
      !   !call MPI_Barrier(MPI_COMM_WORLD,ierr)
      !enddo

      if (myrank == masterproc) then
         DAT = datTemp
         deallocate(datTemp)
      endif

   end subroutine  
   
   subroutine Aggregate2Ddat(nny,nnx,DAT,masterproc)
      ! gather other proc's data pieces to masterproc's DAT
      implicit none
      integer :: nG, nny, nnx, i
      integer :: ierr, recv_request!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: masterproc
      !real*8, dimension(:,:), pointer :: datPiece, DAT, datPass
      !real*8, dimension(nny,nnx) :: datPiece
      real*8, dimension(nny,nnx) :: DAT
      real*8, dimension(:,:), pointer :: datTemp => null()

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      if (myrank == masterproc) then
         allocate(datTemp(nny,nnx))
         datTemp = 0D0
      endif
      !write(*,*) myrank, size(datPass)

      ! MPI doing things in C point of view
      ! it is said that sending array, start from 1;
      ! but for recv subarray, start from 0.
      do i=0,nproc-1
         if (i .ne. masterproc) then
            if (myrank == masterproc) then ! receive
               call MPI_Irecv(datTemp, nny*nnx, MPI_real8, i, i, MPI_COMM_WORLD, recv_request, ierr)     
               call MPI_Wait(recv_request, status)
               DAT = DAT + datTemp
               datTemp = 0D0
            elseif (myrank == i) then
               call MPI_Send(DAT, nny*nnx, MPI_real8, masterproc, i, MPI_COMM_WORLD, ierr)
            endif
         endif
         call MPI_Barrier(MPI_COMM_WORLD,ierr)
      enddo

      if (myrank == masterproc) then
         deallocate(datTemp)
      endif

   end subroutine

   subroutine riverRunPatch(k, nr, thisrun1, thisrun2)
   ! return the beg and end index (in w.CR, not river's number) under this step of loop
   ! beg: thisrun1, end: thisrun 2
   ! k: the index (in w.CR) that ran by that proc.
   ! nr: number of rivers
      implicit none
      integer :: k, nr, thisrun1, thisrun2, ierr, mrun!, nproc
      
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)
      mrun = (k-1)/nproc + 1 ! this is the mrun-th loop
      thisrun2 = nproc*mrun
      thisrun1 = thisrun2 - nproc + 1
      thisrun2 = min(thisrun2, nr)
   
   end subroutine
   
      subroutine riverDSh(ridUp, ridDn, dir)
      ! request for downstream data : especially for F_oc_dw step
      ! so only pass riv%h
      ! in rLateral.f90(recv), r_MacCormack.f90(send)
      ! Tag: downstream -> upstream ID
      use vdata, only: r, w
      implicit none
      integer :: ridUp, ridDn, direction
      integer, optional :: dir 
      integer :: ierr, recv_request(1)!, myrank, nproc
      integer status(MPI_STATUS_SIZE)
      integer :: OtherNode, tag, i, k, nn, k1, k2
      integer :: Node1, Node2

      tag = 10 * ridDn + ridUp

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,ierr)

      if (present(dir)) then
          if (dir == 0) then
              direction = 0
              do i= 1, size(w%CR)
                  if (w%CR(i) .eq. ridUp) then
                      k = i
                      exit
                  endif
              enddo
          else
              direction = 1
              do i= 1, size(w%CR)
                  if (w%CR(i) .eq. ridDn) then
                      k = i
                      exit
                  endif
              enddo
          endif
          OtherNode = mod(k-1, nproc)
          if (OtherNode .eq. myrank) return
      else
          nn = 0
          do i= 1, size(w%CR)
              if (w%CR(i) .eq. ridUp) then
                  k1 = i
                  nn = nn + 1
              endif
              if (w%CR(i) .eq. ridDn) then
                  k2 = i
                  nn = nn + 1
              endif
              if (nn > 1) exit
          enddo
          Node1 = mod(k1-1, nproc)
          Node2 = mod(k2-1, nproc)
          if (Node1 .eq. Node2) return
          if (myrank .eq. Node1)  then
              direction = 0! I am upstream
              OtherNode = Node2
          elseif (myrank .eq. Node2) then
              direction = 1! I am downstream
              OtherNode = Node1
          else
              return ! none of my business
          endif
      endif

      if (direction == 0) then ! recving, so I am Upstream
         call MPI_Irecv(r(ridDn)%Riv%h, size(r(ridDn)%Riv%h), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, recv_request(1), ierr) 
         call MPI_Wait(recv_request(1), status)
      else ! sending, so I am downstream
         call MPI_Send(r(ridDn)%Riv%h, size(r(ridDn)%Riv%h), MPI_real8, OtherNode, tag, MPI_COMM_WORLD, ierr) 
      endif

   end subroutine
   
   function AllReduceOr(logVal)
   ! MPI_AllReduce for logical or operation
      implicit none
      logical AllReduceOr, logVal
      logical logGlo
      integer :: ierr
      
      logGlo = .false.
      call MPI_Allreduce(logVal,logGlo,1,MPI_logical,MPI_LOR,MPI_COMM_WORLD,ierr)
      AllReduceOr = logGlo

   end function

   function AllReduceAnd(logVal)
   ! MPI_AllReduce for logical and operation
      implicit none
      logical AllReduceAnd, logVal
      logical logGlo
      integer :: ierr
      
      logGlo = .false.
      call MPI_Allreduce(logVal,logGlo,1,MPI_logical,MPI_LAND,MPI_COMM_WORLD,ierr)
      AllReduceAnd = logGlo

   end function
   
   function AllReduceMax(Val)
   ! MPI_AllReduce for max operation
      implicit none
      real*8 AllReduceMax, Val
      real*8 valGlo
      integer :: ierr
      
      valGlo = 0D0
      call MPI_Allreduce(Val,valGlo,1,MPI_real8,MPI_MAX,MPI_COMM_WORLD,ierr)
      AllReduceMax = valGlo

   end function
      
   function AllReduceMin(Val)
   ! MPI_AllReduce for min operation
      implicit none
      real*8 AllReduceMin, Val
      real*8 valGlo
      integer :: ierr
      
      valGlo = 0D0
      call MPI_Allreduce(Val,valGlo,1,MPI_real8,MPI_MIN,MPI_COMM_WORLD,ierr)
      AllReduceMin = valGlo

   end function
   
   function AllReduceMinI(Val)
   ! MPI_AllReduce for min operation
      implicit none
      integer AllReduceMinI, Val
      integer valGlo
      integer :: ierr
      
      valGlo = 0D0
      call MPI_Allreduce(Val,valGlo,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,ierr)
      AllReduceMinI = valGlo

   end function
   
   
   function allReduce1D(Val,nx)
   ! MPI_AllReduce for min operation
      implicit none
      integer nx
      real*8 allReduce1D(nx), Val(nx)
      real*8 valGlo(nx)
      integer :: ierr
      
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      valGlo = 0D0
      call MPI_Allreduce(Val,valGlo,nx,MPI_real8,MPI_SUM,MPI_COMM_WORLD,ierr)
      allReduce1D = valGlo

   end function
   
   function allReduce0D(Val)
   ! MPI_AllReduce for min operation
      implicit none
      integer nx
      real*8 allReduce0D, Val
      real*8 valGlo
      integer :: ierr
      
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      valGlo = 0D0
      call MPI_Allreduce(Val,valGlo,1,MPI_real8,MPI_SUM,MPI_COMM_WORLD,ierr)
      allReduce0D = valGlo

   end function
   
   function bcast0D(Val,rroot)
   ! MPI_AllReduce for min operation
      implicit none
      integer rroot
      real*8 bcast0D, Val
      integer :: ierr
      
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      call MPI_Bcast(Val, 1, MPI_real8, rroot, MPI_COMM_WORLD, ierr)
      bcast0D = Val

   end function
   
   subroutine ExchFluxReduce(BC,gid)
   ! taking care of cond%ExchFlux
      use vdata, only: BC_type, oned_ptr, gA
      implicit none
      integer :: gid, ierr, leng, i
      type(BC_type) :: BC
      real*8, dimension(:), pointer :: gloColumn => null(), S1d
      integer, dimension(:), pointer :: putCondIdx, idxDir
      integer :: masterproc = 0
      real*8, dimension(pci%YDim(gid), pci%XDim(gid)) :: S2D
      integer :: procRowBoundNG(2), procRowBoundWG(2)
      
      leng = size(BC%ExchFlux,1)
      allocate(gloColumn(leng))
      gloColumn = 0D0
      
      ! now get the value
      if (BC%Jtype < 60) then
          call MPI_Reduce(BC%ExchFlux(:,4),gloColumn,leng,MPI_real8,MPI_SUM,masterproc,MPI_COMM_WORLD,ierr)
          if (myrank == masterproc) BC%ExchFlux(:,4) = gloColumn
          call MPI_Barrier(MPI_COMM_WORLD,ierr)
          call MPI_Reduce(BC%ExchFlux(:,3),gloColumn,leng,MPI_real8,MPI_SUM,masterproc,MPI_COMM_WORLD,ierr)
          if (myrank == masterproc) BC%ExchFlux(:,3) = gloColumn
          deallocate(gloColumn)
          call gather2Ddat(gid,size(gA(gid)%OVN%S,1), size(gA(gid)%OVN%S,2), gA(gid)%OVN%S, pci%YDim(gid),pci%XDim(gid),S2D,masterproc)
          if (myrank == masterproc) then
              idxDir => BC%BIndexIn1
              S1d => oned_ptr(size(S2D),S2D)
              if (size(BC%idxV,1)>0) then
                  ! V directional flux
                  putCondIdx => BC%idxV(:,2); ! When we talk about flux, there is X and there is Y direction
                  ! this flux is in the unit of m2 (Qy)
                  ! going from m3 to m/s
                  S1d(idxDir(putCondIdx)) = S1d(idxDir(putCondIdx)) +  BC%ExchFlux(putCondIdx,4)/ (gA(gid)%OVN%dt * 86400.0D0 * gA(gid)%DM%d(1)**2)
              endif
              if (size(BC%idxU,1)>0) then
                  putCondIdx => BC%idxU(:,2); ! When we talk about flux, there is X and there is Y direction
                  S1d(idxDir(putCondIdx)) = S1d(idxDir(putCondIdx)) +  BC%ExchFlux(putCondIdx,3)/ (gA(gid)%OVN%dt * 86400.0D0 * gA(gid)%DM%d(1)**2)
              endif
              nullify(S1d)
          endif
          call MPI_Barrier(MPI_COMM_WORLD,ierr)
          call scatter2Ddat(gid,pci%YDim(gid),pci%XDim(gid),S2D,size(gA(gid)%OVN%S,1), size(gA(gid)%OVN%S,2), gA(gid)%OVN%S, masterproc)
      elseif (BC%Jtype > 60) then
          do i = 2, 5
              call MPI_Allreduce(BC%ExchFlux(:,i),gloColumn,leng,MPI_real8,MPI_SUM,MPI_COMM_WORLD,ierr)
              BC%ExchFlux(:,i) = gloColumn
          enddo
          deallocate(gloColumn)
          procRowBoundNG(1) = pci%Range(gid)%X(1,myrank+1) * pci%YDim(gid) + 1 ! Don't include procs'Ghost here
          procRowBoundNG(2) = (pci%Range(gid)%X(2,myrank+1) - 1) * pci%YDim(gid)
          procRowBoundWG(1) = (pci%Range(gid)%X(1,myrank+1) - 1) * pci%YDim(gid) + 1
          procRowBoundWG(2) = pci%Range(gid)%X(2,myrank+1) * pci%YDim(gid)
          do i = 1, leng
              if ((BC%ExchFlux(i,1) < procRowBoundNG(1) ) .or. (BC%ExchFlux(i,1) > procRowBoundNG(2)) ) then
                  BC%ExchFlux(i,6) = -99
              else
                  BC%ExchFlux(i,6) = BC%ExchFlux(i,1) - procRowBoundWG(1) + 1
              endif
          enddo
      endif
      
   end subroutine
   
   subroutine ovnBCTypeBV(BC, me, it)
   ! taking care of BV setting -- some mesh/proc crossing problem
      use vdata 
      implicit none
      type(BC_type) :: BC
      integer :: me, it
      integer :: ierr, partleng, NNPos
      integer, dimension(:),pointer :: idxIt,idxMe,idNew=>null()
      real*8, dimension(:),pointer :: hNeighbor, ENeighbor, ENeighborMe, BVTemp=>null()
      real*8, dimension(:,:),pointer :: TopoE=>null()
      real*8, dimension(pci%YDim(it), pci%XDim(it)) :: ovnH2DIt, Topo2DIt
      integer, save :: topoApp = 0
      integer, save :: Ns, newLength
      integer :: procRowBoundWG(2)
      integer :: masterproc = 0
      
      if (BC%Jtype .eq. 51) then
          idxIt => BC%BIndexIn1
          idxMe => BC%BIndexIn2
          partleng = size(BC%indices)
          
          procRowBoundWG(1) = (pci%Range(me)%X(1,myrank+1) - 1) * pci%YDim(me) + 1 
          procRowBoundWG(2) = pci%Range(me)%X(2,myrank+1) * pci%YDim(me)
          
          allocate(BVTemp(size(idxMe)))
          call gather2Ddat(it,size(gA(it)%OVN%h,1), size(gA(it)%OVN%h,2), gA(it)%OVN%h, pci%YDim(it),pci%XDim(it),ovnH2DIt,masterproc)
          call gather2Ddat(it,size(gA(it)%Topo%E,1), size(gA(it)%Topo%E,2), gA(it)%Topo%E, pci%YDim(it),pci%XDim(it),Topo2DIt,masterproc)
          if (topoApp < 1) then
              allocate(TopoE(pci%YDim(me), pci%XDim(me)))
              call gather2Ddat(me,size(gA(me)%Topo%E,1), size(gA(me)%Topo%E,2), gA(me)%Topo%E, pci%YDim(me),pci%XDim(me),TopoE,masterproc)
              call CondIndBound(size(BC%BIndexIn2),BC%BIndexIn2, procRowBoundWG, Ns, newLength) ! Just evaluate Ns and newlwngth
          endif
          if (myrank .eq. masterproc) then
              hNeighbor => oneD_Ptr(size(ovnH2DIt), ovnH2DIt)
              ENeighbor => oneD_Ptr(size(Topo2DIt), Topo2DIt)
              BVTemp = hNeighbor(idxIt)  + ENeighbor(idxIt)
              if (topoApp < 1) then
                  ENeighborMe => oneD_Ptr(size(TopoE), TopoE)
                  ENeighborMe(idxMe) = ENeighbor(idxIt)
                  nullify(ENeighborMe)
              endif
              nullify(hNeighbor)
              nullify(ENeighbor)
          endif
          call MPI_Barrier(MPI_COMM_WORLD,ierr)
          call MPI_Bcast(BVTemp, size(BVTemp), MPI_real8, masterproc, MPI_COMM_WORLD, ierr)
          BC%BV(1:partleng) = BVTemp(Ns:(Ns+newLength-1))
          
          if (topoApp < 1) then
              call scatter2Ddat(me,pci%YDim(me),pci%XDim(me),TopoE,size(gA(me)%Topo%E,1), size(gA(me)%Topo%E,2), gA(me)%Topo%E, masterproc)
              topoApp = 2
              deallocate(TopoE)
          endif
          deallocate(BVTemp) 
      elseif (BC%Jtype .eq. 61) then
          idxIt => BC%BIndexIn1
          !it = BC%nm_idx(1)
          idxMe => BC%Cidx
          call gather2Ddat(it,size(gA(it)%Topo%E,1), size(gA(it)%Topo%E,2), gA(it)%Topo%E, pci%YDim(it),pci%XDim(it),Topo2DIt,masterproc)
          
          allocate(TopoE(pci%YDim(me), pci%XDim(me)))
          call gather2Ddat(me,size(gA(me)%Topo%E,1), size(gA(me)%Topo%E,2), gA(me)%Topo%E, pci%YDim(me),pci%XDim(me),TopoE,masterproc)
          if (myrank .eq. masterproc) then
              ENeighbor => oneD_Ptr(size(Topo2DIt), Topo2DIt)
              ENeighborMe => oneD_Ptr(size(TopoE), TopoE)
              where (idxIt > 0)
                  ENeighborMe(idxMe) = ENeighbor(idxIt)
              end where
          endif
          call MPI_Barrier(MPI_COMM_WORLD,ierr)
          call scatter2Ddat(me,pci%YDim(me),pci%XDim(me),TopoE,size(gA(me)%Topo%E,1), size(gA(me)%Topo%E,2), gA(me)%Topo%E, masterproc)
          nullify(ENeighborMe)
          nullify(ENeighbor)
          nullify(idxIt)
          nullify(idxMe)
          deallocate(TopoE)
      endif
      
   
   end subroutine
   
   
end module proc_mod


