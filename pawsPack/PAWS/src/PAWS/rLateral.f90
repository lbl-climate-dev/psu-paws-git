#include <preproc.h>
!
      subroutine rLateral(rid, kRiv)
      ! (E) Compute Source Term and Boundary Conditions for the River Segments-
      ! Source Term: (m3/s/m)
      ! 1.Prcp; (m/s) --*w
      ! 2.Evaporation; (m/day)--*w/86400
      ! 3.Overland Inflow; 4.Tributaries (cms); --/dx
      ! 5.Groundwater Seepage/Contribution (m3/day/m) /86400
      ! 6.Upstream/Downstream BC
      ! Units of all source terms are m2/day (m3/hr/m). so Sprcp = prcp*w
      ! We are not expanding Sprcp when top width increases with increasing flow-
      ! with the thought that the prcp landing in the adjacent cell will-
      ! contribute that water anyway. In stead of doing thing on both sides,
      ! just ignore this step.
      ! Currently, overland exchange is directly applied in F_oc_dw, rather-
      ! than a source term. r.Riv.Qoc = 0 at all times. So this is now mainly-
      ! computing tributary inflows
      ! Input:
      ! tributary flows: r.Riv.Qx
      ! Output: 
      ! r(rid).Riv.S and upstream, downstream boundary conditions
      
      use Vdata
      use polyEval, Only: polyInterp1
#ifdef NC
      use proc_mod, only: riverPal, riverTrib, riverDS, riverRunPatch, myrank
      use memoryChunk, Only:tempStorage
#endif
      implicit none
      integer rid, kRiv
      integer, dimension(:), pointer :: rL=>NULL()
      real*8 :: dt, dx, dt2, DTT, nn, frac,dtInflow
      integer i, l, idx, k
      integer, parameter :: n_poly = 2 ! order of accuracy will be n_poly - 1
      integer :: mode=1
      real*8,save :: MX(n_poly,n_poly), tt1(10)
      real*8 :: P(n_poly),ints(n_poly),interval(2),v1,res,t1
      integer, save :: state = 0
      real*8,pointer::tt(:),vv(:)
      integer :: thisrun1, thisrun2, kk,ns=10
      real*8,dimension(:),pointer::gOVNh1D=>NULL()
      
      ! Compute Tributary Contributions (m3/s)
      dt = r(rid)%Riv%dt
      dx = r(rid)%DM%d(1)
      DO i=1,size(w%CR)
          IF (w%CR(I) .eq. rid) then
              k = i
              EXIT
          endif
      ENDDO
      if (associated(r(rid)%Net%Trib)) then
        rL => r(rid)%Net%Trib
#ifdef NC
        if (riverPal) then
         !DO i=1,size(w%CR)
         !   IF (w%CR(I) .eq. rid) then
         !       k = i
         !       EXIT
         !   endif
         !ENDDO
         call riverRunPatch(k, size(w%CR), thisrun1, thisrun2)
        endif
#endif
        do i = 1, size(rL)
          l = rL(i)
#ifdef NC 
          if (riverPal) then
           do kk = thisrun1, k-1! since rivers should be ordered from up to down
              if (w%CR(kk) .eq. l) then ! not ran yet
                  call riverTrib(l, rid, 0)
                  exit
              endif  
           enddo
          endif
#endif
          idx = r(rid)%Net%TribC(i);
          ! Ab = r(rid).Riv.Ab(idx);
      
          dt2 = r(l)%Riv%dt
          DTT = max(dt, dt2)
          r(rid)%Riv%trib(idx) = r(rid)%Riv%trib(idx)+r(l)%Net%DSQ/(DTT/dt2)
          r(l)%Net%DSQ = 0
          !/Ab; % DSQ already upscaled. This is NOT normalized to Ab
        enddo
      endif

      ! Obtain Upstream and Downstream boundary conditions
      !if ~isempty(r(rid).Net.UPS) ! Upstream BC most likely Flux BC
      if(associated(r(rid)%Net%UPS)) then
        if (r(rid)%Net%UPS>size(r) .AND. r(rid)%Net%UPS > 1E6) then
        ! encoding of UPS: 10000+idx.mode (mode =0 for daily forward flow)
        ! currently requires usgs.t to be evenly spaced, meaning: integrated value for the day/interval
          ! if r(rid)%Net%UPS>1 million, then it means this is a recorded inflow condition
          ! r(rid)%Net%UPS - 1e6 gives us the id of the w.tData.usgs station number to retrieve the data from
          ! we put dt of inflow condition in the fraction. if it is 0, then dt is 1.
          ! note the data in the usgs time series must be consecutive
          ! r(rid)%Net%UPST = 2 r(rid)%Net%UPST is not valid
          r(rid)%Riv%UPST = 2D0;
          idx = int(r(rid)%Net%UPS - 1D6 + 1D-14) !r(rid)%Net%UPS is real*8
          vv=>w%tData%usgs(idx)%v
          tt=>w%tData%usgs(idx)%t
          mode = (r(rid)%Net%UPS - 1D6 - idx)*10 
          call polyInterp1(size(vv),tt,vv,(/ r(rid)%Riv%t, r(rid)%Riv%t+r(rid)%Riv%dt /),res,mode,n_poly,MX,state)
          r(rid)%Riv%UPSV = max(res,0D0) ! near 0, sometimes integral interpolation can give negative results
          
          !frac = (r(rid)%Riv%t - w%tData%usgs(idx)%v(k))/(w%tData%usgs(idx)%v(k+1)-w%tData%usgs(idx)%v(k)) ! linearly interpolate
          !r(rid)%Riv%UPSV = (1D0-frac) * w%tData%usgs(idx)%v(k) + frac * w%tData%usgs(idx)%v(k+1)
        else
          r(rid)%Net%UPST = 2
          l = r(rid)%Net%UPS
          nn= r(l)%DM%msize(1) - r(l)%DM%nGho(1)
          r(rid)%Riv%Ac(1) = r(l)%Riv%Ac(int(nn))
          r(rid)%Riv%UPSV = r(l)%Net%DSQ
        endif
      else
        r(rid)%Riv%UPST = 0.0D0
      end if

      ! now support fixed head Downstream BC
      !if ~isempty(r(rid).Net.DS)
      if(.NOT. associated(r(rid)%Net%DS)) then
        !if(r(rid)%Riv%DST .ne. 5) r(rid)%Riv%DST = 3      ! Free flowing BC !20180606
        if(r(rid)%Riv%DST < 5) r(rid)%Riv%DST = 3      ! Free flowing BC
      !if(.NOT. associated(r(rid)%Net%DS)) then
      !    r(rid)%Riv%DST = 3      ! Free flowing BC
      else  
        if (r(rid)%Net%DS > 0D0) then 
            r(rid)%Riv%DST = 4D0
            l = r(rid)%Net%DS
!#ifdef NC
!            if (ini .eq. 0) then
!               call riverDS(rid, l, 0)
!            else
!               ini = 0 ! first time entering: no need to update.
!            endif
!#endif
            idx = r(rid)%Net%DSC
            r(rid)%Riv%DSV(1) = max(r(l)%Riv%h(idx),0.0)
            !r(rid).Riv.DSV(1) = max(r(l).Riv.EstE(idx)-r(l).Riv.E(idx),0);
            r(rid)%Riv%DSV(2) = r(rid)%Riv%Qx(r(rid)%DM%msize(1)-1) ! Q from last time step. added w 01/30/2014
            r(rid)%Riv%w(size(r(rid)%Riv%E)) = r(l)%Riv%w(idx) ! added w 01/30/2014
            r(rid)%Riv%wF(size(r(rid)%Riv%E)) = r(l)%DM%d(1)      ! downstream dx. added w 01/30/2014. This is a lazy-add, wF(nx) means width at the interface, but it doesn't do anything in the code. To be formalized later
            r(rid)%Riv%E(size(r(rid)%Riv%E)) = r(l)%Riv%E(idx) 
            !######### NEED TO THINK ABOUT THE VALIDITY OF ONE EXTRA CELL HERE
            !######### SHOULD JUST MAKE IT AN OVERLAPPING CELL
        elseif (r(rid)%Net%DS .eq. 0D0) then 
            r(rid)%Riv%DST = 0
        endif    
      end if

      ! Sum up Source Terms
      r(rid)%Riv%S = 0.0D0
      r(rid)%Riv%S = r(rid)%Riv%prcp * r(rid)%Riv%w + (r(rid)%Riv%Qoc + r(rid)%Riv%trib)/dx +  &
     &   r(rid)%Riv%Qgc + r(rid)%Riv%Ql - r(rid)%Riv%evap       ! dA/dt
      ! Currently, only trib matters because Qoc is applied directly in F_oc_dw, prcp is 0 (rained on ground)
      ! Qgc is solved inside riv_cv, Ql and evap are 0s. trib has unit of m2/s because DSQ has m3/s
      
      !4;
      ! if ~isempty(find(r(rid).Riv.S<0))
      !     warning('5');
      ! end
      ! Clear Transitional Fluxes
      !em = zeros(size(r(rid).Riv.h));
      
      ! DSV = [1.h, 2. Qx, 3. predictedH, 4.Elevation, 5.s, 6.dx, 7-n. sumQ(t--> t-k)]
      
      !if (r(rid)%Riv%DST .eq. 5 .AND. kRiv .eq. 1) then !20180606
      if (r(rid)%Riv%DST .ge. 5 .AND. kRiv .eq. 1) then
          idx = g%Riv(k)%Lidx(size(g%Riv(k)%Lidx))
          tt1 =0D0
          ns = size(r(rid)%Riv%DSV)

#ifdef NC
          call tempStorage('ovnHFull',gOVNh1D,1)
          !r(rid)%Riv%DSV(1) = gOVNh1D(idx)
          !if (r(rid)%Riv%DST .eq. 5) then
          if (r(rid)%Riv%DST .ge. 5) then
              r(rid)%Riv%DSV(1) = (gOVNh1D(idx) + r(rid)%Riv%DSV(9))*0.5d0
          else
              r(rid)%Riv%DSV(1) = gOVNh1D(idx)
          endif
          r(rid)%Riv%DSV(2) = 0D0
          r(rid)%Riv%DSV(3) = 0D0
          
          nullify(gOVNh1D)
          call tempStorage('TopoEFull',gOVNh1D,1)
          r(rid)%Riv%DSV(4) = gOVNh1D(idx)
          r(rid)%Riv%E(size(r(rid)%Riv%E)) = r(rid)%Riv%DSV(4)
          
          nullify(gOVNh1D)
          call tempStorage('ovnSFull',gOVNh1D,1)
          r(rid)%Riv%DSV(5) = gOVNh1D(idx)
          
          nullify(gOVNh1D)
          call tempStorage('ovnQxFull',gOVNh1D,1)
          tt1(1) = gOVNh1D(idx-g%OVN%ms(1)) ! Qx(i,j-1), positive is inflow (m2/s)
          tt1(2) = gOVNh1D(idx) ! Qx(i,j), positive is outflow
          
          nullify(gOVNh1D)
          call tempStorage('ovnQyFull',gOVNh1D,1)
          tt1(3) = gOVNh1D(idx-1) ! Qy(i-1,j), positive is inflow (m2/s)
          tt1(4) = gOVNh1D(idx) ! Qy(i,j), positive is outflow
          r(rid)%Riv%DSV(6) = g%DM%d(1) 
          
          r(rid)%Riv%DSV(8:ns) = r(rid)%Riv%DSV(7:ns-1)
          r(rid)%Riv%DSV(7) = tt1(1)-tt1(2)+tt1(3)-tt1(4)
          !t1 = r(rid)%Riv%DSV(6) - r(rid)%Riv%DSV(7) + r(rid)%Riv%DSV(8) - r(rid)%Riv%DSV(9) 
          
          nullify(gOVNh1D)
          call tempStorage('ovnUFull',gOVNh1D,1)
          if(tt1(1) .ne. 0D0) tt1(5) = gOVNh1D(idx-g%OVN%ms(1)) ! u(i,j-1), positive is inflow (m/s)
          if(tt1(2) .ne. 0D0) tt1(6) = gOVNh1D(idx) ! u(i,j), positive is outflow
          
          nullify(gOVNh1D)
          call tempStorage('ovnVFull',gOVNh1D,1)
          if(tt1(3) .ne. 0D0) tt1(7) = gOVNh1D(idx-1) ! v(i-1,j), positive is inflow (m/s)
          if(tt1(4) .ne. 0D0) tt1(8) = gOVNh1D(idx) ! v(i,j), positive is outflow
          r(rid)%Riv%DSV(ns) = tt1(5)-tt1(6)+tt1(7)-tt1(8)
          
          r(rid)%Riv%DSV(9) = gOVNh1D(idx) ! smooth, by saving h
#else
          gOVNh1D => OneD_ptr(size(g%OVN%h),g%OVN%h)
          !r(rid)%Riv%DSV(1) = gOVNh1D(idx)
          !if (r(rid)%Riv%DST .eq. 5) then
          if (r(rid)%Riv%DST .ge. 5) then
              r(rid)%Riv%DSV(1) = (gOVNh1D(idx) + r(rid)%Riv%DSV(9))*0.5d0
          else
              r(rid)%Riv%DSV(1) = gOVNh1D(idx)
          endif
          r(rid)%Riv%DSV(2) = 0D0
          r(rid)%Riv%DSV(3) = 0D0
          
          gOVNh1D => OneD_ptr(size(g%Topo%E),g%OVN%E)
          r(rid)%Riv%DSV(4) = gOVNh1D(idx)
          r(rid)%Riv%E(size(r(rid)%Riv%E)) = r(rid)%Riv%DSV(4)
          
          gOVNh1D => OneD_ptr(size(g%OVN%S),g%OVN%S)
          r(rid)%Riv%DSV(5) = gOVNh1D(idx)
          
          gOVNh1D => OneD_ptr(size(g%OVN%Qx),g%OVN%Qx)
          tt1(1) = gOVNh1D(idx-g%OVN%ms(1)) ! Qx(i,j-1), positive is inflow (m2/s)
          tt1(2) = gOVNh1D(idx) ! Qx(i,j), positive is outflow
          
          gOVNh1D => OneD_ptr(size(g%OVN%Qy),g%OVN%Qy)
          tt1(3) = gOVNh1D(idx-1) ! Qy(i-1,j), positive is inflow (m2/s)
          tt1(4) = gOVNh1D(idx) ! Qy(i,j), positive is outflow
          r(rid)%Riv%DSV(6) = g%DM%d(1) 
          
          r(rid)%Riv%DSV(8:ns) = r(rid)%Riv%DSV(7:ns-1)
          r(rid)%Riv%DSV(7) = tt1(1)-tt1(2)+tt1(3)-tt1(4)
          !t1 = r(rid)%Riv%DSV(6) - r(rid)%Riv%DSV(7) + r(rid)%Riv%DSV(8) - r(rid)%Riv%DSV(9) 
          
          gOVNh1D => OneD_ptr(size(g%OVN%U),g%OVN%U)
          if(tt1(1) .ne. 0D0) tt1(5) = gOVNh1D(idx-g%OVN%ms(1)) ! u(i,j-1), positive is inflow (m/s)
          if(tt1(2) .ne. 0D0) tt1(6) = gOVNh1D(idx) ! u(i,j), positive is outflow
          
          gOVNh1D => OneD_ptr(size(g%OVN%V),g%OVN%V)
          if(tt1(3) .ne. 0D0) tt1(7) = gOVNh1D(idx-1) ! v(i-1,j), positive is inflow (m/s)
          if(tt1(4) .ne. 0D0) tt1(8) = gOVNh1D(idx) ! v(i,j), positive is outflow
          r(rid)%Riv%DSV(ns) = tt1(5)-tt1(6)+tt1(7)-tt1(8)
          
          r(rid)%Riv%DSV(9) = gOVNh1D(idx) ! smooth, by saving h
          
          !write(272+rid,FMT='(4E20.10)') w%t, r(rid)%Riv%DSV(7), sum(r(rid)%Riv%S), r(rid)%Riv%DSV(ns) !20180606
#endif          
          
          
          nullify(gOVNh1D)
      endif

      end subroutine rLateral

      subroutine setOvnBC(gid,lastLoc)
      ! this subroutine (1) connects internal boundary conditions between possible grids
      ! (2) prepare some BCs, e.g., tidal BCs so that the BV is correctly set
      use vdata
      use polyEval
#ifdef NC
      use proc_mod, only: ExchFluxReduce, myrank, ovnBCTypeBV
#endif
      implicit none
      
      type(OV_type),pointer :: OV
      TYPE(BC_type),dimension(:),pointer :: BBC
      TYPE(BC_type),pointer :: BC_UP
      integer gid
      integer i,j
      real*8 lastLoc(size(gA(gid)%OVN%Cond))
      real*8 res, loc
      integer :: nm
      real*8,dimension(:,:),pointer:: Qx,Qy,SRC,h
      integer,dimension(:),pointer::nIndex
      integer,dimension(:),pointer::idxDir,idxMe,putCondIdx,ExtIdx
      integer,dimension(:,:),pointer::idxV,idxU,idx
      real*8,dimension(:),pointer :: hNeighbor, ENeighbor, S1d, ENeighborMe
      integer, save :: topoApp = 0
      !real*8 :: outp(4)
      
      OV => gA(gid)%OVN
      do i=1,size(OV%Cond)
         BBC =>  OV%Cond
         SRC => gA(gid)%OVN%S
         S1d => oned_ptr(size(SRC),SRC)
         !h  => gA(gid)%OVN%h
         
         select case (BBC(i)%JType)
             
            case(5)  ! time-dependent Dirichlet BC
                 if (.not. associated(BBC(i)%BV)) allocate(BBC(i)%BV(size(BBC(i)%V,2))) 
                 do j=1,size(BBC(i)%v,2)
                   call linearInterp1d(BBC(i)%t,BBC(i)%v(:,j),w%t+OV%dt/2D0,res,loc,lastLoc(i))
                   BBC(i)%BV(j) = res
                   lastloc(i) = loc
                 enddo
                 if (associated(OV%c)) then
                     idxMe => BBC(i)%indices
                     ENeighborMe => oneD_Ptr(size(OV%h), OV%c)
                     ENeighborMe(idxMe) = 1D0
                 endif
                 !write(UNIT=1997,FMT='(4f16.4)') w%t,BBC(i)%BV
                !endif
            case(51)  ! Exchange BC where Upstream gets a DBC, and downstream gets a source term
                      ! 51 is an upstream BC. And all information can be found here.
                nm = BBC(i)%nm_idx(1)
                Qx => gA(nm)%OVN%Qx
                Qy => gA(nm)%OVN%Qy
                idxDir => BBC(i)%BIndexIn1  ! These are valid cells on neighbor grid
                nm = BBC(i)%nm_idx(1)
                idxMe => BBC(i)%indices  ! These could be ghost cells
                if (.not. associated(BBC(i)%BV)) allocate(BBC(i)%BV(size(idxMe)*3) )
#ifdef NC
                BBC(i)%BV = 0D0
                call ovnBCTypeBV(BBC(i), gid, nm)
#else
                hNeighbor => oneD_Ptr(size(gA(nm)%OVN%h), gA(nm)%OVN%h)
                !ENeighbor => oneD_Ptr(size(gA(nm)%OVN%h), gA(nm)%Topo%E)
                !ENeighborMe => oneD_Ptr(size(gA(gid)%OVN%h), gA(gid)%Topo%E)
                ENeighbor => oneD_Ptr(size(gA(nm)%OVN%h), gA(nm)%OVN%E)
                ENeighborMe => oneD_Ptr(size(gA(gid)%OVN%h), gA(gid)%OVN%E)
                
                ENeighborMe(idxMe) = ENeighbor(idxDir)
                BBC(i)%BV(1:size(idxMe)) = hNeighbor(idxDir)  + ENeighbor(idxDir) ! It's basically a Dirichlet BC
                BBC(i)%BV(size(idxMe)+1:size(idxMe)*2) = 0D0 ! It's basically a Dirichlet BC
                BBC(i)%BV(size(idxMe)*2+1:size(idxMe)*3) = 0D0 ! It's basically a Dirichlet BC
                ! idxMe is implemented in ApplyCondOVN
#endif                
            case(52)  ! Exchange BC where downstream should now get a source term boost
                nm = BBC(i)%nm_idx(1)
                do j=1,size(gA(nm)%OVN%Cond)
                    if (gA(nm)%OVN%Cond(j)%type .eq. 51 .AND. gA(nm)%OVN%Cond(j)%nm_idx(1) .eq. gid) then            ! found the match. You must find it
                        BC_UP => gA(nm)%OVN%Cond(j)
                        EXIT
                    endif
                    if (j .eq. size(gA(nm)%OVN%Cond)) then
                        call ch_assert('setOvnBC :: I have not found a matching BC in my neighbor')
                    endif
                enddo
                idxDir => BC_UP%BIndexIn1
#ifdef NC
                call ExchFluxReduce(BC_UP,gid)
#else                
                if (size(BC_UP%idxV,1)>0) then
                    ! V directional flux
                    putCondIdx => BC_UP%idxV(:,2); ! When we talk about flux, there is X and there is Y direction
                    ! this flux is in the unit of m2 (Qy)
                    ! going from m3 to m/s
                    S1d(idxDir(putCondIdx)) = S1d(idxDir(putCondIdx)) +  BC_UP%ExchFlux(putCondIdx,4)/ (gA(gid)%OVN%dt * 86400.0D0 * gA(gid)%DM%d(1)**2)
                endif
                if (size(BC_UP%idxU,1)>0) then
                    putCondIdx => BC_UP%idxU(:,2); ! When we talk about flux, there is X and there is Y direction
                    S1d(idxDir(putCondIdx)) = S1d(idxDir(putCondIdx)) +  BC_UP%ExchFlux(putCondIdx,3)/ (gA(gid)%OVN%dt * 86400.0D0 * gA(gid)%DM%d(1)**2)
                endif
                BC_UP%ExchFlux = 0D0 ! XY: directly make it zero here 20180625
#endif
            case(61) ! A ovn_cv Dirichlet
                if (topoApp < 1) then
                    nm = BBC(i)%nm_idx(1)
#ifdef NC
                    call ovnBCTypeBV(BBC(i), gid, nm)
#else
                    idxDir => BBC(i)%BIndexIn1  ! These are my ghost cells on neighbor grid
                    idxMe => BBC(i)%indices  ! These could be ghost cells
                    ENeighbor => oneD_Ptr(size(gA(nm)%OVN%h), gA(nm)%Topo%E)
                    ENeighborMe => oneD_Ptr(size(gA(gid)%OVN%h), gA(gid)%Topo%E)
                    where (idxDir > 0)
                        ENeighborMe(idxMe) = ENeighbor(idxDir)
                    end where
#endif
                    topoApp = 1
                endif
            case (62)
                !nm = BBC(i)%nm_idx(1)
                !do j=1,size(gA(nm)%OVN%Cond)
                !    if (gA(nm)%OVN%Cond(j)%type .eq. 61 .AND. gA(nm)%OVN%Cond(j)%nm_idx(1) .eq. gid) then            ! found the match. You must find it
                !        BC_UP => gA(nm)%OVN%Cond(j)
                !        EXIT
                !    endif
                !    if (j .eq. size(gA(nm)%OVN%Cond)) then
                !        call ch_assert('setOvnBC :: I have not found a matching BC in my neighbor')
                !    endif
                !enddo
                !idxDir => BC_UP%BIndexIn2
#ifdef NC
                nm = BBC(i)%nm_idx(1)
                do j=1,size(gA(nm)%OVN%Cond)
                    if (gA(nm)%OVN%Cond(j)%type .eq. 61 .AND. gA(nm)%OVN%Cond(j)%nm_idx(1) .eq. gid) then            ! found the match. You must find it
                        BC_UP => gA(nm)%OVN%Cond(j)
                        EXIT
                    endif
                    if (j .eq. size(gA(nm)%OVN%Cond)) then
                        call ch_assert('setOvnBC :: I have not found a matching BC in my neighbor')
                    endif
                enddo
                call ExchFluxReduce(BC_UP,gid)
#endif
                
            end select
      
             
!if (.not. associated(BBC(i)%BV)) allocate(BBC(i)%BV(size(BBC(i)%V,2)))
             
          !endif
      enddo
      
    end subroutine
     
    ! get data from g(DS) to g(US), based on Cond registration on g(US)
    !idxDir = g(US).OVN.Cond(2).BIndexIn1; ! grabbing data from downstream Dirichlet locations: where to grab them
    !nm_fr = g(US).OVN.Cond(2).nm_idx;
    !idxMe = g(US).OVN.Cond(2).indices;
    !M3(idxMe(idxMe>0)) = M2(idxDir(idxMe>0));
    
    !deposit flux. Having be done in ApplyCondOVN when resolving Dirichlet
    !ExtIdx = abs(idxV(:,1)); putCondIdx = idxV(:,2);
    !Flux(putCondIdx,1)=Qy(ExtIdx)*sign(idxV(:,1));
    
    !extract flux and send to receiving grids
    !M2(idxDir(putCondIdx))=Flux(putCondIdx)
    
    ! to generate multiple grid data. do the following:
    ! cd Y:\Amazon\Shilong\Calado\Gdata
    ! run_Master_File('master.txt')
    ! gid = gRefineOvnMesh(1,1)
    ! g(1).OVN.scheme = 0
    ! g(2).OVN.scheme = 1  !upstream.  We can use ovn_cv, but the boundary condition need to support this type of boundary condition
    ! g(3).OVN.scheme = 1  !downstream
    ! portModel2Fortran('H:\Shilong\Calado_6\calado_cp.mat',[],2)