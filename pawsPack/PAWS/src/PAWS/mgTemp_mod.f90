#include "preproc.h"
!
    MODULE mgTemp_mod
      IMPLICIT NONE
      private 
      
      public :: mgTemp2D_type, alloc2dmg, mg_Base
      
      type mgTemp2D_type
          integer :: state = 0
          REAL*8,dimension(:,:),pointer :: ovn, Topo
          REAL*8,dimension(:,:),pointer :: ovns,ovnU,ovnV,ovnQx,ovnQy
          logical,dimension(:,:),pointer :: mask
          logical,dimension(:,:),pointer :: mask2
          REAL*8,dimension(:,:),pointer :: datPass
          logical,dimension(:,:),pointer :: datPassL
      end type
      type(mgTemp2D_type),dimension(:),pointer:: mg_Base
      
    contains
       subroutine alloc2dmg(mgTemp, ny, nx, nny, nnx, nG)
          ! use proc_mod, only: pci
#ifdef NC
          include 'mpif.h'
          integer :: myrank, ierr, nproc
#endif
          type(mgTemp2D_type) mgTemp
          integer ny, nx, nG, nny, nnx!, nL
          
          if (mgTemp%state .eq. 0) then
              !allocate(mgTemp%ovn(pci%YDim(nL),pci%XDim(nL)))
              allocate(mgTemp%ovn(ny, nx))
              allocate(mgTemp%Topo(ny, nx))
              allocate(mgTemp%mask(ny, nx))
              allocate(mgTemp%mask2(ny, nx))
              allocate(mgTemp%ovns(ny, nx))
              allocate(mgTemp%ovnU(ny, nx))
              allocate(mgTemp%ovnV(ny, nx))
              allocate(mgTemp%ovnQx(ny, nx))
              allocate(mgTemp%ovnQy(ny, nx))
              mgTemp%state = 1
#ifdef NC
              call MPI_Comm_rank(MPI_COMM_WORLD,myrank,ierr)
              call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
              if (myrank == 0) then
                  allocate(mgTemp%datPass(nny,nnx-nG)) 
                  allocate(mgTemp%datPassL(nny,nnx-nG))  
              elseif (myrank == nproc-1) then
                  allocate(mgTemp%datPass(nny,nnx-nG))
                  allocate(mgTemp%datPassL(nny,nnx-nG))  
              else
                  allocate(mgTemp%datPass(nny,nnx-2*nG))
                  allocate(mgTemp%datPassL(nny,nnx-2*nG))
              endif
#endif
          endif
      
       end subroutine
      
    end module