#include <preproc.h> 
!

      subroutine distWea(H,JD)
      !% (E) This solver normally works on hourly time step on a station level, it does:
      !% (1) distribute prcp using reference precip unit hyetograph
      !% (2) calculate net radiation, reference (FAO grass) ET
      !% (3) store some ET coefficients
      !% data structure:
      !% a struct called 'day' include data distributed for each day for each-
      !% station. day.prcp(1:24,i) is for the i-th station.
      !% Input:
      !% wid: watershed ID, H: hour of day, JD: julian day (of the year)
      !% Output:
      !% Updated: g.Wea.day, g.OVN.prcp, g.Wea.t
      use Vdata
      use polyEval, Only: linearInterp1d
      implicit none
      type gIndArray
          integer,dimension(:), pointer :: procGIndices => null()
      end type  
      integer :: H, JD, k, datevec(3), yy, mm, dd, i, j
      real*8 :: gid, PREC, frac, RFRAC, loc=0D0, lastLoc=0D0, t1, p1
      real*8, dimension(:), pointer :: prcp=>NULL()
      ! two lines below added 12/13/13 for running constant forcing hypothetic case.
      ! normally, implementConst = .false. . If turned true, it will detect the YYMMDD specified in constYY, etc
      ! climate forcing everyday after constYYconstMMconstDD will be set to the same as this day.
      logical*1 ::  implementConst = .false., constantDay=.false., constantDaySWAP=.true.,constantDaySWAPini=.false.
      integer:: constYY2 = 2006, constMM2 = 4, constDD2 = 15, JD2
      integer:: constYY = 2006, constMM = 4, constDD = 15
      type(gIndArray), allocatable, dimension(:), save :: gIndA

      !global g w
      !% It can be: daily data distributed to arbitrary time steps
      !% Only at beginning of the day will re-distribution happen
      !% If this day prcp=0, then that station won't do anything throughout the
      !% day. This is achieved by making DData=[];
      !% Sta = w(wid).Stations;
      gid = w%g
      PREC = g%Wea%dt/10.0D0
      !%k = round((g(gid).Wea.t-floor(g(gid).Wea.t+PREC))/g(gid).Wea.dt)+1;
      k = H
      frac = 1.0D0
      !RFRAC = 1.08D0    !% solar radiation from SPOKAS is reduced!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      RFRAC = 1.0D0   ! after reducing Sp0 to 1320, now this is put back to 1.0
      ![yy,mm,dd]=datevec(g(gid).Wea.t);
      if (k .ne. 0) then
        datevec = datevec2(int(g%Wea%t))
        yy = datevec(1)
        mm = datevec(2)
        dd = datevec(3)
      endif
#ifndef MEX
      ! XY: pre-process for local gIndices
      if(.not. allocated(gIndA)) then
          allocate(gIndA(size(w%Stations)))
          call progIndices(gIndA)
      end if

      if (abs(w%t-floor(w%t))<1D-10) then
        !% first step of the day, distribute the data
        !g(gid).OVN.prcp = zeros(size(g(gid).OVN.h));
        if (implementConst .AND. (.NOT. constantDay)) then
          if (yy >= constYY .and. mm >= constMM .and. dd >= constDD) then
            constantDay = .true.
          endif
        endif
        if (constantDay .and. constantDaySWAP .and. (.NOT. constantDaySWAPini)) then
            call ymd2jd((/constYY2, constMM2, constDD2/), 3, JD2)
            call makeDay(RFRAC,frac,constYY2,constMM2,JD2,g%Wea%day)
            constantDaySWAPini = .true.
        endif
        if (.not. constantDay) then
        g%OVN%prcp = 0.0D0
        !g(gid).Wea.day = makeDay(wid,RFRAC,frac,yy,mm,JD,g(gid).Wea.day);
        call makeDay(RFRAC,frac,yy,mm,JD,g%Wea%day)
        !call makeDayFromHour(RFRAC,yy,mm,JD,g%Wea%day)
        endif
      endif
#endif
      !% If this day has prcp = 0,(DData=[]) then don't do anything throughout this day
      do i = 1, size(w%Stations)
        if (associated(w%Stations(i)%gIndices)) then
            prcp => oneD_Ptr(size(g%OVN%prcp),g%OVN%prcp)
            if (k .eq. 0) then
                ! a signal that it is not called by wtrshd_day. Rather, a continuous time series
                ! just do a linear interpolation of values.
                t1 = w%t + w%dt/2D0
                call linearInterp1d(w%Stations(i)%datenums,w%Stations(i)%prcp,t1,p1,loc,lastLoc); 
                lastLoc = loc;
                prcp(gIndA(i)%procGIndices) = p1
            else    
                if (g%Wea%day%prcp(k,i)>=0) then
                  prcp(gIndA(i)%procGIndices) = g%Wea%day%prcp(k,i)  
                else
                  !% no need to set to 0 as it is zero by itself, see
                  !% typeIIrainfall.mat
                endif    
            endif
        endif
      enddo

      g%Wea%t = g%Wea%t + g%Wea%dt
     
    contains
#ifdef NC 
     subroutine progIndices(gIndA)
         use proc_mod
         implicit none
         include 'mpif.h'
         type(gIndArray), dimension(:) :: gIndA
         integer :: i, j, k
         integer :: begio, endio, ierr!, myrank
         integer, pointer :: temp(:) => NULL()
         
         !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, ierr)
         begio = (pci%Range(1)%X(1,myrank+1) - 1) * pci%YDim(1) + 1 
         endio = pci%Range(1)%X(2,myrank+1) * pci%YDim(1)
         !write(*,*) myrank, ',', begio, endio
         do i = 1, size(gIndA)
            k = 0
            if (.not. associated(temp)) allocate(temp(size(w%Stations(i)%gIndices)))
            do j = 1, size(w%Stations(i)%gIndices)
                if ((w%Stations(i)%gIndices(j) >= begio) .and. (w%Stations(i)%gIndices(j) <= endio)) then
                    k = k + 1;
                    temp(k) = w%Stations(i)%gIndices(j) - begio + 1
                endif
            enddo
            allocate(gIndA(i)%procGIndices(k))
            gIndA(i)%procGIndices = temp(1:k)
            if (associated(temp)) deallocate(temp)  
            !write(*,*) myrank, ',',i,',',gIndA(i)%procGIndices
         enddo
         call MPI_Barrier(MPI_COMM_WORLD, ierr)
         !stop
      end subroutine progIndices
#else      
     subroutine progIndices(gIndA)
         implicit none
         type(gIndArray), dimension(:) :: gIndA
         integer :: i, j, k
         
         do i = 1, size(gIndA)
            k = size(w%Stations(i)%gIndices)
            allocate(gIndA(i)%procGIndices(k))
            gIndA(i)%procGIndices = w%Stations(i)%gIndices
         enddo
      end subroutine progIndices
#endif

     subroutine griddedInputs()     
     
     
     end subroutine 
     
     
     end subroutine distWea

