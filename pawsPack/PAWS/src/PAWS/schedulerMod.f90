#include <preproc.h>
    
module schedulerMod
      use gd_Mod
      use Vdata, Only: datenum2
      use memoryChunk, Only: tempStorage
#ifdef NC      
      use pncTableHier, only: buildNCTable
#else 
#include "fintrf.h"
      use matTable
      
      private
      mwpointer, dimension(:), pointer :: arg_pa=>null()
      character*(100), dimension(:), pointer :: arg_names=>null()
#endif      
      integer, parameter :: lenL=1000, lenS=100 !lenL for filename that may contain paths and super long names
      ! this scheduler can link to a "file". A file can contain data for multiple micro time steps which are described by a part of the data in the file
      ! if there are no micro time steps, nt should be 1
      TYPE scheduler
        character*(lenS) :: Name ! Name could be 'precipitation', or 'irrigation'
        REAL*8  :: ts,te,dt      ! arrays of [time_start time_end, micro_time_step] for each file
        REAL*8,dimension(:),pointer  :: t=>null()      ! arrays of [time_start time_end, micro_time_step] for each file
        integer :: nt=0                                ! an array of the number of micro time steps
        character(len=lenL):: file                     ! linked filenames
        REAL*8,DIMENSION(:),POINTER  :: par=>null()    ! reserved for advanced use
        REAL*8,DIMENSION(:,:),POINTER  :: lat=>null()  ! reserved for advanced use
        REAL*8,DIMENSION(:,:),POINTER  :: lon=>null()  ! reserved for advanced use
      END TYPE scheduler
      
      Type schedulerGroup
        character*(lenS) :: Name ! Name could be 'precipitation', or 'irrigation'
        type(scheduler),pointer,dimension(:) :: scheds=>null()
        character(len=lenL),dimension(:),pointer:: files=>null()
        real*8, dimension(:),pointer :: params=>null()
        integer :: curr = 0
      end type schedulerGroup
      
      type(gd):: gd_schedData, gd_schedData_fineGrid
      character(len=lenS),dimension(10) :: schedVarnames
      character(len=lenS),dimension(10) :: schedVarnamesC
      type(schedulerGroup),dimension(10) :: schedArrs
      integer, dimension(10) :: rOpts
      integer :: convMode = 1 ! unit conversion control
      integer :: nv(10)=-1
      integer,parameter :: nf=8
      
      public:: schedVarnames, schedVarnamesC, nv, nf, lenS, lenL
      PUBLIC:: schedArrs,schedulerGroup,scheduler
      public:: schedulerMain, schedulerVarStep
      public:: gd_schedData, gd_schedData_fineGrid
      
    CONTAINS
          
      subroutine readFileList(masterFile,nFile,fileList,par)
      ! This function is a workaround constructor for a filelist from a file
      ! This do not need to be the format for creating a masterSched. A masterSched can be constructed in other ways. 
      ! read a file that contains a list of directives for operations and files, inspired by climate simulations
      ! in the future this can be passed in through memory
      character(len=lenL) :: masterFile
      character(len=lenL),dimension(:),pointer :: fileList, opList=>null()
      type(scheduler) masterSched
      integer :: eof, nFile, i
      integer :: uid=71 ! how to manage these file ids?
      real*8  :: par(*)
      
      !nFile = 0
      !OPEN(unit = uid, file = trim(masterFile), ACTION='READ',IOSTAT = eof )
      !
      !if (eof .eq. 0) then
      !read(uid,*,iostat = eof) ! a parameter line to throw away
      !do while (eof .eq. 0)
      !   read(uid,*,iostat = eof) 
      !   if (eof .eq. 0) nFile = nFile + 1
      !enddo
      !close(uid)
      nFile = nfileLines(masterFile)-1
      
      if (nFile>0) then
          allocate(fileList(nFile))
          OPEN(unit = uid, file = trim(masterFile), ACTION='READ',IOSTAT = eof )
          read(uid,*,iostat = eof) par(1:4) ! a parameter line to throw away
          i = 1
          
          do while (eof .eq. 0 .AND. i<=nFile)
              read(uid,'(A120)') fileList(i)
              i = i + 1
          enddo
          close(uid)
      endif
      
      end subroutine
      
      
      function needNewFile(sched,t)
      ! this function returns how many new files need to be read
      ! for the moment it is returning either 0 (no need for new file) or 1 (need to read new file) 
      type(scheduler) :: sched
      integer needNewFile, n
      real*8 :: t
      real*8 :: prec= 1D-10
      
      if (sched%nt .eq. 0 .OR. t+PREC>sched%te) then
          n = 1
      else
          n = 0
      endif   
      needNewFile = n
      end function
      
      subroutine obtainSchedInfo(file, opt, sched, timeField, params)
      ! this subroutine extracts crucial information from a file, e.g., a .nc or a .mat file
      ! such information include dt, ts, nt
      ! this info may be extracted in different ways depending on opt
#ifdef NC  
      use proc_mod, only : myrank
#endif
      implicit none
      character*(lenS), dimension(100) :: varnames
      type(gd),pointer:: mat=>NULL()
      type(gd),pointer:: nc=>NULL()
      type(gd) :: gdt
      integer opt,d1(1),i
      character(len=lenL) :: file
      type(scheduler) :: sched
      integer :: nL, ppos
      real*8 :: tbase
      real*8,dimension(:),pointer :: t=>null()
      real*8,allocatable,dimension(:) :: tt
      character*(*) :: timeField
      real*8,dimension(*) :: params
      real*8,dimension(:,:),pointer :: dat=>null()
      logical :: doMAsterRead = .false.
      integer :: masterproc = 0
      
      sched%file = file

      IF (opt .eq. 1) THEN
      ppos = scan(trim(file),".", BACK= .true.)
      if (file(ppos: ppos+2) .eq. ".nc") then
        ! t=ncread(file,'time')
         !call ch_assert("obtainSchedInfo:: read nc files not coded yet!!")
         varnames(1) = trim(timeField); 
         varnames(2:3)=(/'lat','lon'/)
         varnames(4)=""
#ifdef NC   
         call buildNCTable(file, gdt, doMAsterRead)
         if ((doMasterRead .and. (myrank .eq. masterproc)) .or. (.not. doMasterRead)) then
             nc => gdt .G. file;
             t => nc .F. trim(timeField)
             allocate(tt(size(t)))
         endif
#endif  
      elseif (file(ppos: ppos+3) .eq. ".mat") then
#ifndef NC             
         !timeField = 't'
         varnames(1) = trim(timeField); 
         varnames(2:3)=(/'lat','lon'/)
         varnames(4)="" 
         call buildMatTable(file,gdpIn=gdt,arg_pa=arg_pa,arg_names=arg_names,varnames=varnames)
         mat => gdt .G. file; 
         t => mat .F. trim(timeField)
#endif
         allocate(tt(size(t)))
      endif
      
      !if (size(t)>1) then !XY: modified 2018.11.2
      if (associated(t)) then
          d1= floor(params(1)) ! params =[tBase, timezone]. +timezone shift data from Greenwich time (RCM's system) to local time
          tbase = datenum2(d1)+params(2)/24D0 ! params(1) is the YYYYMMDD of the start of simulation
          tt = t + tbase
          sched%dt = tt(2)-tt(1) ! assuming uniform steps
          sched%ts = tt(1)
          sched%nt = size(tt)
          sched%te = tt(size(tt))+sched%dt ! "time bounds"
          !if (associated(sched%t)) deallocate(sched%t)
          !allocate(sched%t(size(t)))
          !sched%t = t
      endif
      if (associated(mat)) then
        if (.not. isempty(mat,'lat') .AND. .not. associated(sched%lat)) then
          dat => mat .FF. 'lat'; allocate( sched%lat(size(dat,1),size(dat,2)) )
          sched%lat = dat
          dat => mat .FF. 'lon'; allocate( sched%lon(size(dat,1),size(dat,2)) )
          sched%lon = dat
        endif
      endif
      if (associated(nc)) then
        if (.not. isempty(nc,'lat') .AND. .not. associated(sched%lat)) then
          dat => nc .FF. 'lat'; allocate( sched%lat(size(dat,1),size(dat,2)) )
          sched%lat = dat
          dat => nc .FF. 'lon'; allocate( sched%lon(size(dat,1),size(dat,2)) )
          sched%lon = dat
        endif
      endif
      
      ENDIF
      
      if (allocated(tt)) deallocate(tt)
#ifndef NC            
      nullify(t); nullify(mat)
      do i=1,size(arg_pa)
        if (arg_pa(i) .ne. 0) call mxDestroyArray(arg_pa(i))
      enddo
      deallocate(arg_pa); deallocate(arg_names)    
#endif 
      call deallocate_gd(gdt)
      end subroutine obtainSchedInfo
      
      subroutine prepSchedArray(masterFile,schedArr)
      implicit none
      integer nfile,i,ppos
      ! input:
      character(len=lenL) :: masterFile
      ! output:
      character(len=lenL),dimension(:),pointer :: fileList=>null()
      type(schedulerGroup) :: schedArr
      
      allocate(schedArr%params(4))
      call readFileList(masterFile,nFile,schedArr%files,schedArr%params)
      
      allocate(schedArr%scheds(nFile))
      do i=1,nfile
#ifdef NC
          call obtainSchedInfo(schedArr%files(i), 1, schedArr%scheds(i), "time", schedArr%params)    ! .nc version
#else
          call obtainSchedInfo(schedArr%files(i), 1, schedArr%scheds(i), "t", schedArr%params)    ! .mat version
#endif
      enddo
      
      ppos = scan(trim(masterFile),".", BACK= .true.)
      schedArr%Name = masterFile(1:ppos-1)
      end subroutine prepSchedArray
      
      function findCurrSched(schedArr,t,isIn)
      implicit none
      real*8 :: t
      type(schedulerGroup) :: schedArr
      integer k,findCurrSched,k1,i
      real*8 :: prec=1D-6
      integer, optional  :: isIn
      integer :: is, N
      
      is = 1
      if (present(isIn)) is=max(1,isIn)
      
      k=0;
      ! assuming sorted
      N = size(schedArr%scheds)
      if (N <= 0) return
      
      if (t < schedArr%scheds(1)%ts-PREC .OR. t > schedArr%scheds(n)%te+PREC) then
          k = -1 ! out of bounds
      else
          do i=is,size(schedArr%files)
              !k1 = needNewFile(schedArr%scheds(i),t)
              if (schedArr%scheds(i)%ts-PREC<t .AND. schedArr%scheds(i)%te>t+PREC) then
                  k = i
                  exit
              endif
          enddo
      endif
      findCurrSched = k
      end function findCurrSched
      
      function findCurrInd(sched,t)
      implicit none
      real*8 :: t
      type(scheduler) :: sched
      integer k,k1,i
      real*8 findCurrInd
      real*8 :: prec=1D-10, frac
      integer :: is
      
      if (t<sched%ts-PREC .OR. t> sched%te+PREC) then
          k = -1
      else
          frac = (t - sched%ts)/sched%dt
      endif
      
      if (frac<0 .AND. frac>-PREC) frac = 0D0
      if (frac>sched%nt .AND. frac<sched%nt+PREC) frac = sched%nt
      
      findCurrInd = frac
      end function findCurrInd
      
      subroutine readFileMapData(sched,var,ptr)
      ! read a file
      ! get a ptr to new data. (3D array)--> first 2D will be of the same size as model dimension
      ! save a copy to tempStorage(trim("sched")// trim(var), datTS, size(datTS,3))
      ! use tempStorage because you don't have to specify dimensions ahead of time
      implicit none
      type(scheduler) sched
      character(*) var
      character*(lenS), dimension(100) :: varnames
      real*8, dimension(:,:,:),pointer :: ptr
      type(gd)::gdt
      type(gd),pointer:: gdPtr=>null()
      real*8,dimension(:,:,:),pointer:: datRead=>null(), datTS=>null()
      integer i, nk

         varnames(1) = var; varnames(2)="" 
#ifdef NC            
         call buildNCTable(sched%file,gdt)   ! <<<<<<===== subroutine readFileMapData is not used
#else
         call buildMatTable(sched%file,gdpIn=gdt,arg_pa=arg_pa,arg_names=arg_names,varnames=varnames)

#endif       
         
         datRead => (gdt .G. sched%file) .FFF. var
         if (associated(ptr)) nullify(ptr)
         allocate(ptr(size(datRead,1),size(datRead,2),size(datRead,3)))
         
         ptr = datRead ! here we might do a spatial interpolation
         
#ifndef NC      
         ! clean up and avoid memory leak
         ! that's why we have to make a copy above
         do i=1,size(arg_pa)
           if (arg_pa(i) .ne. 0) call mxDestroyArray(arg_pa(i))
         enddo
         deallocate(arg_pa); deallocate(arg_names)
#endif  
         call deallocate_gd(gdt)
    
      end subroutine
        
      subroutine RCMregrid(datCurr_temp,RCM_coarse_grid,paws_grid_size,new_datCurr) 
      integer paws_grid_size(6), gridSize, grid_step 
      integer RCM_coarse_grid(2)
      integer i, j, ii, jj
      !real*8,dimension(:,:,:),pointer:: newRCM_centre_data => null()
      !real*8,dimension(:,:),pointer:: newRCM_centre_data => null()
      
      integer*8,dimension(:),pointer:: X_paws => null(), Y_paws => null(), X_RCM => null(), Y_RCM => null()
      integer*8,dimension(:),pointer:: IX => null(), IY => null()
      !real*8,dimension(:,:,:),pointer:: newPAWS_grid_data => null()
      !real*8,dimension(:,:),pointer:: newPAWS_grid_data => null()
      !real*8,dimension(:,:) :: datCurr_out
      !character*(lenS) :: vn
      !integer RCM_coarse_grid(maxDim)
      !type(gd) gdPtr
      real*8, dimension(:,:) :: datCurr_temp !<=work
      real*8, dimension(paws_grid_size(1),paws_grid_size(2)) :: new_datCurr !<=work
      
      gridSize=9 ! hardcode, should modify later, 2018-08-27
      grid_step=gridSize/2
      
      !ns_grid(1)=paws_grid_size(1)
      !ns_grid(2)=paws_grid_size(2)
      !newRCM_grid(1)=RCM_coarse_grid(1)
      !newRCM_grid(2)=RCM_coarse_grid(2)   
      
      !allocate(newRCM_centre_data(newRCM_grid(1),newRCM_grid(2),ns(3)))
      !allocate(newRCM_centre_data(newRCM_grid(1),newRCM_grid(2)))
      allocate(X_paws(paws_grid_size(1)))
      allocate(Y_paws(paws_grid_size(2)))
      allocate(X_RCM(RCM_coarse_grid(1)))
      allocate(Y_RCM(RCM_coarse_grid(2)))
      allocate(IX(paws_grid_size(1)))
      allocate(IY(paws_grid_size(2)))
      !allocate(newPAWS_grid_data(ns_grid(1),ns_grid(2),ns(3)))
      !allocate(newPAWS_grid_data(ns_grid(1),ns_grid(2)))
            
      !do i=1, newRCM_grid(1)
          !do j=1, newRCM_grid(2)
              !newRCM_centre_data(i,j)=datCurr_temp(1+grid_step+(i-1)*gridSize,1+grid_step+(j-1)*gridSize)
          !enddo
      !enddo
           
      do i=1, paws_grid_size(1)        
          X_paws(i)=i    
      enddo   
      do j=1, paws_grid_size(2)        
          Y_paws(j)=j    
      enddo   
      do ii=1, RCM_coarse_grid(1)        
          X_RCM(ii)=ii    
      enddo   
      do jj=1, RCM_coarse_grid(2)        
          Y_RCM(jj)=jj   
      enddo
               
      IX = (((X_paws-X_RCM(1))/gridSize)+1)
      do i=1, paws_grid_size(1)
          if (IX(i) > RCM_coarse_grid(1)) then
              IX(i)=IX(i)-1 
          endif    
      enddo   
      IY = (((Y_paws-Y_RCM(1))/gridSize)+1)
      do j=1, paws_grid_size(2)
          if (IY(j) > RCM_coarse_grid(2)) then            
              IY(j)=IY(j)-1       
          endif  
      enddo
      
          do i=1, paws_grid_size(1)
              do j=1, paws_grid_size(2)
                  !newPAWS_grid_data(i,j,k)=newRCM_centre_data(IX(i),IY(j),k)
                  !newPAWS_grid_data=newRCM_centre_data(IX,IY)
                  new_datCurr(i,j)=datCurr_temp(IX(i),IY(j))
              enddo
          enddo          
      
      !gdPtr%P(1)%S3=newPAWS_grid_data
      
      !datCurr_temp=newPAWS_grid_data
      
      !deallocate(newRCM_centre_data)
      deallocate(X_paws)
      deallocate(Y_paws)
      deallocate(X_RCM)
      deallocate(Y_RCM)
      deallocate(IX)
      deallocate(IY)
      !deallocate(newPAWS_grid_data)
      
      end subroutine   
      
      subroutine spatialInterp(datIn, datOut, params)
      real*8,dimension(:,:,:) :: datIn, datOut
      real*8,dimension(:) :: params
      ! for the moment being it is simply a copy operation. In the future we will add grid mapping
      datOut = datIn
      end subroutine

#ifdef NC    
! nc version
      subroutine schedulerVarStep(schedArr,t,mDt,var,rOpt,ks,paws_grid_size)
      ! carries out a time step for a scedulerArr (one variable)
      ! tasks: for each variable. 
      ! (1) check if new data file needs to be read; (2) read new data if needed and save to tempStorage; (3) retrieve the correct part of memory
      ! (4) interpolate; (5) set in range (prevent <0)
      use polyEval, Only: linearInterp1d, polyInterp3
      use proc_mod, only : myrank, scatter2Ddat, pci
      implicit none
      include 'mpif.h'
      integer nvars, rOpt, ierr
      real*8 :: t, mDt
      character(*) :: var
      type(schedulerGroup) :: schedArr
      type(scheduler),pointer:: sched=>null()
      integer ::i,k,k1,order=3 ! order==3 gives a second order polynomial interpolation
      real*8 ::frac, prec=1D-12, interval(2)
      real*8, dimension(:,:), pointer:: datCurr=>null(), datBeforeUC=>null(), datCurr_out=>null(), datCurr_gl=>null()
      real*8, dimension(:,:,:),pointer:: datTS=>null(), datRead=>null()
      real*4, dimension(:,:,:),pointer:: datReadS=>null(), datTSS=>null()
      type(gd)::gdt
      type(gd),pointer:: gdPtr
      integer ndim, ns(maxDim),ns2(3), kS
      character*(lenS), dimension(100) :: varnames
      real*8 gridReadPar(4) ![LLX,LLY,DX] or other descriptors that enable a mapping before data grid and 
      character*(10) dType
      character*(lenS) :: vn, vnc
      logical :: doAllo
      integer :: internal_state = 0
      integer :: masterproc = 0
      logical :: doMAsterRead = .true.
      
      integer paws_grid_size(6) !added for coarse RCM - 2018-09-08
      integer paws_grid_size_gl(6) !added for coarse RCM - 2018-09-08
      
      ! if it does not exist, initialize
      
      k = findCurrSched(schedArr,t,schedArr%curr); ks=k; 

      !paws_grid_size_gl(1) = pci%YDim(1); paws_grid_size_gl(2) = pci%XDim(1);  !<<<<<===== XY default, need to check again; ny=85, nx=70, ns(1)=7, ns(2)=9, 2018-11-06 
      paws_grid_size_gl(1) = pci%XDim(1); paws_grid_size_gl(2) = pci%YDim(1);  ! <<<<<===== validated !!! Right!!! WP 2018-11-07
      
      vn = trim("sched")// trim(var)
      vnc = trim("schedCurr")// trim(var)
      !if (internal_state .eq. 0 .OR. & ! force reading at first step to get size information
      
!write(272+myrank,*) '==============1=============='      
!write(272+myrank,*) vn, k
      if (k>0 .AND. k<=size(schedArr%files) .AND. k .ne. schedArr%curr ) then
          ! needs new file
          ! I put the following inside this function, instead of calling readFileMapData, why?
          ! This way it saves one copy. When there is a lot of data, this can amounts to quite some time
          ! call readFileMapData(sched,var,datTS)
         sched => schedArr%scheds(k)
         varnames(1) = var; varnames(2)="" 
         if (.not. isAllocated(gd_schedData)) call allocate_gd(gd_schedData)
         
         ! ========== for fine grid
         if (.not. isAllocated(gd_schedData_fineGrid)) call allocate_gd(gd_schedData_fineGrid)
         ! ========== for fine grid
         

         call buildNCTable(sched%file,gdt,doMAsterRead)

         ! ==== MASTERPROC Warning BEGIN ====
         if ((doMasterRead .and. (myrank .eq. masterproc)) .or. (.not. doMasterRead)) then 
             gdPtr => gdt .G. sched%file; ndim = gdnDim(gdPtr,var)
             
             dType = returnType(gdPtr,var)                 
         
             if (dType .eq. 'R3') then
                 ! addR3cp(field,datND,main)                    
                 call addPtrcp(vn, gdPtr .FFF. var, gd_schedData) ! gd_schedData holds the data in its original resolution
                 !call addPtrcp(vn, gdPtr .FFF. var, gd_schedData_fineGrid) ! gd_schedData holds the data in its original resolution
                 !call spatialInterp(datRead, datTS, schedArr%params) ! interpolate in space
             elseif (dType .eq. 'S3')  then
                 call addPtrcp(vn, gdPtr .SSS. var, gd_schedData)
                 !call addPtrcp(vn, gdPtr .SSS. var, gd_schedData_fineGrid) ! gd_schedData holds the data in its original resolution
                 !call spatialInterp(DBLE(datReadS), datTS, schedArr%params) ! interpolate in space
             endif
         
             call deallocate_gd(gdPtr)
             call deallocate_gd(gdt)
         endif
         ! ==== MASTERPROC Warning END ====
         
         schedArr%curr = max(0,k)
         internal_state = 1
      endif
      
      if (k<=0) return
      sched => schedArr%scheds(k)
      frac = findCurrInd(sched,t) ! found the index in k-th sched
         
      ! interpolate in time
      
      !call tempStorage(trim("sched")// trim(var), datTS) !! raw data. will not change shape
      !call tempStorage(trim("schedCurr")// trim(var), datCurr) !! raw data
     !
     if (isempty(gd_schedData_fineGrid,vnc)) then     
          allocate(datCurr_out(paws_grid_size(1),paws_grid_size(2))); datCurr_out = 0D0 !  <<<<<=====********************
          call addptr(paws_grid_size,vnc,datCurr_out,gd_schedData_fineGrid); nullify(datCurr_out)
     endif
     call getPtr(gd_schedData_fineGrid,vnc,datCurr_out)
     
  ! ==== MASTERPROC Warning BEGIN ====    
  if ((doMasterRead .and. (myrank .eq. masterproc)) .or. (.not. doMasterRead)) then

      !allocate(datCurr_gl(pci%YDim(1), pci%XDim(1))) ! <<<<<===== XY default, need to check again; ny=85, nx=70, ns(1)=7, ns(2)=9, 2018-11-06  
      allocate(datCurr_gl(pci%XDim(1), pci%YDim(1))) ! <<<<<===== validated !!! Right!!! WP 2018-11-07
      !allocate(datCurr_gl(paws_grid_size_gl(1), paws_grid_size_gl(2))) ! <<<<<===== test above
      dType = returnType(gd_schedData,vn)
      ns = returnSizes(gd_schedData,vn)
         
      if (isempty(gd_schedData,vnc)) then
          allocate(datCurr(ns(1),ns(2))); datCurr = 0D0
          call addptr(ns,vnc,datCurr,gd_schedData); nullify(datCurr)
      endif
      call getPtr(gd_schedData,vnc,datCurr)
     
      select case (rOpt)
      case(1)    
          ! direct copy over from the period covered
          ! useful when (i) forcing time step is the same as model time step
          ! or (ii) simply nearest neighbor interpolation is desired
          if (dtype .eq. 'S3') then         
              call getPtr(gd_schedData,vn,datTSS)
              !call getPtr(gd_schedData_fineGrid,vn,datTSS)
              datCurr = datTSS(:,:,int(frac+1+PREC))
              call RCMregrid(datCurr,ns,paws_grid_size_gl,datCurr_gl) !  <<<<<=====********************
          elseif (dtype .eq. 'R3') then     
              call getPtr(gd_schedData,vn,datTS)
              !call getPtr(gd_schedData_fineGrid,vn,datTS)
              datCurr = datTS(:,:,int(frac+1+PREC))
              call RCMregrid(datCurr,ns,paws_grid_size_gl,datCurr_gl) !  <<<<<=====********************
          endif
      case(2)    
          ! do a linear interpolation
          !(x,y,x1,y1,loc,lastLoc)
          !datCurr = datTS(:,:,int(frac+1+PREC))
          interval = (/t,t+mDt/)
          
          if (dtype .eq. 'S3') then
              call getPtr(gd_schedData,vn,datTSS)
              !call getPtr(gd_schedData_fineGrid,vn,datTSS)
              call polyInterp3(0,(/sched%ts,sched%te,sched%dt/),datTSS,interval,datCurr,0,order)
              call RCMregrid(datCurr,ns,paws_grid_size_gl,datCurr_gl) !  <<<<<=====********************
          elseif (dtype .eq. 'R3') then
              call getPtr(gd_schedData,vn,datTS)
              !call getPtr(gd_schedData_fineGrid,vn,datTS)
              call polyInterp3(0,(/sched%ts,sched%te,sched%dt/),datTS,interval,datCurr,0,order)
              call RCMregrid(datCurr,ns,paws_grid_size_gl,datCurr_gl) !  <<<<<=====********************
          endif
      end select
      call rcmUnitConvRange(var, datCurr_gl) !  <<<<<=====********************
      !call scatter2Ddat(1,pci%YDim(1),pci%XDim(1),datCurr_gl,paws_grid_size(1),paws_grid_size(2), datCurr_out,masterproc) ! <<<<<===== XY default, need to check again; ny=85, nx=70, ns(1)=7, ns(2)=9, 2018-11-06 
      
!write(272+myrank,*) '==============2=============='
  endif
   ! ==== MASTERPROC Warning END ====
  if (doMasterRead) then
      call MPI_Barrier(MPI_COMM_WORLD,ierr)
      call scatter2Ddat(1,pci%YDim(1),pci%XDim(1),datCurr_gl,paws_grid_size(2),paws_grid_size(1), datCurr_out,masterproc) ! <<<<<===== validated !!! Right!!! WP 2018-11-07
      
!write(272+myrank,*) datCurr_out
!write(272+myrank,*) '============================='
      if (myrank .eq. masterproc) deallocate(datCurr_gl) 
  endif
      
      
      !call rcmUnitConvRange(var, datCurr)
      ! calls unit conversion subroutine after a file is read in
      !call rcmUnitConv(var,datCurr)      
      end subroutine

#else
! matlab version
      subroutine schedulerVarStep(schedArr,t,mDt,var,rOpt,ks,paws_grid_size)
      ! carries out a time step for a scedulerArr (one variable)
      ! tasks: for each variable. 
      ! (1) check if new data file needs to be read; (2) read new data if needed and save to tempStorage; (3) retrieve the correct part of memory
      ! (4) interpolate; (5) set in range (prevent <0)
      use polyEval, Only: linearInterp1d, polyInterp3
      implicit none
      integer nvars, rOpt
      real*8 :: t, mDt
      character(*) :: var
      type(schedulerGroup) :: schedArr
      type(scheduler),pointer:: sched=>null()
      integer ::i,k,k1,order=3 ! order==3 gives a second order polynomial interpolation
      real*8 ::frac, prec=1D-12, interval(2)
      real*8, dimension(:,:), pointer:: datCurr=>null(), datBeforeUC=>null(), datCurr_out=>null()
      real*8, dimension(:,:,:),pointer:: datTS=>null(), datRead=>null()
      real*4, dimension(:,:,:),pointer:: datReadS=>null(), datTSS=>null()
      type(gd)::gdt
      type(gd),pointer:: gdPtr
      integer ndim, ns(maxDim),ns2(3), kS
      character*(lenS), dimension(100) :: varnames
      real*8 gridReadPar(4) ![LLX,LLY,DX] or other descriptors that enable a mapping before data grid and 
      character*(10) dType
      character*(lenS) :: vn, vnc
      logical :: doAllo
      integer :: internal_state = 0
      logical :: doMAsterRead = .true.
      
      integer paws_grid_size(6) !added for coarse RCM - 2018-09-08
      
      ! if it does not exist, initialize
      
      k = findCurrSched(schedArr,t,schedArr%curr); ks=k; 
            
      vn = trim("sched")// trim(var)
      vnc = trim("schedCurr")// trim(var)
      !if (internal_state .eq. 0 .OR. & ! force reading at first step to get size information
      if (k>0 .AND. k<=size(schedArr%files) .AND. k .ne. schedArr%curr ) then
          ! needs new file
          ! I put the following inside this function, instead of calling readFileMapData, why?
          ! This way it saves one copy. When there is a lot of data, this can amounts to quite some time
          ! call readFileMapData(sched,var,datTS)
         sched => schedArr%scheds(k)
         varnames(1) = var; varnames(2)="" 

         call buildMatTable(sched%file,gdpIn=gdt,arg_pa=arg_pa,arg_names=arg_names,varnames=varnames) 
         gdPtr => gdt .G. sched%file; ndim = gdnDim(gdPtr,var)
         if (.not. isAllocated(gd_schedData)) call allocate_gd(gd_schedData)
         
         ! ========== for fine grid
         if (.not. isAllocated(gd_schedData_fineGrid)) call allocate_gd(gd_schedData_fineGrid)
         ! ========== for fine grid
         
         dType = returnType(gdPtr,var)                 
         
         if (dType .eq. 'R3') then
             ! addR3cp(field,datND,main)             
             call addPtrcp(vn, gdPtr .FFF. var, gd_schedData) ! gd_schedData holds the data in its original resolution
             call addPtrcp(vn, gdPtr .FFF. var, gd_schedData_fineGrid) ! gd_schedData holds the data in its original resolution
             !call spatialInterp(datRead, datTS, schedArr%params) ! interpolate in space
         elseif (dType .eq. 'S3')  then
             call addPtrcp(vn, gdPtr .SSS. var, gd_schedData)
             call addPtrcp(vn, gdPtr .SSS. var, gd_schedData_fineGrid) ! gd_schedData holds the data in its original resolution
             !call spatialInterp(DBLE(datReadS), datTS, schedArr%params) ! interpolate in space
         endif
            
         ! clean up and avoid memory leak
         ! that's why we have to make a copy above
         do i=1,size(arg_pa)
           call mxDestroyArray(arg_pa(i))
         enddo
         deallocate(arg_pa); deallocate(arg_names)
         call deallocate_gd(gdPtr)
         call deallocate_gd(gdt)
         
         schedArr%curr = max(0,k)
         internal_state = 1
      endif
      
      if (k<=0) return
      sched => schedArr%scheds(k)
      frac = findCurrInd(sched,t) ! found the index in k-th sched
         
      ! interpolate in time
      
      !call tempStorage(trim("sched")// trim(var), datTS) !! raw data. will not change shape
      !call tempStorage(trim("schedCurr")// trim(var), datCurr) !! raw data
      dType = returnType(gd_schedData,vn)
      ns = returnSizes(gd_schedData,vn)
         
      if (isempty(gd_schedData,vnc)) then
          allocate(datCurr(ns(1),ns(2))); datCurr = 0D0
          call addptr(ns,vnc,datCurr,gd_schedData); nullify(datCurr)
      endif
      
     if (isempty(gd_schedData_fineGrid,vnc)) then     
          allocate(datCurr_out(paws_grid_size(1),paws_grid_size(2))); datCurr_out = 0D0 !  <<<<<=====********************
          call addptr(paws_grid_size,vnc,datCurr_out,gd_schedData_fineGrid); nullify(datCurr_out)
     endif
      
      call getPtr(gd_schedData,vnc,datCurr)
      call getPtr(gd_schedData_fineGrid,vnc,datCurr_out)
      select case (rOpt)
      case(1)    
          ! direct copy over from the period covered
          ! useful when (i) forcing time step is the same as model time step
          ! or (ii) simply nearest neighbor interpolation is desired
          if (dtype .eq. 'S3') then
              !call getPtr(gd_schedData,vn,datTSS)
              call getPtr(gd_schedData_fineGrid,vn,datTSS)
              datCurr = datTSS(:,:,int(frac+1+PREC))
              call RCMregrid(datCurr,ns,paws_grid_size,datCurr_out) !  <<<<<=====********************
          elseif (dtype .eq. 'R3') then
              !call getPtr(gd_schedData,vn,datTS)
              call getPtr(gd_schedData_fineGrid,vn,datTS)
              datCurr = datTS(:,:,int(frac+1+PREC))
              call RCMregrid(datCurr,ns,paws_grid_size,datCurr_out) !  <<<<<=====********************
          endif
      case(2)    
          ! do a linear interpolation
          !(x,y,x1,y1,loc,lastLoc)
          !datCurr = datTS(:,:,int(frac+1+PREC))
          interval = (/t,t+mDt/)
          
          if (dtype .eq. 'S3') then
              !call getPtr(gd_schedData,vn,datTSS)
              call getPtr(gd_schedData_fineGrid,vn,datTSS)
              call polyInterp3(0,(/sched%ts,sched%te,sched%dt/),datTSS,interval,datCurr,0,order)
              call RCMregrid(datCurr,ns,paws_grid_size,datCurr_out) !  <<<<<=====********************
          elseif (dtype .eq. 'R3') then
              !call getPtr(gd_schedData,vn,datTS)
              call getPtr(gd_schedData_fineGrid,vn,datTS)
              call polyInterp3(0,(/sched%ts,sched%te,sched%dt/),datTS,interval,datCurr,0,order)
              call RCMregrid(datCurr,ns,paws_grid_size,datCurr_out) !  <<<<<=====********************
          endif
      end select
      
      call rcmUnitConvRange(var, datCurr_out) !  <<<<<=====********************
      !call rcmUnitConvRange(var, datCurr)
      ! calls unit conversion subroutine after a file is read in
      !call rcmUnitConv(var,datCurr)      
      end subroutine
#endif

      subroutine rcmUnitConvRange(var,datCurr)
      character*(*) var
      real*8,dimension(:,:) :: datCurr
      real*8 :: PREC=1D-9
      !schedVarnames(1:nf) = (/'PRECT','FSDS','FLDS','PS','QBOT','TBOT','UBOT','VBOT'/)
      if (var .eq. 'PRECT') then
          datCurr = datCurr*1D3 ! from CESM it is m/s. In my CLM it is mm/s
          if (datCurr(1,1)>1D-5) then
              datCurr(1,1)=datCurr(1,1)+PREC
          endif
          
          where (datCurr<=1D-7) datCurr=0D0 ! ~0.01 mm/day
      elseif (var .ne. 'TBOT' .AND. var .ne. 'UBOT' .AND. var .ne. 'VBOT') then
          where(datCurr<PREC) datCurr = 0D0
      endif
      
      end subroutine
      
      subroutine schedulerMain(t,mDt,Ks,paws_grid_size)
      ! (1) define variables and master files for variables. for now it may be hard-coded
      ! (2) put definitions to module save variables
      ! (3) save or interpolate files if necessary
      ! (4) call unit conversion subroutine
      
      ! All in all, this subroutine will create datCurr which is queryable from gd_schedData
      ! use schedulerMod to call schedulerMain(t,1D0/24D0)
      ! and query gd_schedData with schedCurr$Var$ to obtain the data pointer, presently 2D array
     ! use proc_mod, only: myrank
      implicit none
#ifdef NC
      include 'mpif.h'
#endif      
      integer nfile,i,ppos,ierr
      ! input:
      character(len=lenL) :: masterFile
      character(len=lenS) :: f
      ! output:
      character(len=lenL),dimension(:),pointer :: fileList=>null()
      type(schedulerGroup) :: schedArr
      integer,parameter :: nf=8
      integer, save:: state= 0
      integer :: Ks(nf),nc
      real*8 t,mDt
      
      integer paws_grid_size(6) !added for coarse RCM - 2018-09-08
      nc = sum(Ks)
      if (nc .ne. 0 .or. state .eq. 0) then
          if (state .eq. 0) then
              schedVarnames(1:nf) = (/'PRECT','FSDS','FLDS','PS','QBOT','TBOT','UBOT','VBOT'/)
              
              rOpts=2; rOpts(1)=1
      
              nc = 0
              do i=1,nf
                  f = schedVarnames(i)
                  masterFile = trim(f)//'.txt'
                  call prepSchedArray(masterFile,schedArrs(i))
              enddo
#ifdef NC              
              call MPI_Barrier(MPI_COMM_WORLD,ierr)
#endif
              state = 1

          endif
          do i=1,nf
            KS(i) = 0  
            if (size(schedArrs(i)%scheds)>0) then
                call schedulerVarStep(schedArrs(i),t,mDt,schedVarnames(i),rOpts(i),KS(i),paws_grid_size)
                ! variables saved to tempStorage
            endif
! if (i>3) stop
            nv(i) = KS(i)
          enddo 
      endif
      end subroutine schedulerMain
      
      
        subroutine linearInterp3d(x,y,x1,y1,loc,lastLoc)
        ! interpolation, but in the 3rd dimension
        ! modified from linearInterp1d in utilities
        real*8, optional :: lastLoc ! good for a loop of calls with monotonically ascending search position
        real*8, intent(in) :: x(:), y(:,:,:), x1
        real*8, intent(out) :: y1(:,:), loc
        real*8 d, d1
        integer n, i,j, k, i1
        n = size(x)
        if (present(lastLoc)) then
            i1 = max(int(lastLoc),1)
        else
            i1 = 1
        endif
        if (x1<x(1)) then
            y1=y(:,:,1)
            return
        elseif (x1>x(n)) then
            y1=y(:,:,n)
            return
        else
          do i=i1, n-1
            if (x(i)<=x1 .and. x(i+1)>x1) then
               d = x(i+1)-x(i)
               d1= x1 - x(i)
               loc = d1/d + i
               y1 = y(:,:,i)+ (loc-int(loc))*(y(:,:,i+1)-y(:,:,i))
               exit
            endif
          enddo
        endif  
        end subroutine

end module schedulerMod