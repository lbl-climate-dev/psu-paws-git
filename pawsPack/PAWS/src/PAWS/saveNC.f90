MODULE NCsave


   use gd_mod
   use proc_mod
   use netcdf
   use vdata, only : num2str, oneD_PtrLO, oneD_ptrI
   !include 'mpif.h'


   public :: saveNC, saveNCVar, saveNCgd, saveVar
   integer, parameter :: maxStrLen=100
   
   interface VarDimDef
   module procedure VarDimDefR1, VarDimDefR2, VarDimDefR3, VarDimDefR4, VarDimDefR5, VarDimDefR6
   module procedure VarDimDefI1, VarDimDefI2, VarDimDefI3, VarDimDefI4, VarDimDefI5, VarDimDefI6
   module procedure VarDimDefL1, VarDimDefL2, VarDimDefL3, VarDimDefL4, VarDimDefL5, VarDimDefL6
   end interface

   interface saveNCVar
   module procedure saveNCVarR1, saveNCVarR2, saveNCVarR3, saveNCVarR4, saveNCVarR5, saveNCVarR6
   module procedure saveNCVarI1, saveNCVarI2, saveNCVarI3, saveNCVarI4, saveNCVarI5, saveNCVarI6
   module procedure saveNCVarL1, saveNCVarL2, saveNCVarL3, saveNCVarL4, saveNCVarL5, saveNCVarL6
   end interface

   contains


   subroutine saveNC(filename)
      ! save all running data into filename.nc and filename_CLM.nc
      ! if the file is already exist, cover it.
      use proc_mod
      use gd_mod
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character(len=100) :: filename_CLM
      integer :: ncid, ncid2
      integer :: i, status!, myrank
      logical :: exi
      type(gd) :: gdT

      inquire(file=filename,exist=exi)
      call MPI_Barrier(MPI_COMM_WORLD, status)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (myrank==0) then
         if (exi) then
            open(unit=666,file=filename,status='old')
            close(666,status='delete')
         endif
      endif   

      call MPI_Barrier(MPI_COMM_WORLD, status)
      status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
      if (status /= nf90_noerr) then
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_create"
      end if
      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_enddef"
      end if
      call MPI_Barrier(MPI_COMM_WORLD, status)

      ! save: g, w, r, proc
      !write(*,*) gd_base%f(1) 
      !gdT = gd_base .G. 'proc'
      call preDimSet(ncid)
      call saveGDfield(gd_base%g(1), ncid)   

      !write(*,*) gd_base%f(2)
      !gdT = gd_base .G. 'example3.nc' 
      call saveGDfield(gd_base%g(2), ncid)    

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if


      ! save: CLM file
      i = index(filename, '.nc') 
      filename_CLM = ' '
      filename_CLM(1:i-1) = filename(1:i-1)
      filename_CLM(i:i+6) = '_CLM.nc'
      inquire(file=filename_CLM,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (myrank==0) then
         if (exi) then
            open(unit=666,file=filename_CLM,status='old')
            close(666,status='delete')
         endif
      endif   

      call MPI_Barrier(MPI_COMM_WORLD, status)
      status = nf90_create(filename_CLM,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
      if (status /= nf90_noerr) then
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_create"
      end if
      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_enddef"
      end if
      call MPI_Barrier(MPI_COMM_WORLD, status)
      
      !write(*,*) gd_base%f(3)
      !gdT = gd_base .G. 'example3.nc' 
      call saveGDfield(gd_base%g(3), ncid)    



      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if


   end subroutine



   recursive subroutine saveGDfield(main, ncid)
      implicit none
      include 'mpif.h'
      type(gd),target :: main
      integer :: ncid
      character(maxStrLen) :: savename
      character*10 fieldType
      !character*(*) :: filename

      integer :: i, j, num, rec=0, status
      integer :: dimids(NF90_MAX_VAR_DIMS)
      integer :: dims(6) = (/0, 0, 0, 0, 0, 0/)
      integer :: varid
      
      do i=1,main%np
         fieldType = returnTypeN(main,i)
         !write(*,*) fieldType
         if (fieldType .eq. 'R1') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%p1, savename)
         elseif (fieldType .eq. 'R2') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%p2, savename)
         elseif (fieldType .eq. 'R3') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%p3, savename)
         elseif (fieldType .eq. 'R4') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%p4, savename)
         elseif (fieldType .eq. 'R5') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%p5, savename)
         elseif (fieldType .eq. 'R6') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%p6, savename)
         elseif (fieldType .eq. 'J1') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%j1, savename)
         elseif (fieldType .eq. 'J2') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%j2, savename)
         elseif (fieldType .eq. 'J3') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%j3, savename)
         elseif (fieldType .eq. 'J4') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%j4, savename)
         elseif (fieldType .eq. 'J5') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%j5, savename)
         elseif (fieldType .eq. 'J6') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%j6, savename)
         elseif (fieldType .eq. 'L1') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%l1, savename)
         elseif (fieldType .eq. 'L2') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%l2, savename)
         elseif (fieldType .eq. 'L3') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%l3, savename)
         elseif (fieldType .eq. 'L4') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%l4, savename)
         elseif (fieldType .eq. 'L5') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%l5, savename)
         elseif (fieldType .eq. 'L6') then
            call getSaveName(savename,main,i)
            call VarDimDef(ncid, main%p(i)%l6, savename)
         elseif (fieldType .eq. 'gd') then
            call saveGDfield(main%g(i),ncid)
         endif    

      enddo




   end subroutine


   subroutine getSaveName(savename, gdT, ind)
      implicit none
      character(maxStrLen) :: savename 
      character(maxStrLen) :: mdname
      type(gd) :: gdT
      integer :: ind, loc

      mdname = gdT%name(1)
      loc = index(mdname, '.nc.')
      if (loc > 0) then
         mdname = mdname(loc+4:)
      endif
      !write(*,*) mdname
      savename = trim(mdname) // trim('.') // trim(gdT%F(ind))
      !write(*,*) savename
   end subroutine


   subroutine dimExistCheck(ncid, dims, ndim, flagdim, dimid, numdim)
      implicit none
      integer ncid, ndim
      integer :: dims(6), flagdim(6), dimid(6) ! gd deals with 6 dims max now
      integer i, j, status, tempd
      !integer :: dimids(NF90_MAX_VAR_DIMS)
      integer :: numdim

      flagdim = 0
      dimid = 0

      status = nf90_inquire(ncid, nDimensions=numdim)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_inquire"
      end if

      do i=1,ndim
         do j=1,numdim
            status = nf90_inquire_dimension(ncid, j, len=tempd)
            if (status /= nf90_noerr) then 
                print *, trim(nf90_strerror(status))
                stop "Stopped in nf90_inquire_dimension"
            end if
            if (tempd == dims(i)) then
               flagdim(i) = 1 ! exist
               dimid(i) = j
               exit
            endif
         enddo
      enddo

   end subroutine 


   subroutine dimCheck(ncid, dims, ndim, dimid)
      implicit none
      integer ncid, ndim
      integer :: dims(6), dimid(6) ! gd deals with 6 dims max now
      integer i, j, status, tempd
      integer :: numdim

      dimid = 0

      status = nf90_inquire(ncid, nDimensions=numdim)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_inquire"
      end if

      do i=1,ndim
         do j=1,numdim
            status = nf90_inquire_dimension(ncid, j, len=tempd)
            if (status /= nf90_noerr) then 
                print *, trim(nf90_strerror(status))
                stop "Stopped in nf90_inquire_dimension"
            end if
            if (tempd == dims(i)) then
               dimid(i) = j
               exit
            endif
         enddo
      enddo

   end subroutine 


   subroutine startCount(ndims, startInd, countInd, dims, flag_2D)
      implicit none
      include 'mpif.h'
      
      integer ndims
      integer, dimension(ndims) :: startInd, countInd
      integer :: dims(6)

      integer :: i, j, nL, status!, myrank, nproc
      integer :: ind_X, flag_2D, flagY, flagX

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
      !call MPI_Comm_size(MPI_COMM_WORLD,nproc,status)
      startInd = 1
      flag_2D = 0; ind_X = 0
      flagY = 0; flagX = 0
      do j=1,size(pci%YDim)
       do i=1,ndims
         if (dims(i) == pci%Range(j)%Y(2,myrank+1) .and. flagY < 1) then
            flag_2D = flag_2D + 1
            flagY = 2
         endif
         if (dims(i) == (pci%nx(myrank+1,j)+2*pci%nG(j)) .and. flagX < 1) then
            flag_2D = flag_2D + 1
            flagX = 2
            ind_X = i
         endif 
         countInd(i) = dims(i)
       enddo
       if (flag_2d > 1) then
           nL = j
           exit
       else
           flagY = 0; flagX = 0; flag_2D = 0
       endif
      enddo
      flag_2D = AllReduceMinI(flag_2D)
      if (flag_2D > 1) then ! 2D decomposition flag hit
         !if (myrank == 0) then ! #0, don't shift beginning
            !startInd(ind_X) = pci%Xrange(1, myrank+1)
            !countInd(ind_X) = pci%nx(myrank+1) + pci%nG(1) 
            !! XY: the reason I comment all these out -- if do these, in 3D it would enter next column. so does others, let it overlap...   
         !elseif (myrank == (nproc-1)) then! #N-1, don't shift ending
            !startInd(ind_X) = pci%Xrange(1, myrank+1) + 1
        !    countInd(ind_X) = pci%nx(myrank+1) + 2*pci%nG(1)   
         !else ! don't include another ghost to write
            !startInd(ind_X) = pci%Xrange(1, myrank+1) + 1
         !   countInd(ind_X) = pci%nx(myrank+1) + pci%nG(1) 
         !endif  
         startInd(ind_X) = pci%Range(nL)%X(1, myrank+1) 
         countInd(ind_X) = pci%nx(myrank+1,nL) + 2*pci%nG(nL)   
         dims(ind_X) = pci%XDim(nL) 
      endif

   end subroutine

   subroutine addDimDef(ncid, newdim, numdim, dimid)
      implicit none
      integer ncid, newdim, numdim, status, dimid
      character*4 dimname

      dimname = trim('m') // trim(num2str(numdim+1))
      status = nf90_def_dim(ncid, dimname, newdim, dimid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_def_dim"
      end if 

   end subroutine

   subroutine saveNCgd(filename, gdT, sp)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      !character*(*) :: savename
      integer :: ncid
      integer :: i, status, singproc!, myrank
      logical :: exi
      type(gd) :: gdT
      integer, optional :: sp
      !character(maxStrLen) :: savename

      singproc = 0 ! defaultly multi proc
      if (present(sp)) singproc = sp
      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      if (singproc .eq. 0) call MPI_Barrier(MPI_COMM_WORLD, status)

      call saveGDfield(gdT, ncid) 

      if (singproc .eq. 0)call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine




!! ====== interface saveNCVar ======
!! if repeatively save the same name, it could die.
   subroutine saveNCVarR1(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      real*8, pointer :: dat(:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarR2(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      real*8, pointer :: dat(:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarR3(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      real*8, pointer :: dat(:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarR4(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      real*8, pointer :: dat(:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarR5(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      real*8, pointer :: dat(:,:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarR6(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      real*8, pointer :: dat(:,:,:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarI1(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      integer, pointer :: dat(:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarI2(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      integer, pointer :: dat(:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarI3(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      integer, pointer :: dat(:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarI4(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      integer, pointer :: dat(:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarI5(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      integer, pointer :: dat(:,:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarI6(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      integer, pointer :: dat(:,:,:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarL1(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      logical*1, pointer :: dat(:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarL2(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      logical*1, pointer :: dat(:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarL3(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      logical*1, pointer :: dat(:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarL4(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      logical*1, pointer :: dat(:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarL5(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      logical*1, pointer :: dat(:,:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine

   subroutine saveNCVarL6(filename, dat, savename)
      ! save specific data into filename.nc
      implicit none
      include 'mpif.h'
      character*(*) :: filename
      character*(*) :: savename
      integer :: ncid
      integer :: i, status!, myrank
      logical :: exi
      logical*1, pointer :: dat(:,:,:,:,:,:)
      !character(maxStrLen) :: savename

      inquire(file=filename,exist=exi)
      !call MPI_Comm_rank(MPI_COMM_WORLD, myrank, status)
      if (exi) then   
         status = nf90_open(filename,IOR(NF90_WRITE,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_open"
         end if
      else
         status = nf90_create(filename,IOR(NF90_NETCDF4,NF90_MPIIO),ncid,comm=MPI_COMM_WORLD,info=MPI_INFO_NULL)
         if (status /= nf90_noerr) then
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_create"
         end if
         status = nf90_enddef(ncid)
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped in nf90_enddef"
         end if               
      endif

      call MPI_Barrier(MPI_COMM_WORLD, status)

      call VarDimDef(ncid, dat, savename)

      call MPI_Barrier(MPI_COMM_WORLD, status)
 
      status = nf90_close(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          stop "Stopped in nf90_close"
      end if
   end subroutine




!! ====== end of interface saveNCVar ======



!! ====== interface VarDimDef ======

   subroutine VarDimDefR1(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      real*8, pointer :: dat(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=1, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim
      !integer :: myrank, nproc
      integer :: flag_2D = 0
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat)
  
      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
      ! temporarily not considering 2D decomposition that reflects on 1D.     
 
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      if (flagd(1)<1) then
         call addDimDef(ncid, dims(1), numdim, dimid(1))
         numdim = numdim + 1
         !write(*,*)dimid(1),numdim
      endif   

      status = nf90_def_var(ncid, trim(savename), NF90_DOUBLE, dimid(1), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
      ! parallel storing part
      ! wait to add

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif  

   end subroutine

   subroutine VarDimDefR2(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      real*8, pointer :: dat(:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=2, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(2), countInd(2)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
      
     !if (myrank == 0) then
      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,2
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim, dims(i),myrank
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_DOUBLE, (/dimid(1),dimid(2)/), varid) 
     !write(*,*) varid,trim(savename),myrank
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
     !call MPI_Barrier(MPI_COMM_WORLD, status) 
     !if (myrank /= 0) then
     ! status = nf90_inquire(ncid, nVariables=varid)
     !write(*,*) varid,trim(savename),myrank
     ! if (status /= nf90_noerr) then 
     !     print *, trim(nf90_strerror(status))
     !     stop "Stopped in nf90_inquire"
     ! end if
     !endif

     !call MPI_Bcast(varid, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, status)
     ! write(*,*) varid,trim(savename),myrank
     ! write(*,*) myrank, startInd, countInd
      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif
  
   end subroutine

   subroutine VarDimDefR3(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      real*8, pointer :: dat(:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=3, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(3), countInd(3)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,3
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_DOUBLE, (/dimid(1),dimid(2),dimid(3)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
     !call MPI_Barrier(MPI_COMM_WORLD, status) 
     !if (myrank /= 0) then
     ! status = nf90_inquire(ncid, nVariables=varid)
     ! if (status /= nf90_noerr) then 
     !     print *, trim(nf90_strerror(status))
     !     stop "Stopped in nf90_inquire"
     ! end if
     !endif

      !write(*,*) myrank, startInd, countInd
      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine

   subroutine VarDimDefR4(ncid, dat, savename)
      implicit none
      include 'mpif.h'      
      integer ncid, varid
      real*8, pointer :: dat(:,:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=4, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(4), countInd(4)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,4
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_DOUBLE, (/dimid(1),dimid(2),dimid(3),dimid(4)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
     !if (myrank /= 0) then
     ! call MPI_Barrier(MPI_COMM_WORLD, status) 
     ! status = nf90_inquire(ncid, nVariables=varid)
     ! if (status /= nf90_noerr) then 
     !     print *, trim(nf90_strerror(status))
     !     stop "Stopped in nf90_inquire"
     ! end if
     !endif


      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine

   subroutine VarDimDefR5(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      real*8, pointer :: dat(:,:,:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=5, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(5), countInd(5)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
      dims(5) = size(dat,5)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,5
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_DOUBLE, (/dimid(1),dimid(2),dimid(3),dimid(4),dimid(5)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
     !if (myrank /= 0) then
     ! call MPI_Barrier(MPI_COMM_WORLD, status) 
     ! status = nf90_inquire(ncid, nVariables=varid)
     ! if (status /= nf90_noerr) then 
     !     print *, trim(nf90_strerror(status))
     !     stop "Stopped in nf90_inquire"
     ! end if
     !endif


      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine

   subroutine VarDimDefR6(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      real*8, pointer :: dat(:,:,:,:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=6, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(6), countInd(6)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
      dims(5) = size(dat,5)
      dims(6) = size(dat,6)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,6
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_DOUBLE, (/dimid(1),dimid(2),dimid(3),dimid(4),dimid(5),dimid(6)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
     !if (myrank /= 0) then
     ! call MPI_Barrier(MPI_COMM_WORLD, status) 
     ! status = nf90_inquire(ncid, nVariables=varid)
     ! if (status /= nf90_noerr) then 
     !     print *, trim(nf90_strerror(status))
     !     stop "Stopped in nf90_inquire"
     ! end if
     !endif


      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine



   subroutine VarDimDefI1(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      integer, pointer :: dat(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=1, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim
      !integer :: myrank, nproc
      integer :: flag_2D = 0
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat)
  
      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
      ! temporarily not considering 2D decomposition that reflects on 1D.     

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      if (flagd(1)<1) then
         call addDimDef(ncid, dims(1), numdim, dimid(1))
         numdim = numdim + 1
         !write(*,*)dimid(1),numdim
      endif   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, dimid(1), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
      ! parallel storing part
      ! wait to add

     if ((flag_2D > 1) .or. myrank == 0) then
      status = nf90_put_var(ncid, varid, dat) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_put_var"
      end if
     endif
  
   end subroutine

   subroutine VarDimDefI2(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      integer, pointer :: dat(:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=2, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(2), countInd(2)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,2
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
    ! call MPI_Barrier(MPI_COMM_WORLD, status) 
    ! if (myrank /= 0) then
    !  status = nf90_inquire(ncid, nVariables=varid)
    !  if (status /= nf90_noerr) then 
    !      print *, trim(nf90_strerror(status))
    !      stop "Stopped in nf90_inquire"
    !  end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif
  
   end subroutine

   subroutine VarDimDefI3(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      integer, pointer :: dat(:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=3, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(3), countInd(3)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,3
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine

   subroutine VarDimDefI4(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      integer, pointer :: dat(:,:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=4, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(4), countInd(4)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,4
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3),dimid(4)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine

   subroutine VarDimDefI5(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      integer, pointer :: dat(:,:,:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=5, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(5), countInd(5)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
      dims(5) = size(dat,5)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,5
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3),dimid(4),dimid(5)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine

   subroutine VarDimDefI6(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      integer, pointer :: dat(:,:,:,:,:,:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=6, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim

      integer :: startInd(6), countInd(6)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
      dims(5) = size(dat,5)
      dims(6) = size(dat,6)
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,6
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3),dimid(4),dimid(5),dimid(6)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, dat, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 

   end subroutine

   subroutine VarDimDefL1(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      logical*1, pointer :: dat(:)
      integer, pointer :: datI(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=1, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim, nn
      !integer :: myrank, nproc
      integer :: flag_2D = 0
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat)
      allocate(datI(dims(1)))
      do i=1,dims(1)
         if (dat(i)) then
            datI(i) = 1
         else
            datI(i) = 0
         endif
      enddo
  
      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
      ! temporarily not considering 2D decomposition that reflects on 1D.     

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      if (flagd(1)<1) then
         call addDimDef(ncid, dims(1), numdim, dimid(1))
         numdim = numdim + 1
         !write(*,*)dimid(1),numdim
      endif   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, dimid(1), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
      ! parallel storing part
      ! wait to add

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, datI) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 
      deallocate(datI)
 
   end subroutine

   subroutine VarDimDefL2(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      logical*1, pointer :: dat(:,:), datptr1(:)
      integer, pointer :: datI(:,:), datIptr1(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=2, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim, nn

      integer :: startInd(2), countInd(2)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      allocate(datI(dims(1),dims(2)))
      nn = size(dat)
      datptr1 => oneD_ptrLO(nn,dat,1)
      datIptr1 => oneD_ptrI(nn,datI)
      do i=1,nn
         if (datptr1(i)) then
            datIptr1(i) = 1
         else
            datIptr1(i) = 0
         endif
      enddo
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,2
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif
     !call MPI_Barrier(MPI_COMM_WORLD, status) 
     !if (myrank /= 0) then
     ! status = nf90_inquire(ncid, nVariables=varid)
     ! if (status /= nf90_noerr) then 
     !     print *, trim(nf90_strerror(status))
     !     stop "Stopped in nf90_inquire"
      !end if
     !endif


      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, datI, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 
      nullify(datIptr1) 
      deallocate(datI) 

   end subroutine

   subroutine VarDimDefL3(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      logical*1, pointer :: dat(:,:,:), datptr1(:)
      integer, pointer :: datI(:,:,:),datIptr1(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=3, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim, nn

      integer :: startInd(3), countInd(3)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      allocate(datI(dims(1),dims(2),dims(3)))
      nn = size(dat)
      datptr1 => oneD_ptrLO(nn,dat,1)
      datIptr1 => oneD_ptrI(nn,datI)
      do i=1,nn
         if (datptr1(i)) then
            datIptr1(i) = 1
         else
            datIptr1(i) = 0
         endif
      enddo
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,3
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, datI, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 
      nullify(datIptr1) 
      deallocate(datI) 

   end subroutine

   subroutine VarDimDefL4(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      logical*1, pointer :: dat(:,:,:,:), datptr1(:)
      integer, pointer :: datI(:,:,:,:), datIptr1(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=4, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim, nn

      integer :: startInd(4), countInd(4)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
      allocate(datI(dims(1),dims(2),dims(3),dims(4)))
      nn = size(dat)
      datptr1 => oneD_ptrLO(nn,dat,1)
      datIptr1 => oneD_ptrI(nn,datI)
      do i=1,nn
         if (datptr1(i)) then
            datIptr1(i) = 1
         else
            datIptr1(i) = 0
         endif
      enddo
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,4
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3),dimid(4)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, datI, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 
      nullify(datIptr1) 
      deallocate(datI) 

   end subroutine

   subroutine VarDimDefL5(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      logical*1, pointer :: dat(:,:,:,:,:), datptr1(:)
      integer, pointer :: datI(:,:,:,:,:), datIptr1(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=5, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim, nn

      integer :: startInd(5), countInd(5)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
      dims(5) = size(dat,5)
      allocate(datI(dims(1),dims(2),dims(3),dims(4),dims(5)))
      nn = size(dat)
      datptr1 => oneD_ptrLO(nn,dat,1)
      datIptr1 => oneD_ptrI(nn,datI)
      do i=1,nn
         if (datptr1(i)) then
            datIptr1(i) = 1
         else
            datIptr1(i) = 0
         endif
      enddo
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)
      
      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,5
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3),dimid(4),dimid(5)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, datI, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 
      nullify(datIptr1) 
      deallocate(datI) 

   end subroutine

   subroutine VarDimDefL6(ncid, dat, savename)
      implicit none
      include 'mpif.h'
      integer ncid, varid
      logical*1, pointer :: dat(:,:,:,:,:,:), datptr1(:)
      integer, pointer :: datI(:,:,:,:,:,:), datIptr1(:)
      character*(*) :: savename
      !character(maxStrLen) :: savename

      integer :: ndim=6, dims(6)
      integer :: flagd(6), dimid(6)
      integer status, i, numdim, nn

      integer :: startInd(6), countInd(6)
      integer :: ind_X, flag_2D, flagY, flagX
      !integer :: myrank, nproc
      
      dims = 0; flagd = 0; dimid = 0;
      dims(1) = size(dat,1)
      dims(2) = size(dat,2)
      dims(3) = size(dat,3)
      dims(4) = size(dat,4)
      dims(5) = size(dat,5)
      dims(6) = size(dat,6)
      allocate(datI(dims(1),dims(2),dims(3),dims(4),dims(5),dims(6)))
      nn = size(dat)
      datptr1 => oneD_ptrLO(nn,dat,1)
      datIptr1 => oneD_ptrI(nn,datI)
      do i=1,nn
         if (datptr1(i)) then
            datIptr1(i) = 1
         else
            datIptr1(i) = 0
         endif
      enddo
  
      !call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

      call startCount(ndim, startInd, countInd, dims, flag_2D)

      !call MPI_Comm_rank(MPI_COMM_WORLD,myrank,status)

      call dimExistCheck(ncid, dims, ndim, flagd, dimid, numdim)

     !if (myrank == 0) then
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_redef"
      end if

      do i=1,6
         if (flagd(i)<1) then
            call addDimDef(ncid, dims(i), numdim, dimid(i))
            numdim = numdim + 1
            !write(*,*)dimid(i),numdim
         endif
      enddo   

      status = nf90_def_var(ncid, trim(savename), NF90_INT, (/dimid(1),dimid(2),dimid(3),dimid(4),dimid(5),dimid(6)/), varid) 
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_def_var"
      end if

      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in storing ' // trim(savename) 
          stop "Stopped in nf90_enddef"
      end if
     !endif

      if ((flag_2D > 1) .or. myrank == 0) then
         status = nf90_put_var(ncid, varid, datI, start = startInd, count = countInd) 
         if (status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            print *, 'in storing ' // trim(savename) 
            stop "Stopped in nf90_put_var"
         end if
      endif 
      nullify(datIptr1) 
      deallocate(datI) 

   end subroutine

   subroutine preDimSet(ncid)
      use proc_mod, only: nproc, pci
      implicit none
      include 'mpif.h'
      integer ncid, status, i, j, numdim, nL, newdim
      integer :: tempd, flagd, dimid
      
      status = nf90_redef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in preDimSet'
          stop "Stopped in nf90_redef"
      end if
      
      do nL=1,size(pci%YDim)   
          ! Only local X
          do i = 1, nproc
              newdim = pci%nx(i,nL) + 2*pci%nG(nL)
              status = nf90_inquire(ncid, nDimensions=numdim)
              if (status /= nf90_noerr) then 
                  print *, trim(nf90_strerror(status))
                  stop "Stopped in nf90_inquire"
              end if
              flagd = 0
              do j=1,numdim
                  status = nf90_inquire_dimension(ncid, j, len=tempd)
                  if (status /= nf90_noerr) then 
                      print *, trim(nf90_strerror(status))
                      stop "Stopped in nf90_inquire_dimension"
                  end if
                  if (tempd == newdim) then
                      flagd = 1
                      exit
                  endif
              enddo
              ! not exist
              if (flagd < 1) then
                  call addDimDef(ncid, newdim, numdim, dimid)
              endif
          enddo
      enddo
   
      call MPI_Barrier(MPI_COMM_WORLD, status)
      status = nf90_enddef(ncid)
      if (status /= nf90_noerr) then 
          print *, trim(nf90_strerror(status))
          print *, 'in preDimSet'
          stop "Stopped in nf90_enddef"
      end if

    
              
      
   end subroutine
   
   
   
   !====================================================================================================
      SUBROUTINE saveVar(varname,m1,m2,ptr,sigIn,seq)
      ! optional argument mpIn allows you to save to a whole different channel of mat files
      ! like saveVar1601_m2
      ! for 1, it will use the default main channel
      use VData
      implicit none
      integer m1,m2,sigIn,ncid,sig,seq
      integer,save :: ncid_save
      CHARACTER*(*) varname
      CHARACTER*(32) fname,vn
      real*8, dimension(:,:),pointer :: ptr
      
      sig = sigIn
      vn = 'saveVar'
      fname = TRIM(vn)//TRIM(num2str(seq))
      fname = TRIM(fname)//'.nc'
      
      call saveNCVarR2(fname, ptr, varname)
      
      END subroutine saveVar
   
end module NCsave
