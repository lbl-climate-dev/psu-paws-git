!

    subroutine ovnGw_multigrid(C_all,dir,option,state)
       ! parallel version: gather/scatter all meshes ovn.h and ovn.s with gCopyData
       ! and GW(1).h, Topo.e, manning coefficient are needed.
       ! and g%OVN%Mann
       ! dir 1: 1 -> others
       ! dir 2: others -> 1
       ! option 0: including ghost,
       ! option 1: no ghost
       use proc_mod
       use vdata
       use memoryChunk, Only:tempStorage
       use gridOp
       use mgTemp_mod
       implicit none
       include 'mpif.h'
       integer :: dir, nL
       integer, target :: C_ALL(4,3,nGMax,nGMax)
       real*8, dimension(pci%YDim(1), pci%XDim(1)) :: ovnS2D,ovnMan2D!,ovnH2D
       integer :: ierr, masterproc = 0
       integer :: state, option,i
       integer,dimension(:,:),pointer:: C
       real*8,dimension(:,:),pointer:: ovn0=>null(),ovnH2D=>null(),topo=>null(),ovns=>null()

       
       call tempStorage('ovnH2D',ovnH2D,(/pci%YDim(1),pci%XDim(1)/))
       call MPI_Barrier(MPI_COMM_WORLD, ierr)
       call gather2Ddat(1,size(gA(1)%OVN%h,1), size(gA(1)%OVN%h,2), gA(1)%OVN%h, pci%YDim(1),pci%XDim(1),ovnH2D,masterproc)      

       if (dir .eq. 1) then ! gather after ovn_driver
           call gather2Ddat(1,size(gA(1)%OVN%h,1), size(gA(1)%OVN%h,2), gA(1)%OVN%S, pci%YDim(1),pci%XDim(1),ovnS2D,masterproc)
           if (state .eq. 0)  call gather2Ddat(1,size(gA(1)%OVN%h,1), size(gA(1)%OVN%h,2), gA(1)%OVN%Mann, pci%YDim(1),pci%XDim(1),ovnMan2D,masterproc)
           DO I=2,size(gA)
              C=>C_ALL(:,:,1,I) ! C will be populated if they are zero going into gCopyData, and it will be done only once
     !         allocate(ovn0(pci%YDim(i),pci%XDim(i))) 
              ovn0 => mg_Base(i)%ovn
              ovns => mg_Base(i)%ovns
              topo => mg_Base(i)%Topo
              call gather2Ddat(i,size(gA(i)%OVN%h,1), size(gA(i)%OVN%h,2), gA(i)%OVN%h, pci%YDim(i),pci%XDim(i),ovn0,masterproc)
              if (myrank .eq. masterproc) then
                  call gCopyData(1,I,ovnH2D,ovn0,C,option)
              endif
              call MPI_Barrier(MPI_COMM_WORLD,ierr)
                  
              ! ovn.s
              if (state .eq. 0) call gather2Ddat(1,size(gA(i)%OVN%h,1), size(gA(i)%OVN%h,2), gA(i)%OVN%E, pci%YDim(i),pci%XDim(i),topo,masterproc)
              ovns = 0D0
              if (myrank .eq. masterproc) then
                  !call gCopyData(1,I,ovnS2D,ovn0,C,10)
                  call gCopyData(1,I,ovnS2D,ovns,C,10,topo,ovn0) ! topo and h as reference 
              endif
              call MPI_Barrier(MPI_COMM_WORLD,ierr)
              call scatter2Ddat(i,pci%YDim(i),pci%XDim(i),ovn0,size(gA(i)%OVN%h,1), size(gA(i)%OVN%h,2), gA(i)%OVN%h,masterproc)      
              call scatter2Ddat(i,pci%YDim(i),pci%XDim(i),ovns,size(gA(i)%OVN%S,1), size(gA(i)%OVN%S,2), gA(i)%OVN%S,masterproc)
              
              !call gCopyData(1,I,g%OVN%u,gA(I)%OVN%u,C,0)
              !call gCopyData(1,I,g%OVN%v,gA(I)%OVN%v,C,0)
              if (state .eq. 0)  then !call gCopyData(1,I,g%OVN%Mann,gA(I)%OVN%Mann,C,0)
                 ovn0 = 0D0
                 if (myrank .eq. masterproc) then
                     call gCopyData(1,I,ovnMan2D,ovn0,C,10)
                 endif
                 call MPI_Barrier(MPI_COMM_WORLD,ierr)
                 call scatter2Ddat(i,pci%YDim(i),pci%XDim(i),ovn0,size(gA(i)%OVN%Mann,1), size(gA(i)%OVN%Mann,2), gA(i)%OVN%Mann,masterproc)         
              endif
      !        deallocate(ovn0)
              nullify(ovn0)
              nullify(ovns)
              nullify(topo)
           ENDDO
       elseif (dir .eq. 2) then
           DO I=size(gA),2,-1
              C=>C_ALL(:,:,I,1) ! C will be populated if they are zero going into gCopyData, and it will be done only once
              ! XY: this one we shouldn't copy ghost cells.
              !if (associated(ovn0)) deallocate(ovn0)
      !        allocate(ovn0(pci%YDim(i),pci%XDim(i))) 
              ovn0 => mg_Base(i)%ovn
              call gather2Ddat(i,size(gA(i)%OVN%h,1), size(gA(i)%OVN%h,2), gA(i)%OVN%h, pci%YDim(i),pci%XDim(i),ovn0,masterproc)
              if (myrank .eq. masterproc) then
                  call gCopyData(I,1,ovn0,ovnH2D,C,option)
                  !call gCopyData(I,1,gA(I)%OVN%h,g%OVN%h,C,1)
              endif      
              call MPI_Barrier(MPI_COMM_WORLD,ierr)  
      !        deallocate(ovn0)
              nullify(ovn0)
              !call gCopyData(I,1,gA(I)%OVN%u,g%OVN%u,C,1)
              !call gCopyData(I,1,gA(I)%OVN%v,g%OVN%v,C,1)
           ENDDO
           call MPI_Barrier(MPI_COMM_WORLD,ierr)
           call scatter2Ddat(1,pci%YDim(1),pci%XDim(1),ovnH2D,size(gA(1)%OVN%h,1), size(gA(1)%OVN%h,2), gA(1)%OVN%h, masterproc) 
       else
           call ch_assert('ovnGw_multigrid :: wrong dir')
       endif

    end subroutine